$.fn.dataTable.ext.errMode = 'none';      

var tabelkontak = null,
    katId = null;

$('#contact-table').on('dblclick','tr',function(e){
  $('#contact-table').DataTable().rows(this).select();              
  restable();
});


 $("#badd_vendor").click(() => { 
  if(!$('#badd_vendor').hasClass('disabled')){  
    $.ajax({ 
      "url"    : base_url+"Modal/form_kontak", 
      "type"   : "POST", 
      "dataType" : "html",
      "beforeSend": function(){
        parent.window.$(".loader-wrap").removeClass("d-none");      
        parent.window.$(".modal").modal("show");                  
        parent.window.$(".modal-title").html("Tambah Kontak");
        parent.window.$("#modaltrigger").val("iframe-page-kontak");        
      },     
      "error": function(){
        parent.window.$(".loader-wrap").addClass("d-none");      
        console.log('error menampilkan modal form kontak...');
        return;
      },
      "success": async function(result) {
        await parent.window.$(".main-modal-body").html(result);
        await parent.window._inputFormat();      
        parent.window.$('.modal-body').css('min-height','calc(100vh - 30vh)');          
        parent.window.$(".loader-wrap").addClass("d-none");      
      } 
    })   
  }  
}) 

$("#bpilihkontak").click(restable);

var _kontakdatatable = function(){
  katId = $('#idkatkontak').val();
  tabelkontak= $('#contact-table').DataTable({
    "destroy":true,
    "processing": true,
    "serverSide": true,
    "lengthChange": false,
    "searching": true,
    "ordering": true,
    "pagingType":"simple",        
    "order": [[ 2, 'asc' ]],      
    "select":true,      
    "dom": '<"#sTable"f><"top"p>tr<"clear">',        
    "ajax": {
        "url":base_url+"Datatable_Master/view_table_kontak/"+katId,
        "type":"post",
        "beforeSend": function(){
          $(".loader-wrap").addClass("d-none");            
        }                   
    },
    "deferRender": true,
    "bInfo":false,
    "aLengthMenu": [[20, 50, 100],[20, 50, 100]],
    "language": 
    {          
      "processing": "<i class='fas fa-circle-notch fa-spin text-primary'></i>",
    },                              
    "columns": [
          { "data": "id" },
          {
          orderable:      false,
          data:           null,
          defaultContent: "<i class='fas fa-caret-right text-sm'></i>"
          },    
          { "data": "kode" },
          { "data": "nama" },
          { "data": "tipe" },
          { "data": "alamat" },          
    ],
    "drawCallback": function(settings, json) {
      var total = tabelkontak.data().count();

      if(total>0){
        $(".modal-body").removeClass("noresultfound");                                   
      }else{
        $(".modal-body").addClass("noresultfound");                                   
      }
      $('#modal input').focus();                                     
      $('#contact-table').removeClass("d-none"); 
    }        
  });    
}

var _lstkategorikontak = function(){
  $.ajax({ 
    "url"    : base_url+"Select_Master/view_kategori_kontak", 
    "type"   : "POST", 
    "dataType" : "json", 
    error: function(){
      console.log('error ambil kategori kontak...');
      return;
    },
    success: function(data) {
      var list = ` <a href="javascript:void(0)" class="dropdown-item text-sm" onClick="_pilihkategorikontak('All');">
                    <i id="cAll" class="cTrue fas fa-check mr-2 d-none"></i>Semua
                  </a> `;
      $('.list-kategori').append(list);

      $.each(data, function(index,element) {
        list = ` <a href="javascript:void(0)" class="dropdown-item text-sm" onClick="_pilihkategorikontak('`+element.id+`');">
                      <i id="c`+element.id+`" class="cTrue fas fa-check mr-2 d-none"></i> `+element.text+
               ` </a> `;
        $('.list-kategori').append(list);
      });
  } 
  });
  $("#nav-kkontak").removeClass("d-none");  
}

function _pilihkategorikontak(id){
  if(id!=='All'){
    $('#idkatkontak').val(id);
  }else{
    $('#idkatkontak').val('');          
  }

  $('.cTrue').addClass('d-none');
  $('#c'+id).removeClass('d-none');  
  _kontakdatatable();
}       

function restable(){
const id = $('#contact-table').DataTable().cell($('#contact-table').DataTable().rows({selected:true}),0).data(),
      kode = $('#contact-table').DataTable().cell($('#contact-table').DataTable().rows({selected:true}),2).data(),
      nama = $('#contact-table').DataTable().cell($('#contact-table').DataTable().rows({selected:true}),3).data(),
      alamat = $('#contact-table').DataTable().cell($('#contact-table').DataTable().rows({selected:true}),5).data(),     
      trigger = $('#modaltrigger').val(),
      coltrigger = $('#coltrigger').val();

                                                
    $("#"+trigger).contents().find("#alamat").val(alamat);    
  
  if(coltrigger=='vendor'){
    $("#"+trigger).contents().find("#idkontak").val(id); 
    $("#"+trigger).contents().find("#kontak").val(kode);                     
    $("#"+trigger).contents().find("#namakontak").html(nama);                                        
    $('#modal').modal('hide');        
    $("#"+trigger).contents().find("#kontak").focus();
  }else if(coltrigger=='kontak'){
    $("#"+trigger).contents().find("#idkontak").val(id); 
    $("#"+trigger).contents().find("#kontak").val(kode);                     
    $("#"+trigger).contents().find("#namakontak").html(nama);                                           
    $('#modal').modal('hide');        
    $("#"+trigger).contents().find("#kontak").focus();
  }else if(coltrigger=='kontak2'){
    $("#"+trigger).contents().find("#idkontak2").val(id); 
    $("#"+trigger).contents().find("#kontak2").val(kode);                     
    $("#"+trigger).contents().find("#namakontak2").html(nama);                                           
    $('#modal').modal('hide');        
    $("#"+trigger).contents().find("#kontak2").focus();          
  }else if(coltrigger=='customer'){
    $("#"+trigger).contents().find("#idkontak").val(id); 
    $("#"+trigger).contents().find("#kontak").val(kode);                     
    $("#"+trigger).contents().find("#namakontak").html(nama);                
    $("#"+trigger).contents().find("#alamat").val(alamat);                                                          
    $('#modal').modal('hide');        
    $("#"+trigger).contents().find("#kontak").focus();          
  }else if(coltrigger=='bagbeli'){
    $("#"+trigger).contents().find("#idbagbeli").val(id); 
    $("#"+trigger).contents().find("#bagbeli").val(nama);                     
    $('#modal').modal('hide');        
    $("#"+trigger).contents().find("#bagbeli").focus();
  }else if(coltrigger=='salesman'){
    $("#"+trigger).contents().find("#idsalesman").val(id); 
    $("#"+trigger).contents().find("#salesman").val(nama);                     
    $('#modal').modal('hide');        
    $("#"+trigger).contents().find("#salesman").focus();
  }else if(coltrigger=='baggudang'){
    $("#"+trigger).contents().find("#idbaggudang").val(id); 
    $("#"+trigger).contents().find("#baggudang").val(nama);                     
    $('#modal').modal('hide');        
    $("#"+trigger).contents().find("#baggudang").focus();          
  }else if(coltrigger=='karyawan'){
    $("#"+trigger).contents().find("#idkaryawan").val(id); 
    $("#"+trigger).contents().find("#karyawan").val(nama);                     
    $('#modal').modal('hide');        
    $("#"+trigger).contents().find("#karyawan").focus();          
  }                          
  return;        
}