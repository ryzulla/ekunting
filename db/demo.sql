-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 12, 2022 at 01:16 AM
-- Server version: 5.7.33
-- PHP Version: 7.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `demo`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_HISTORI_BB` (IN `v_coa1` VARCHAR(25), IN `v_coa2` VARCHAR(25), IN `v_tgl1` DATE, IN `v_tgl2` DATE)  BEGIN
TRUNCATE TABLE tmp_bb;
INSERT INTO tmp_bb 
SELECT A.cuid,F.mlink,F.mcaption1,A.cunotransaksi,A.cusumber, 
	   A.cutanggal,
	   C.knama, A.cuuraian, ROUND(B.cddebit,2), ROUND(B.cdkredit,2), null,
       D.cnocoa,D.cnama,D.cid  
  FROM ctransaksiu A
  INNER JOIN ctransaksid B ON A.cuid=B.cdidu   
  LEFT JOIN bkontak C ON A.cukontak=C.kid
  LEFT JOIN bcoa D ON B.cdnocoa=D.cid 
  LEFT JOIN anomor E ON A.cusumber=E.nkode  
  LEFT JOIN amenu F ON E.nfmenu=F.mid 
  WHERE D.cnocoa BETWEEN v_coa1 AND v_coa2  
  AND A.cutanggal BETWEEN v_tgl1 AND v_tgl2;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_HISTORI_BB_2` (IN `v_tgl1` DATE, IN `v_coa1` VARCHAR(25), IN `v_coa2` VARCHAR(25))  BEGIN
	DECLARE v_sts INT DEFAULT FALSE;
	DECLARE v_idcoa INT(11);
    DECLARE v_coa VARCHAR(255);
    DECLARE cur_tmp CURSOR FOR 
     SELECT cid,coa 
       FROM tmp_bb 
      WHERE idcoa BETWEEN v_coa1 AND v_coa2 
   GROUP BY cid ORDER BY cid ASC;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_sts = TRUE;
DECLARE EXIT HANDLER FOR SQLEXCEPTION, SQLWARNING
	BEGIN
    	ROLLBACK;
    	RESIGNAL;
  	END;  

    OPEN cur_tmp; 

	START TRANSACTION;

	looping: LOOP
    FETCH cur_tmp INTO v_idcoa,v_coa;
    IF v_sts THEN
       LEAVE looping;
    END IF;

	INSERT INTO tmp_bb VALUES(
 0, '', '', 'SALDO AWAL', '',
                               NULL, '', '',
        IFNULL((SELECT SUM(ROUND(B.cddebit,2)) 
                                         FROM ctransaksiu A
                                   INNER JOIN ctransaksid B ON A.cuid=B.cdidu 
                                    LEFT JOIN bcoa C ON B.cdnocoa=C.cid 
                                        WHERE B.cdnocoa=v_idcoa 
                                          AND C.ctipe < 11 
                                          AND A.cutanggal < v_tgl1),0), 
                               IFNULL((SELECT SUM(ROUND(B.cdkredit,2)) 
                                         FROM ctransaksiu A
                                   INNER JOIN ctransaksid B ON A.cuid=B.cdidu 
                                    LEFT JOIN bcoa C ON B.cdnocoa=C.cid
                                        WHERE B.cdnocoa=v_idcoa 
                                          AND C.ctipe < 11 
                                          AND A.cutanggal < v_tgl1),0), 
                               IFNULL((SELECT SUM(ROUND(B.cddebit,2)) - SUM(ROUND(B.cdkredit,2)) 
                                         FROM ctransaksiu A
                                   INNER JOIN ctransaksid B ON A.cuid=B.cdidu 
                                    LEFT JOIN bcoa C ON B.cdnocoa=C.cid
                                        WHERE B.cdnocoa=v_idcoa 
                                          AND C.ctipe < 11 
                                          AND A.cutanggal < v_tgl1),0),  
                               '',v_coa,v_idcoa);

	INSERT INTO tmp_bb VALUES(
 2147483647, '', '', 'SALDO AKHIR', '',
                               NULL, '', '',
        0, 
0, 
                               0,  
                               '',v_coa,v_idcoa);

    END LOOP;
    
    CLOSE cur_tmp;
COMMIT;        

	SELECT COUNT(*) INTO @jml 
      FROM tmp_bb 
     WHERE idcoa BETWEEN v_coa1 AND v_coa2;

IF @jml=0 THEN 
kosong: BEGIN
	DECLARE v_sts INT DEFAULT FALSE;
	DECLARE v_idcoa INT(11);
    DECLARE v_coa VARCHAR(255);
    DECLARE cur_tmp CURSOR FOR 
     SELECT cid,cnama 
       FROM bcoa  
      WHERE cnocoa BETWEEN v_coa1 AND v_coa2 
   ORDER BY cid ASC;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_sts = TRUE;
DECLARE EXIT HANDLER FOR SQLEXCEPTION, SQLWARNING
	BEGIN
    	ROLLBACK;
    	RESIGNAL;
  	END;  

    OPEN cur_tmp; 

	START TRANSACTION;

	looping: LOOP
    FETCH cur_tmp INTO v_idcoa,v_coa;
    IF v_sts THEN
       LEAVE looping;
    END IF;

	INSERT INTO tmp_bb VALUES(
 0, '', '', 'SALDO AWAL', '',
                               NULL, '', '',
        IFNULL((SELECT SUM(ROUND(B.cddebit,2)) 
                                         FROM ctransaksiu A
                                   INNER JOIN ctransaksid B ON A.cuid=B.cdidu 
                                    LEFT JOIN bcoa C ON B.cdnocoa=C.cid 
                                        WHERE B.cdnocoa=v_idcoa 
                                          AND C.ctipe < 11 
                                          AND A.cutanggal < v_tgl1),0), 
                               IFNULL((SELECT SUM(ROUND(B.cdkredit,2)) 
                                         FROM ctransaksiu A
                                   INNER JOIN ctransaksid B ON A.cuid=B.cdidu 
                                    LEFT JOIN bcoa C ON B.cdnocoa=C.cid
                                        WHERE B.cdnocoa=v_idcoa 
                                          AND C.ctipe < 11 
                                          AND A.cutanggal < v_tgl1),0), 
                               IFNULL((SELECT SUM(ROUND(B.cddebit,2)) - SUM(ROUND(B.cdkredit,2)) 
                                         FROM ctransaksiu A
                                   INNER JOIN ctransaksid B ON A.cuid=B.cdidu 
                                    LEFT JOIN bcoa C ON B.cdnocoa=C.cid
                                        WHERE B.cdnocoa=v_idcoa 
                                          AND C.ctipe < 11 
                                          AND A.cutanggal < v_tgl1),0),  
                               '',v_coa,v_idcoa);    
    END LOOP;
    
    CLOSE cur_tmp;
COMMIT;        
END kosong;
END IF;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_HITUNG_HPP_ADD` (IN `v_id` INT(11))  BEGIN
DECLARE v_sts INT DEFAULT FALSE;
DECLARE v_nomor INT(11);
DECLARE v_item INT(11);
DECLARE v_qty DOUBLE;

DECLARE cur_detil_jual CURSOR FOR 
SELECT A.sdid,A.sditem,A.sdkeluar   
       FROM fstokd A 
 INNER JOIN bitem B ON A.sditem=B.iid  
      WHERE A.sdidsu=v_id AND A.sdkeluar>0  
   ORDER BY A.sdurutan ASC;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_sts = TRUE;
DECLARE EXIT HANDLER FOR SQLEXCEPTION, SQLWARNING
	BEGIN
    	ROLLBACK;
    	RESIGNAL;
  	END;  

SET @v_row=1;
START TRANSACTION;

OPEN cur_detil_jual;
  	detil_loop: LOOP
    FETCH cur_detil_jual INTO v_nomor, v_item, v_qty;
    IF v_sts THEN
		CLOSE cur_detil_jual;    
    	LEAVE detil_loop;
    END IF;
    
        HPP:BEGIN
		DECLARE v_sts_hpp INT DEFAULT FALSE;
        DECLARE v_sdid INT(11);
        DECLARE v_sditem INT(11);
        DECLARE v_qtyd DOUBLE;
        DECLARE v_qtyhpp DOUBLE;
        DECLARE v_hargahpp DOUBLE;
        DECLARE v_qtypakai DOUBLE DEFAULT 0;
        DECLARE v_totalhpp DOUBLE DEFAULT 0;
        
        DECLARE c_hpp CURSOR FOR
        SELECT B.SDID,B.SDITEM,B.SDMASUK 'QTY',  
               (B.SDMASUK-B.SDPAKAIHPP) 'QTYHPP', (B.SDHARGA-B.SDDISKON) 'HARGAHPP' 
          FROM fstoku A 
    INNER JOIN fstokd B ON A.SUID=B.SDIDSU  
         WHERE B.SDITEM = v_item AND (B.SDMASUK-B.SDPAKAIHPP)>0 
      ORDER BY A.SUTANGGAL ASC, B.SDID ASC;
		
        DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_sts_hpp = TRUE;     
        	
            SET @v_qtysisa = v_qty;
            OPEN c_hpp;
            hpp_loop:LOOP
            FETCH c_hpp INTO v_sdid, v_sditem, v_qtyd, v_qtyhpp, v_hargahpp;  
            IF v_sts_hpp THEN
                CLOSE c_hpp;
                LEAVE hpp_loop;
            END IF;
            
			SET v_qty = v_qty-v_qtypakai;
            
            IF v_qtyhpp >= v_qty THEN
                INSERT INTO fstokh SET shidsd=v_nomor, shidhpp=v_sdid, shqty=v_qty, shharga=v_hargahpp;
                UPDATE fstokd SET sdpakaihpp = sdpakaihpp + v_qty WHERE sdid = v_sdid;
				SET v_totalhpp = v_totalhpp+(v_qty*v_hargahpp);	
                SET @v_qtysisa = @v_qtysisa - v_qty;                
				LEAVE hpp_loop;
			ELSE
                INSERT INTO fstokh SET shidsd=v_nomor, shidhpp=v_sdid, shqty=v_qtyhpp, shharga=v_hargahpp;
                UPDATE fstokd SET sdpakaihpp = sdpakaihpp + v_qtyhpp WHERE sdid = v_sdid;
				SET v_totalhpp = v_totalhpp+(v_qtyhpp*v_hargahpp);	
				SET v_qtypakai = v_qtypakai+v_qtyhpp;
                SET @v_qtysisa = @v_qtysisa - v_qtyhpp;
			END IF;
            
            END LOOP;
			
            IF v_hargahpp IS NULL THEN
            	SET v_hargahpp = 0;
            END IF;

            IF @v_qtysisa > 0 THEN
                INSERT INTO fstokh SET shidsd=v_nomor, shidhpp=v_sdid, shqty=@v_qtysisa, shharga=v_hargahpp;
                UPDATE fstokd SET sdpakaihpp = sdpakaihpp + @v_qtysisa WHERE sdid = v_sdid;
			END IF;
        END HPP;
    END LOOP;
COMMIT;    
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_HITUNG_HPP_DEL` (IN `v_id` INT(11))  BEGIN
DECLARE v_sts INT DEFAULT FALSE;
DECLARE v_nomor INT(11);

DECLARE cur_detil_jual CURSOR FOR 
 SELECT A.sdid FROM fstokd A 
  WHERE A.sdidsu=v_id  
  ORDER BY A.sdurutan ASC;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_sts = TRUE;
DECLARE EXIT HANDLER FOR SQLEXCEPTION, SQLWARNING
	BEGIN
    	ROLLBACK;
    	RESIGNAL;
  	END;  

START TRANSACTION;

OPEN cur_detil_jual;
  	detil_loop: LOOP
    FETCH cur_detil_jual INTO v_nomor;
    IF v_sts THEN
		CLOSE cur_detil_jual;    
    	LEAVE detil_loop;
    END IF;
    
    DELETE FROM fstokh WHERE shidsd=v_nomor;
    
    END LOOP;
COMMIT;    
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_JURNAL_HPP_OUT_ADD` (IN `v_id` INT(11))  BEGIN
	DECLARE v_sts INT DEFAULT FALSE;
  	DECLARE v_item INT(11);
    DECLARE v_coa_persediaan INT(11);
    DECLARE v_coa_hpp INT(11);    
    DECLARE v_hpp DOUBLE;
    DECLARE v_divisi INT(11);
    DECLARE v_proyek INT(11);
    
	   DECLARE c_detil CURSOR FOR
        SELECT A.sditem,B.icoapersediaan,B.icoahpp,A.sddivisi,A.sdproyek, SUM(C.shqty*C.shharga) 'HPP'   
          FROM fstokd A 
    INNER JOIN bitem B ON A.sditem=B.iid 
    INNER JOIN fstokh C ON A.sdid=C.SHIDSD
         WHERE A.sdidsu = v_id GROUP BY A.sditem 
      ORDER BY A.sdurutan ASC;      

  	DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_sts = TRUE;
  	DECLARE EXIT HANDLER FOR SQLEXCEPTION, SQLWARNING
  	BEGIN
    	ROLLBACK;
    	RESIGNAL;
  	END;  

    SELECT cccoa INTO @v_coaselisih FROM cconfigcoa WHERE cckode='SJ' AND ccketerangan='PERSEDIAANTERKIRIM';

	IF @v_coaselisih > 0 THEN 
  	OPEN c_detil;  
  	START TRANSACTION;

	SELECT susumber,sunotransaksi,sutanggal,
    	   sukontak,suuraian,sutotaltransaksi,
           sucreateu      
      INTO @v_sumber,@v_nomor,@v_tanggal,@v_kontak,@v_uraian,@v_total,@v_createu    
      FROM fstoku
     WHERE suid = v_id;         
    
    INSERT INTO ctransaksiu SET 
    	cusumber = @v_sumber,
        cunotransaksi = @v_nomor,
        cutanggal = @v_tanggal,
        cukontak = @v_kontak,
        cuuraian = @v_uraian,
        cutotaltrans = @v_total,
        cucreateu = @v_createu;
    
    SET @cuid = LAST_INSERT_ID();
    SET @v_row = 1;
    SET @v_coahpp_temp = '';
    SET @v_coapersediaan_temp = '';
    
  	hpp_loop: LOOP
    FETCH c_detil INTO v_item, v_coa_persediaan, v_coa_hpp, v_divisi, v_proyek, v_hpp;
    IF v_sts THEN
	  CLOSE c_detil;    
      LEAVE hpp_loop;
    END IF;
    
    -- JURNAL PERSEDIAAN TERKIRIM (DEBIT)
    IF v_coa_hpp <> @v_coahpp_temp THEN    
        INSERT INTO ctransaksid SET 
                    cdidu=@cuid,
                    cdurutan=@v_row,
                    cdnocoa=@v_coaselisih,
                    cddebit=v_hpp,
                    cduang=1,
                    cddvisi=v_divisi,
                    cdproyek=v_proyek;
	ELSE 
    	SET @cdid = LAST_INSERT_ID();
        UPDATE ctransaksid SET cddebit = cddebit + v_hpp, cddvisi=v_divisi, cdproyek=v_proyek 
         WHERE cdid = @cdid;
	END IF;

	SET @v_row = @v_row+1;
	SET @v_coahpp_temp = v_coa_hpp;
    END LOOP;    
	
	state2: BEGIN
	DECLARE v_sts INT DEFAULT FALSE;
  	DECLARE v_item INT(11);
    DECLARE v_coa_persediaan INT(11);
    DECLARE v_coa_hpp INT(11);    
    DECLARE v_hpp DOUBLE;
    DECLARE v_divisi INT(11);
    DECLARE v_proyek INT(11);
    
	   DECLARE c_detil CURSOR FOR
        SELECT A.sditem,B.icoapersediaan,B.icoahpp,A.sddivisi,A.sdproyek, SUM(C.shqty*C.shharga) 'HPP'   
          FROM fstokd A 
    INNER JOIN bitem B ON A.sditem=B.iid 
    INNER JOIN fstokh C ON A.sdid=C.SHIDSD
         WHERE A.sdidsu = v_id GROUP BY A.sditem 
      ORDER BY A.sdurutan ASC;      

  	DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_sts = TRUE;
  	DECLARE EXIT HANDLER FOR SQLEXCEPTION, SQLWARNING
  	BEGIN
    	ROLLBACK;
    	RESIGNAL;
  	END;  

  	OPEN c_detil;  
    SET @v_coapersediaan_temp = '';
    
  	stock_loop: LOOP
    FETCH c_detil INTO v_item, v_coa_persediaan, v_coa_hpp, v_divisi, v_proyek, v_hpp;
    IF v_sts THEN
	  CLOSE c_detil;    
      LEAVE stock_loop;
    END IF;
    
    -- JURNAL PERSEDIAAN (KREDIT)
    IF v_coa_persediaan <> @v_coapersediaan_temp THEN    
        INSERT INTO ctransaksid SET 
                    cdidu=@cuid,
                    cdurutan=@v_row,
                    cdnocoa=v_coa_persediaan,
                    cdkredit=v_hpp,
                    cduang=1,
                    cddvisi=v_divisi,
                    cdproyek=v_proyek;
	ELSE 
    	SET @cdid = LAST_INSERT_ID();
        UPDATE ctransaksid SET cdkredit = cdkredit + v_hpp, cddvisi=v_divisi, cdproyek=v_proyek 
         WHERE cdid = @cdid;
	END IF;

	SET @v_row = @v_row+1;
	SET @v_coapersediaan_temp = v_coa_persediaan;
    END LOOP;

    END state2;

  	COMMIT; 
	END IF;    
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_JURNAL_HPP_OUT_DEL` (IN `v_id` INT(11))  BEGIN
	DECLARE v_sts INT DEFAULT FALSE;
	DECLARE v_sumber VARCHAR(50);
    DECLARE v_nomor VARCHAR(50);
    
  	DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_sts = TRUE;
  	DECLARE EXIT HANDLER FOR SQLEXCEPTION, SQLWARNING
  	BEGIN
    	ROLLBACK;
    	RESIGNAL;
  	END;  

	SELECT susumber, sunotransaksi INTO v_sumber,v_nomor
      FROM fstoku WHERE suid=v_id;

	SELECT cuid INTO @cuid FROM ctransaksiu WHERE cusumber=v_sumber AND cunotransaksi=v_nomor;

  	START TRANSACTION;
		DELETE FROM ctransaksid WHERE cdidu=@cuid;
        DELETE FROM ctransaksiu WHERE cuid=@cuid;
  	COMMIT;     
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_JURNAL_INVOICE_PEMBELIAN_ADD` (`v_id` INT(11))  BEGIN
	DECLARE v_sts INT DEFAULT FALSE;
  	DECLARE v_item INT(11);
    DECLARE v_coa_persediaan INT(11);
    DECLARE v_harga DOUBLE;
    DECLARE v_diskon DOUBLE;
    DECLARE v_qty DOUBLE;
    DECLARE v_nocoa INT(11);
    DECLARE v_divisi INT(11);
    DECLARE v_proyek INT(11);
    DECLARE v_total_beli DOUBLE DEFAULT 0;
    
	DECLARE c_detil CURSOR FOR
     SELECT A.ipditem,A.ipdharga,A.ipddiskon,A.ipdmasuk,B.icoapersediaan,A.ipddivisi,A.ipdproyek   
       FROM einvoicepenjualand A 
 INNER JOIN bitem B ON A.ipditem=B.iid  
      WHERE A.ipdidipu = v_id ORDER BY B.icoapersediaan ASC;

  	DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_sts = TRUE;
  	DECLARE EXIT HANDLER FOR SQLEXCEPTION, SQLWARNING
  	BEGIN
    	ROLLBACK;
    	RESIGNAL;
  	END;  

    SELECT cccoa INTO @v_coaselisih FROM cconfigcoa WHERE cckode='TB' AND ccketerangan='PERSEDIAANBELUMFAKTUR';

  	OPEN c_detil;  
  	START TRANSACTION;
	
    SELECT cccoa INTO @v_coapajak FROM cconfigcoa WHERE cckode='PJ' AND ccketerangan='PAJAKMASUKAN';
    SELECT B.pcoain INTO @v_coappn FROM ainfo A INNER JOIN bpajak B ON A.ippnbeli=B.pid WHERE A.iid=1;
    SELECT B.pcoain INTO @v_coapph22 FROM ainfo A INNER JOIN bpajak B ON A.ipph22beli=B.pid WHERE A.iid=1;    
    SELECT cccoa INTO @v_coahutang FROM cconfigcoa WHERE cckode='PJ' AND ccketerangan='HUTANGDAGANG';
    
	SELECT IPU.ipusumber,IPU.ipunotransaksi,IPU.iputanggal,
    	   IPU.ipukontak,IPU.ipuuraian,IPU.iputotaltransaksi,
           IPU.ipucreateu,IPU.iputotalpajak,IPU.ipujumlahdp,
           IPU.ipuiddp,IPU.ipujenispajak,IPU.iputotalpph22       
      INTO @v_sumber,@v_nomor,@v_tanggal,@v_kontak,@v_uraian,@v_total,@v_createu,@v_pajak,@v_dp,@v_iddp,@v_jnspajak,@v_pph22    
      FROM einvoicepenjualanu IPU
     WHERE IPU.ipuid = v_id;         
    
    INSERT INTO ctransaksiu SET 
    	cusumber = @v_sumber,
        cunotransaksi = @v_nomor,
        cutanggal = @v_tanggal,
        cukontak = @v_kontak,
        cuuraian = @v_uraian,
        cutotaltrans = @v_total,
        cucreateu = @v_createu;
    
    SET @cuid = LAST_INSERT_ID();
    SET @v_row = 1;
    SET @v_nocoa_temp = '';
    
    IF @v_coaselisih > 0 THEN 
        INSERT INTO ctransaksid SET 
                    cdidu=@cuid,
                    cdurutan=@v_row,
                    cdnocoa=@v_coaselisih,
                    cddebit=@v_total-@v_pajak,
                    cduang=1,
                    cddvisi=v_divisi,
                    cdproyek=v_proyek;    
    ELSE 
        detil_loop: LOOP
        FETCH c_detil INTO v_item, v_harga, v_diskon, v_qty, v_nocoa, v_divisi, v_proyek;
        IF v_sts THEN
          LEAVE detil_loop;
        END IF;

        -- JURNAL PERSEDIAAN (DEBIT)
        IF v_nocoa <> @v_nocoa_temp THEN
            IF @v_jnspajak=2 THEN 
                SET v_total_beli = ((v_harga-v_diskon)*v_qty) - (((100/110)*((v_harga-v_diskon)*v_qty))*(10/100));
            ELSE
                SET v_total_beli = (v_harga-v_diskon)*v_qty;    
            END IF;

            INSERT INTO ctransaksid SET 
                        cdidu=@cuid,
                        cdurutan=@v_row,
                        cdnocoa=v_nocoa,
                        cddebit=v_total_beli,
                        cduang=1,
                        cddvisi=v_divisi,
                        cdproyek=v_proyek;
        ELSE 
            SET @cdid = LAST_INSERT_ID();
            IF @v_jnspajak=2 THEN 
                SET v_total_beli = ((v_harga-v_diskon)*v_qty) - (((100/110)*((v_harga-v_diskon)*v_qty))*(10/100));
            ELSE
                SET v_total_beli = (v_harga-v_diskon)*v_qty;    
            END IF;

            UPDATE ctransaksid SET cddebit = cddebit + v_total_beli, cddvisi=v_divisi, cdproyek=v_proyek 
             WHERE cdid = @cdid;
        END IF;

        SET @v_row = @v_row+1;
        SET @v_nocoa_temp = v_nocoa;
        END LOOP;        
    END IF;

  	CLOSE c_detil;
    
	-- JURNAL PAJAK MASUKAN (DEBIT)
	IF @v_pajak <> 0 THEN
    	SET @v_row = @v_row+1;
        INSERT INTO ctransaksid SET 
                    cdidu=@cuid,
                    cdurutan=@v_row,
                    cdnocoa=@v_coappn,
                    cddebit=@v_pajak,
                    cduang=1,
                    cddvisi=v_divisi,
                    cdproyek=v_proyek;    
    END IF;

	-- JURNAL PAJAK PPH22 (DEBIT)
	IF @v_pph22 <> 0 THEN
    	SET @v_row = @v_row+1;
        INSERT INTO ctransaksid SET 
                    cdidu=@cuid,
                    cdurutan=@v_row,
                    cdnocoa=@v_coapph22,
                    cddebit=@v_pph22,
                    cduang=1,
                    cddvisi=v_divisi,
                    cdproyek=v_proyek;    
    END IF;

	-- JURNAL HUTANG DAGANG (KREDIT) 
        SET @v_row = @v_row+1;
        INSERT INTO ctransaksid SET 
                    cdidu=@cuid,
                    cdurutan=@v_row,
                    cdnocoa=@v_coahutang,
                    cdkredit=@v_total,
                    cduang=1,
                    cddvisi=v_divisi,
                    cdproyek=v_proyek;

	-- JURNAL UANG MUKA PEMBELIAN
    IF @v_dp<>0 THEN
    	SET @cdid = LAST_INSERT_ID();
        IF @v_total-@v_dp <> 0 THEN
	        UPDATE ctransaksid SET cdkredit = cdkredit - @v_dp WHERE cdid=@cdid;
		ELSE 
        	DELETE FROM ctransaksid WHERE cdid=@cdid;
        END IF;
        
		statedp: BEGIN 
		DECLARE v_sts INT DEFAULT FALSE;        
        DECLARE v_coadp INT(11);
        DECLARE v_amtdp DOUBLE DEFAULT 0;
        
        DECLARE c_uangmuka CURSOR FOR
		SELECT DP.dpcdp,IF(DP.dppajak=1,TA.idpjumlahdp-TA.idppajakdp,TA.idpjumlahdp) 'jumlahdp'
          FROM einvoicepenjualandp TA LEFT JOIN ddp DP ON TA.IDPIDDP=DP.DPID 
         WHERE TA.IDPIDU=v_id ORDER BY DP.dpcdp ASC;

        DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_sts = TRUE;
        DECLARE EXIT HANDLER FOR SQLEXCEPTION, SQLWARNING
        BEGIN
            ROLLBACK;
            RESIGNAL;
        END;  

        OPEN c_uangmuka;
    	SET @v_row = @v_row+1;
        SET @v_coadp_temp = '';
        
        dp_loop: LOOP
        FETCH c_uangmuka INTO v_coadp,v_amtdp;
        IF v_sts THEN
          LEAVE dp_loop;
        END IF;        

		IF v_coadp <> @v_coadp_temp THEN
            INSERT INTO ctransaksid SET 
                        cdidu=@cuid,
                        cdurutan=@v_row,
                        cdnocoa=v_coadp,
                        cdkredit=v_amtdp,
                        cduang=1,
                        cddvisi=v_divisi,
                        cdproyek=v_proyek;    	
        ELSE 
            SET @cdid = LAST_INSERT_ID();
            UPDATE ctransaksid SET cdkredit = cdkredit + v_amtdp  
             WHERE cdid = @cdid;            
        END IF;
        
		SET @v_row = @v_row+1;
        SET @v_coadp_temp = v_coadp;
        END LOOP;
                    
	  	CLOSE c_uangmuka;                    
        END statedp;
     END IF;

  	COMMIT; 
    
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_JURNAL_INVOICE_PEMBELIAN_DEL` (`v_id` INT(11))  BEGIN
	DECLARE v_sts INT DEFAULT FALSE;
	DECLARE v_sumber VARCHAR(50);
    DECLARE v_nomor VARCHAR(50);
    
  	DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_sts = TRUE;
  	DECLARE EXIT HANDLER FOR SQLEXCEPTION, SQLWARNING
  	BEGIN
    	ROLLBACK;
    	RESIGNAL;
  	END;  

	SELECT ipusumber, ipunotransaksi INTO v_sumber,v_nomor
      FROM einvoicepenjualanu WHERE ipuid=v_id;

	SELECT cuid INTO @cuid FROM ctransaksiu WHERE cusumber=v_sumber AND cunotransaksi=v_nomor;

  	START TRANSACTION;
		DELETE FROM ctransaksid WHERE cdidu=@cuid;
        DELETE FROM ctransaksiu WHERE cuid=@cuid;
  	COMMIT;     
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_JURNAL_INVOICE_PENJUALAN_ADD` (`v_id` INT(11))  BEGIN
	DECLARE v_sts INT DEFAULT FALSE;
  	DECLARE v_item INT(11);
    DECLARE v_coa_persediaan INT(11);
    DECLARE v_harga DOUBLE;
    DECLARE v_diskon DOUBLE;
    DECLARE v_qty DOUBLE;
    DECLARE v_nocoa INT(11);
    DECLARE v_divisi INT(11);
    DECLARE v_proyek INT(11);
    DECLARE v_total_jual DOUBLE DEFAULT 0;
	DECLARE v_id_bkg INT(11);

	DECLARE c_detil CURSOR FOR
     SELECT A.ipditem,A.ipdharga,A.ipddiskon,A.ipdkeluar,B.icoapendapatan,A.ipddivisi,A.ipdproyek   
       FROM einvoicepenjualand A 
 INNER JOIN bitem B ON A.ipditem=B.iid  
      WHERE A.ipdidipu = v_id ORDER BY B.icoapendapatan ASC;

  	DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_sts = TRUE;
  	DECLARE EXIT HANDLER FOR SQLEXCEPTION, SQLWARNING
  	BEGIN
    	ROLLBACK;
    	RESIGNAL;
  	END;  

  	OPEN c_detil;  
  	START TRANSACTION;
	
    SELECT cccoa INTO @v_coapajak FROM cconfigcoa WHERE cckode='IV' AND ccketerangan='PAJAKKELUARAN';
    SELECT cccoa INTO @v_coapiutang FROM cconfigcoa WHERE cckode='IV' AND ccketerangan='PIUTANGDAGANG';
    SELECT B.pcoaout INTO @v_coappn FROM ainfo A INNER JOIN bpajak B ON A.ippnjual=B.pid WHERE A.iid=1;
    SELECT B.pcoaout INTO @v_coapph22 FROM ainfo A INNER JOIN bpajak B ON A.ipph22jual=B.pid WHERE A.iid=1;    

	SELECT IPU.ipusumber,IPU.ipunotransaksi,IPU.iputanggal,
    	   IPU.ipukontak,IPU.ipuuraian,IPU.iputotaltransaksi,
           IPU.ipucreateu,IPU.iputotalpajak,IPU.ipujumlahdp,
           IPU.ipuiddp,IPU.ipujenispajak,IPU.iputotalpph22       
      INTO @v_sumber,@v_nomor,@v_tanggal,@v_kontak,@v_uraian,@v_total,@v_createu,@v_pajak,@v_dp,@v_iddp,@v_jnspajak,@v_pph22    
      FROM einvoicepenjualanu IPU
     WHERE IPU.ipuid = v_id;         
    
    INSERT INTO ctransaksiu SET 
    	cusumber = @v_sumber,
        cunotransaksi = @v_nomor,
        cutanggal = @v_tanggal,
        cukontak = @v_kontak,
        cuuraian = @v_uraian,
        cutotaltrans = @v_total,
        cucreateu = @v_createu;
    
    SET @cuid = LAST_INSERT_ID();
    SET @v_row = 1;

	-- JURNAL PIUTANG DAGANG (DEBIT)    
    INSERT INTO ctransaksid SET 
                cdidu=@cuid,
                cdurutan=@v_row,
                cdnocoa=@v_coapiutang,
                cddebit=@v_total,
                cduang=1,
                cddvisi=v_divisi,
                cdproyek=v_proyek;

	-- JURNAL PEMBALIK UANG MUKA PENJUALAN    
    IF @v_dp<>0 THEN
    	SET @cdid = LAST_INSERT_ID();
        IF @v_total-@v_dp <> 0 THEN
			UPDATE ctransaksid SET cddebit = cddebit - @v_dp WHERE cdid=@cdid;
		ELSE 
        	DELETE FROM ctransaksid WHERE cdid=@cdid;
        END IF;
        
		statedp: BEGIN 
		DECLARE v_sts INT DEFAULT FALSE;        
        DECLARE v_coadp INT(11);
        DECLARE v_amtdp DOUBLE DEFAULT 0;
        
        DECLARE c_uangmuka CURSOR FOR
		SELECT DP.dpcdp,IF(DP.dppajak=1,TA.idpjumlahdp-TA.idppajakdp,TA.idpjumlahdp) 'jumlahdp'
          FROM einvoicepenjualandp TA LEFT JOIN ddp DP ON TA.IDPIDDP=DP.DPID 
         WHERE TA.IDPIDU=v_id ORDER BY DP.dpcdp ASC;

        DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_sts = TRUE;
        DECLARE EXIT HANDLER FOR SQLEXCEPTION, SQLWARNING
        BEGIN
            ROLLBACK;
            RESIGNAL;
        END;  

        OPEN c_uangmuka;
    	SET @v_row = @v_row+1;
        SET @v_coadp_temp = '';
        
        dp_loop: LOOP
        FETCH c_uangmuka INTO v_coadp,v_amtdp;
        IF v_sts THEN
          LEAVE dp_loop;
        END IF;        

		IF v_coadp <> @v_coadp_temp THEN
            INSERT INTO ctransaksid SET 
                        cdidu=@cuid,
                        cdurutan=@v_row,
                        cdnocoa=v_coadp,
                        cddebit=v_amtdp,
                        cduang=1,
                        cddvisi=v_divisi,
                        cdproyek=v_proyek;    	
        ELSE 
            SET @cdid = LAST_INSERT_ID();
            UPDATE ctransaksid SET cddebit = cddebit + v_amtdp  
             WHERE cdid = @cdid;            
        END IF;
        
		SET @v_row = @v_row+1;
        SET @v_coadp_temp = v_coadp;
        END LOOP;
                    
	  	CLOSE c_uangmuka;                    
        END statedp;                        
    END IF;

	-- JURNAL PAJAK KELUARAN (KREDIT)
	IF @v_pajak <> 0 THEN
    	SET @v_row = @v_row+1;
        INSERT INTO ctransaksid SET 
                    cdidu=@cuid,
                    cdurutan=@v_row,
                    cdnocoa=@v_coappn,
                    cdkredit=@v_pajak,
                    cduang=1,
                    cddvisi=v_divisi,
                    cdproyek=v_proyek;    
    END IF;

	-- JURNAL PAJAK PPH22 (KREDIT)
	IF @v_pph22 <> 0 THEN
    	SET @v_row = @v_row+1;
        INSERT INTO ctransaksid SET 
                    cdidu=@cuid,
                    cdurutan=@v_row,
                    cdnocoa=@v_coapph22,
                    cdkredit=@v_pph22,
                    cduang=1,
                    cddvisi=v_divisi,
                    cdproyek=v_proyek;    
    END IF;

	SET v_sts = FALSE;
	SET @v_row = @v_row+1;
	SET @v_nocoa_temp = '';    
    
  	detil_loop: LOOP
    FETCH c_detil INTO v_item, v_harga, v_diskon, v_qty, v_nocoa, v_divisi, v_proyek;
    IF v_sts THEN
      LEAVE detil_loop;
    END IF;
    
    -- JURNAL PENDAPATAN (KREDIT)
    IF v_nocoa <> @v_nocoa_temp THEN
    	IF @v_jnspajak=2 THEN 
	    	SET v_total_jual = ((v_harga-v_diskon)*v_qty) - (((100/110)*((v_harga-v_diskon)*v_qty))*(10/100));
	    ELSE
    		SET v_total_jual = (v_harga-v_diskon)*v_qty;    
	    END IF;
    
        INSERT INTO ctransaksid SET 
                    cdidu=@cuid,
                    cdurutan=@v_row,
                    cdnocoa=v_nocoa,
                    cdkredit=v_total_jual,
                    cduang=1,
                    cddvisi=v_divisi,
                    cdproyek=v_proyek;
	ELSE 
    	SET @cdid = LAST_INSERT_ID();
    	IF @v_jnspajak=2 THEN 
	    	SET v_total_jual = ((v_harga-v_diskon)*v_qty) - (((100/110)*((v_harga-v_diskon)*v_qty))*(10/100));
	    ELSE
    		SET v_total_jual = (v_harga-v_diskon)*v_qty;    
	    END IF;

        UPDATE ctransaksid SET cdkredit = cdkredit + v_total_jual, cddvisi=v_divisi, cdproyek=v_proyek 
         WHERE cdid = @cdid;
	END IF;

	SET @v_row = @v_row+1;
	SET @v_nocoa_temp = v_nocoa;
    END LOOP;    
    
  	CLOSE c_detil;

	SELECT ipunobkg INTO v_id_bkg FROM einvoicepenjualanu WHERE ipuid=v_id;

JURNAL_HPP: BEGIN
	DECLARE v_sts INT DEFAULT FALSE;
  	DECLARE v_item INT(11);
    DECLARE v_jenisitem INT(11);
    DECLARE v_coa_persediaan INT(11);
    DECLARE v_coa_hpp INT(11);    
    DECLARE v_hpp DOUBLE;
    DECLARE v_divisi INT(11);
    DECLARE v_proyek INT(11);
    
	   DECLARE c_detil_hpp CURSOR FOR
        SELECT A.sditem,B.icoapersediaan,B.icoahpp,A.sddivisi,A.sdproyek,SUM(C.shqty*C.shharga) 'HPP',B.ijenisitem    
          FROM fstokd A 
    INNER JOIN bitem B ON A.sditem=B.iid 
    INNER JOIN fstokh C ON A.sdid=C.SHIDSD
         WHERE A.sdidsu IN (SELECT ipdsju FROM einvoicepenjualand WHERE ipdidipu=v_id) GROUP BY A.sditem 
      ORDER BY B.icoahpp ASC;      

  	DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_sts = TRUE;

  	OPEN c_detil_hpp;  

    SELECT cccoa INTO @v_coaselisih FROM cconfigcoa WHERE cckode='SJ' AND ccketerangan='PERSEDIAANTERKIRIM';

    SET @v_coahpp_temp = '';
    SET @v_coapersediaan_temp = '';
	SET @v_sum_hpp = 0;
    
  	hpp_loop: LOOP
    FETCH c_detil_hpp INTO v_item, v_coa_persediaan, v_coa_hpp, v_divisi, v_proyek, v_hpp, v_jenisitem;
    IF v_sts THEN
	  CLOSE c_detil_hpp;    
      LEAVE hpp_loop;
    END IF;
    
    -- JURNAL HPP (DEBIT)
    IF v_jenisitem = 0 THEN 
        IF v_coa_hpp <> @v_coahpp_temp THEN    
            INSERT INTO ctransaksid SET 
                        cdidu=@cuid,
                        cdurutan=@v_row,
                        cdnocoa=v_coa_hpp,
                        cddebit=v_hpp,
                        cduang=1,
                        cddvisi=v_divisi,
                        cdproyek=v_proyek;
        ELSE 
            SET @cdid = LAST_INSERT_ID();
            UPDATE ctransaksid SET cddebit = cddebit + v_hpp, cddvisi=v_divisi, cdproyek=v_proyek 
             WHERE cdid = @cdid;
        END IF;
	END IF;
    
	SET @v_row = @v_row+1;
	SET @v_coahpp_temp = v_coa_hpp;
	SET @v_sum_hpp = @v_sum_hpp + v_hpp;    
    
    END LOOP;    

    IF @v_coaselisih > 0 THEN 
    	INSERT INTO ctransaksid SET 
                    cdidu=@cuid,
                    cdurutan=@v_row,
                    cdnocoa=@v_coaselisih,
                    cdkredit=@v_sum_hpp,
                    cduang=1,
                    cddvisi=v_divisi,
                    cdproyek=v_proyek;    
    ELSE 
        state2: BEGIN
        DECLARE v_sts INT DEFAULT FALSE;
        DECLARE v_item INT(11);
        DECLARE v_jenisitem INT(11);
        DECLARE v_coa_persediaan INT(11);
        DECLARE v_coa_hpp INT(11);    
        DECLARE v_hpp DOUBLE;
        DECLARE v_divisi INT(11);
        DECLARE v_proyek INT(11);

           DECLARE c_detil_hpp CURSOR FOR
            SELECT A.sditem,B.icoapersediaan,B.icoahpp,A.sddivisi,A.sdproyek,SUM(C.shqty*C.shharga) 'HPP',B.ijenisitem    
              FROM fstokd A 
        INNER JOIN bitem B ON A.sditem=B.iid 
        INNER JOIN fstokh C ON A.sdid=C.SHIDSD
             WHERE A.sdidsu IN (SELECT ipdsju FROM einvoicepenjualand WHERE ipdidipu=v_id) GROUP BY A.sditem 
          ORDER BY B.icoapersediaan ASC;      

        DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_sts = TRUE;

        OPEN c_detil_hpp;  
        SET @v_coapersediaan_temp = '';

        stock_loop: LOOP
        FETCH c_detil_hpp INTO v_item, v_coa_persediaan, v_coa_hpp, v_divisi, v_proyek, v_hpp, v_jenisitem;
        IF v_sts THEN
          CLOSE c_detil_hpp;    
          LEAVE stock_loop;
        END IF;

        -- JURNAL PERSEDIAAN (KREDIT)
        IF v_jenisitem = 0 THEN 
            IF v_coa_persediaan <> @v_coapersediaan_temp THEN    
                INSERT INTO ctransaksid SET 
                            cdidu=@cuid,
                            cdurutan=@v_row,
                            cdnocoa=v_coa_persediaan,
                            cdkredit=v_hpp,
                            cduang=1,
                            cddvisi=v_divisi,
                            cdproyek=v_proyek;
            ELSE 
                SET @cdid = LAST_INSERT_ID();
                UPDATE ctransaksid SET cdkredit = cdkredit + v_hpp, cddvisi=v_divisi, cdproyek=v_proyek 
                 WHERE cdid = @cdid;
            END IF;
		END IF;
        
        SET @v_row = @v_row+1;
        SET @v_coapersediaan_temp = v_coa_persediaan;
        END LOOP;
        END state2;    
    END IF;
END JURNAL_HPP;
COMMIT; 
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_JURNAL_INVOICE_PENJUALAN_DEL` (`v_id` INT(11))  BEGIN
	DECLARE v_sts INT DEFAULT FALSE;
	DECLARE v_sumber VARCHAR(50);
    DECLARE v_nomor VARCHAR(50);
    
  	DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_sts = TRUE;
  	DECLARE EXIT HANDLER FOR SQLEXCEPTION, SQLWARNING
  	BEGIN
    	ROLLBACK;
    	RESIGNAL;
  	END;  

	SELECT ipusumber, ipunotransaksi INTO v_sumber,v_nomor
      FROM einvoicepenjualanu WHERE ipuid=v_id;

	SELECT cuid INTO @cuid FROM ctransaksiu WHERE cusumber=v_sumber AND cunotransaksi=v_nomor;

  	START TRANSACTION;
		DELETE FROM ctransaksid WHERE cdidu=@cuid;
        DELETE FROM ctransaksiu WHERE cuid=@cuid;
  	COMMIT;     
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_JURNAL_PEMBAYARAN_HUTANG_ADD` (IN `v_id` INT(11))  BEGIN
	DECLARE v_sts INT DEFAULT FALSE;
    DECLARE v_totalretur DOUBLE DEFAULT 0;    
	DECLARE v_coahutang INT(11);
    DECLARE v_coahutangdp INT(11);
    DECLARE v_coadiskon INT(11);
    DECLARE v_coaretur INT(11);
	DECLARE v_idinvoice INT(11);
	DECLARE v_iddp INT(11);    
	DECLARE v_total DOUBLE DEFAULT 0;    
    
	DECLARE cur_epembayaraninvoicei CURSOR FOR
     SELECT PIIIDINVOICE,PIIIDDP,PIIJMLBAYAR
       FROM epembayaraninvoicei WHERE PIIIDU = v_id ORDER BY PIIIDINVOICE ASC;
    
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_sts = TRUE;    
  	DECLARE EXIT HANDLER FOR SQLEXCEPTION, SQLWARNING
  	BEGIN
    	ROLLBACK;
    	RESIGNAL;
  	END; 

    SELECT cccoa INTO v_coahutangdp FROM cconfigcoa WHERE cckode='VP' AND ccketerangan='HUTANGUANGMUKA';
    SELECT cccoa INTO v_coahutang FROM cconfigcoa WHERE cckode='VP' AND ccketerangan='HUTANGDAGANG';
    SELECT cccoa INTO v_coadiskon FROM cconfigcoa WHERE cckode='VP' AND ccketerangan='DISKONPEMBELIAN';    
    SELECT cccoa INTO v_coaretur FROM cconfigcoa WHERE cckode='VP' AND ccketerangan='HUTANGRETUR';

	-- SELECT SUM(PIRJMLBAYAR) INTO v_totalretur FROM epembayaraninvoicer WHERE PIRIDU = v_id;
	SELECT epembayaraninvoiceu.piusumber,epembayaraninvoiceu.piunotransaksi,epembayaraninvoiceu.piutanggal,
    	   epembayaraninvoiceu.piukontak,epembayaraninvoiceu.piuuraian,epembayaraninvoiceu.piujmlkas,
           epembayaraninvoiceu.piucreateu,epembayaraninvoiceu.piucoakas,epembayaraninvoiceu.piudiskon,
           epembayaraninvoiceu.piunilaibuktipotong,bpajak.PCOAIN,bpajak.PCOAOUT,epembayaraninvoiceu.piustatuspph,
		   epembayaraninvoiceu.piutotalretur,epembayaraninvoiceu.piuselisihbayar,epembayaraninvoiceu.piuakunselisih 	           
      INTO @v_sumber,@v_nomor,@v_tanggal,@v_kontak,@v_uraian,@v_total,@v_createu,@v_coakas,@v_totaldiskon,
      	   @v_totalpph,@v_coapphin,@v_coapphout,@v_statuspph,@v_totalretur,@v_totalselisih,@v_coaselisih     
      FROM epembayaraninvoiceu LEFT JOIN bpajak ON epembayaraninvoiceu.PIUPPH=bpajak.PID 
     WHERE epembayaraninvoiceu.piuid = v_id;         
    
  	OPEN cur_epembayaraninvoicei;
  	START TRANSACTION;    
    
    INSERT INTO ctransaksiu SET 
    	cusumber = @v_sumber,
        cunotransaksi = @v_nomor,
        cutanggal = @v_tanggal,
        cukontak = @v_kontak,
        cuuraian = @v_uraian,
        cutotaltrans = @v_total,
        cucreateu = @v_createu;
    
    SET @cuid = LAST_INSERT_ID();
    SET @v_row = 1;
    SET @v_nocoa_temp = '';
    
    detil_loop: LOOP
    FETCH cur_epembayaraninvoicei INTO v_idinvoice, v_iddp, v_total;
    IF v_sts THEN
    	LEAVE detil_loop;
   	END IF;    

        IF v_idinvoice IS NULL THEN
            SET @v_coahutang = v_coahutangdp;
        ELSE
			SET @v_coahutang = v_coahutang;
        END IF;

        -- JURNAL HUTANG DAGANG & HUTANG UANG MUKA (DEBIT)
        IF @v_coahutang <> @v_nocoa_temp THEN 
            INSERT INTO ctransaksid SET 
                        cdidu=@cuid,
                        cdurutan=@v_row,
                        cdnocoa=@v_coahutang,
                        cddebit=v_total,
                        cduang=1;
	
    		SET @v_row = @v_row+1;                        
        ELSE 
            SET @cdid = LAST_INSERT_ID();        
            UPDATE ctransaksid SET cddebit = cddebit + v_total WHERE cdid = @cdid;
        END IF;

        SET @v_nocoa_temp = @v_coahutang;
    END LOOP;
   
    -- JURNAL KAS/BANK (DEBIT/KREDIT)
    IF @v_total < 0 THEN
    SET @v_row = @v_row+1;    
        INSERT INTO ctransaksid SET 
                    cdidu=@cuid,
                    cdurutan=@v_row,
                    cdnocoa=@v_coakas,
                    cddebit=ABS(@v_total),
                    cduang=1;       
	ELSE
    SET @v_row = @v_row+1;    
        INSERT INTO ctransaksid SET 
                    cdidu=@cuid,
                    cdurutan=@v_row,
                    cdnocoa=@v_coakas,
                    cdkredit=ABS(@v_total),
                    cduang=1;       
	END IF;   

    -- JURNAL DISKON PEMBELIAN (KREDIT)
    IF @v_totaldiskon <> 0 THEN 
	    SET @v_row = @v_row+1;        
            INSERT INTO ctransaksid SET 
                        cdidu=@cuid,
                        cdurutan=@v_row,
                        cdnocoa=v_coadiskon,
                        cdkredit=@v_totaldiskon,
                        cduang=1;
    END IF;

    -- JURNAL PPH (KREDIT)
    IF @v_totalpph <> 0 THEN 
	    SET @v_row = @v_row+1;        
        INSERT INTO ctransaksid SET 
                    cdidu=@cuid,
                    cdurutan=@v_row,
                    cdnocoa=@v_coapphin,
                    cdkredit=@v_totalpph,
                    cduang=1;
    END IF;

	-- JURNAL HUTANG RETUR (KREDIT)
    IF @v_totalretur <> 0 THEN     
	    SET @v_row = @v_row+1;        
            INSERT INTO ctransaksid SET 
                        cdidu=@cuid,
                        cdurutan=@v_row,
                        cdnocoa=v_coaretur,
                        cdkredit=@v_totalretur,
                        cduang=1;
    END IF;
    
    -- JURNAL SELISIH (KREDIT)
    IF @v_totalselisih < 0 THEN
            SET @v_row = @v_row+1;        
            INSERT INTO ctransaksid SET 
                        cdidu=@cuid,
                        cdurutan=@v_row,
                        cdnocoa=@v_coaselisih,
                        cdkredit=ABS(@v_totalselisih),
                        cduang=1;        
    END IF;

    -- JURNAL SELISIH (DEBIT)
    IF @v_totalselisih > 0 THEN
            SET @v_row = @v_row+1;        
            INSERT INTO ctransaksid SET 
                        cdidu=@cuid,
                        cdurutan=@v_row,
                        cdnocoa=@v_coaselisih,
                        cddebit=@v_totalselisih,
                        cduang=1;        
    END IF;

  	COMMIT; 
    CLOSE cur_epembayaraninvoicei;     
    
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_JURNAL_PEMBAYARAN_HUTANG_DEL` (`v_id` INT(11))  BEGIN
	DECLARE v_sts INT DEFAULT FALSE;
	DECLARE v_sumber VARCHAR(50);
    DECLARE v_nomor VARCHAR(50);
    
  	DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_sts = TRUE;
  	DECLARE EXIT HANDLER FOR SQLEXCEPTION, SQLWARNING
  	BEGIN
    	ROLLBACK;
    	RESIGNAL;
  	END;  

	SELECT piusumber, piunotransaksi INTO v_sumber,v_nomor
      FROM epembayaraninvoiceu WHERE piuid=v_id;

	SELECT cuid INTO @cuid FROM ctransaksiu WHERE cusumber=v_sumber AND cunotransaksi=v_nomor;

  	START TRANSACTION;
		DELETE FROM ctransaksid WHERE cdidu=@cuid;
        DELETE FROM ctransaksiu WHERE cuid=@cuid;
  	COMMIT;     
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_JURNAL_PEMBAYARAN_PIUTANG_ADD` (`v_id` INT(11))  BEGIN
	DECLARE v_sts INT DEFAULT FALSE;
    DECLARE v_totalretur DOUBLE DEFAULT 0;    
	DECLARE v_coapiutang INT(11);
    DECLARE v_coapiutangdp INT(11);
    DECLARE v_coadiskon INT(11);
    DECLARE v_coaretur INT(11);
	DECLARE v_idinvoice INT(11);
	DECLARE v_iddp INT(11);    
	DECLARE v_total DOUBLE DEFAULT 0;    
        
	DECLARE cur_epembayaraninvoicei CURSOR FOR
     SELECT PIIIDINVOICE,PIIIDDP,PIIJMLBAYAR
       FROM epembayaraninvoicei WHERE PIIIDU = v_id ORDER BY PIIIDINVOICE ASC;
    
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_sts = TRUE;    
  	DECLARE EXIT HANDLER FOR SQLEXCEPTION, SQLWARNING
  	BEGIN
    	ROLLBACK;
    	RESIGNAL;
  	END; 

    SELECT cccoa INTO v_coapiutangdp FROM cconfigcoa WHERE cckode='CP' AND ccketerangan='PIUTANGUANGMUKA';
    SELECT cccoa INTO v_coapiutang FROM cconfigcoa WHERE cckode='CP' AND ccketerangan='PIUTANGDAGANG';
    SELECT cccoa INTO v_coadiskon FROM cconfigcoa WHERE cckode='CP' AND ccketerangan='DISKONPENJUALAN';    
    SELECT cccoa INTO v_coaretur FROM cconfigcoa WHERE cckode='CP' AND ccketerangan='PIUTANGRETUR';

	-- SELECT SUM(PIRJMLBAYAR) INTO v_totalretur FROM epembayaraninvoicer WHERE PIRIDU = v_id;

	SELECT epembayaraninvoiceu.piusumber,epembayaraninvoiceu.piunotransaksi,epembayaraninvoiceu.piutanggal,
    	   epembayaraninvoiceu.piukontak,epembayaraninvoiceu.piuuraian,epembayaraninvoiceu.piujmlkas,
           epembayaraninvoiceu.piucreateu,epembayaraninvoiceu.piucoakas,epembayaraninvoiceu.piudiskon,
           epembayaraninvoiceu.piunilaibuktipotong,bpajak.PCOAIN,bpajak.PCOAOUT,epembayaraninvoiceu.piustatuspph,
		   epembayaraninvoiceu.piutotalretur,epembayaraninvoiceu.piuselisihbayar,epembayaraninvoiceu.piuakunselisih 	           
      INTO @v_sumber,@v_nomor,@v_tanggal,@v_kontak,@v_uraian,@v_total,@v_createu,@v_coakas,@v_totaldiskon,
      	   @v_totalpph,@v_coapphin,@v_coapphout,@v_statuspph,@v_totalretur,@v_totalselisih,@v_coaselisih     
      FROM epembayaraninvoiceu LEFT JOIN bpajak ON epembayaraninvoiceu.PIUPPH=bpajak.PID 
     WHERE epembayaraninvoiceu.piuid = v_id;         
    
  	OPEN cur_epembayaraninvoicei;
  	START TRANSACTION;    
    
    INSERT INTO ctransaksiu SET 
    	cusumber = @v_sumber,
        cunotransaksi = @v_nomor,
        cutanggal = @v_tanggal,
        cukontak = @v_kontak,
        cuuraian = @v_uraian,
        cutotaltrans = @v_total,
        cucreateu = @v_createu;
    
    SET @cuid = LAST_INSERT_ID();

    -- JURNAL KAS/BANK (DEBIT/KREDIT)
    IF @v_total < 0 THEN    
        SET @v_row = 1;
        INSERT INTO ctransaksid SET 
                    cdidu=@cuid,
                    cdurutan=@v_row,
                    cdnocoa=@v_coakas,
                    cdkredit=ABS(@v_total),
                    cduang=1;   
     ELSE
        SET @v_row = 1;
        INSERT INTO ctransaksid SET 
                    cdidu=@cuid,
                    cdurutan=@v_row,
                    cdnocoa=@v_coakas,
                    cddebit=ABS(@v_total),
                    cduang=1;        
     END IF;
   
    -- JURNAL PIUTANG RETUR (DEBIT)
    IF @v_totalretur <> 0 THEN 
	    SET @v_row = @v_row+1;        
            INSERT INTO ctransaksid SET 
                        cdidu=@cuid,
                        cdurutan=@v_row,
                        cdnocoa=v_coaretur,
                        cddebit=@v_totalretur,
                        cduang=1;
    END IF;

    -- JURNAL DISKON PENJUALAN (DEBIT)
    IF @v_totaldiskon <> 0 THEN 
	    SET @v_row = @v_row+1;        
            INSERT INTO ctransaksid SET 
                        cdidu=@cuid,
                        cdurutan=@v_row,
                        cdnocoa=v_coadiskon,
                        cddebit=@v_totaldiskon,
                        cduang=1;
    END IF;

    -- JURNAL PPH (DEBIT)
    IF @v_totalpph <> 0 THEN
    	IF @v_statuspph = 0 THEN 
            SET @v_row = @v_row+1;        
            INSERT INTO ctransaksid SET 
                        cdidu=@cuid,
                        cdurutan=@v_row,
                        cdnocoa=@v_coapphout,
                        cddebit=@v_totalpph,
                        cduang=1;        
        END IF;
    END IF;

    -- JURNAL SELISIH (KREDIT)
    IF @v_totalselisih > 0 THEN
            SET @v_row = @v_row+1;        
            INSERT INTO ctransaksid SET 
                        cdidu=@cuid,
                        cdurutan=@v_row,
                        cdnocoa=@v_coaselisih,
                        cdkredit=@v_totalselisih,
                        cduang=1;        
    END IF;

    -- JURNAL SELISIH (DEBIT)
    IF @v_totalselisih < 0 THEN
            SET @v_row = @v_row+1;        
            INSERT INTO ctransaksid SET 
                        cdidu=@cuid,
                        cdurutan=@v_row,
                        cdnocoa=@v_coaselisih,
                        cddebit=ABS(@v_totalselisih),
                        cduang=1;        
    END IF;

	SET @v_row = @v_row+1;        
	SET @v_nocoa_temp = '';
    
    detil_loop: LOOP
    FETCH cur_epembayaraninvoicei INTO v_idinvoice, v_iddp, v_total;
    IF v_sts THEN
    	LEAVE detil_loop;
   	END IF;    

        IF v_idinvoice IS NULL THEN
            SET @v_coapiutang = v_coapiutangdp;
        ELSE
			SET @v_coapiutang = v_coapiutang;
        END IF;

        -- JURNAL PIUTANG DAGANG & PIUTANG UANG MUKA (KREDIT)
        IF @v_coapiutang <> @v_nocoa_temp THEN 
            INSERT INTO ctransaksid SET 
                        cdidu=@cuid,
                        cdurutan=@v_row,
                        cdnocoa=@v_coapiutang,
                        cdkredit=v_total,
                        cduang=1;
	
    		SET @v_row = @v_row+1;                        
        ELSE 
            SET @cdid = LAST_INSERT_ID();        
            UPDATE ctransaksid SET cdkredit = cdkredit + v_total WHERE cdid = @cdid;
        END IF;

        SET @v_nocoa_temp = @v_coapiutang;
    END LOOP;
    
  	COMMIT; 
    CLOSE cur_epembayaraninvoicei;     
    
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_JURNAL_PEMBAYARAN_PIUTANG_DEL` (`v_id` INT(11))  BEGIN
	DECLARE v_sts INT DEFAULT FALSE;
	DECLARE v_sumber VARCHAR(50);
    DECLARE v_nomor VARCHAR(50);
    
  	DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_sts = TRUE;
  	DECLARE EXIT HANDLER FOR SQLEXCEPTION, SQLWARNING
  	BEGIN
    	ROLLBACK;
    	RESIGNAL;
  	END;  

	SELECT piusumber, piunotransaksi INTO v_sumber,v_nomor
      FROM epembayaraninvoiceu WHERE piuid=v_id;

	SELECT cuid INTO @cuid FROM ctransaksiu WHERE cusumber=v_sumber AND cunotransaksi=v_nomor;

  	START TRANSACTION;
		DELETE FROM ctransaksid WHERE cdidu=@cuid;
        DELETE FROM ctransaksiu WHERE cuid=@cuid;
  	COMMIT;     
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_JURNAL_PEMBELIAN_AKTIVA_ADD` (`v_id` INT(11))  BEGIN
	DECLARE v_sts INT DEFAULT FALSE;
  	DECLARE v_item INT(11);
    DECLARE v_harga DOUBLE;
    DECLARE v_nocoa INT(11);
    DECLARE v_divisi INT(11);
    DECLARE v_proyek INT(11);
    DECLARE v_total DOUBLE DEFAULT 0;
    
	DECLARE c_detil CURSOR FOR
     SELECT A.sditem,A.sdharga,B.acoaaktiva,A.sddivisi,A.sdproyek   
       FROM fasetd A 
 INNER JOIN baktiva B ON A.sditem=B.aid  
      WHERE A.sdidsu = v_id ORDER BY A.sdurutan ASC;

  	DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_sts = TRUE;
  	DECLARE EXIT HANDLER FOR SQLEXCEPTION, SQLWARNING
  	BEGIN
    	ROLLBACK;
    	RESIGNAL;
  	END;  

  	OPEN c_detil;  
  	START TRANSACTION;
    
	SELECT SU.susumber,SU.sunotransaksi,SU.sutanggal,
    	   SU.sukontak,SU.suuraian,SU.sutotaltransaksi,
           SU.sucreateu,SU.sucoa      
      INTO @v_sumber,@v_nomor,@v_tanggal,@v_kontak,@v_uraian,@v_total,@v_createu,@v_coapembelian    
      FROM fasetu SU
     WHERE SU.suid = v_id;         
    
    INSERT INTO ctransaksiu SET 
    	cusumber = @v_sumber,
        cunotransaksi = @v_nomor,
        cutanggal = @v_tanggal,
        cukontak = @v_kontak,
        cuuraian = @v_uraian,
        cutotaltrans = @v_total,
        cucreateu = @v_createu;
    
    SET @cuid = LAST_INSERT_ID();
    SET @v_row = 1;
    
  	detil_loop: LOOP
    FETCH c_detil INTO v_item, v_harga, v_nocoa, v_divisi, v_proyek;
    IF v_sts THEN
      LEAVE detil_loop;
    END IF;
    
    -- JURNAL ASET TETAP (DEBIT)
        INSERT INTO ctransaksid SET 
                    cdidu=@cuid,
                    cdurutan=@v_row,
                    cdnocoa=v_nocoa,
                    cddebit=v_harga,
                    cduang=1,
                    cddvisi=v_divisi,
                    cdproyek=v_proyek;
                    
	SET @v_row = @v_row+1;

    END LOOP;    

	-- JURNAL PEMBELIAN AKTIVA (KREDIT)    
	SET @v_row = @v_row+1;
    INSERT INTO ctransaksid SET 
                cdidu=@cuid,
                cdurutan=@v_row,
                cdnocoa=@v_coapembelian,
                cdkredit=@v_total,
                cduang=1,
                cddvisi=v_divisi,
                cdproyek=v_proyek;

  	COMMIT; 
  	CLOSE c_detil;
    
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_JURNAL_PENJUALAN_TUNAI_ADD` (`v_id` INT(11))  BEGIN
	DECLARE v_sts INT DEFAULT FALSE;
  	DECLARE v_item INT(11);
    DECLARE v_coa_persediaan INT(11);
    DECLARE v_harga DOUBLE;
    DECLARE v_diskon DOUBLE;
    DECLARE v_qty DOUBLE;
    DECLARE v_nocoa INT(11);
    DECLARE v_divisi INT(11);
    DECLARE v_proyek INT(11);
    DECLARE v_total_jual DOUBLE DEFAULT 0;
	DECLARE v_id_bkg INT(11);

	DECLARE c_detil CURSOR FOR
     SELECT A.sditem,A.sdharga,A.sddiskon,A.sdkeluar,B.icoapendapatan,A.sddivisi,A.sdproyek   
       FROM fstokd A 
 INNER JOIN bitem B ON A.sditem=B.iid  
      WHERE A.sdidsu = v_id ORDER BY B.icoapendapatan ASC;

  	DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_sts = TRUE;
  	DECLARE EXIT HANDLER FOR SQLEXCEPTION, SQLWARNING
  	BEGIN
    	ROLLBACK;
    	RESIGNAL;
  	END;  

  	OPEN c_detil;  
  	START TRANSACTION;

    SELECT B.pcoaout INTO @v_coapajak FROM ainfo A INNER JOIN bpajak B ON A.ippnpos=B.pid WHERE A.iid=1;
    SELECT cccoa INTO @v_coakasbank FROM cconfigcoa WHERE cckode='IP' AND ccketerangan='KASBANK';
    SELECT cccoa INTO @v_coakartudebit FROM cconfigcoa WHERE cckode='IP' AND ccketerangan='PIUTANGKARTUDEBIT';
    SELECT cccoa INTO @v_coakartukredit FROM cconfigcoa WHERE cckode='IP' AND ccketerangan='PIUTANGKARTUKREDIT';    

	SELECT SU.susumber,SU.sunotransaksi,SU.sutanggal,
    	   SU.sukontak,SU.suuraian,SU.sutotaltransaksi,
           SU.sucreateu,SU.sutotalpajak,SU.sutotalkas,SU.sutotalsisa,
           SU.sutotalkartudebit,SU.sutotalkartukredit,SU.supajak      
      INTO @v_sumber,@v_nomor,@v_tanggal,@v_kontak,@v_uraian,@v_total,@v_createu,@v_pajak,@v_kasbank,@v_sisa,@v_kartudebit,
      	   @v_kartukredit,@v_jnspajak    
      FROM fstoku SU
     WHERE SU.suid = v_id;         
    
    INSERT INTO ctransaksiu SET 
    	cusumber = @v_sumber,
        cunotransaksi = @v_nomor,
        cutanggal = @v_tanggal,
        cukontak = @v_kontak,
        cuuraian = @v_uraian,
        cutotaltrans = @v_total,
        cucreateu = @v_createu;
    
    SET @cuid = LAST_INSERT_ID();
    SET @v_row = 0;

	-- JURNAL PENERIMAAN KAS/BANK/PIUTANG EDP (DEBIT)    
	IF @v_kasbank>0 THEN
    SET @v_row = @v_row + 1;    
	INSERT INTO ctransaksid SET 
                cdidu=@cuid,
                cdurutan=@v_row,
                cdnocoa=@v_coakasbank,
                cddebit=@v_kasbank,
                cduang=1,
                cddvisi=v_divisi,
                cdproyek=v_proyek;
	END IF;
	IF @v_kartudebit>0 THEN 
    SET @v_row = @v_row + 1;        
	INSERT INTO ctransaksid SET 
                cdidu=@cuid,
                cdurutan=@v_row,
                cdnocoa=@v_coakartudebit,
                cddebit=@v_kartudebit,
                cduang=1,
                cddvisi=v_divisi,
                cdproyek=v_proyek;
	END IF;    
	IF @v_kartukredit>0 THEN 
    SET @v_row = @v_row + 1;        
	INSERT INTO ctransaksid SET 
                cdidu=@cuid,
                cdurutan=@v_row,
                cdnocoa=@v_coakartukredit,
                cddebit=@v_kartukredit,
                cduang=1,
                cddvisi=v_divisi,
                cdproyek=v_proyek;
	END IF;        
	IF @v_sisa<0 THEN
    SET @v_row = @v_row + 1;    
	INSERT INTO ctransaksid SET 
                cdidu=@cuid,
                cdurutan=@v_row,
                cdnocoa=@v_coakasbank,
                cdkredit=ABS(@v_sisa),
                cduang=1,
                cddvisi=v_divisi,
                cdproyek=v_proyek;
	END IF;    
	-- JURNAL PAJAK KELUARAN (KREDIT)
	IF @v_pajak <> 0 THEN
    	SET @v_row = @v_row+1;
        INSERT INTO ctransaksid SET 
                    cdidu=@cuid,
                    cdurutan=@v_row,
                    cdnocoa=@v_coapajak,
                    cdkredit=@v_pajak,
                    cduang=1,
                    cddvisi=v_divisi,
                    cdproyek=v_proyek;    
    END IF;

	SET @v_row = @v_row+1;
	SET @v_nocoa_temp = '';    
    
  	detil_loop: LOOP
    FETCH c_detil INTO v_item, v_harga, v_diskon, v_qty, v_nocoa, v_divisi, v_proyek;
    IF v_sts THEN
      LEAVE detil_loop;
    END IF;
    
    -- JURNAL PENDAPATAN (KREDIT)
    IF v_nocoa <> @v_nocoa_temp THEN
    	IF @v_jnspajak=2 THEN 
	    	SET v_total_jual = ((v_harga-v_diskon)*v_qty) - (((100/110)*((v_harga-v_diskon)*v_qty))*(10/100));
	    ELSE
    		SET v_total_jual = (v_harga-v_diskon)*v_qty;    
	    END IF;
    
        INSERT INTO ctransaksid SET 
                    cdidu=@cuid,
                    cdurutan=@v_row,
                    cdnocoa=v_nocoa,
                    cdkredit=v_total_jual,
                    cduang=1,
                    cddvisi=v_divisi,
                    cdproyek=v_proyek;
	ELSE 
    	SET @cdid = LAST_INSERT_ID();
    	IF @v_jnspajak=2 THEN 
	    	SET v_total_jual = ((v_harga-v_diskon)*v_qty) - (((100/110)*((v_harga-v_diskon)*v_qty))*(10/100));
	    ELSE
    		SET v_total_jual = (v_harga-v_diskon)*v_qty;    
	    END IF;

        UPDATE ctransaksid SET cdkredit = cdkredit + v_total_jual, cddvisi=v_divisi, cdproyek=v_proyek 
         WHERE cdid = @cdid;
	END IF;

	SET @v_row = @v_row+1;
	SET @v_nocoa_temp = v_nocoa;
    END LOOP;    
    
  	CLOSE c_detil;

JURNAL_HPP: BEGIN
	DECLARE v_sts INT DEFAULT FALSE;
  	DECLARE v_item INT(11);
    DECLARE v_coa_persediaan INT(11);
    DECLARE v_coa_hpp INT(11);    
    DECLARE v_hpp DOUBLE;
    DECLARE v_divisi INT(11);
    DECLARE v_proyek INT(11);
    
	   DECLARE c_detil_hpp CURSOR FOR
        SELECT A.sditem,B.icoapersediaan,B.icoahpp,A.sddivisi,A.sdproyek, SUM(C.shqty*C.shharga) 'HPP'   
          FROM fstokd A 
    INNER JOIN bitem B ON A.sditem=B.iid 
    INNER JOIN fstokh C ON A.sdid=C.SHIDSD
         WHERE A.sdidsu = v_id GROUP BY A.sditem 
      ORDER BY B.icoahpp ASC;      

  	DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_sts = TRUE;

  	OPEN c_detil_hpp;  
    
    SET @v_coahpp_temp = '';
    SET @v_coapersediaan_temp = '';
	SET @v_sum_hpp = 0;
    
  	hpp_loop: LOOP
    FETCH c_detil_hpp INTO v_item, v_coa_persediaan, v_coa_hpp, v_divisi, v_proyek, v_hpp;
    IF v_sts THEN
	  CLOSE c_detil_hpp;    
      LEAVE hpp_loop;
    END IF;
    
    -- JURNAL HPP (DEBIT)
    IF v_coa_hpp <> @v_coahpp_temp THEN    
        INSERT INTO ctransaksid SET 
                    cdidu=@cuid,
                    cdurutan=@v_row,
                    cdnocoa=v_coa_hpp,
                    cddebit=v_hpp,
                    cduang=1,
                    cddvisi=v_divisi,
                    cdproyek=v_proyek;
	ELSE 
    	SET @cdid = LAST_INSERT_ID();
        UPDATE ctransaksid SET cddebit = cddebit + v_hpp, cddvisi=v_divisi, cdproyek=v_proyek 
         WHERE cdid = @cdid;
	END IF;

	SET @v_row = @v_row+1;
	SET @v_coahpp_temp = v_coa_hpp;
	SET @v_sum_hpp = @v_sum_hpp + v_hpp;    
    
    END LOOP;    
    
	SET @v_coaselisih = 0;

    IF @v_coaselisih > 0 THEN 
    	INSERT INTO ctransaksid SET 
                    cdidu=@cuid,
                    cdurutan=@v_row,
                    cdnocoa=@v_coaselisih,
                    cdkredit=@v_sum_hpp,
                    cduang=1,
                    cddvisi=v_divisi,
                    cdproyek=v_proyek;    
    ELSE 
        state2: BEGIN
        DECLARE v_sts INT DEFAULT FALSE;
        DECLARE v_item INT(11);
        DECLARE v_coa_persediaan INT(11);
        DECLARE v_coa_hpp INT(11);    
        DECLARE v_hpp DOUBLE;
        DECLARE v_divisi INT(11);
        DECLARE v_proyek INT(11);

           DECLARE c_detil_hpp CURSOR FOR
            SELECT A.sditem,B.icoapersediaan,B.icoahpp,A.sddivisi,A.sdproyek, SUM(C.shqty*C.shharga) 'HPP'   
              FROM fstokd A 
        INNER JOIN bitem B ON A.sditem=B.iid 
        INNER JOIN fstokh C ON A.sdid=C.SHIDSD
             WHERE A.sdidsu = v_id GROUP BY A.sditem 
          ORDER BY B.icoapersediaan ASC;      

        DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_sts = TRUE;

        OPEN c_detil_hpp;  
        SET @v_coapersediaan_temp = '';

        stock_loop: LOOP
        FETCH c_detil_hpp INTO v_item, v_coa_persediaan, v_coa_hpp, v_divisi, v_proyek, v_hpp;
        IF v_sts THEN
          CLOSE c_detil_hpp;    
          LEAVE stock_loop;
        END IF;

        -- JURNAL PERSEDIAAN (KREDIT)
        IF v_coa_persediaan <> @v_coapersediaan_temp THEN    
            INSERT INTO ctransaksid SET 
                        cdidu=@cuid,
                        cdurutan=@v_row,
                        cdnocoa=v_coa_persediaan,
                        cdkredit=v_hpp,
                        cduang=1,
                        cddvisi=v_divisi,
                        cdproyek=v_proyek;
        ELSE 
            SET @cdid = LAST_INSERT_ID();
            UPDATE ctransaksid SET cdkredit = cdkredit + v_hpp, cddvisi=v_divisi, cdproyek=v_proyek 
             WHERE cdid = @cdid;
        END IF;

        SET @v_row = @v_row+1;
        SET @v_coapersediaan_temp = v_coa_persediaan;
        END LOOP;

        END state2;    
    END IF;
    
END JURNAL_HPP;

COMMIT; 

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_JURNAL_PENYESUAIAN_PERSEDIAAN_ADD` (`v_id` INT(11))  BEGIN
	DECLARE v_sts INT DEFAULT FALSE;
    DECLARE v_idsd INT(11);
  	DECLARE v_item INT(11);
    DECLARE v_coa_persediaan INT(11);
    DECLARE v_coa_hpp INT(11);    
    DECLARE v_coa_selisih INT(11);
    DECLARE v_harga DOUBLE;
    DECLARE v_diskon DOUBLE;
    DECLARE v_qty DOUBLE;
    DECLARE v_divisi INT(11);
    DECLARE v_proyek INT(11);
    DECLARE v_total DOUBLE DEFAULT 0;    
    
	DECLARE c_detil CURSOR FOR
     SELECT A.sdid, A.sditem,A.sdharga,A.sddiskon,(A.sdmasuk-A.sdkeluar) AS 'sdqty',B.icoapersediaan,B.icoahpp,A.sddivisi,A.sdproyek   
       FROM fstokd A 
 INNER JOIN bitem B ON A.sditem=B.iid  
      WHERE A.sdidsu = v_id ORDER BY A.sdurutan ASC;

  	DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_sts = TRUE;
  	DECLARE EXIT HANDLER FOR SQLEXCEPTION, SQLWARNING
  	BEGIN
    	ROLLBACK;
    	RESIGNAL;
  	END;  

  	OPEN c_detil;  
  	START TRANSACTION;
    
	SELECT SU.susumber,SU.sunotransaksi,SU.sutanggal,
    	   SU.sukontak,SU.suuraian,SU.sutotaltransaksi,
           SU.sucreateu,SU.sujenispenyesuaian       
      INTO @v_sumber,@v_nomor,@v_tanggal,@v_kontak,@v_uraian,@v_total,@v_createu,@jenispenyesuaian    
      FROM fstoku SU
     WHERE SU.suid = v_id;         

	SELECT jakunbiaya INTO v_coa_selisih FROM bjenispenyesuaian WHERE jid=@jenispenyesuaian;

    INSERT INTO ctransaksiu SET 
    	cusumber = @v_sumber,
        cunotransaksi = @v_nomor,
        cutanggal = @v_tanggal,
        cukontak = @v_kontak,
        cuuraian = @v_uraian,
        cutotaltrans = @v_total,
        cucreateu = @v_createu;
    
    SET @cuid = LAST_INSERT_ID();
    SET @v_row = 1;
    
  	detil_loop: LOOP
    FETCH c_detil INTO v_idsd, v_item, v_harga, v_diskon, v_qty, v_coa_persediaan, v_coa_hpp, v_divisi, v_proyek;
    IF v_sts THEN
      LEAVE detil_loop;
    END IF;

	SET v_total = (v_harga-v_diskon)*ABS(v_qty);    
    
    -- JURNAL PENYESUAIAN PERSEDIAAN (IF=BERTAMBAH / ELSE=BERKURANG)
    IF v_qty > 0 THEN
        INSERT INTO ctransaksid SET 
                    cdidu=@cuid,
                    cdurutan=@v_row,
                    cdnocoa=v_coa_persediaan,
                    cddebit=v_total,
                    cduang=1,
                    cddvisi=v_divisi,
                    cdproyek=v_proyek;
                    
        INSERT INTO ctransaksid SET 
                    cdidu=@cuid,
                    cdurutan=@v_row,
                    cdnocoa=v_coa_selisih,
                    cdkredit=v_total,
                    cduang=1,
                    cddvisi=v_divisi,
                    cdproyek=v_proyek;                    
	ELSE 
		SELECT (SUM(A.shharga*A.shqty)) 
          INTO @v_total_hpp 
          FROM fstokh A 
         WHERE A.shidsd=v_idsd;
         
        INSERT INTO ctransaksid SET 
                    cdidu=@cuid,
                    cdurutan=@v_row,
                    cdnocoa=v_coa_selisih,
                    cddebit=@v_total_hpp,
                    cduang=1,
                    cddvisi=v_divisi,
                    cdproyek=v_proyek;
                    
        INSERT INTO ctransaksid SET 
                    cdidu=@cuid,
                    cdurutan=@v_row,
                    cdnocoa=v_coa_persediaan,
                    cdkredit=@v_total_hpp,
                    cduang=1,
                    cddvisi=v_divisi,
                    cdproyek=v_proyek;                    
	END IF;

	SET @v_row = @v_row+1;
    END LOOP;    

  	COMMIT; 
  	CLOSE c_detil;
    
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_JURNAL_PENYESUAIAN_PERSEDIAAN_DEL` (`v_id` INT(11))  BEGIN
	DECLARE v_sts INT DEFAULT FALSE;
	DECLARE v_sumber VARCHAR(50);
    DECLARE v_nomor VARCHAR(50);
    
  	DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_sts = TRUE;
  	DECLARE EXIT HANDLER FOR SQLEXCEPTION, SQLWARNING
  	BEGIN
    	ROLLBACK;
    	RESIGNAL;
  	END;  

	SELECT susumber, sunotransaksi INTO v_sumber,v_nomor
      FROM fstoku WHERE suid=v_id;

	SELECT cuid INTO @cuid FROM ctransaksiu WHERE cusumber=v_sumber AND cunotransaksi=v_nomor;

  	START TRANSACTION;
		DELETE FROM ctransaksid WHERE cdidu=@cuid;
        DELETE FROM ctransaksiu WHERE cuid=@cuid;
  	COMMIT;     
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_JURNAL_RETUR_PEMBELIAN_ADD` (`v_id` INT(11), `v_id_bkg` INT(11))  BEGIN
	DECLARE v_sts INT DEFAULT FALSE;
  	DECLARE v_item INT(11);
    DECLARE v_coa_persediaan INT(11);
    DECLARE v_harga DOUBLE;
    DECLARE v_diskon DOUBLE;
    DECLARE v_qty DOUBLE;
    DECLARE v_nocoa INT(11);
    DECLARE v_nocoahpp INT(11);    
    DECLARE v_divisi INT(11);
    DECLARE v_proyek INT(11);
    DECLARE v_total_retur DOUBLE DEFAULT 0;    
    
	DECLARE c_detil CURSOR FOR
       SELECT A.sditem, SUM(C.shharga*C.shqty) 'harga', A.sdkeluar, B.icoapersediaan, B.icoahpp, A.sddivisi, A.sdproyek  
         FROM fstokd A
   INNER JOIN bitem B ON A.sditem = B.iid
    LEFT JOIN fstokh C ON A.sdid=C.shidsd 
        WHERE A.sdidsu = v_id_bkg ORDER BY B.icoapersediaan ASC;
        
--     SELECT A.ipditem,A.ipdharga,A.ipddiskon,A.ipdkeluar,B.icoapersediaan,A.ipddivisi,A.ipdproyek   
--       FROM einvoicepenjualand A 
-- INNER JOIN bitem B ON A.ipditem=B.iid  
--      WHERE A.ipdidipu = v_id ORDER BY A.ipdurutan ASC;

  	DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_sts = TRUE;
  	DECLARE EXIT HANDLER FOR SQLEXCEPTION, SQLWARNING
  	BEGIN
    	ROLLBACK;
    	RESIGNAL;
  	END;  

  	OPEN c_detil;  
  	START TRANSACTION;
	
    SELECT cccoa INTO @v_coapajak FROM cconfigcoa WHERE cckode='PR' AND ccketerangan='PAJAKMASUKAN';
    SELECT cccoa INTO @v_coahutang FROM cconfigcoa WHERE cckode='PR' AND ccketerangan='HUTANGRETUR';
    SELECT cccoa INTO @v_coaretur FROM cconfigcoa WHERE cckode='PR' AND ccketerangan='RETURPEMBELIAN';
    SELECT cccoa INTO @v_coaselisih FROM cconfigcoa WHERE cckode='ITH' AND ccketerangan='HARGAPOKOK';
    SELECT B.pcoain INTO @v_coappn FROM ainfo A INNER JOIN bpajak B ON A.ippnbeli=B.pid WHERE A.iid=1;
    SELECT B.pcoain INTO @v_coapph22 FROM ainfo A INNER JOIN bpajak B ON A.ipph22beli=B.pid WHERE A.iid=1;    

	SELECT IPU.ipusumber,IPU.ipunotransaksi,IPU.iputanggal,
    	   IPU.ipukontak,IPU.ipuuraian,IPU.iputotaltransaksi,
           IPU.ipucreateu,IPU.iputotalpajak,IPU.ipujenispajak,IPU.iputotalpph22       
      INTO @v_sumber,@v_nomor,@v_tanggal,@v_kontak,@v_uraian,@v_total,@v_createu,@v_pajak,@v_jnspajak,@v_pph22    
      FROM einvoicepenjualanu IPU
     WHERE IPU.ipuid = v_id;         
    
    INSERT INTO ctransaksiu SET 
    	cusumber = @v_sumber,
        cunotransaksi = @v_nomor,
        cutanggal = @v_tanggal,
        cukontak = @v_kontak,
        cuuraian = @v_uraian,
        cutotaltrans = @v_total,
        cucreateu = @v_createu;
    
    SET @cuid = LAST_INSERT_ID();
    SET @v_row = 1;
    SET @v_nocoa_temp = '';
	SET @temp_total = 0;
    
	-- JURNAL HUTANG RETUR (DEBIT)    
    INSERT INTO ctransaksid SET 
                cdidu=@cuid,
                cdurutan=@v_row,
                cdnocoa=@v_coahutang,
                cddebit=@v_total,
                cduang=1,
                cddvisi=v_divisi,
                cdproyek=v_proyek;

	-- JURNAL PAJAK MASUKAN (KREDIT)
	IF @v_pajak <> 0 THEN
    	SET @v_row = @v_row+1;
        INSERT INTO ctransaksid SET 
                    cdidu=@cuid,
                    cdurutan=@v_row,
                    cdnocoa=@v_coappn,
                    cdkredit=@v_pajak,
                    cduang=1,
                    cddvisi=v_divisi,
                    cdproyek=v_proyek;    
    END IF;

	-- JURNAL PAJAK PPH22 (KREDIT)
	IF @v_pph22 <> 0 THEN
    	SET @v_row = @v_row+1;
        INSERT INTO ctransaksid SET 
                    cdidu=@cuid,
                    cdurutan=@v_row,
                    cdnocoa=@v_coapph22,
                    cdkredit=@v_pph22,
                    cduang=1,
                    cddvisi=v_divisi,
                    cdproyek=v_proyek;    
    END IF;

    SET @v_row = @v_row+1;    
    
	detil_loop: LOOP
    FETCH c_detil INTO v_item, v_harga, v_qty, v_nocoa, v_nocoahpp, v_divisi, v_proyek;
    IF v_sts THEN
      LEAVE detil_loop;
    END IF;
        -- JURNAL RETUR / PERSEDIAAN (KREDIT)
        IF v_nocoa <> @v_nocoa_temp THEN
        	SET v_total_retur = v_harga;
            
            INSERT INTO ctransaksid SET 
                        cdidu=@cuid,
                        cdurutan=@v_row,
                        cdnocoa=v_nocoa,
                        cdkredit=v_total_retur,
                        cduang=1,
                        cddvisi=v_divisi,
                        cdproyek=v_proyek;
        ELSE 
            SET @cdid = LAST_INSERT_ID();
            SET v_total_retur = v_harga;    

            UPDATE ctransaksid SET cdkredit = cdkredit + v_total_retur, cddvisi=v_divisi, cdproyek=v_proyek 
             WHERE cdid = @cdid;
        END IF;

        SET @v_row = @v_row+1;
        SET @v_nocoa_temp = v_nocoa;
        SET @temp_total = @temp_total+v_total_retur;
    END LOOP;    

	SET @v_selisih = @v_total - (@temp_total+@v_pajak+@v_pph22);
    
    IF @v_selisih>0 THEN
        -- JURNAL SELISIH (KREDIT)    
        INSERT INTO ctransaksid SET 
                    cdidu=@cuid,
                    cdurutan=@v_row,
                    cdnocoa=@v_coaselisih,
                    cdkredit=@v_selisih,
                    cduang=1,
                    cddvisi=v_divisi,
                    cdproyek=v_proyek;    
    END IF;

    IF @v_selisih<0 THEN
        -- JURNAL SELISIH (DEBIT)    
        INSERT INTO ctransaksid SET 
                    cdidu=@cuid,
                    cdurutan=@v_row,
                    cdnocoa=@v_coaselisih,
                    cddebit=ABS(@v_selisih),
                    cduang=1,
                    cddvisi=v_divisi,
                    cdproyek=v_proyek;    
    END IF;

  	COMMIT; 
  	CLOSE c_detil;
    
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_JURNAL_RETUR_PEMBELIAN_DEL` (`v_id` INT(11))  BEGIN
	DECLARE v_sts INT DEFAULT FALSE;
	DECLARE v_sumber VARCHAR(50);
    DECLARE v_nomor VARCHAR(50);
    
  	DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_sts = TRUE;
  	DECLARE EXIT HANDLER FOR SQLEXCEPTION, SQLWARNING
  	BEGIN
    	ROLLBACK;
    	RESIGNAL;
  	END;  

	SELECT ipusumber, ipunotransaksi INTO v_sumber,v_nomor
      FROM einvoicepenjualanu WHERE ipuid=v_id;

	SELECT cuid INTO @cuid FROM ctransaksiu WHERE cusumber=v_sumber AND cunotransaksi=v_nomor;

  	START TRANSACTION;
		DELETE FROM ctransaksid WHERE cdidu=@cuid;
        DELETE FROM ctransaksiu WHERE cuid=@cuid;
  	COMMIT;     
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_JURNAL_RETUR_PENJUALAN_ADD` (IN `v_id` INT(11), IN `v_id_bkg` INT(11))  BEGIN
	DECLARE v_sts INT DEFAULT FALSE;
  	DECLARE v_item INT(11);
    DECLARE v_coa_pendapatan INT(11);
    DECLARE v_harga DOUBLE;
    DECLARE v_diskon DOUBLE;
    DECLARE v_qty DOUBLE;
    DECLARE v_nocoa INT(11);
    DECLARE v_divisi INT(11);
    DECLARE v_proyek INT(11);
    DECLARE v_total_retur DOUBLE DEFAULT 0;
    
	DECLARE c_detil CURSOR FOR
     SELECT A.ipditem,A.ipdharga,A.ipddiskon,A.ipdmasuk,B.icoapendapatan,A.ipddivisi,A.ipdproyek   
       FROM einvoicepenjualand A 
 INNER JOIN bitem B ON A.ipditem=B.iid  
      WHERE A.ipdidipu = v_id ORDER BY B.icoapendapatan ASC;

  	DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_sts = TRUE;
  	DECLARE EXIT HANDLER FOR SQLEXCEPTION, SQLWARNING
  	BEGIN
    	ROLLBACK;
    	RESIGNAL;
  	END;  

  	OPEN c_detil;  
  	START TRANSACTION;
	
    SELECT cccoa INTO @v_coapajak FROM cconfigcoa WHERE cckode='SR' AND ccketerangan='PAJAKKELUARAN';
    SELECT cccoa INTO @v_coapiutang FROM cconfigcoa WHERE cckode='SR' AND ccketerangan='PIUTANGRETUR';
    SELECT cccoa INTO @v_coaretur FROM cconfigcoa WHERE cckode='SR' AND ccketerangan='RETURPENJUALAN';
    SELECT B.pcoaout INTO @v_coappn FROM ainfo A INNER JOIN bpajak B ON A.ippnjual=B.pid WHERE A.iid=1;
    SELECT B.pcoaout INTO @v_coapph22 FROM ainfo A INNER JOIN bpajak B ON A.ipph22jual=B.pid WHERE A.iid=1;    
	SELECT IPU.ipusumber,IPU.ipunotransaksi,IPU.iputanggal,
    	   IPU.ipukontak,IPU.ipuuraian,IPU.iputotaltransaksi,
           IPU.ipucreateu,IPU.iputotalpajak,IPU.ipujenispajak,IPU.iputotalpph22       
      INTO @v_sumber,@v_nomor,@v_tanggal,@v_kontak,@v_uraian,@v_total,@v_createu,@v_pajak,@v_jnspajak,@v_pph22   
      FROM einvoicepenjualanu IPU
     WHERE IPU.ipuid = v_id;         
    
    INSERT INTO ctransaksiu SET 
    	cusumber = @v_sumber,
        cunotransaksi = @v_nomor,
        cutanggal = @v_tanggal,
        cukontak = @v_kontak,
        cuuraian = @v_uraian,
        cutotaltrans = @v_total,
        cucreateu = @v_createu;
    
    SET @cuid = LAST_INSERT_ID();
    SET @v_row = 1;
    SET @v_nocoa_temp = '';

	detil_loop: LOOP
    FETCH c_detil INTO v_item, v_harga, v_diskon, v_qty, v_nocoa, v_divisi, v_proyek;
    IF v_sts THEN
      LEAVE detil_loop;
    END IF;
        -- JURNAL RETUR / PENDAPATAN (DEBIT)
        IF v_nocoa <> @v_nocoa_temp THEN
            IF @v_jnspajak=2 THEN 
                SET v_total_retur = ((v_harga-v_diskon)*v_qty) - (((100/110)*((v_harga-v_diskon)*v_qty))*(10/100));
            ELSE
                SET v_total_retur = (v_harga-v_diskon)*v_qty;    
            END IF;

            INSERT INTO ctransaksid SET 
                        cdidu=@cuid,
                        cdurutan=@v_row,
                        cdnocoa=v_nocoa,
                        cddebit=v_total_retur,
                        cduang=1,
                        cddvisi=v_divisi,
                        cdproyek=v_proyek;
        ELSE 
            SET @cdid = LAST_INSERT_ID();
            IF @v_jnspajak=2 THEN 
                SET v_total_retur = ((v_harga-v_diskon)*v_qty) - (((100/110)*((v_harga-v_diskon)*v_qty))*(10/100));
            ELSE
                SET v_total_retur = (v_harga-v_diskon)*v_qty;    
            END IF;

            UPDATE ctransaksid SET cddebit = cddebit + v_total_retur, cddvisi=v_divisi, cdproyek=v_proyek 
             WHERE cdid = @cdid;
        END IF;

        SET @v_row = @v_row+1;
        SET @v_nocoa_temp = v_nocoa;
    END LOOP;    

	-- JURNAL PAJAK KELUARAN (DEBIT)
	IF @v_pajak <> 0 THEN
    	SET @v_row = @v_row+1;
        INSERT INTO ctransaksid SET 
                    cdidu=@cuid,
                    cdurutan=@v_row,
                    cdnocoa=@v_coappn,
                    cddebit=@v_pajak,
                    cduang=1,
                    cddvisi=v_divisi,
                    cdproyek=v_proyek;    
    END IF;

	-- JURNAL PAJAK PPH22 (DEBIT)
	IF @v_pph22 <> 0 THEN
    	SET @v_row = @v_row+1;
        INSERT INTO ctransaksid SET 
                    cdidu=@cuid,
                    cdurutan=@v_row,
                    cdnocoa=@v_coapph22,
                    cddebit=@v_pph22,
                    cduang=1,
                    cddvisi=v_divisi,
                    cdproyek=v_proyek;    
    END IF;

	-- JURNAL PIUTANG RETUR (KREDIT)    
    SET @v_row = @v_row+1;    
	INSERT INTO ctransaksid SET 
                cdidu=@cuid,
                cdurutan=@v_row,
                cdnocoa=@v_coapiutang,
                cdkredit=@v_total,
                cduang=1,
                cddvisi=v_divisi,
                cdproyek=v_proyek;

CLOSE c_detil;

JURNAL_HPP: BEGIN
	DECLARE v_sts INT DEFAULT FALSE;
  	DECLARE v_item INT(11);
    DECLARE v_coa_persediaan INT(11);
    DECLARE v_coa_hpp INT(11);    
    DECLARE v_hpp DOUBLE;
    DECLARE v_divisi INT(11);
    DECLARE v_proyek INT(11);
    
	   DECLARE c_detil_hpp CURSOR FOR
        SELECT A.sditem,B.icoapersediaan,B.icoahpp,A.sddivisi,A.sdproyek, SUM(A.sdmasuk*A.sdharga) 'HPP'   
          FROM fstokd A 
    INNER JOIN bitem B ON A.sditem=B.iid 
         WHERE A.sdidsu = v_id_bkg GROUP BY A.sditem 
      ORDER BY B.icoahpp ASC;      

  	DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_sts = TRUE;

  	OPEN c_detil_hpp;  
    
    SET @v_coahpp_temp = '';
    SET @v_coapersediaan_temp = '';
    
  	hpp_loop: LOOP
    FETCH c_detil_hpp INTO v_item, v_coa_persediaan, v_coa_hpp, v_divisi, v_proyek, v_hpp;
    IF v_sts THEN
	  CLOSE c_detil_hpp;    
      LEAVE hpp_loop;
    END IF;
    
    -- JURNAL HPP (KREDIT)
    IF v_coa_hpp <> @v_coahpp_temp THEN    
        INSERT INTO ctransaksid SET 
                    cdidu=@cuid,
                    cdurutan=@v_row,
                    cdnocoa=v_coa_hpp,
                    cdkredit=v_hpp,
                    cduang=1,
                    cddvisi=v_divisi,
                    cdproyek=v_proyek;
	ELSE 
    	SET @cdid = LAST_INSERT_ID();
        UPDATE ctransaksid SET cdkredit = cdkredit + v_hpp, cddvisi=v_divisi, cdproyek=v_proyek 
         WHERE cdid = @cdid;
	END IF;

	SET @v_row = @v_row+1;
	SET @v_coahpp_temp = v_coa_hpp;
    END LOOP;    
	
	state2: BEGIN
	DECLARE v_sts INT DEFAULT FALSE;
  	DECLARE v_item INT(11);
    DECLARE v_coa_persediaan INT(11);
    DECLARE v_coa_hpp INT(11);    
    DECLARE v_hpp DOUBLE;
    DECLARE v_divisi INT(11);
    DECLARE v_proyek INT(11);
    
	   DECLARE c_detil_hpp CURSOR FOR
        SELECT A.sditem,B.icoapersediaan,B.icoahpp,A.sddivisi,A.sdproyek, SUM(A.sdmasuk*A.sdharga) 'HPP'   
          FROM fstokd A 
    INNER JOIN bitem B ON A.sditem=B.iid 
         WHERE A.sdidsu = v_id_bkg GROUP BY A.sditem 
      ORDER BY B.icoapersediaan ASC;      

  	DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_sts = TRUE;

  	OPEN c_detil_hpp;  
    SET @v_coapersediaan_temp = '';
    
  	stock_loop: LOOP
    FETCH c_detil_hpp INTO v_item, v_coa_persediaan, v_coa_hpp, v_divisi, v_proyek, v_hpp;
    IF v_sts THEN
	  CLOSE c_detil_hpp;    
      LEAVE stock_loop;
    END IF;
    
    -- JURNAL PERSEDIAAN (DEBIT)
    IF v_coa_persediaan <> @v_coapersediaan_temp THEN    
        INSERT INTO ctransaksid SET 
                    cdidu=@cuid,
                    cdurutan=@v_row,
                    cdnocoa=v_coa_persediaan,
                    cddebit=v_hpp,
                    cduang=1,
                    cddvisi=v_divisi,
                    cdproyek=v_proyek;
	ELSE 
    	SET @cdid = LAST_INSERT_ID();
        UPDATE ctransaksid SET cddebit = cddebit + v_hpp, cddvisi=v_divisi, cdproyek=v_proyek 
         WHERE cdid = @cdid;
	END IF;

	SET @v_row = @v_row+1;
	SET @v_coapersediaan_temp = v_coa_persediaan;
    END LOOP;

    END state2;
    
END JURNAL_HPP;

COMMIT;     
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_JURNAL_RETUR_PENJUALAN_DEL` (`v_id` INT(11))  BEGIN
	DECLARE v_sts INT DEFAULT FALSE;
	DECLARE v_sumber VARCHAR(50);
    DECLARE v_nomor VARCHAR(50);
    
  	DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_sts = TRUE;
  	DECLARE EXIT HANDLER FOR SQLEXCEPTION, SQLWARNING
  	BEGIN
    	ROLLBACK;
    	RESIGNAL;
  	END;  

	SELECT ipusumber, ipunotransaksi INTO v_sumber,v_nomor
      FROM einvoicepenjualanu WHERE ipuid=v_id;

	SELECT cuid INTO @cuid FROM ctransaksiu WHERE cusumber=v_sumber AND cunotransaksi=v_nomor;

  	START TRANSACTION;
		DELETE FROM ctransaksid WHERE cdidu=@cuid;
        DELETE FROM ctransaksiu WHERE cuid=@cuid;
  	COMMIT;     
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_JURNAL_SALDO_AWAL_AKTIVA_ADD` (`v_id` INT(11))  BEGIN
	DECLARE v_sts INT DEFAULT FALSE;
  	DECLARE v_item INT(11);
    DECLARE v_harga DOUBLE;
    DECLARE v_nocoa INT(11);
    DECLARE v_divisi INT(11);
    DECLARE v_proyek INT(11);
    DECLARE v_total DOUBLE DEFAULT 0;
    DECLARE v_akumulasi DOUBLE;
    DECLARE v_coa_akum INT(11);    
    
	DECLARE c_detil CURSOR FOR
     SELECT A.sditem,A.sdharga,B.acoaaktiva,A.sddivisi,A.sdproyek,A.sdakumulasi,B.acoadepresiasiakum    
       FROM fasetd A 
 INNER JOIN baktiva B ON A.sditem=B.aid  
      WHERE A.sdidsu = v_id ORDER BY A.sdurutan ASC;

  	DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_sts = TRUE;
  	DECLARE EXIT HANDLER FOR SQLEXCEPTION, SQLWARNING
  	BEGIN
    	ROLLBACK;
    	RESIGNAL;
  	END;  

  	OPEN c_detil;  
  	START TRANSACTION;
	
    SELECT cccoa INTO @v_coabalance FROM cconfigcoa WHERE cckode='RL' AND ccketerangan='BERJALAN';
    
	SELECT SU.susumber,SU.sunotransaksi,SU.sutanggal,
    	   SU.sukontak,SU.suuraian,SU.sutotaltransaksi,
           SU.sucreateu      
      INTO @v_sumber,@v_nomor,@v_tanggal,@v_kontak,@v_uraian,@v_total,@v_createu    
      FROM fasetu SU
     WHERE SU.suid = v_id;         
    
    INSERT INTO ctransaksiu SET 
    	cusumber = @v_sumber,
        cunotransaksi = @v_nomor,
        cutanggal = @v_tanggal,
        cukontak = @v_kontak,
        cuuraian = @v_uraian,
        cutotaltrans = @v_total,
        cucreateu = @v_createu;
    
    SET @cuid = LAST_INSERT_ID();
    SET @v_row = 1;
    
  	detil_loop: LOOP
    FETCH c_detil INTO v_item, v_harga, v_nocoa, v_divisi, v_proyek, v_akumulasi, v_coa_akum;
    IF v_sts THEN
      LEAVE detil_loop;
    END IF;

    -- JURNAL SALDO AWAL AKTIVA (DEBIT)
        INSERT INTO ctransaksid SET 
                    cdidu=@cuid,
                    cdurutan=@v_row,
                    cdnocoa=v_nocoa,
                    cddebit=v_harga,
                    cduang=1,
                    cddvisi=v_divisi,
                    cdproyek=v_proyek;

	SET @v_row = @v_row+1;
	-- JURNAL SALDO AWAL AKTIVA BALANCE (KREDIT)    
        INSERT INTO ctransaksid SET 
                    cdidu=@cuid,
                    cdurutan=@v_row,
                    cdnocoa=@v_coabalance,
                    cdkredit=v_harga,
                    cduang=1,
                    cddvisi=v_divisi,
                    cdproyek=v_proyek;

	IF v_akumulasi>0 THEN
	SET @v_row = @v_row+1;
	-- JURNAL SALDO AWAL AKUMULASI BALANCE (DEBIT)    
        INSERT INTO ctransaksid SET 
                    cdidu=@cuid,
                    cdurutan=@v_row,
                    cdnocoa=@v_coabalance,
                    cddebit=v_akumulasi,
                    cduang=1,
                    cddvisi=v_divisi,
                    cdproyek=v_proyek; 

	SET @v_row = @v_row+1;
	-- JURNAL SALDO AWAL AKUMULASI (KREDIT)    
        INSERT INTO ctransaksid SET 
                    cdidu=@cuid,
                    cdurutan=@v_row,
                    cdnocoa=v_coa_akum,
                    cdkredit=v_akumulasi,
                    cduang=1,
                    cddvisi=v_divisi,
                    cdproyek=v_proyek; 
                    
    END IF;

	SET @v_row = @v_row+1;

    END LOOP;    

  	COMMIT; 
  	CLOSE c_detil;
    
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_JURNAL_SALDO_AWAL_STOK_ADD` (IN `v_id` INT(11))  BEGIN
	DECLARE v_sts INT DEFAULT FALSE;
    DECLARE v_idsd INT(11);
  	DECLARE v_item INT(11);
    DECLARE v_coa_persediaan INT(11);
    DECLARE v_coa_selisih INT(11);
    DECLARE v_harga DOUBLE;
    DECLARE v_diskon DOUBLE;
    DECLARE v_qty DOUBLE;
    DECLARE v_divisi INT(11);
    DECLARE v_proyek INT(11);
    DECLARE v_total DOUBLE DEFAULT 0;    
    
	DECLARE c_detil CURSOR FOR
     SELECT A.sdid, A.sditem,A.sdharga,A.sddiskon,(A.sdmasuk-A.sdkeluar) AS 'sdqty',B.icoapersediaan,A.sddivisi,A.sdproyek   
       FROM fstokd A 
 INNER JOIN bitem B ON A.sditem=B.iid  
      WHERE A.sdidsu = v_id ORDER BY A.sdurutan ASC;

  	DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_sts = TRUE;
  	DECLARE EXIT HANDLER FOR SQLEXCEPTION, SQLWARNING
  	BEGIN
    	ROLLBACK;
    	RESIGNAL;
  	END;  

  	OPEN c_detil;  
  	START TRANSACTION;
    
	SELECT SU.susumber,SU.sunotransaksi,SU.sutanggal,
    	   SU.sukontak,SU.suuraian,SU.sutotaltransaksi,
           SU.sucreateu       
      INTO @v_sumber,@v_nomor,@v_tanggal,@v_kontak,@v_uraian,@v_total,@v_createu    
      FROM fstoku SU
     WHERE SU.suid = v_id;         

    SELECT cccoa INTO v_coa_selisih FROM cconfigcoa WHERE cckode='RL' AND ccketerangan='BERJALAN';
    
    INSERT INTO ctransaksiu SET 
    	cusumber = @v_sumber,
        cunotransaksi = @v_nomor,
        cutanggal = @v_tanggal,
        cukontak = @v_kontak,
        cuuraian = @v_uraian,
        cutotaltrans = @v_total,
        cucreateu = @v_createu;
    
    SET @cuid = LAST_INSERT_ID();
    SET @v_row = 1;
    
  	detil_loop: LOOP
    FETCH c_detil INTO v_idsd, v_item, v_harga, v_diskon, v_qty, v_coa_persediaan, v_divisi, v_proyek;
    IF v_sts THEN
      LEAVE detil_loop;
    END IF;

	SET v_total = (v_harga-v_diskon)*ABS(v_qty);    
    INSERT INTO ctransaksid SET cdidu=@cuid,
                    cdurutan=@v_row,
                    cdnocoa=v_coa_persediaan,
                    cddebit=v_total,
                    cduang=1,
                    cddvisi=v_divisi,
                    cdproyek=v_proyek;

	SET @v_row = @v_row+1;

	INSERT INTO ctransaksid SET cdidu=@cuid,
                    cdurutan=@v_row,
                    cdnocoa=v_coa_selisih,
                    cdkredit=v_total,
                    cduang=1,
                    cddvisi=v_divisi,
                    cdproyek=v_proyek;                        

	SET @v_row = @v_row+1;
    END LOOP;    

  	COMMIT; 
  	CLOSE c_detil;
    
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_JURNAL_SALDO_AWAL_TAGIHAN_ADD` (`v_id` INT(11))  BEGIN
	DECLARE v_sts INT DEFAULT FALSE;
    DECLARE v_coa_selisih INT(11);
    DECLARE v_nocoa INT(11);

  	DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_sts = TRUE;
  	DECLARE EXIT HANDLER FOR SQLEXCEPTION, SQLWARNING
  	BEGIN
    	ROLLBACK;
    	RESIGNAL;
  	END;  

  	START TRANSACTION;
    
	SELECT IPU.ipusumber,IPU.ipunotransaksi,IPU.iputanggal,
    	   IPU.ipukontak,IPU.ipuuraian,IPU.iputotaltransaksi,
           IPU.ipucreateu, K.ktipe       
      INTO @v_sumber,@v_nomor,@v_tanggal,@v_kontak,@v_uraian,@v_total,@v_createu,@v_kategori  
      FROM einvoicepenjualanu IPU LEFT JOIN bkontak K ON IPU.ipukontak=K.kid 
     WHERE IPU.ipuid = v_id;         

	IF @v_kategori=6 THEN 
	    -- SUPPLIER
		SELECT cccoa INTO v_nocoa FROM cconfigcoa WHERE cckode='PJ' AND ccketerangan='HUTANGDAGANG';
    ELSEIF @v_kategori=2 THEN
    	-- PELANGGAN
	    SELECT cccoa INTO v_nocoa FROM cconfigcoa WHERE cckode='IV' AND ccketerangan='PIUTANGDAGANG';
    END IF;
    
    -- BALANCE ACCOUNT
    SELECT cccoa INTO v_coa_selisih FROM cconfigcoa WHERE cckode='RL' AND ccketerangan='BERJALAN';

    INSERT INTO ctransaksiu SET 
    	cusumber = @v_sumber,
        cunotransaksi = @v_nomor,
        cutanggal = @v_tanggal,
        cukontak = @v_kontak,
        cuuraian = @v_uraian,
        cutotaltrans = @v_total,
        cucreateu = @v_createu;
    
    SET @cuid = LAST_INSERT_ID();
    SET @v_row = 0;
    
	IF @v_kategori=6 THEN 
	    -- SUPPLIER
	    SET @v_row = @v_row + 1;        
        INSERT INTO ctransaksid SET 
                    cdidu=@cuid,
                    cdurutan=@v_row,
                    cdnocoa=v_coa_selisih,
                    cddebit=@v_total,
                    cduang=1;
                    
	    SET @v_row = @v_row + 1;        
        INSERT INTO ctransaksid SET 
                    cdidu=@cuid,
                    cdurutan=@v_row,
                    cdnocoa=v_nocoa,
                    cdkredit=@v_total,
                    cduang=1;
                    
    ELSEIF @v_kategori=2 THEN
    	-- PELANGGAN
	    SET @v_row = @v_row + 1;        
        INSERT INTO ctransaksid SET 
                    cdidu=@cuid,
                    cdurutan=@v_row,
                    cdnocoa=v_nocoa,
                    cddebit=@v_total,
                    cduang=1;
                    
	    SET @v_row = @v_row + 1;        
        INSERT INTO ctransaksid SET 
                    cdidu=@cuid,
                    cdurutan=@v_row,
                    cdnocoa=v_coa_selisih,
                    cdkredit=@v_total,
                    cduang=1;
    END IF;    
                
  	COMMIT; 
    
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_JURNAL_TERIMA_BARANG_ADD` (`v_id` INT(11))  BEGIN
	DECLARE v_sts INT DEFAULT FALSE;
  	DECLARE v_item INT(11);
    DECLARE v_coa_persediaan INT(11);
    DECLARE v_harga DOUBLE;
    DECLARE v_diskon DOUBLE;
    DECLARE v_qty DOUBLE;
    DECLARE v_nocoa INT(11);
    DECLARE v_divisi INT(11);
    DECLARE v_proyek INT(11);
    DECLARE v_total_beli DOUBLE DEFAULT 0;
    
	DECLARE c_detil CURSOR FOR
     SELECT A.sditem,A.sdharga,A.sddiskon,A.sdmasuk,B.icoapersediaan,A.sddivisi,A.sdproyek   
       FROM fstokd A 
 INNER JOIN bitem B ON A.sditem=B.iid  
      WHERE A.sdidsu = v_id ORDER BY B.icoapersediaan ASC;

  	DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_sts = TRUE;
  	DECLARE EXIT HANDLER FOR SQLEXCEPTION, SQLWARNING
  	BEGIN
    	ROLLBACK;
    	RESIGNAL;
  	END;  

    SELECT cccoa INTO @v_coaselisih FROM cconfigcoa WHERE cckode='TB' AND ccketerangan='PERSEDIAANBELUMFAKTUR';

	IF @v_coaselisih > 0 THEN
        OPEN c_detil;  
        START TRANSACTION;

        SELECT SU.susumber,SU.sunotransaksi,SU.sutanggal,
               SU.sukontak,SU.suuraian,SU.sutotaltransaksi-SU.sutotalpajak,
               SU.sucreateu       
          INTO @v_sumber,@v_nomor,@v_tanggal,@v_kontak,@v_uraian,@v_total,@v_createu     
          FROM fstoku SU
         WHERE SU.suid = v_id;         

        INSERT INTO ctransaksiu SET 
            cusumber = @v_sumber,
            cunotransaksi = @v_nomor,
            cutanggal = @v_tanggal,
            cukontak = @v_kontak,
            cuuraian = @v_uraian,
            cutotaltrans = @v_total,
            cucreateu = @v_createu;

        SET @cuid = LAST_INSERT_ID();
        SET @v_row = 1;
        SET @v_nocoa_temp = '';

        detil_loop: LOOP
        FETCH c_detil INTO v_item, v_harga, v_diskon, v_qty, v_nocoa, v_divisi, v_proyek;
        IF v_sts THEN
          LEAVE detil_loop;
        END IF;

        -- JURNAL PERSEDIAAN (DEBIT)
        IF v_nocoa <> @v_nocoa_temp THEN
        	SET v_total_beli = (v_harga-v_diskon)*v_qty;    
            INSERT INTO ctransaksid SET 
                        cdidu=@cuid,
                        cdurutan=@v_row,
                        cdnocoa=v_nocoa,
                        cddebit=v_total_beli,
                        cduang=1,
                        cddvisi=v_divisi,
                        cdproyek=v_proyek;
        ELSE 
            SET @cdid = LAST_INSERT_ID();
            SET v_total_beli = (v_harga-v_diskon)*v_qty;    
            UPDATE ctransaksid SET cddebit = cddebit + v_total_beli, cddvisi=v_divisi, cdproyek=v_proyek 
             WHERE cdid = @cdid;
        END IF;

        SET @v_row = @v_row+1;
        SET @v_nocoa_temp = v_nocoa;
        END LOOP;    

        -- JURNAL PERANTARA PENERIMAAN PERSEDIAAN BELUM DITAGIH (KREDIT)    
        SET @v_row = @v_row+1;
        INSERT INTO ctransaksid SET 
                    cdidu=@cuid,
                    cdurutan=@v_row,
                    cdnocoa=@v_coaselisih,
                    cdkredit=@v_total,
                    cduang=1,
                    cddvisi=v_divisi,
                    cdproyek=v_proyek;

        COMMIT; 
        CLOSE c_detil;
	END IF;    
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_JURNAL_TERIMA_BARANG_DEL` (`v_id` INT(11))  BEGIN
	DECLARE v_sts INT DEFAULT FALSE;
	DECLARE v_sumber VARCHAR(50);
    DECLARE v_nomor VARCHAR(50);
    
  	DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_sts = TRUE;
  	DECLARE EXIT HANDLER FOR SQLEXCEPTION, SQLWARNING
  	BEGIN
    	ROLLBACK;
    	RESIGNAL;
  	END;  

	SELECT susumber, sunotransaksi INTO v_sumber,v_nomor
      FROM fstoku WHERE suid=v_id;

	SELECT cuid INTO @cuid FROM ctransaksiu WHERE cusumber=v_sumber AND cunotransaksi=v_nomor;

  	START TRANSACTION;
		DELETE FROM ctransaksid WHERE cdidu=@cuid;
        DELETE FROM ctransaksiu WHERE cuid=@cuid;
  	COMMIT;     
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_JURNAL_UM_PEMBELIAN_ADD` (IN `v_id` INT(11))  BEGIN
	DECLARE v_sts INT DEFAULT FALSE;
	DECLARE v_uangmuka DOUBLE DEFAULT 0;
    
  	DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_sts = TRUE;
  	DECLARE EXIT HANDLER FOR SQLEXCEPTION, SQLWARNING
  	BEGIN
    	ROLLBACK;
    	RESIGNAL;
  	END;  

  	START TRANSACTION;
	
    SELECT cccoa INTO @v_coapajak FROM cconfigcoa WHERE cckode='DPV' AND ccketerangan='PAJAKMASUKAN';
    SELECT cccoa INTO @v_coahutang FROM cconfigcoa WHERE cckode='DPV' AND ccketerangan='HUTANGUANGMUKA';
    
	SELECT dpsumber,dpnotransaksi,dptanggal,
    	   dpkontak,dpketerangan,dpjumlah,
           dpcreateu,dppajakn,dppajak,dpcdp,dpckas,dptipe       
      INTO @v_sumber,@v_nomor,@v_tanggal,@v_kontak,@v_uraian,@v_total,@v_createu,@v_pajak,@v_jnspajak,@v_coadp,@v_coakas,@v_tipedp    
      FROM ddp
     WHERE dpid = v_id;         
    
    INSERT INTO ctransaksiu SET 
    	cusumber = @v_sumber,
        cunotransaksi = @v_nomor,
        cutanggal = @v_tanggal,
        cukontak = @v_kontak,
        cuuraian = @v_uraian,
        cutotaltrans = @v_total,
        cucreateu = @v_createu;
    
    SET @cuid = LAST_INSERT_ID();
    SET @v_row = 1;
  
    
    -- JURNAL UANG MUKA (DEBIT)
	IF @v_jnspajak IN (1,2) THEN 
    	SET v_uangmuka = @v_total-@v_pajak;
    ELSE
    	SET v_uangmuka = @v_total;    
    END IF;
    
    INSERT INTO ctransaksid SET 
                cdidu=@cuid,
                cdurutan=@v_row,
                cdnocoa=@v_coadp,
                cddebit=v_uangmuka,
                cduang=1;

	-- JURNAL PAJAK MASUKAN (DEBIT)
	IF @v_pajak <> 0 THEN
    	SET @v_row = @v_row+1;
        INSERT INTO ctransaksid SET 
                    cdidu=@cuid,
                    cdurutan=@v_row,
                    cdnocoa=@v_coapajak,
                    cddebit=@v_pajak,
                    cduang=1;
    END IF;

	IF @v_tipedp = 0 THEN 
        -- JURNAL HUTANG UANG MUKA (KREDIT)    
        SET @v_row = @v_row+1;
        INSERT INTO ctransaksid SET 
                    cdidu=@cuid,
                    cdurutan=@v_row,
                    cdnocoa=@v_coahutang,
                    cdkredit=@v_total,
                    cduang=1;
	ELSE 
        -- JURNAL KAS BANK (KREDIT)    
        SET @v_row = @v_row+1;
        INSERT INTO ctransaksid SET 
                    cdidu=@cuid,
                    cdurutan=@v_row,
                    cdnocoa=@v_coakas,
                    cdkredit=@v_total,
                    cduang=1;    
    END IF;
  	COMMIT; 
    
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_JURNAL_UM_PEMBELIAN_DEL` (IN `v_id` INT(11), IN `v_flag` INT(11))  BEGIN
	DECLARE v_sts INT DEFAULT FALSE;
    DECLARE v_dpid INT(11);
	DECLARE v_sumber VARCHAR(50);
    DECLARE v_nomor VARCHAR(50);
    
  	DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_sts = TRUE;
  	DECLARE EXIT HANDLER FOR SQLEXCEPTION, SQLWARNING
  	BEGIN
    	ROLLBACK;
    	RESIGNAL;
  	END;  

	SELECT dpsumber, dpnotransaksi INTO v_sumber, v_nomor 
      FROM ddp WHERE dpid = v_id;

	SELECT cuid INTO @cuid FROM ctransaksiu WHERE cusumber=v_sumber AND cunotransaksi=v_nomor;

  	START TRANSACTION;
		DELETE FROM ctransaksid WHERE cdidu=@cuid;
        DELETE FROM ctransaksiu WHERE cuid=@cuid;
        
        IF v_flag=1 THEN
	       	DELETE FROM ddp WHERE dpid=v_id;
    	END IF;  
        
  	COMMIT;     
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_JURNAL_UM_PENJUALAN_ADD` (`v_id` INT(11))  BEGIN
	DECLARE v_sts INT DEFAULT FALSE;
	DECLARE v_uangmuka DOUBLE DEFAULT 0;
    
  	DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_sts = TRUE;
  	DECLARE EXIT HANDLER FOR SQLEXCEPTION, SQLWARNING
  	BEGIN
    	ROLLBACK;
    	RESIGNAL;
  	END;  

  	START TRANSACTION;
	
    SELECT cccoa INTO @v_coapajak FROM cconfigcoa WHERE cckode='DPC' AND ccketerangan='PAJAKKELUARAN';
    SELECT cccoa INTO @v_coahutang FROM cconfigcoa WHERE cckode='DPC' AND ccketerangan='PIUTANGUANGMUKA';
    
	SELECT dpsumber,dpnotransaksi,dptanggal,
    	   dpkontak,dpketerangan,dpjumlah,
           dpcreateu,dppajakn,dppajak,dpcdp,dpckas,dptipe       
      INTO @v_sumber,@v_nomor,@v_tanggal,@v_kontak,@v_uraian,@v_total,@v_createu,@v_pajak,@v_jnspajak,@v_coadp,@v_coakas,@v_tipedp    
      FROM ddp
     WHERE dpid = v_id;         
    
    INSERT INTO ctransaksiu SET 
    	cusumber = @v_sumber,
        cunotransaksi = @v_nomor,
        cutanggal = @v_tanggal,
        cukontak = @v_kontak,
        cuuraian = @v_uraian,
        cutotaltrans = @v_total,
        cucreateu = @v_createu;
    
    SET @cuid = LAST_INSERT_ID();
    SET @v_row = 0;

	IF @v_tipedp = 0 THEN 
        -- JURNAL PIUTANG UANG MUKA (DEBIT)    
        SET @v_row = @v_row+1;
        INSERT INTO ctransaksid SET 
                    cdidu=@cuid,
                    cdurutan=@v_row,
                    cdnocoa=@v_coahutang,
                    cddebit=@v_total,
                    cduang=1;
	ELSE 
        -- JURNAL KAS BANK (DEBIT)    
        SET @v_row = @v_row+1;
        INSERT INTO ctransaksid SET 
                    cdidu=@cuid,
                    cdurutan=@v_row,
                    cdnocoa=@v_coakas,
                    cddebit=@v_total,
                    cduang=1;    
    END IF;

    -- JURNAL UANG MUKA (KREDIT)
	IF @v_jnspajak IN (1,2) THEN 
    	SET v_uangmuka = @v_total-@v_pajak;
    ELSE
    	SET v_uangmuka = @v_total;    
    END IF;
    
    INSERT INTO ctransaksid SET 
                cdidu=@cuid,
                cdurutan=@v_row,
                cdnocoa=@v_coadp,
                cdkredit=v_uangmuka,
                cduang=1;

	-- JURNAL PAJAK MASUKAN (DEBIT)
	IF @v_pajak <> 0 THEN
    	SET @v_row = @v_row+1;
        INSERT INTO ctransaksid SET 
                    cdidu=@cuid,
                    cdurutan=@v_row,
                    cdnocoa=@v_coapajak,
                    cdkredit=@v_pajak,
                    cduang=1;
    END IF;

  	COMMIT; 
    
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_JURNAL_UM_PENJUALAN_DEL` (`v_id` INT(11))  BEGIN
	DECLARE v_sts INT DEFAULT FALSE;
	DECLARE v_sumber VARCHAR(50);
    DECLARE v_nomor VARCHAR(50);
    
  	DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_sts = TRUE;
  	DECLARE EXIT HANDLER FOR SQLEXCEPTION, SQLWARNING
  	BEGIN
    	ROLLBACK;
    	RESIGNAL;
  	END;  

	SELECT dpsumber, dpnotransaksi INTO v_sumber, v_nomor 
      FROM ddp WHERE dpid = v_id;

	SELECT cuid INTO @cuid FROM ctransaksiu WHERE cusumber=v_sumber AND cunotransaksi=v_nomor;

  	START TRANSACTION;
		DELETE FROM ctransaksid WHERE cdidu=@cuid;
        DELETE FROM ctransaksiu WHERE cuid=@cuid;
    COMMIT;     
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_RECALC_HPP` ()  BEGIN
DECLARE v_sts INT DEFAULT FALSE;
DECLARE v_suid INT(11);

	DECLARE cur_first CURSOR FOR
	SELECT suid
      FROM fstoku ORDER BY sutanggal ASC, suid ASC;

	DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_sts = TRUE;

	OPEN cur_first;
  	detil_loop: LOOP
    FETCH cur_first INTO v_suid;
    IF v_sts THEN
		CLOSE cur_first;    
    	LEAVE detil_loop;
    END IF;
    	CALL SP_HITUNG_HPP_DEL(v_suid); 
    	CALL SP_HITUNG_HPP_ADD(v_suid);
    END LOOP;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_REJURNAL_INVOICE_PENJUALAN` (`v_sumber` VARCHAR(5))  BEGIN
DECLARE v_sts INT DEFAULT FALSE;
DECLARE v_ipuid INT(11);

	DECLARE cur_first CURSOR FOR
	SELECT ipuid
      FROM einvoicepenjualanu 
     WHERE ipusumber=v_sumber 
  ORDER BY iputanggal ASC, ipuid ASC;

	DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_sts = TRUE;

	OPEN cur_first;
  	detil_loop: LOOP
    FETCH cur_first INTO v_ipuid;
    IF v_sts THEN
		CLOSE cur_first;    
    	LEAVE detil_loop;
    END IF;
    	CALL SP_JURNAL_INVOICE_PENJUALAN_DEL(v_ipuid); 
    	CALL SP_JURNAL_INVOICE_PENJUALAN_ADD(v_ipuid);
    END LOOP;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_REJURNAL_PENJUALAN_TUNAI` (`v_sumber` VARCHAR(5))  BEGIN
DECLARE v_sts INT DEFAULT FALSE;
DECLARE v_suid INT(11);

   DECLARE cur_first CURSOR FOR
	SELECT suid 
      FROM fstoku 
     WHERE susumber=v_sumber 
  ORDER BY sutanggal ASC, suid ASC;

	DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_sts = TRUE;

	OPEN cur_first;
  	detil_loop: LOOP
    FETCH cur_first INTO v_suid;
    IF v_sts THEN
		CLOSE cur_first;    
    	LEAVE detil_loop;
    END IF;
    	CALL SP_JURNAL_HPP_OUT_DEL(v_suid); 
    	CALL SP_JURNAL_PENJUALAN_TUNAI_ADD(v_suid);
    END LOOP;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_REJURNAL_RETUR_PENJUALAN` (`v_sumber` VARCHAR(5))  BEGIN
DECLARE v_sts INT DEFAULT FALSE;
DECLARE v_ipuid INT(11);
DECLARE v_ipubkg INT(11);

   DECLARE cur_first CURSOR FOR
	SELECT ipuid,ipunobkg 
      FROM einvoicepenjualanu 
     WHERE ipusumber=v_sumber 
  ORDER BY iputanggal ASC, ipuid ASC;

	DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_sts = TRUE;

	OPEN cur_first;
  	detil_loop: LOOP
    FETCH cur_first INTO v_ipuid,v_ipubkg;
    IF v_sts THEN
		CLOSE cur_first;    
    	LEAVE detil_loop;
    END IF;
    	CALL SP_JURNAL_RETUR_PENJUALAN_DEL(v_ipuid); 
    	CALL SP_JURNAL_RETUR_PENJUALAN_ADD(v_ipuid,v_ipubkg);
    END LOOP;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_TABEL_LEDGER` (`v_coa` INT(11))  BEGIN
    SELECT A.cuid 'id',A.cunotransaksi 'nomor', 
            DATE_FORMAT(A.cutanggal,'%d-%m-%Y') 'tanggal',C.knama 'kontak', A.cuuraian 'uraian', 			ROUND(B.cddebit,2) 'debit', ROUND(B.cdkredit,2) 'kredit', ROUND('0',2) 'saldo', 
            D.cnocoa 'idcoa',D.cnama 'coa' 
     FROM ctransaksiu A
    INNER JOIN ctransaksid B ON A.cuid=B.cdidu   
     LEFT JOIN bkontak C ON A.cukontak=C.kid
     LEFT JOIN bcoa D ON B.cdnocoa=D.cid
    WHERE B.cdnocoa=v_coa ORDER BY A.cutanggal ASC, B.cdnocoa ASC; 

END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `ainfo`
--

CREATE TABLE `ainfo` (
  `iid` int(11) NOT NULL,
  `inama` varchar(100) DEFAULT NULL,
  `ialamat1` varchar(255) DEFAULT NULL,
  `ialamat2` varchar(255) DEFAULT NULL,
  `ikota` varchar(50) DEFAULT NULL,
  `ipropinsi` varchar(50) DEFAULT NULL,
  `ikodepos` varchar(15) DEFAULT NULL,
  `inegara` varchar(25) DEFAULT NULL,
  `itelepon1` varchar(25) DEFAULT NULL,
  `itelepon2` varchar(25) DEFAULT NULL,
  `ifaks` varchar(25) DEFAULT NULL,
  `iemail` varchar(50) DEFAULT NULL,
  `ibulanaktif` int(11) DEFAULT NULL,
  `itahunaktif` int(11) DEFAULT NULL,
  `iuang` int(11) NOT NULL DEFAULT '1',
  `icetakpos` int(11) NOT NULL DEFAULT '0',
  `ikontakpos` int(11) DEFAULT NULL,
  `ibarcodepos` int(11) NOT NULL DEFAULT '0',
  `ipajakpos` int(11) NOT NULL DEFAULT '1',
  `ippnpos` int(11) DEFAULT NULL,
  `ipajakbeli` int(11) NOT NULL DEFAULT '1',
  `ippnbeli` int(11) DEFAULT NULL,
  `ipph22beli` int(11) DEFAULT NULL,
  `ipajakjual` int(11) NOT NULL DEFAULT '1',
  `ippnjual` int(11) DEFAULT NULL,
  `ipph22jual` int(11) DEFAULT NULL,
  `idivisi` tinyint(4) NOT NULL DEFAULT '0',
  `iproyek` tinyint(4) NOT NULL DEFAULT '0',
  `isatuan` tinyint(4) NOT NULL DEFAULT '0',
  `imatauang` tinyint(4) NOT NULL DEFAULT '0',
  `idecimalqty` int(11) NOT NULL DEFAULT '2',
  `ilogo` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ainfo`
--

INSERT INTO `ainfo` (`iid`, `inama`, `ialamat1`, `ialamat2`, `ikota`, `ipropinsi`, `ikodepos`, `inegara`, `itelepon1`, `itelepon2`, `ifaks`, `iemail`, `ibulanaktif`, `itahunaktif`, `iuang`, `icetakpos`, `ikontakpos`, `ibarcodepos`, `ipajakpos`, `ippnpos`, `ipajakbeli`, `ippnbeli`, `ipph22beli`, `ipajakjual`, `ippnjual`, `ipph22jual`, `idivisi`, `iproyek`, `isatuan`, `imatauang`, `idecimalqty`, `ilogo`) VALUES
(1, 'PT Contoh Demo', 'Jl Perintis Kemerdekaan No.45', '-', 'Tangerang Selatan', 'Banten', '11111', 'Indonesia', '021-77777777', '-', '-', 'demo@gmail.com', 9, 8, 1, 1, 3, 1, 1, 4, 1, 4, 0, 1, 4, 0, 1, 1, 0, 0, 2, 'environmental-benefits-environment-icon-red-png-image-with-environment-icon-png-840_859.png');

-- --------------------------------------------------------

--
-- Table structure for table `amenu`
--

CREATE TABLE `amenu` (
  `MID` int(11) NOT NULL,
  `MNAMA` varchar(50) DEFAULT NULL,
  `MDESCRIPTION` varchar(100) DEFAULT NULL,
  `MURUTAN` smallint(6) DEFAULT '0',
  `MPARENT` smallint(6) DEFAULT '0',
  `MTYPE` smallint(6) DEFAULT '0',
  `MACTIVE` smallint(6) DEFAULT '0',
  `MSHORTNAME` varchar(25) DEFAULT NULL,
  `MLINK` varchar(25) DEFAULT NULL,
  `MVIEW` varchar(255) DEFAULT NULL,
  `MCAPTION1` varchar(100) DEFAULT NULL,
  `MCAPTION2` varchar(100) DEFAULT NULL,
  `MICON` varchar(200) DEFAULT NULL,
  `MREPORT` int(11) DEFAULT NULL,
  `MCATATAN` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `amenu`
--

INSERT INTO `amenu` (`MID`, `MNAMA`, `MDESCRIPTION`, `MURUTAN`, `MPARENT`, `MTYPE`, `MACTIVE`, `MSHORTNAME`, `MLINK`, `MVIEW`, `MCAPTION1`, `MCAPTION2`, `MICON`, `MREPORT`, `MCATATAN`) VALUES
(1, 'Fina', NULL, 1, 0, 2, 1, NULL, NULL, NULL, 'Fina', NULL, 'fas fa-money-check', NULL, NULL),
(2, 'Pembelian', '', 10, 0, 2, 1, NULL, '', NULL, 'Pembelian', NULL, 'fas fa-shopping-basket', NULL, NULL),
(3, 'Penjualan', '', 20, 0, 2, 1, NULL, '', NULL, 'Penjualan', NULL, 'fas fa-shopping-cart', NULL, NULL),
(4, 'Inventori', '', 30, 0, 2, 1, NULL, '', NULL, 'Inventori', NULL, 'fas fa-cube', NULL, NULL),
(20, 'Bukti Kas Masuk', '', 2, 1, 2, 1, NULL, 'page/bkm', NULL, 'Bukti Kas Masuk', NULL, '', 11, NULL),
(21, 'Bukti Kas Keluar', '', 3, 1, 2, 1, NULL, 'page/bkk', NULL, 'Bukti Kas Keluar', NULL, '', 12, NULL),
(22, 'Bukti Bank Masuk', '', 4, 1, 2, 1, NULL, 'page/bbm', NULL, 'Bukti Bank Masuk', NULL, '', 13, NULL),
(23, 'Bukti Bank Keluar', '', 5, 1, 2, 1, NULL, 'page/bbk', NULL, 'Bukti Bank Keluar', NULL, '', 14, NULL),
(24, 'Jurnal Umum', '', 6, 1, 2, 1, NULL, 'page/ju', NULL, 'Jurnal Umum', NULL, '', 15, NULL),
(100, 'Data Akun', '', 40, 0, 3, 1, NULL, 'page/coa', NULL, 'Data Akun', NULL, 'fas fa-calendar-alt', NULL, NULL),
(199, 'Setup Program', '', 61, 0, 4, 1, NULL, 'page/settings', NULL, 'Setup Program', NULL, 'fas fa-cog', NULL, NULL),
(200, 'Administrasi User', '', 62, 0, 4, 1, NULL, 'page/au', NULL, 'Administrasi User', NULL, 'fas fa-users', NULL, NULL),
(201, 'Administrasi Menu', '', 63, 0, 4, 1, NULL, 'page/am', NULL, 'Administrasi Menu', NULL, 'fas fa-th-large', NULL, NULL),
(557, 'Laporan Keuangan', '', 70, 0, 1, 1, NULL, '', NULL, 'Keuangan', NULL, '', NULL, NULL),
(558, 'Laporan Pembelian', '', 90, 0, 1, 1, NULL, '', NULL, 'Pembelian', NULL, '', NULL, NULL),
(559, 'Laporan Penjualan', '', 110, 0, 1, 1, NULL, '', NULL, 'Penjualan', NULL, '', NULL, NULL),
(560, 'Laporan Persediaan', '', 130, 0, 1, 1, NULL, '', NULL, 'Persediaan', NULL, '', NULL, NULL),
(561, 'Laporan Aktiva Tetap', '', 150, 0, 1, 1, NULL, '', NULL, 'Aktiva Tetap', NULL, '', NULL, NULL),
(562, 'Print Form Transaksi', '', 170, 0, 1, 1, NULL, '', NULL, 'Form Transaksi', NULL, '', 0, ''),
(563, 'Laporan Neraca Keuangan', 'Menampilkan neraca keuangan sesuai periode yang dipilih', 71, 557, 1, 1, NULL, '', NULL, 'Neraca Keuangan', NULL, '', 102, NULL),
(564, 'Laporan Laba Rugi', 'Menampilkan laporan laba rugi sesuai dengan periode yang dipilih', 72, 557, 1, 1, NULL, '', NULL, 'Laba Rugi', NULL, '', 103, NULL),
(565, 'Laporan Hutang', 'Menampilkan daftar hutang ke supplier sesuai dengan periode yang dipilih', 91, 558, 1, 1, NULL, '', NULL, 'Daftar Hutang Usaha', NULL, '', 124, ''),
(566, 'Laporan Piutang', 'Menampilkan laporan piutang pelanggan sesuai dengan periode yang dipilih', 111, 559, 1, 1, NULL, '', NULL, 'Daftar Piutang Usaha', NULL, '', 125, ''),
(570, 'Aktiva Tetap', '', 8, 1, 2, 1, NULL, 'page/fa', NULL, 'Aktiva Tetap', NULL, '', 0, ''),
(571, 'Order Pembelian', '', 11, 2, 2, 1, NULL, 'page/opb', NULL, 'Order Pembelian', NULL, '', 16, NULL),
(572, 'Penerimaan Barang', '', 13, 2, 2, 0, NULL, 'page/pbbnondp', NULL, 'Penerimaan Barang', NULL, '', 17, ''),
(573, 'Faktur Pembelian', '', 14, 2, 2, 0, NULL, 'page/fpb', NULL, 'Faktur Pembelian', NULL, '', 18, ''),
(574, 'Pengembalian Barang', '', 15, 2, 2, 0, NULL, 'page/bkr', NULL, 'Pengembalian Barang', NULL, '', 19, ''),
(575, 'Retur Pembelian', '', 16, 2, 2, 1, NULL, 'page/rpb', NULL, 'Retur Pembelian', NULL, '', 20, NULL),
(576, 'Pembayaran Pembelian', '', 17, 2, 2, 1, NULL, 'page/phb', NULL, 'Pembayaran Hutang', NULL, '', 21, ''),
(577, 'Order Penjualan', '', 21, 3, 2, 1, NULL, 'page/opj', NULL, 'Order Penjualan', NULL, '', 22, NULL),
(578, 'Pengiriman Barang', '', 23, 3, 2, 0, NULL, 'page/sj', NULL, 'Pengiriman Barang', NULL, '', 23, ''),
(579, 'Faktur Penjualan', '', 24, 3, 2, 0, NULL, 'page/fpj', NULL, 'Faktur Penjualan', NULL, '', 24, ''),
(580, 'Terima Barang Retur', '', 25, 3, 2, 0, NULL, 'page/btr', NULL, 'Terima Barang Retur', NULL, '', 25, ''),
(581, 'Retur Penjualan', '', 26, 3, 2, 1, NULL, 'page/rpj', NULL, 'Retur Penjualan', NULL, '', 26, NULL),
(582, 'Pembayaran Penjualan', '', 27, 3, 2, 1, NULL, 'page/ppj', NULL, 'Pembayaran Piutang', NULL, '', 27, ''),
(583, 'Mutasi Barang', '', 31, 4, 2, 1, NULL, 'page/mutasi', NULL, 'Mutasi Barang', NULL, '', 28, NULL),
(584, 'Stok Opname', '', 32, 4, 2, 1, NULL, 'page/opname', NULL, 'Stok Opname', NULL, '', 29, NULL),
(585, 'Penyesuaian Persediaan', '', 33, 4, 2, 1, NULL, 'page/stokadj', NULL, 'Penyesuaian Persediaan', NULL, '', 30, NULL),
(586, 'Data Item', '', 41, 0, 3, 1, NULL, 'page/item', NULL, 'Data Item', NULL, 'fas fa-cubes', NULL, NULL),
(587, 'Data Kontak', '', 42, 0, 3, 1, NULL, 'page/kontak', NULL, 'Data Kontak', NULL, 'far fa-envelope', NULL, NULL),
(588, 'Data Lainnya', '', 45, 0, 3, 1, NULL, '', NULL, 'Data Lainnya', NULL, 'far fa-plus-square', NULL, NULL),
(589, 'Data Bank', '', 46, 588, 3, 1, NULL, 'page/bank', NULL, 'Data Bank', NULL, '', NULL, NULL),
(590, 'Mata Uang', '', 47, 588, 3, 1, NULL, 'page/matauang', NULL, 'Mata Uang', NULL, '', NULL, NULL),
(591, 'Termin Tagihan', '', 48, 588, 3, 1, NULL, 'page/termin', NULL, 'Termin Tagihan', NULL, '', NULL, NULL),
(592, 'Pajak', '', 49, 588, 3, 1, NULL, 'page/pajak', NULL, 'Pajak', NULL, '', NULL, NULL),
(593, 'Kelompok Aktiva Tetap', '', 50, 588, 3, 1, NULL, 'page/fatipe', NULL, 'Kelompok Aktiva Tetap', NULL, '', NULL, NULL),
(594, 'Aktiva Tetap', '', 51, 588, 3, 1, NULL, 'page/fadata', NULL, 'Aktiva Tetap', NULL, '', NULL, NULL),
(595, 'Data Proyek', '', 52, 588, 3, 1, NULL, 'page/proyek', NULL, 'Data Proyek', NULL, '', NULL, NULL),
(596, 'Data Gudang', '', 53, 588, 3, 1, NULL, 'page/gudang', NULL, 'Data Gudang', NULL, '', NULL, NULL),
(597, 'Data Satuan Item', '', 54, 588, 3, 1, NULL, 'page/satuan', NULL, 'Satuan Item', NULL, '', NULL, NULL),
(598, 'Kategori Kontak', '', 55, 588, 3, 1, NULL, 'page/tipekontak', NULL, 'Kategori Kontak', NULL, '', NULL, NULL),
(599, 'Data Divisi', '', 56, 588, 3, 1, NULL, 'page/divisi', NULL, 'Data Divisi', NULL, '', NULL, NULL),
(600, 'Administrasi Laporan', '', 64, 0, 4, 1, NULL, 'page/ar', NULL, 'Administrasi Laporan', NULL, 'fas fa-file', NULL, NULL),
(601, 'User Log', '', 65, 0, 4, 1, NULL, 'page/al', NULL, 'User Log', NULL, 'fas fa-user', 0, ''),
(602, 'Laporan Buku Besar', 'Menampilkan laporan laba buku besar akun sesuai dengan periode yang dipilih', 73, 557, 1, 1, NULL, '', NULL, 'Buku Besar', NULL, '', 104, NULL),
(603, 'Laporan Transaksi Jurnal', 'Menampilkan laporan transaksi jurnal sesuai dengan periode yang dipilih', 74, 557, 1, 1, NULL, '', NULL, 'Laporan Jurnal', NULL, '', 110, NULL),
(604, 'Laporan Penyusutan Aktiva', 'Menampilkan daftar penyusutan aktiva tetap dalam periode yang dipilih', 76, 561, 1, 0, NULL, '', NULL, 'Penyusutan Aktiva', NULL, '', 0, ''),
(605, 'Laporan Neraca Saldo', 'Menampilkan laporan neraca lajur sesuai dengan periode yang dipilih', 75, 557, 1, 0, NULL, '', NULL, 'Neraca Saldo', NULL, '', 0, ''),
(606, 'Laporan Arus Kas', 'Menampilkan laporan arus kas perusahaan sesuai periode yang dipilih', 77, 557, 1, 0, NULL, '', NULL, 'Arus Kas', NULL, '', 0, ''),
(607, 'Laporan Jurnal Umum', 'Menampilkan laporan dari transaksi jurnal umum sesuai dengan periode yang dipilih', 78, 557, 1, 1, NULL, '', NULL, 'Daftar Jurnal Umum', NULL, '', 109, ''),
(608, 'Laporan Kas Masuk', 'Menampilkan laporan transaksi kas masuk sesuai dengan periode yang dipilih', 79, 557, 1, 1, NULL, '', NULL, 'Daftar Kas Masuk', NULL, '', 105, ''),
(609, 'Laporan Kas Keluar', 'Menampilkan laporan transaksi kas keluar sesuai dengan periode yang dipilih', 80, 557, 1, 1, NULL, '', NULL, 'Daftar Kas Keluar', NULL, '', 106, ''),
(610, 'Laporan Bank Masuk', 'Menampilkan laporan transaksi bank masuk sesuai dengan periode yang dipilih', 81, 557, 1, 1, NULL, '', NULL, 'Daftar Bank Masuk', NULL, '', 107, ''),
(611, 'Laporan Bank Keluar', 'Menampilkan laporan transaksi bank keluar sesuai dengan periode yang dipilih', 82, 557, 1, 1, NULL, '', NULL, 'Daftar Bank Keluar', NULL, '', 108, ''),
(612, 'Laporan Kartu Stok', 'Menampilkan kartu stok sesuai dengan periode yang dipilih', 131, 560, 1, 1, NULL, '', NULL, 'Kartu Stok', NULL, '', 111, NULL),
(613, 'Laporan Pembelian Aktiva', 'Menampilkan Transaksi Pembelian Aktiva Tetap Sesuai Periode Yang Dipilih', 151, 561, 1, 0, NULL, '', NULL, 'Pembelian Aktiva', NULL, '', 0, ''),
(614, 'Laporan Kartu Persediaan', 'Menampilkan kartu nilai persediaan sesuai dengan periode yang dipilih ', 132, 560, 1, 1, NULL, '', NULL, 'Kartu Persediaan', NULL, '', 112, NULL),
(615, 'Jenis Penyesuaian Persediaan', '', 57, 588, 3, 1, NULL, 'page/jenispenyesuaian', NULL, 'Jenis Penyesuaian', NULL, '', 0, NULL),
(616, 'Histori Buku Besar', '', 7, 1, 2, 1, NULL, 'page/gl', NULL, 'Histori Buku Besar', NULL, '', 0, ''),
(617, 'Laporan Mutasi Barang', 'Menampilkan laporan pemindahan barang antar gudang sesuai dengan periode yang dipilih ', 133, 560, 1, 1, NULL, '', NULL, 'Daftar Mutasi Stok', NULL, '', 129, ''),
(618, 'Uang Muka Pembelian', '', 12, 2, 2, 1, NULL, 'page/umb', NULL, 'Uang Muka Pembelian', NULL, '', 113, NULL),
(619, 'Uang Muka Penjualan', '', 22, 3, 2, 1, NULL, 'page/umj', NULL, 'Uang Muka Penjualan', NULL, '', 114, NULL),
(620, 'Penjualan Tunai', '', 28, 3, 2, 1, NULL, 'page/pos', NULL, 'Penjualan Tunai', NULL, '', 115, ''),
(621, 'Faktur Pembelian Alur 2', '', 14, 2, 2, 1, NULL, 'page/fpbn', NULL, 'Invoice Pembelian', NULL, '', 18, 'Menu ini digunakan untuk alur tanpa penerimaan barang'),
(622, 'Faktur Penjualan Alur 2', '', 24, 3, 2, 1, NULL, 'page/fpjn', NULL, 'Invoice Penjualan', NULL, '', 24, 'Menu Faktur Penjualan Untuk Alur Tanpa Surat Jalan / Pengiriman Barang'),
(623, 'Laporan Order Pembelian', 'Menampilkan daftar pesanan pembelian sesuai dengan periode yang dipilih', 92, 558, 1, 1, NULL, '', NULL, 'Daftar Order Pembelian', NULL, '', 116, ''),
(624, 'Laporan Invoice Pembelian', 'Menampilkan daftar invoice pembelian sesuai dengan periode yang dipilih ', 93, 558, 1, 1, NULL, '', NULL, 'Daftar Invoice Pembelian', NULL, '', 117, ''),
(625, 'Laporan Retur Pembelian', 'Menampilkan daftar retur pembelian sesuai dengan periode yang dipilih', 94, 558, 1, 1, NULL, '', NULL, 'Daftar Retur Pembelian', NULL, '', 118, ''),
(626, 'Laporan Pembayaran Hutang', 'Menampilkan daftar transaksi pembayaran hutang ke pemasok sesuai dengan periode yang dipilih', 95, 558, 1, 1, NULL, '', NULL, 'Daftar Pembayaran Hutang', NULL, '', 119, ''),
(627, 'Laporan Order Penjualan', 'Menampilkan daftar pesanan penjualan sesuai dengan periode yang dipilih', 112, 559, 1, 1, NULL, '', NULL, 'Daftar Order Penjualan', NULL, '', 120, ''),
(628, 'Laporan Invoice Penjualan', 'Menampilkan daftar invoice penjualan sesuai dengan periode yang dipilih ', 113, 559, 1, 1, NULL, '', NULL, 'Daftar Invoice Penjualan', NULL, '', 121, ''),
(629, 'Laporan Retur Penjualan', 'Menampilkan daftar retur penjualan sesuai dengan periode yang dipilih', 114, 559, 1, 1, NULL, '', NULL, 'Daftar Retur Penjualan', NULL, '', 122, ''),
(630, 'Laporan Pembayaran Piutang', 'Menampilkan daftar transaksi pembayaran piutang pelanggan sesuai dengan periode yang dipilih', 115, 559, 1, 1, NULL, '', NULL, 'Daftar Pembayaran Piutang', NULL, '', 123, ''),
(631, 'Laporan Uang Muka Pembelian', 'Menampilkan daftar uang muka ke pemasok sesuai dengan periode yang dipilih', 96, 558, 1, 1, NULL, '', NULL, 'Daftar Uang Muka Pembelian', NULL, '', 126, ''),
(632, 'Laporan Uang Muka Penjualan', 'Menampilkan daftar uang muka ke pemasok sesuai dengan periode yang dipilih', 116, 559, 1, 1, NULL, '', NULL, 'Daftar Uang Muka Penjualan', NULL, '', 127, ''),
(633, 'Laporan Daftar Aktiva', 'Menampilkan daftar aktiva tetap sesuai kriteria yang dipilih', 153, 561, 1, 1, NULL, '', NULL, 'Daftar Aktiva', NULL, '', 128, ''),
(634, 'Laporan Stok Opname', 'Menampilkan laporan kegiatan stok opname sesuai dengan periode yang dipilih ', 134, 560, 1, 1, NULL, '', NULL, 'Daftar Stok Opname', NULL, '', 130, ''),
(635, 'Laporan Penyesuaian Persediaan', 'Menampilkan laporan penyesuaian persediaan sesuai dengan periode yang dipilih ', 135, 560, 1, 1, NULL, '', NULL, 'Daftar Penyesuaian Stok', NULL, '', 131, ''),
(636, 'Print Voucher Kas Masuk', 'Menampilkan voucher atau bukti transaksi kas masuk sesuai dengan nomor yang dipilih', 171, 562, 1, 1, NULL, '', NULL, 'Bukti Kas Masuk', NULL, '', 132, ''),
(637, 'Print Voucher Kas Keluar', 'Menampilkan voucher atau bukti transaksi kas keluar sesuai dengan nomor yang dipilih', 172, 562, 1, 1, NULL, '', NULL, 'Bukti Kas Keluar', NULL, '', 133, ''),
(638, 'Print Voucher Bank Masuk', 'Menampilkan voucher atau bukti transaksi bank masuk sesuai dengan nomor yang dipilih', 173, 562, 1, 1, NULL, '', NULL, 'Bukti Bank Masuk', NULL, '', 134, ''),
(639, 'Print Voucher Bank Keluar', 'Menampilkan voucher atau bukti transaksi bank keluar sesuai dengan nomor yang dipilih', 174, 562, 1, 1, NULL, '', NULL, 'Bukti Bank Keluar', NULL, '', 135, ''),
(640, 'Print Voucher Jurnal Umum', 'Menampilkan voucher atau bukti transaksi Jurnal Umum sesuai dengan nomor yang dipilih', 175, 562, 1, 1, NULL, '', NULL, 'Bukti Jurnal Umum', NULL, '', 136, ''),
(641, 'Print Voucher UM Pembelian', 'Menampilkan voucher atau bukti transaksi uang muka pembelian sesuai dengan nomor yang dipilih', 177, 562, 1, 1, NULL, '', NULL, 'Uang Muka Pembelian', NULL, '', 137, ''),
(642, 'Print Voucher UM Penjualan', 'Menampilkan voucher atau bukti transaksi uang muka penjualan sesuai dengan nomor yang dipilih', 178, 562, 1, 1, NULL, '', NULL, 'Uang Muka Penjualan', NULL, '', 138, ''),
(643, 'Print Order Pembelian', 'Menampilkan bukti pesanan pembelian sesuai dengan nomor yang dipilih', 180, 562, 1, 1, NULL, '', NULL, 'Pesanan Pembelian', NULL, '', 139, ''),
(644, 'Print Invoice Pembelian', 'Menampilkan bukti invoice pembelian sesuai dengan nomor yang dipilih', 182, 562, 1, 1, NULL, '', NULL, 'Invoice Pembelian', NULL, '', 140, ''),
(645, 'Print Retur Pembelian', 'Menampilkan bukti retur pembelian sesuai dengan nomor yang dipilih', 183, 562, 1, 1, NULL, '', NULL, 'Retur Pembelian', NULL, '', 141, ''),
(646, 'Print Pembayaran Hutang', 'Menampilkan bukti atau voucher pembayaran hutang sesuai dengan nomor yang dipilih', 184, 562, 1, 1, NULL, '', NULL, 'Pembayaran Hutang', NULL, '', 142, ''),
(647, 'Print Order Penjualan', 'Menampilkan bukti pesanan penjualan sesuai dengan nomor yang dipilih', 190, 562, 1, 1, NULL, '', NULL, 'Pesanan Penjualan', NULL, '', 143, ''),
(648, 'Print Invoice Penjualan', 'Menampilkan bukti invoice penjualan sesuai dengan nomor yang dipilih', 193, 562, 1, 1, NULL, '', NULL, 'Invoice Penjualan', NULL, '', 144, ''),
(649, 'Print Retur Penjualan', 'Menampilkan bukti retur penjualan sesuai dengan nomor yang dipilih', 194, 562, 1, 1, NULL, '', NULL, 'Retur Penjualan', NULL, '', 145, ''),
(650, 'Print Pembayaran Piutang', 'Menampilkan bukti atau voucher pembayaran piutang sesuai dengan nomor yang dipilih', 195, 562, 1, 1, NULL, '', NULL, 'Pembayaran Piutang', NULL, '', 146, ''),
(651, 'Laporan PPN Keluaran', 'Menampilkan daftar pajak keluaran dari transaksi penjualan sesuai periode yang dipilih', 117, 559, 1, 1, NULL, '', NULL, 'Daftar PPN Keluaran', NULL, '', 148, ''),
(652, 'Laporan PPN Masukan', 'Menampilkan daftar pajak masukan dari transaksi pembelian sesuai periode yang dipilih', 97, 558, 1, 1, NULL, '', NULL, 'Daftar PPN Masukan', NULL, '', 147, ''),
(653, 'Laporan Penjualan Tunai', 'Menampilkan daftar transaksi penjualan tunai (POS) sesuai dengan periode yang dipilih ', 118, 559, 1, 1, NULL, '', NULL, 'Daftar Penjualan Tunai', NULL, '', 149, ''),
(654, 'Laporan Daftar Item', 'Menampilkan laporan daftar item barang & jasa beserta posisi stok terakhir', 136, 560, 1, 1, NULL, '', NULL, 'Daftar Item', NULL, '', 150, ''),
(655, 'Laporan Penjualan Per Item', 'Menampilkan daftar penjualan per item barang/jasa sesuai dengan periode yang dipilih ', 119, 559, 1, 1, NULL, '', NULL, 'Daftar Penjualan Per Item', NULL, '', 151, ''),
(656, 'Laporan Penjualan Detil', 'Menampilkan detil daftar penjualan barang/jasa sesuai dengan periode yang dipilih ', 112, 559, 1, 1, NULL, '', NULL, 'Daftar Penjualan Detil', NULL, '', 152, ''),
(657, 'Laporan Pembelian Detil', 'Menampilkan detil daftar pembelian barang/jasa sesuai dengan periode yang dipilih ', 91, 558, 1, 1, NULL, '', NULL, 'Daftar Pembelian Detil', NULL, '', 153, '');

-- --------------------------------------------------------

--
-- Table structure for table `anomor`
--

CREATE TABLE `anomor` (
  `NID` int(11) NOT NULL,
  `NKODE` varchar(5) NOT NULL,
  `NKETERANGAN` varchar(255) DEFAULT NULL,
  `NTABEL` varchar(50) NOT NULL,
  `NFLDTANGGAL` varchar(25) NOT NULL,
  `NFLDSUMBER` varchar(25) DEFAULT NULL,
  `NFLDNOTRANSAKSI` varchar(25) NOT NULL,
  `NFLDURAIAN` varchar(25) NOT NULL,
  `NFLDTOTALTRANS` varchar(25) NOT NULL,
  `NFLDKONTAK` varchar(25) NOT NULL,
  `NFLDID` varchar(25) DEFAULT NULL,
  `NFA` smallint(6) DEFAULT '0',
  `NKODENOMOR` varchar(25) DEFAULT NULL,
  `NFMENU` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `anomor`
--

INSERT INTO `anomor` (`NID`, `NKODE`, `NKETERANGAN`, `NTABEL`, `NFLDTANGGAL`, `NFLDSUMBER`, `NFLDNOTRANSAKSI`, `NFLDURAIAN`, `NFLDTOTALTRANS`, `NFLDKONTAK`, `NFLDID`, `NFA`, `NKODENOMOR`, `NFMENU`) VALUES
(687, 'KM', 'Bukti Kas Masuk', 'ctransaksiu', 'CUTANGGAL', 'CUSUMBER', 'CUNOTRANSAKSI', 'CUURAIAN', 'CUTOTALTRANS', 'CUKONTAK', 'CUID', 1, NULL, 20),
(688, 'KK', 'Bukti Kas Keluar', 'ctransaksiu', 'CUTANGGAL', 'CUSUMBER', 'CUNOTRANSAKSI', 'CUURAIAN', 'CUTOTALTRANS', 'CUKONTAK', 'CUID', 1, NULL, 21),
(689, 'BM', 'Bukti Bank Masuk', 'ctransaksiu', 'CUTANGGAL', 'CUSUMBER', 'CUNOTRANSAKSI', 'CUURAIAN', 'CUTOTALTRANS', 'CUKONTAK', 'CUID', 1, NULL, 22),
(690, 'BK', 'Bukti Bank Keluar', 'ctransaksiu', 'CUTANGGAL', 'CUSUMBER', 'CUNOTRANSAKSI', 'CUURAIAN', 'CUTOTALTRANS', 'CUKONTAK', 'CUID', 1, NULL, 23),
(691, 'JU', 'Jurnal Umum', 'ctransaksiu', 'CUTANGGAL', 'CUSUMBER', 'CUNOTRANSAKSI', 'CUURAIAN', 'CUTOTALTRANS', 'CUKONTAK', 'CUID', 1, NULL, 24),
(692, 'SAA', 'Saldo Awal Aktiva', 'fasetu', 'SUTANGGAL', 'SUSUMBER', 'SUNOTRANSAKSI', 'SUURAIAN', 'SUTOTALTRANSAKSI', 'SUKONTAK', 'SUID', 1, NULL, NULL),
(693, 'AT', 'Penyusutan Aktiva', 'ctransaksiu', 'CUTANGGAL', 'CUSUMBER', 'CUNOTRANSAKSI', 'CUURAIAN', 'CUTOTALTRANS', 'CUKONTAK', 'CUID', 1, NULL, NULL),
(695, 'PO', 'Order Pembelian', 'esalesorderu', 'SOUTANGGAL', 'SOUSUMBER', 'SOUNOTRANSAKSI', 'SOUURAIAN', 'SOUTOTALTRANSAKSI', 'SOUKONTAK', 'SOUID', 0, NULL, 571),
(696, 'TB', 'Penerimaan Barang', 'fstoku', 'SUTANGGAL', 'SUSUMBER', 'SUNOTRANSAKSI', 'SUURAIAN', 'SUTOTALTRANSAKSI', 'SUKONTAK', 'SUID', 1, NULL, 572),
(698, 'SO', 'Order Penjualan', 'esalesorderu', 'SOUTANGGAL', 'SOUSUMBER', 'SOUNOTRANSAKSI', 'SOUURAIAN', 'SOUTOTALTRANSAKSI', 'SOUKONTAK', 'SOUID', 0, NULL, 577),
(700, 'IV', 'Invoice Penjualan', 'einvoicepenjualanu', 'IPUTANGGAL', 'IPUSUMBER', 'IPUNOTRANSAKSI', 'IPUURAIAN', 'IPUTOTALTRANSAKSI', 'IPUKONTAK', 'IPUID', 1, NULL, 622),
(701, 'SR', 'Retur Penjualan', 'einvoicepenjualanu', 'IPUTANGGAL', 'IPUSUMBER', 'IPUNOTRANSAKSI', 'IPUURAIAN', 'IPUTOTALTRANSAKSI', 'IPUKONTAK', 'IPUID', 1, NULL, 581),
(702, 'BKR', 'Pengeluaran Barang Retur', 'fstoku', 'SUTANGGAL', 'SUSUMBER', 'SUNOTRANSAKSI', 'SUURAIAN', 'SUTOTALTRANSAKSI', 'SUKONTAK', 'SUID', 1, NULL, 574),
(703, 'CP', 'Customer Payment', 'epembayaraninvoiceu', 'PIUTANGGAL', 'PIUSUMBER', 'PIUNOTRANSAKSI', 'PIUURAIAN', 'PIUJMLKAS', 'PIUKONTAK', 'PIUID', 1, NULL, 582),
(705, 'MB', 'Mutasi Barang', 'fstoku', 'SUTANGGAL', 'SUSUMBER', 'SUNOTRANSAKSI', 'SUURAIAN', 'SUTOTALTRANSAKSI', 'SUKONTAK', 'SUID', 0, NULL, 583),
(706, 'PY', 'Penyesuaian Barang', 'fstoku', 'SUTANGGAL', 'SUSUMBER', 'SUNOTRANSAKSI', 'SUURAIAN', 'SUTOTALTRANSAKSI', 'SUKONTAK', 'SUID', 1, NULL, 585),
(707, 'OP', 'Stok Opname', 'fstokopnameu', 'SOUTANGGAL', 'SOUSUMBER', 'SOUNOTRANSAKSI', 'SOUURAIAN', 'SOUTOTALTRANSAKSI', 'SOUKONTAK', 'SOUID', 0, NULL, 584),
(708, 'PR', 'Retur Pembelian', 'einvoicepenjualanu', 'IPUTANGGAL', 'IPUSUMBER', 'IPUNOTRANSAKSI', 'IPUURAIAN', 'IPUTOTALTRANSAKSI', 'IPUKONTAK', 'IPUID', 1, NULL, 575),
(709, 'BTR', 'Penerimaan Barang Retur', 'fstoku', 'SUTANGGAL', 'SUSUMBER', 'SUNOTRANSAKSI', 'SUURAIAN', 'SUTOTALTRANSAKSI', 'SUKONTAK', 'SUID', 1, NULL, 580),
(710, 'VP', 'Vendor Payment', 'epembayaraninvoiceu', 'PIUTANGGAL', 'PIUSUMBER', 'PIUNOTRANSAKSI', 'PIUURAIAN', 'PIUJMLKAS', 'PIUKONTAK', 'PIUID', 1, NULL, 576),
(715, 'PJ', 'Invoice Pembelian', 'einvoicepenjualanu', 'IPUTANGGAL', 'IPUSUMBER', 'IPUNOTRANSAKSI', 'IPUURAIAN', 'IPUTOTALTRANSAKSI', 'IPUKONTAK', 'IPUID', 1, NULL, 621),
(717, 'SAIT', 'Saldo Awal Stok', 'fstoku', 'SUTANGGAL', 'SUSUMBER', 'SUNOTRANSAKSI', 'SUURAIAN', 'SUTOTALTRANSAKSI', 'SUKONTAK', 'SUID', 1, NULL, NULL),
(718, 'SJ', 'Surat Jalan', 'fstoku', 'SUTANGGAL', 'SUSUMBER', 'SUNOTRANSAKSI', 'SUURAIAN', 'SUTOTALTRANSAKSI', 'SUKONTAK', 'SUID', 1, NULL, 578),
(719, 'PA', 'Pembelian Aktiva', 'fasetu', 'SUTANGGAL', 'SUSUMBER', 'SUNOTRANSAKSI', 'SUURAIAN', 'SUTOTALTRANSAKSI', 'SUKONTAK', 'SUID', 1, NULL, NULL),
(720, 'DPV', 'Uang Muka Pembelian', 'ddp', 'DPTANGGAL', 'DPSUMBER', 'DPNOTRANSAKSI', 'DPKETERANGAN', 'DPJUMLAH', 'DPKONTAK', 'DPID', 1, NULL, 618),
(721, 'DPC', 'Uang Muka Penjualan', 'ddp', 'DPTANGGAL', 'DPSUMBER', 'DPNOTRANSAKSI', 'DPKETERANGAN', 'DPJUMLAH', 'DPKONTAK', 'DPID', 1, NULL, 619),
(722, 'SA', 'Saldo Awal', 'ctransaksiu', 'CUTANGGAL', 'CUSUMBER', 'CUNOTRANSAKSI', 'CUURAIAN', 'CUTOTALTRANS', 'CUKONTAK', 'CUID', 1, NULL, NULL),
(723, 'SAI', 'Saldo Awal Tagihan', 'einvoicepenjualanu', 'IPUTANGGAL', 'IPUSUMBER', 'IPUNOTRANSAKSI', 'IPUURAIAN', 'IPUTOTALTRANSAKSI', 'IPUKONTAK', 'IPUID', 1, NULL, NULL),
(724, 'IP', 'Penjualan Tunai', 'einvoicepenjualanu', 'IPUTANGGAL', 'IPUSUMBER', 'IPUNOTRANSAKSI', 'IPUURAIAN', 'IPUTOTALTRANSAKSI', 'IPUKONTAK', 'IPUID', 1, NULL, 620);

-- --------------------------------------------------------

--
-- Table structure for table `areport`
--

CREATE TABLE `areport` (
  `ARID` int(11) NOT NULL,
  `ARLINK` varchar(150) DEFAULT 'index',
  `ARNAME` varchar(50) DEFAULT NULL,
  `ARNAME2` varchar(50) DEFAULT NULL,
  `ARPAPERORINTED` int(11) DEFAULT '0',
  `ARPAPERSIZE` int(11) DEFAULT '1',
  `ARACTIVE` int(11) NOT NULL DEFAULT '1',
  `ARTITLE` varchar(150) DEFAULT NULL,
  `ARMARGINLEFT` int(11) NOT NULL DEFAULT '6',
  `ARMARGINTOP` int(11) NOT NULL DEFAULT '6',
  `ARDATE1F` tinyint(4) NOT NULL DEFAULT '0',
  `ARDATE2F` tinyint(4) NOT NULL DEFAULT '0',
  `ARKONTAKF` tinyint(4) NOT NULL DEFAULT '0',
  `ARCOAF` tinyint(4) NOT NULL DEFAULT '0',
  `ARSOURCEF` tinyint(4) NOT NULL DEFAULT '0',
  `ARITEMF` tinyint(4) NOT NULL DEFAULT '0',
  `ARSALDOF` tinyint(4) NOT NULL DEFAULT '0',
  `ARGUDANGF` tinyint(4) NOT NULL DEFAULT '0',
  `ARNOMORF` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `areport`
--

INSERT INTO `areport` (`ARID`, `ARLINK`, `ARNAME`, `ARNAME2`, `ARPAPERORINTED`, `ARPAPERSIZE`, `ARACTIVE`, `ARTITLE`, `ARMARGINLEFT`, `ARMARGINTOP`, `ARDATE1F`, `ARDATE2F`, `ARKONTAKF`, `ARCOAF`, `ARSOURCEF`, `ARITEMF`, `ARSALDOF`, `ARGUDANGF`, `ARNOMORF`) VALUES
(11, 'formulir-kas-masuk', 'Bukti Kas Masuk', 'Cash Receipt Voucher', 1, 1, 1, '', 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(12, 'formulir-kas-keluar', 'Bukti Kas Keluar', 'Cash Payment Voucher', 1, 1, 1, '', 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, 1),
(13, 'formulir-bank-masuk', 'Bukti Bank Masuk', 'Bank Receipt Voucher', 1, 1, 1, '', 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(14, 'formulir-bank-keluar', 'Bukti Bank Keluar', 'Bank Payment Voucher', 1, 1, 1, '', 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(15, 'formulir-jurnal-umum', 'Bukti Jurnal Umum', 'Journal Voucher', 1, 1, 1, '', 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(16, 'formulir-pesanan-pembelian', 'Pesanan Pembelian', 'Purchase Order Voucher', 1, 1, 1, '', 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(17, 'formulir-penerimaan-pembelian', 'Penerimaan Barang', 'Receive Item Voucher', 1, 1, 1, '', 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(18, 'formulir-faktur-pembelian', 'Invoice Pembelian', 'Purchase Invoice', 1, 1, 1, '', 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(19, 'formulir-pengembalian-barang', 'Pengembalian Barang', 'Return Item Voucher', 1, 1, 1, '', 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(20, 'formulir-retur-pembelian', 'Retur Pembelian', 'Purchase Return Voucher', 1, 1, 1, '', 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(21, 'formulir-pembayaran-hutang', 'Pembayaran Hutang', 'Vendor Payment Voucher', 1, 1, 1, '', 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(22, 'formulir-pesanan-penjualan', 'Pesanan Penjualan', 'Sales Order Voucher', 1, 1, 1, '', 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(23, 'formulir-surat-jalan', 'Surat Jalan', 'Sales Delivery Voucher', 1, 1, 1, '', 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(24, 'formulir-faktur-penjualan', 'Invoice Penjualan', 'Sales Invoice Voucher', 1, 1, 1, '', 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(25, 'formulir-terima-retur', 'Bukti Terima Retur', 'Receive Item Return', 1, 1, 1, '', 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(26, 'formulir-retur-penjualan', 'Retur Penjualan', 'Sales Return Voucher', 1, 1, 1, '', 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(27, 'formulir-pembayaran-piutang', 'Pembayaran Piutang', 'Customer Payment Voucher', 1, 1, 1, '', 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(28, 'formulir-mutasi-barang', 'Mutasi Barang', 'Item Moving Voucher', 1, 1, 1, '', 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(29, 'formulir-opname', 'Stok Opname', 'Stock Opname Voucher', 1, 1, 1, '', 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(30, 'formulir-penyesuaian-persediaan', 'Penyesuaian Stok', 'Inventory Adjustment Voucher', 1, 1, 1, '', 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(102, 'laporan-neraca', 'Laporan Neraca', 'Balance Sheet', 1, 3, 1, '', 6, 6, 0, 1, 0, 0, 0, 0, 1, 0, 0),
(103, 'laporan-laba-rugi', 'Laporan Laba Rugi', 'Income Statement', 1, 3, 1, '', 6, 6, 1, 0, 0, 0, 0, 0, 1, 0, 0),
(104, 'laporan-buku-besar', 'Laporan Buku Besar', 'General Ledger', 2, 1, 1, '', 6, 6, 1, 0, 0, 1, 0, 0, 1, 0, 0),
(105, 'laporan-kas-masuk', 'Daftar Kas Masuk', 'Cash Receipt Report', 1, 1, 1, '', 6, 6, 1, 0, 1, 0, 0, 0, 0, 0, 0),
(106, 'laporan-kas-keluar', 'Daftar Kas Keluar', 'Cash Payment Report', 1, 1, 1, '', 6, 6, 1, 0, 1, 0, 0, 0, 0, 0, 0),
(107, 'laporan-bank-masuk', 'Daftar Bank Masuk', 'Bank Account Receipt', 1, 1, 1, '', 6, 6, 1, 0, 1, 0, 0, 0, 0, 0, 0),
(108, 'laporan-bank-keluar', 'Daftar Bank Keluar', 'Bank Account Payment', 1, 1, 1, '', 6, 6, 1, 0, 1, 0, 0, 0, 0, 0, 0),
(109, 'laporan-jurnal-umum', 'Daftar Jurnal Umum', 'General Journal Report', 1, 1, 1, '', 6, 6, 1, 0, 1, 0, 0, 0, 0, 0, 0),
(110, 'laporan-jurnal', 'Laporan Jurnal', 'All Journal Report', 1, 1, 1, '', 6, 6, 1, 0, 0, 0, 1, 0, 0, 0, 0),
(111, 'laporan-kartu-stok', 'Kartu Stok', 'Item Stock Card Report', 1, 1, 1, '', 6, 6, 1, 0, 0, 0, 0, 1, 1, 1, 0),
(112, 'laporan-kartu-persediaan', 'Kartu Persediaan', 'Inventory Card Report', 2, 1, 1, '', 6, 6, 1, 0, 0, 0, 0, 1, 1, 1, 0),
(113, 'formulir-uang-muka-pembelian', 'Uang Muka Pembelian', 'Down Payment Voucher', 1, 1, 1, '', 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(114, 'formulir-uang-muka-penjualan', 'Uang Muka Penjualan', 'Down Payment Voucher', 1, 1, 1, '', 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(115, 'formulir-penjualan-tunai', 'Penjualan Tunai', 'POS Voucher', 1, 1, 1, '', 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(116, 'laporan-order-pembelian', 'Daftar Order Pembelian', 'Purchase Order List', 1, 1, 1, '', 6, 6, 1, 0, 1, 0, 0, 0, 0, 0, 0),
(117, 'laporan-invoice-pembelian', 'Daftar Invoice Pembelian', 'Purchase Invoice List', 2, 1, 1, '', 6, 6, 1, 0, 1, 0, 0, 0, 0, 0, 0),
(118, 'laporan-retur-pembelian', 'Daftar Retur Pembelian', 'Purchase Return List', 2, 1, 1, '', 6, 6, 1, 0, 1, 0, 0, 0, 0, 0, 0),
(119, 'laporan-pembayaran-hutang', 'Daftar Pembayaran Hutang', 'Vendor Payment List', 2, 1, 1, '', 6, 6, 1, 0, 1, 0, 0, 0, 0, 0, 0),
(120, 'laporan-order-penjualan', 'Daftar Order Penjualan', 'Sales Order List', 1, 1, 1, '', 6, 6, 1, 0, 1, 0, 0, 0, 0, 0, 0),
(121, 'laporan-invoice-penjualan', 'Daftar Invoice Penjualan', 'Sales Invoice List', 2, 1, 1, '', 6, 6, 1, 0, 1, 0, 0, 0, 0, 0, 0),
(122, 'laporan-retur-penjualan', 'Daftar Retur Penjualan', 'Sales Return List', 2, 1, 1, '', 6, 6, 1, 0, 1, 0, 0, 0, 0, 0, 0),
(123, 'laporan-pembayaran-piutang', 'Daftar Pembayaran Piutang', 'Customer Payment Report', 2, 1, 1, '', 6, 6, 1, 0, 1, 0, 0, 0, 0, 0, 0),
(124, 'laporan-hutang', 'Daftar Hutang Usaha', 'Account Payable Report', 1, 1, 1, '', 6, 6, 0, 1, 1, 0, 0, 0, 1, 0, 0),
(125, 'laporan-piutang', 'Daftar Piutang Usaha', 'Account Receivable Report', 1, 1, 1, '', 6, 6, 0, 1, 1, 0, 0, 0, 1, 0, 0),
(126, 'laporan-uangmuka-pembelian', 'Daftar Uang Muka Pembelian', 'Vendor Down Payment', 1, 1, 1, '', 6, 6, 1, 0, 1, 0, 0, 0, 0, 0, 0),
(127, 'laporan-uangmuka-penjualan', 'Daftar Uang Muka Penjualan', 'Customer Down Payment', 1, 1, 1, '', 6, 6, 1, 0, 1, 0, 0, 0, 0, 0, 0),
(128, 'laporan-daftar-aset', 'Daftar Aktiva Tetap', 'Fixed Asset List', 2, 1, 1, '', 6, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(129, 'laporan-mutasi-stok', 'Daftar Mutasi Stok', 'Stock Movement List', 1, 1, 1, '', 6, 6, 1, 0, 0, 0, 0, 0, 0, 1, 0),
(130, 'laporan-stok-opname', 'Daftar Stok Opname', 'Stock Opname List', 1, 1, 1, '', 6, 6, 1, 0, 1, 0, 0, 0, 0, 1, 0),
(131, 'laporan-penyesuaian-stok', 'Daftar Penyesuaian Stok', 'Stock Adjustment List', 1, 1, 1, '', 6, 6, 1, 0, 1, 0, 0, 0, 0, 1, 0),
(132, 'formulir-kas-masuk-multi', 'Bukti Kas Masuk', 'Cash Receipt Voucher (Multi)', 1, 1, 1, '', 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, 1),
(133, 'formulir-kas-keluar-multi', 'Bukti Kas Keluar', 'Cash Payment Voucher (Multi)', 1, 1, 1, '', 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, 1),
(134, 'formulir-bank-masuk-multi', 'Bukti Bank Masuk', 'Bank Receipt Voucher (Multi)', 1, 1, 1, '', 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, 1),
(135, 'formulir-bank-keluar-multi', 'Bukti Bank Keluar', 'Bank Payment Voucher (Multi)', 1, 1, 1, '', 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, 1),
(136, 'formulir-jurnal-umum-multi', 'Bukti Jurnal Umum', 'Journal Voucher (Multi)', 1, 1, 1, '', 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, 1),
(137, 'formulir-uang-muka-pembelian-multi', 'Uang Muka Pembelian', 'Down Payment Voucher (Multi)', 1, 1, 1, '', 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, 1),
(138, 'formulir-uang-muka-penjualan-multi', 'Uang Muka Penjualan', 'Down Payment Voucher (Multi)', 1, 1, 1, '', 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, 1),
(139, 'formulir-pesanan-pembelian-multi', 'Pesanan Pembelian', 'Purchase Order Voucher (Multi)', 1, 1, 1, '', 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, 1),
(140, 'formulir-faktur-pembelian-multi', 'Invoice Pembelian', 'Purchase Invoice (Multi)', 1, 1, 1, '', 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, 1),
(141, 'formulir-retur-pembelian-multi', 'Retur Pembelian', 'Purchase Return (Multi)', 1, 1, 1, '', 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, 1),
(142, 'formulir-pembayaran-hutang-multi', 'Pembayaran Hutang', 'Vendor Payment Voucher (Multi)', 1, 1, 1, '', 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, 1),
(143, 'formulir-pesanan-penjualan-multi', 'Pesanan Penjualan', 'Sales Order Voucher (Multi)', 1, 1, 1, '', 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, 1),
(144, 'formulir-faktur-penjualan-multi', 'Invoice Penjualan', 'Sales Invoice (Multi)', 1, 1, 1, '', 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, 1),
(145, 'formulir-retur-penjualan-multi', 'Retur Penjualan', 'Sales Return (Multi)', 1, 1, 1, '', 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, 1),
(146, 'formulir-pembayaran-piutang-multi', 'Pembayaran Piutang', 'Customer Payment Voucher (Multi)', 1, 1, 1, '', 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, 1),
(147, 'laporan-ppn-masukan', 'Daftar PPN Masukan', 'VAT In List Report', 1, 1, 1, '', 6, 6, 1, 0, 1, 0, 0, 0, 0, 0, 0),
(148, 'laporan-ppn-keluaran', 'Daftar PPN Keluaran', 'VAT Out List Report', 1, 1, 1, '', 6, 6, 1, 0, 1, 0, 0, 0, 0, 0, 0),
(149, 'laporan-penjualan-tunai', 'Daftar Penjualan Tunai', 'Point Of Sale List Report', 1, 1, 1, '', 6, 6, 1, 0, 1, 0, 0, 0, 0, 0, 0),
(150, 'laporan-daftar-item', 'Daftar Item', 'Item List', 1, 1, 1, '', 6, 6, 0, 0, 0, 0, 0, 0, 1, 0, 0),
(151, 'laporan-penjualan-per-item', 'Daftar Penjualan Per Item', 'Sales Per Item List', 1, 1, 1, '', 6, 6, 1, 0, 0, 0, 0, 1, 0, 0, 0),
(152, 'laporan-penjualan-detil', 'Laporan Penjualan Detil', 'Sales Report (Detail)', 2, 1, 1, '', 6, 6, 1, 0, 1, 0, 0, 0, 0, 0, 0),
(153, 'laporan-pembelian-detil', 'Laporan Pembelian Detil', 'Purchase Report (Detail)', 2, 1, 1, '', 6, 6, 1, 0, 1, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `arole`
--

CREATE TABLE `arole` (
  `ARID` int(11) NOT NULL,
  `ARIDMENU` int(11) DEFAULT '0',
  `ARNAMAROLE` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `aserial`
--

CREATE TABLE `aserial` (
  `AID` int(11) NOT NULL,
  `AIDHARDWARE` varchar(100) NOT NULL,
  `AIDSERIAL` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `auser`
--

CREATE TABLE `auser` (
  `uid` int(11) NOT NULL,
  `ukode` varchar(50) DEFAULT NULL,
  `unama` varchar(100) DEFAULT NULL,
  `unamalengkap` varchar(100) DEFAULT NULL,
  `upassword` mediumtext,
  `ucreateu` int(11) DEFAULT NULL,
  `umodifu` int(11) DEFAULT NULL,
  `umodifd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ucreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `uactive` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `auser`
--

INSERT INTO `auser` (`uid`, `ukode`, `unama`, `unamalengkap`, `upassword`, `ucreateu`, `umodifu`, `umodifd`, `ucreated`, `uactive`) VALUES
(0, '1', 'demo', 'Demo User', '701ff7a409784925812cb290ee94380547ed60a35dedffc03c9d773d1cf4311cb58833ace3b654799027b55525214cc45d576df03ed891c4ccf34cfd78daa6c2', NULL, NULL, '2021-10-15 02:41:48', '2021-10-15 02:41:48', 1),
(1, '0', 'Developer', 'Developer', 'bff0cc42103de1b4721370e84dc24f635a7afeca41198c9b3e03946a1b6b7191d14356408a5e57ce6daf77e6e800c66fac7ab0482d57d48d23e6808e4b562daa', NULL, NULL, '0000-00-00 00:00:00', '2018-01-07 23:36:35', 1),
(2, '2', 'Administrator', 'Admin User', 'e9c5c250700493df3024d2aac98827417f38aa7c96178197be59bdf268a17a03436f4641a16d817cbbc94684c2d8b7dc706c782b46a22589a7048eedb63566db', NULL, NULL, '2021-09-09 15:11:05', '2021-09-09 15:11:05', 1),
(3, '3', 'User_demo', 'User Demo', 'd7d2f602e155ba700ed76c48d9a48009b9383e8d17435bfb0fe8ad7c664d4002f16cc7a65c6fb066963714a794f96441ef7f9b9c1b1456acfb9225cbad474fb0', NULL, NULL, '2022-01-12 01:13:32', '2022-01-12 01:13:32', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ausercoa`
--

CREATE TABLE `ausercoa` (
  `AUCID` int(11) NOT NULL,
  `AUCIDUSER` int(11) DEFAULT '0',
  `AUCIDCOA` int(11) DEFAULT '0',
  `AUCAKSES` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `auserlog`
--

CREATE TABLE `auserlog` (
  `ULID` int(11) NOT NULL,
  `ULUSER` int(11) NOT NULL,
  `ULUSERNAME` varchar(50) DEFAULT NULL,
  `ULCOMPUTER` varchar(30) DEFAULT NULL,
  `ULDATE` datetime DEFAULT CURRENT_TIMESTAMP,
  `ULTIME` datetime DEFAULT CURRENT_TIMESTAMP,
  `ULACTIVITY` varchar(255) DEFAULT NULL,
  `ULLEVEL` smallint(6) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `auserlog`
--

INSERT INTO `auserlog` (`ULID`, `ULUSER`, `ULUSERNAME`, `ULCOMPUTER`, `ULDATE`, `ULTIME`, `ULACTIVITY`, `ULLEVEL`) VALUES
(1, 1, 'Developer', '::1', '2022-01-05 07:44:58', '2022-01-05 07:44:58', 'Saldo Awal Stok SAIT21120001', 1),
(2, 1, 'Developer', '::1', '2022-01-05 07:48:10', '2022-01-05 07:48:10', 'Bukti Bank Masuk BM22010001', 1),
(3, 1, 'Developer', '::1', '2022-01-05 07:49:46', '2022-01-05 07:49:46', 'Saldo Awal Stok SAIT21120002', 1),
(4, 1, 'Developer', '::1', '2022-01-05 07:50:37', '2022-01-05 07:50:37', 'Saldo Awal Stok SAIT21120003', 1),
(5, 1, 'Developer', '::1', '2022-01-05 07:51:51', '2022-01-05 07:51:51', 'Order Penjualan SO22010001', 1),
(6, 1, 'Developer', '::1', '2022-01-05 07:54:49', '2022-01-05 07:54:49', 'Uang Muka Penjualan DPC22010001', 1),
(7, 1, 'Developer', '::1', '2022-01-05 07:56:41', '2022-01-05 07:56:41', 'Invoice Penjualan IV22010001', 1),
(8, 1, 'Developer', '::1', '2022-01-05 08:03:19', '2022-01-05 08:03:19', 'Customer Payment CP22010001', 1),
(9, 1, 'Developer', '::1', '2022-01-05 08:13:07', '2022-01-05 08:13:07', 'Saldo Awal Stok SAIT21120004', 1),
(10, 1, 'Developer', '::1', '2022-01-05 08:14:20', '2022-01-05 08:14:20', 'Saldo Awal Stok SAIT21120005', 1),
(11, 1, 'Developer', '::1', '2022-01-05 08:16:13', '2022-01-05 08:16:13', 'Saldo Awal Stok SAIT21120006', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ausermenu`
--

CREATE TABLE `ausermenu` (
  `AUID` int(11) NOT NULL,
  `AUIDUSER` int(11) DEFAULT '0',
  `AUIDMENU` int(11) DEFAULT '0',
  `AUADD` int(11) DEFAULT '0',
  `AUEDIT` int(11) DEFAULT '0',
  `AUDELL` int(11) DEFAULT '0',
  `AUPRINT` int(11) DEFAULT '0',
  `AUAPPROVE` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ausermenu`
--

INSERT INTO `ausermenu` (`AUID`, `AUIDUSER`, `AUIDMENU`, `AUADD`, `AUEDIT`, `AUDELL`, `AUPRINT`, `AUAPPROVE`) VALUES
(4080, 2, 1, 1, 1, 1, 1, 1),
(4081, 2, 20, 1, 1, 1, 1, 1),
(4082, 2, 21, 1, 1, 1, 1, 1),
(4083, 2, 22, 1, 1, 1, 1, 1),
(4084, 2, 23, 1, 1, 1, 1, 1),
(4085, 2, 24, 1, 1, 1, 1, 1),
(4086, 2, 570, 1, 1, 1, 1, 1),
(4087, 2, 616, 1, 1, 1, 1, 1),
(4088, 2, 2, 1, 1, 1, 1, 1),
(4089, 2, 571, 1, 1, 1, 1, 1),
(4090, 2, 618, 1, 1, 1, 1, 1),
(4091, 2, 572, 1, 1, 1, 1, 1),
(4092, 2, 621, 1, 1, 1, 1, 1),
(4093, 2, 573, 1, 1, 1, 1, 1),
(4094, 2, 574, 1, 1, 1, 1, 1),
(4095, 2, 575, 1, 1, 1, 1, 1),
(4096, 2, 576, 1, 1, 1, 1, 1),
(4097, 2, 3, 1, 1, 1, 1, 1),
(4098, 2, 577, 1, 1, 1, 1, 1),
(4099, 2, 619, 1, 1, 1, 1, 1),
(4100, 2, 578, 1, 1, 1, 1, 1),
(4101, 2, 579, 1, 1, 1, 1, 1),
(4102, 2, 622, 1, 1, 1, 1, 1),
(4103, 2, 580, 1, 1, 1, 1, 1),
(4104, 2, 581, 1, 1, 1, 1, 1),
(4105, 2, 582, 1, 1, 1, 1, 1),
(4106, 2, 620, 1, 1, 1, 1, 1),
(4107, 2, 4, 1, 1, 1, 1, 1),
(4108, 2, 583, 1, 1, 1, 1, 1),
(4109, 2, 584, 1, 1, 1, 1, 1),
(4110, 2, 585, 1, 1, 1, 1, 1),
(4111, 2, 100, 1, 1, 1, 1, 1),
(4112, 2, 586, 1, 1, 1, 1, 1),
(4113, 2, 587, 1, 1, 1, 1, 1),
(4114, 2, 588, 1, 1, 1, 1, 1),
(4115, 2, 589, 1, 1, 1, 1, 1),
(4116, 2, 590, 1, 1, 1, 1, 1),
(4117, 2, 591, 1, 1, 1, 1, 1),
(4118, 2, 592, 1, 1, 1, 1, 1),
(4119, 2, 593, 1, 1, 1, 1, 1),
(4120, 2, 594, 1, 1, 1, 1, 1),
(4121, 2, 595, 1, 1, 1, 1, 1),
(4122, 2, 596, 1, 1, 1, 1, 1),
(4123, 2, 597, 1, 1, 1, 1, 1),
(4124, 2, 598, 1, 1, 1, 1, 1),
(4125, 2, 599, 1, 1, 1, 1, 1),
(4126, 2, 615, 1, 1, 1, 1, 1),
(4127, 2, 199, 1, 1, 1, 1, 1),
(4128, 2, 200, 1, 1, 1, 1, 1),
(4129, 2, 600, 1, 1, 1, 1, 1),
(4130, 2, 601, 1, 1, 1, 1, 1),
(4131, 2, 557, 0, 0, 0, 0, 1),
(4132, 2, 563, 0, 0, 0, 0, 1),
(4133, 2, 564, 0, 0, 0, 0, 1),
(4134, 2, 602, 0, 0, 0, 0, 1),
(4135, 2, 603, 0, 0, 0, 0, 1),
(4136, 2, 605, 0, 0, 0, 0, 1),
(4137, 2, 604, 0, 0, 0, 0, 1),
(4138, 2, 606, 0, 0, 0, 0, 1),
(4139, 2, 607, 0, 0, 0, 0, 1),
(4140, 2, 608, 0, 0, 0, 0, 1),
(4141, 2, 609, 0, 0, 0, 0, 1),
(4142, 2, 610, 0, 0, 0, 0, 1),
(4143, 2, 611, 0, 0, 0, 0, 1),
(4144, 2, 558, 0, 0, 0, 0, 1),
(4145, 2, 565, 0, 0, 0, 0, 1),
(4146, 2, 559, 0, 0, 0, 0, 1),
(4147, 2, 566, 0, 0, 0, 0, 1),
(4148, 2, 560, 0, 0, 0, 0, 1),
(4149, 2, 612, 0, 0, 0, 0, 1),
(4150, 2, 614, 0, 0, 0, 0, 1),
(4151, 2, 617, 0, 0, 0, 0, 1),
(4152, 2, 561, 0, 0, 0, 0, 1),
(4153, 2, 613, 0, 0, 0, 0, 1),
(4154, 2, 562, 0, 0, 0, 0, 1),
(7251, 1, 1, 1, 1, 1, 1, 1),
(7252, 1, 20, 1, 1, 1, 1, 1),
(7253, 1, 21, 1, 1, 1, 1, 1),
(7254, 1, 22, 1, 1, 1, 1, 1),
(7255, 1, 23, 1, 1, 1, 1, 1),
(7256, 1, 24, 1, 1, 1, 1, 1),
(7257, 1, 570, 1, 1, 1, 1, 1),
(7258, 1, 616, 1, 1, 1, 1, 1),
(7259, 1, 2, 1, 1, 1, 1, 1),
(7260, 1, 571, 1, 1, 1, 1, 1),
(7261, 1, 618, 1, 1, 1, 1, 1),
(7262, 1, 572, 1, 1, 1, 1, 1),
(7263, 1, 573, 1, 1, 1, 1, 1),
(7264, 1, 621, 1, 1, 1, 1, 1),
(7265, 1, 574, 0, 0, 0, 0, 0),
(7266, 1, 575, 1, 1, 1, 1, 1),
(7267, 1, 576, 1, 1, 1, 1, 1),
(7268, 1, 3, 1, 1, 1, 1, 1),
(7269, 1, 577, 1, 1, 1, 1, 1),
(7270, 1, 619, 1, 1, 1, 1, 1),
(7271, 1, 578, 1, 1, 1, 1, 1),
(7272, 1, 579, 1, 1, 1, 1, 1),
(7273, 1, 622, 1, 1, 1, 1, 1),
(7274, 1, 580, 0, 0, 0, 0, 0),
(7275, 1, 581, 1, 1, 1, 1, 1),
(7276, 1, 582, 1, 1, 1, 1, 1),
(7277, 1, 620, 1, 1, 1, 1, 1),
(7278, 1, 4, 1, 1, 1, 1, 1),
(7279, 1, 583, 1, 1, 1, 1, 1),
(7280, 1, 584, 1, 1, 1, 1, 1),
(7281, 1, 585, 1, 1, 1, 1, 1),
(7282, 1, 100, 1, 1, 1, 1, 1),
(7283, 1, 586, 1, 1, 1, 1, 1),
(7284, 1, 587, 1, 1, 1, 1, 1),
(7285, 1, 588, 1, 1, 1, 1, 1),
(7286, 1, 589, 1, 1, 1, 1, 1),
(7287, 1, 590, 1, 1, 1, 1, 1),
(7288, 1, 591, 1, 1, 1, 1, 1),
(7289, 1, 592, 1, 1, 1, 1, 1),
(7290, 1, 593, 1, 1, 1, 1, 1),
(7291, 1, 594, 1, 1, 1, 1, 1),
(7292, 1, 595, 1, 1, 1, 1, 1),
(7293, 1, 596, 1, 1, 1, 1, 1),
(7294, 1, 597, 1, 1, 1, 1, 1),
(7295, 1, 598, 1, 1, 1, 1, 1),
(7296, 1, 599, 1, 1, 1, 1, 1),
(7297, 1, 615, 1, 1, 1, 1, 1),
(7298, 1, 199, 1, 1, 1, 1, 1),
(7299, 1, 200, 1, 1, 1, 1, 1),
(7300, 1, 201, 1, 1, 1, 1, 1),
(7301, 1, 600, 1, 1, 1, 1, 1),
(7302, 1, 601, 1, 1, 1, 1, 1),
(7303, 1, 557, 0, 0, 0, 0, 1),
(7304, 1, 563, 0, 0, 0, 0, 1),
(7305, 1, 564, 0, 0, 0, 0, 1),
(7306, 1, 602, 0, 0, 0, 0, 1),
(7307, 1, 603, 0, 0, 0, 0, 1),
(7308, 1, 605, 0, 0, 0, 0, 1),
(7309, 1, 604, 0, 0, 0, 0, 1),
(7310, 1, 606, 0, 0, 0, 0, 1),
(7311, 1, 607, 0, 0, 0, 0, 1),
(7312, 1, 608, 0, 0, 0, 0, 1),
(7313, 1, 609, 0, 0, 0, 0, 1),
(7314, 1, 610, 0, 0, 0, 0, 1),
(7315, 1, 611, 0, 0, 0, 0, 1),
(7316, 1, 558, 0, 0, 0, 0, 1),
(7317, 1, 565, 0, 0, 0, 0, 1),
(7318, 1, 623, 0, 0, 0, 0, 1),
(7319, 1, 624, 0, 0, 0, 0, 1),
(7320, 1, 625, 0, 0, 0, 0, 1),
(7321, 1, 626, 0, 0, 0, 0, 1),
(7322, 1, 631, 0, 0, 0, 0, 1),
(7323, 1, 652, 0, 0, 0, 0, 1),
(7324, 1, 657, 0, 0, 0, 0, 1),
(7325, 1, 559, 0, 0, 0, 0, 1),
(7326, 1, 566, 0, 0, 0, 0, 1),
(7327, 1, 627, 0, 0, 0, 0, 1),
(7328, 1, 628, 0, 0, 0, 0, 1),
(7329, 1, 629, 0, 0, 0, 0, 1),
(7330, 1, 630, 0, 0, 0, 0, 1),
(7331, 1, 632, 0, 0, 0, 0, 1),
(7332, 1, 651, 0, 0, 0, 0, 1),
(7333, 1, 653, 0, 0, 0, 0, 1),
(7334, 1, 655, 0, 0, 0, 0, 1),
(7335, 1, 656, 0, 0, 0, 0, 1),
(7336, 1, 560, 0, 0, 0, 0, 1),
(7337, 1, 612, 0, 0, 0, 0, 1),
(7338, 1, 614, 0, 0, 0, 0, 1),
(7339, 1, 617, 0, 0, 0, 0, 1),
(7340, 1, 634, 0, 0, 0, 0, 1),
(7341, 1, 635, 0, 0, 0, 0, 1),
(7342, 1, 654, 0, 0, 0, 0, 1),
(7343, 1, 561, 0, 0, 0, 0, 1),
(7344, 1, 613, 0, 0, 0, 0, 1),
(7345, 1, 633, 0, 0, 0, 0, 1),
(7346, 1, 562, 0, 0, 0, 0, 1),
(7347, 1, 636, 0, 0, 0, 0, 1),
(7348, 1, 637, 0, 0, 0, 0, 1),
(7349, 1, 638, 0, 0, 0, 0, 1),
(7350, 1, 639, 0, 0, 0, 0, 1),
(7351, 1, 640, 0, 0, 0, 0, 1),
(7352, 1, 641, 0, 0, 0, 0, 1),
(7353, 1, 642, 0, 0, 0, 0, 1),
(7354, 1, 643, 0, 0, 0, 0, 1),
(7355, 1, 644, 0, 0, 0, 0, 1),
(7356, 1, 645, 0, 0, 0, 0, 1),
(7357, 1, 646, 0, 0, 0, 0, 1),
(7358, 1, 647, 0, 0, 0, 0, 1),
(7359, 1, 648, 0, 0, 0, 0, 1),
(7360, 1, 649, 0, 0, 0, 0, 1),
(7361, 1, 650, 0, 0, 0, 0, 1),
(7582, 0, 1, 1, 1, 1, 1, 1),
(7583, 0, 20, 1, 1, 1, 1, 1),
(7584, 0, 21, 1, 1, 1, 1, 1),
(7585, 0, 22, 1, 1, 1, 1, 1),
(7586, 0, 23, 1, 1, 1, 1, 1),
(7587, 0, 24, 1, 1, 1, 1, 1),
(7588, 0, 570, 1, 1, 1, 1, 1),
(7589, 0, 616, 1, 1, 1, 1, 1),
(7590, 0, 2, 1, 1, 1, 1, 1),
(7591, 0, 571, 1, 1, 1, 1, 1),
(7592, 0, 618, 1, 1, 1, 1, 1),
(7593, 0, 572, 1, 1, 1, 1, 1),
(7594, 0, 621, 1, 1, 1, 1, 1),
(7595, 0, 573, 1, 1, 1, 1, 1),
(7596, 0, 574, 1, 1, 1, 1, 1),
(7597, 0, 575, 1, 1, 1, 1, 1),
(7598, 0, 576, 1, 1, 1, 1, 1),
(7599, 0, 3, 1, 1, 1, 1, 1),
(7600, 0, 577, 1, 1, 1, 1, 1),
(7601, 0, 619, 1, 1, 1, 1, 1),
(7602, 0, 578, 1, 1, 1, 1, 1),
(7603, 0, 579, 1, 1, 1, 1, 1),
(7604, 0, 622, 1, 1, 1, 1, 1),
(7605, 0, 580, 1, 1, 1, 1, 1),
(7606, 0, 581, 1, 1, 1, 1, 1),
(7607, 0, 582, 1, 1, 1, 1, 1),
(7608, 0, 620, 1, 1, 1, 1, 1),
(7609, 0, 4, 1, 1, 1, 1, 1),
(7610, 0, 583, 1, 1, 1, 1, 1),
(7611, 0, 584, 1, 1, 1, 1, 1),
(7612, 0, 585, 1, 1, 1, 1, 1),
(7613, 0, 100, 1, 1, 1, 1, 1),
(7614, 0, 586, 1, 1, 1, 1, 1),
(7615, 0, 587, 1, 1, 1, 1, 1),
(7616, 0, 588, 1, 1, 1, 1, 1),
(7617, 0, 589, 1, 1, 1, 1, 1),
(7618, 0, 590, 1, 1, 1, 1, 1),
(7619, 0, 591, 1, 1, 1, 1, 1),
(7620, 0, 592, 1, 1, 1, 1, 1),
(7621, 0, 593, 1, 1, 1, 1, 1),
(7622, 0, 594, 1, 1, 1, 1, 1),
(7623, 0, 595, 1, 1, 1, 1, 1),
(7624, 0, 596, 1, 1, 1, 1, 1),
(7625, 0, 597, 1, 1, 1, 1, 1),
(7626, 0, 598, 1, 1, 1, 1, 1),
(7627, 0, 599, 1, 1, 1, 1, 1),
(7628, 0, 615, 1, 1, 1, 1, 1),
(7629, 0, 199, 1, 1, 1, 1, 1),
(7630, 0, 200, 1, 1, 1, 1, 1),
(7631, 0, 600, 1, 1, 1, 1, 1),
(7632, 0, 601, 1, 1, 1, 1, 1),
(7633, 0, 557, 0, 0, 0, 0, 1),
(7634, 0, 563, 0, 0, 0, 0, 1),
(7635, 0, 564, 0, 0, 0, 0, 1),
(7636, 0, 602, 0, 0, 0, 0, 1),
(7637, 0, 603, 0, 0, 0, 0, 1),
(7638, 0, 605, 0, 0, 0, 0, 1),
(7639, 0, 604, 0, 0, 0, 0, 1),
(7640, 0, 606, 0, 0, 0, 0, 1),
(7641, 0, 607, 0, 0, 0, 0, 1),
(7642, 0, 608, 0, 0, 0, 0, 1),
(7643, 0, 609, 0, 0, 0, 0, 1),
(7644, 0, 610, 0, 0, 0, 0, 1),
(7645, 0, 611, 0, 0, 0, 0, 1),
(7646, 0, 558, 0, 0, 0, 0, 1),
(7647, 0, 565, 0, 0, 0, 0, 1),
(7648, 0, 623, 0, 0, 0, 0, 1),
(7649, 0, 624, 0, 0, 0, 0, 1),
(7650, 0, 625, 0, 0, 0, 0, 1),
(7651, 0, 626, 0, 0, 0, 0, 1),
(7652, 0, 631, 0, 0, 0, 0, 1),
(7653, 0, 652, 0, 0, 0, 0, 1),
(7654, 0, 657, 0, 0, 0, 0, 1),
(7655, 0, 559, 0, 0, 0, 0, 1),
(7656, 0, 566, 0, 0, 0, 0, 1),
(7657, 0, 627, 0, 0, 0, 0, 1),
(7658, 0, 628, 0, 0, 0, 0, 1),
(7659, 0, 629, 0, 0, 0, 0, 1),
(7660, 0, 630, 0, 0, 0, 0, 1),
(7661, 0, 632, 0, 0, 0, 0, 1),
(7662, 0, 651, 0, 0, 0, 0, 1),
(7663, 0, 653, 0, 0, 0, 0, 1),
(7664, 0, 655, 0, 0, 0, 0, 1),
(7665, 0, 656, 0, 0, 0, 0, 1),
(7666, 0, 560, 0, 0, 0, 0, 1),
(7667, 0, 612, 0, 0, 0, 0, 1),
(7668, 0, 614, 0, 0, 0, 0, 1),
(7669, 0, 617, 0, 0, 0, 0, 1),
(7670, 0, 634, 0, 0, 0, 0, 1),
(7671, 0, 635, 0, 0, 0, 0, 1),
(7672, 0, 654, 0, 0, 0, 0, 1),
(7673, 0, 561, 0, 0, 0, 0, 1),
(7674, 0, 613, 0, 0, 0, 0, 1),
(7675, 0, 633, 0, 0, 0, 0, 1),
(7676, 0, 562, 0, 0, 0, 0, 1),
(7677, 0, 636, 0, 0, 0, 0, 1),
(7678, 0, 637, 0, 0, 0, 0, 1),
(7679, 0, 638, 0, 0, 0, 0, 1),
(7680, 0, 639, 0, 0, 0, 0, 1),
(7681, 0, 640, 0, 0, 0, 0, 1),
(7682, 0, 641, 0, 0, 0, 0, 1),
(7683, 0, 642, 0, 0, 0, 0, 1),
(7684, 0, 643, 0, 0, 0, 0, 1),
(7685, 0, 644, 0, 0, 0, 0, 1),
(7686, 0, 645, 0, 0, 0, 0, 1),
(7687, 0, 646, 0, 0, 0, 0, 1),
(7688, 0, 647, 0, 0, 0, 0, 1),
(7689, 0, 648, 0, 0, 0, 0, 1),
(7690, 0, 649, 0, 0, 0, 0, 1),
(7691, 0, 650, 0, 0, 0, 0, 1),
(7803, 3, 1, 0, 0, 0, 0, 0),
(7804, 3, 20, 0, 0, 0, 0, 0),
(7805, 3, 21, 0, 0, 0, 0, 0),
(7806, 3, 22, 0, 0, 0, 0, 0),
(7807, 3, 23, 0, 0, 0, 0, 0),
(7808, 3, 24, 1, 1, 1, 1, 1),
(7809, 3, 616, 0, 0, 0, 0, 0),
(7810, 3, 570, 0, 0, 0, 0, 0),
(7811, 3, 2, 0, 0, 0, 0, 0),
(7812, 3, 571, 0, 0, 0, 0, 0),
(7813, 3, 618, 0, 0, 0, 0, 0),
(7814, 3, 572, 0, 0, 0, 0, 0),
(7815, 3, 573, 0, 0, 0, 0, 0),
(7816, 3, 621, 0, 0, 0, 0, 0),
(7817, 3, 574, 0, 0, 0, 0, 0),
(7818, 3, 575, 0, 0, 0, 0, 0),
(7819, 3, 576, 0, 0, 0, 0, 0),
(7820, 3, 3, 0, 0, 0, 0, 0),
(7821, 3, 577, 0, 0, 0, 0, 0),
(7822, 3, 619, 0, 0, 0, 0, 0),
(7823, 3, 578, 0, 0, 0, 0, 0),
(7824, 3, 622, 0, 0, 0, 0, 0),
(7825, 3, 579, 0, 0, 0, 0, 0),
(7826, 3, 580, 0, 0, 0, 0, 0),
(7827, 3, 581, 0, 0, 0, 0, 0),
(7828, 3, 582, 0, 0, 0, 0, 0),
(7829, 3, 620, 0, 0, 0, 0, 0),
(7830, 3, 4, 0, 0, 0, 0, 0),
(7831, 3, 583, 0, 0, 0, 0, 0),
(7832, 3, 584, 0, 0, 0, 0, 0),
(7833, 3, 585, 0, 0, 0, 0, 0),
(7834, 3, 100, 1, 1, 1, 1, 1),
(7835, 3, 586, 1, 1, 1, 1, 1),
(7836, 3, 587, 1, 1, 1, 1, 1),
(7837, 3, 588, 1, 1, 1, 1, 1),
(7838, 3, 589, 1, 1, 1, 1, 1),
(7839, 3, 590, 1, 1, 1, 1, 1),
(7840, 3, 591, 1, 1, 1, 1, 1),
(7841, 3, 592, 1, 1, 1, 1, 1),
(7842, 3, 593, 1, 1, 1, 1, 1),
(7843, 3, 594, 1, 1, 1, 1, 1),
(7844, 3, 595, 1, 1, 1, 1, 1),
(7845, 3, 596, 1, 1, 1, 1, 1),
(7846, 3, 597, 1, 1, 1, 1, 1),
(7847, 3, 598, 1, 1, 1, 1, 1),
(7848, 3, 599, 1, 1, 1, 1, 1),
(7849, 3, 615, 1, 1, 1, 1, 1),
(7850, 3, 199, 1, 1, 1, 1, 1),
(7851, 3, 200, 0, 0, 0, 0, 0),
(7852, 3, 600, 0, 0, 0, 0, 0),
(7853, 3, 601, 1, 1, 1, 1, 1),
(7854, 3, 557, 0, 0, 0, 0, 1),
(7855, 3, 563, 0, 0, 0, 0, 1),
(7856, 3, 564, 0, 0, 0, 0, 1),
(7857, 3, 602, 0, 0, 0, 0, 1),
(7858, 3, 603, 0, 0, 0, 0, 1),
(7859, 3, 605, 0, 0, 0, 0, 1),
(7860, 3, 604, 0, 0, 0, 0, 1),
(7861, 3, 606, 0, 0, 0, 0, 1),
(7862, 3, 607, 0, 0, 0, 0, 1),
(7863, 3, 608, 0, 0, 0, 0, 1),
(7864, 3, 609, 0, 0, 0, 0, 1),
(7865, 3, 610, 0, 0, 0, 0, 1),
(7866, 3, 611, 0, 0, 0, 0, 1),
(7867, 3, 558, 0, 0, 0, 0, 1),
(7868, 3, 565, 0, 0, 0, 0, 1),
(7869, 3, 657, 0, 0, 0, 0, 1),
(7870, 3, 623, 0, 0, 0, 0, 1),
(7871, 3, 624, 0, 0, 0, 0, 1),
(7872, 3, 625, 0, 0, 0, 0, 1),
(7873, 3, 626, 0, 0, 0, 0, 1),
(7874, 3, 631, 0, 0, 0, 0, 1),
(7875, 3, 652, 0, 0, 0, 0, 1),
(7876, 3, 559, 0, 0, 0, 0, 1),
(7877, 3, 566, 0, 0, 0, 0, 1),
(7878, 3, 627, 0, 0, 0, 0, 1),
(7879, 3, 656, 0, 0, 0, 0, 1),
(7880, 3, 628, 0, 0, 0, 0, 1),
(7881, 3, 629, 0, 0, 0, 0, 1),
(7882, 3, 630, 0, 0, 0, 0, 1),
(7883, 3, 632, 0, 0, 0, 0, 1),
(7884, 3, 651, 0, 0, 0, 0, 1),
(7885, 3, 653, 0, 0, 0, 0, 1),
(7886, 3, 655, 0, 0, 0, 0, 1),
(7887, 3, 560, 0, 0, 0, 0, 1),
(7888, 3, 612, 0, 0, 0, 0, 1),
(7889, 3, 614, 0, 0, 0, 0, 1),
(7890, 3, 617, 0, 0, 0, 0, 1),
(7891, 3, 634, 0, 0, 0, 0, 1),
(7892, 3, 635, 0, 0, 0, 0, 1),
(7893, 3, 654, 0, 0, 0, 0, 1),
(7894, 3, 561, 0, 0, 0, 0, 1),
(7895, 3, 613, 0, 0, 0, 0, 1),
(7896, 3, 633, 0, 0, 0, 0, 1),
(7897, 3, 562, 0, 0, 0, 0, 1),
(7898, 3, 636, 0, 0, 0, 0, 1),
(7899, 3, 637, 0, 0, 0, 0, 1),
(7900, 3, 638, 0, 0, 0, 0, 1),
(7901, 3, 639, 0, 0, 0, 0, 1),
(7902, 3, 640, 0, 0, 0, 0, 1),
(7903, 3, 641, 0, 0, 0, 0, 1),
(7904, 3, 642, 0, 0, 0, 0, 1),
(7905, 3, 643, 0, 0, 0, 0, 1),
(7906, 3, 644, 0, 0, 0, 0, 1),
(7907, 3, 645, 0, 0, 0, 0, 1),
(7908, 3, 646, 0, 0, 0, 0, 1),
(7909, 3, 647, 0, 0, 0, 0, 1),
(7910, 3, 648, 0, 0, 0, 0, 1),
(7911, 3, 649, 0, 0, 0, 0, 1),
(7912, 3, 650, 0, 0, 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `auuserrole`
--

CREATE TABLE `auuserrole` (
  `AURID` int(11) NOT NULL,
  `AURIDUSER` int(11) DEFAULT '0',
  `AURIDMENU` int(11) DEFAULT '0',
  `AURIDROLE` int(11) DEFAULT '0',
  `AURSTATUS` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `baktiva`
--

CREATE TABLE `baktiva` (
  `AID` int(11) NOT NULL,
  `AKODE` varchar(25) DEFAULT NULL,
  `ANAMA` varchar(150) DEFAULT NULL,
  `AKELOMPOK` int(11) DEFAULT NULL,
  `ATGLBELI` date DEFAULT NULL,
  `ANOMOR` varchar(255) DEFAULT NULL,
  `ALOKASI` varchar(255) DEFAULT NULL,
  `AJMLAKTIVA` int(11) DEFAULT '0',
  `ADIVISI` int(11) DEFAULT NULL,
  `ATIPEPENYUSUTAN` int(11) DEFAULT '0',
  `ACATATAN` varchar(255) DEFAULT NULL,
  `AUMUR` int(11) DEFAULT NULL,
  `ATGLPAKAI` date DEFAULT NULL,
  `AAKUMBEBAN` double DEFAULT '0',
  `AHARGABELI` double DEFAULT '0',
  `ANILAIBUKU` double DEFAULT '0',
  `ANILAIRESIDU` double DEFAULT '0',
  `ABEBANPERBULAN` double DEFAULT '0',
  `ATGL15` int(11) DEFAULT '0',
  `ACOAWRITEOFF` int(11) DEFAULT NULL,
  `ACOAAKTIVA` int(11) DEFAULT NULL,
  `ACOADEPRESIASIAKUM` int(11) DEFAULT NULL,
  `ACOADEPRESIASI` int(11) DEFAULT NULL,
  `APIN` int(11) DEFAULT '0',
  `AAKUMDEPRTOTAL` double DEFAULT '0',
  `ANILAIBUKUTOTAL` double DEFAULT '0',
  `AMODIFU` int(11) DEFAULT NULL,
  `ACREATED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ACREATEU` int(11) DEFAULT NULL,
  `AMODIFD` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `AAKTIVATIDAKBERWUJUD` int(11) DEFAULT '0',
  `ACUSTOM1` varchar(255) DEFAULT NULL,
  `ACUSTOM2` varchar(255) DEFAULT NULL,
  `ACUSTOM3` varchar(255) DEFAULT NULL,
  `ACUSTOM4` varchar(255) DEFAULT NULL,
  `ACUSTOM5` varchar(255) DEFAULT NULL,
  `ALOCK` smallint(6) DEFAULT '0',
  `AJUAL` smallint(6) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `baktiva`
--

INSERT INTO `baktiva` (`AID`, `AKODE`, `ANAMA`, `AKELOMPOK`, `ATGLBELI`, `ANOMOR`, `ALOKASI`, `AJMLAKTIVA`, `ADIVISI`, `ATIPEPENYUSUTAN`, `ACATATAN`, `AUMUR`, `ATGLPAKAI`, `AAKUMBEBAN`, `AHARGABELI`, `ANILAIBUKU`, `ANILAIRESIDU`, `ABEBANPERBULAN`, `ATGL15`, `ACOAWRITEOFF`, `ACOAAKTIVA`, `ACOADEPRESIASIAKUM`, `ACOADEPRESIASI`, `APIN`, `AAKUMDEPRTOTAL`, `ANILAIBUKUTOTAL`, `AMODIFU`, `ACREATED`, `ACREATEU`, `AMODIFD`, `AAKTIVATIDAKBERWUJUD`, `ACUSTOM1`, `ACUSTOM2`, `ACUSTOM3`, `ACUSTOM4`, `ACUSTOM5`, `ALOCK`, `AJUAL`) VALUES
(2, 'INV2', 'LAPTOP ASUS ', 4, '2020-03-04', '', '', 1, 0, 1, '', 48, '2020-03-04', 1854166.6, 8900000, 0, 0, 185416.66, 1, 0, 32, 33, 177, 0, 0, 0, 0, '2021-10-28 10:47:08', 0, '0000-00-00 00:00:00', 0, NULL, NULL, NULL, NULL, NULL, 0, 0),
(3, 'INV3', 'LAPTOP LENOVO ', 4, '2020-05-05', '', '', 1, 0, 1, '', 48, '2020-05-05', 3150000, 18900000, 0, 0, 393750, 1, 0, 32, 33, 177, 0, 0, 0, NULL, '2021-10-28 10:48:22', 0, '0000-00-00 00:00:00', 0, NULL, NULL, NULL, NULL, NULL, 0, 0),
(5, 'INV5', 'PRINTER EPSON L1300 A3 ', 4, '2020-09-30', '', '', 1, 0, 1, '', 48, '2020-09-30', 359374.98, 5750000, 0, 0, 119791.66, 1, 0, 32, 33, 177, 0, 0, 0, NULL, '2021-10-28 10:52:42', 0, '0000-00-00 00:00:00', 0, NULL, NULL, NULL, NULL, NULL, 0, 0),
(6, 'INV6', 'MITS. ALL NEW TRITON DOUBLE ', 1, '2020-02-11', '', '', 1, 0, 1, '', 96, '2020-02-11', 20052083.26, 175000000, 0, 0, 1822916.66, 1, 0, 30, 31, 175, 0, 0, 0, 0, '2021-10-28 10:59:18', 0, '0000-00-00 00:00:00', 0, NULL, NULL, NULL, NULL, NULL, 0, 0),
(9, 'INV9', 'TOYOTA AVANZA ', 1, '2020-07-03', '', '', 1, 0, 1, '', 96, '2020-07-03', 9375000, 150000000, 0, 0, 1562500, 1, 0, 30, 31, 175, 0, 0, 0, 0, '2021-10-28 11:02:15', 0, '0000-00-00 00:00:00', 0, NULL, NULL, NULL, NULL, NULL, 0, 0),
(10, 'INV10', 'LAPTOP ASUS X441MA', 4, '2020-10-21', '', '', 1, 0, 1, '', 48, '2020-10-21', 181250, 4350000, 0, 0, 90625, 1, 0, 32, 33, 177, 0, 0, 0, 0, '2021-10-28 11:03:05', 0, '0000-00-00 00:00:00', 0, NULL, NULL, NULL, NULL, NULL, 0, 0),
(11, 'INV11', 'PRINTER EPSON L3110', 4, '2020-10-21', '', '', 1, 0, 1, '', 48, '2020-10-21', 97916.66, 2350000, 0, 0, 48958.33, 1, 0, 32, 33, 177, 0, 0, 0, 0, '2021-10-28 11:03:50', 0, '0000-00-00 00:00:00', 0, NULL, NULL, NULL, NULL, NULL, 0, 0),
(14, 'INV14', 'BANGUNAN', 6, '2020-10-16', '', '', 1, 0, 1, '', 24, '2020-10-16', 8333333.32, 100000000, 0, 0, 4166666.66, 1, 0, 196, 197, 174, 0, 0, 0, 0, '2021-10-28 11:06:43', 0, '0000-00-00 00:00:00', 0, NULL, NULL, NULL, NULL, NULL, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `baktivafiles`
--

CREATE TABLE `baktivafiles` (
  `AFID` int(11) NOT NULL,
  `AFIDA` int(11) DEFAULT NULL,
  `AFURUTAN` int(11) DEFAULT NULL,
  `AFNAMAFILE` varchar(255) DEFAULT NULL,
  `AFKETERANGAN` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `baktivakelompok`
--

CREATE TABLE `baktivakelompok` (
  `AKID` int(11) NOT NULL,
  `AKKODE` varchar(25) DEFAULT NULL,
  `AKNAMA` varchar(150) DEFAULT NULL,
  `AKUMUR` int(11) DEFAULT NULL,
  `AKCOAAKTIVA` int(11) DEFAULT NULL,
  `AKCOADEPRESIASI` int(11) DEFAULT NULL,
  `AKCOADEPRESIASIAKUM` int(11) DEFAULT NULL,
  `AKCOAWRITEOFF` int(11) DEFAULT NULL,
  `AKCREATEU` int(11) DEFAULT NULL,
  `AKCREATED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `AKMODIFU` int(11) DEFAULT NULL,
  `AKMODIFD` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `baktivakelompok`
--

INSERT INTO `baktivakelompok` (`AKID`, `AKKODE`, `AKNAMA`, `AKUMUR`, `AKCOAAKTIVA`, `AKCOADEPRESIASI`, `AKCOADEPRESIASIAKUM`, `AKCOAWRITEOFF`, `AKCREATEU`, `AKCREATED`, `AKMODIFU`, `AKMODIFD`) VALUES
(1, 'KENDARAAN', 'KENDARAAN', 8, 30, 175, 31, 0, 1, '2020-11-25 08:23:15', 0, '2021-02-15 07:12:09'),
(3, 'MESIN', 'MESIN', 8, 36, 176, 37, 0, 1, '2021-02-15 05:06:38', 0, '2021-02-15 05:06:36'),
(4, 'INVENTARIS & PERALATAN', 'INVENTARIS & PERALATAN', 4, 32, 177, 33, 0, 1, '2021-02-15 07:11:44', 0, '2021-02-15 07:11:59'),
(6, 'BANGUNAN', 'BANGUNAN', 2, 0, 0, 0, 0, 0, '2021-10-28 10:39:01', 1, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `baktivapenyusutan`
--

CREATE TABLE `baktivapenyusutan` (
  `APID` int(11) NOT NULL,
  `APIDA` int(11) DEFAULT NULL,
  `APIDURUTAN` int(11) DEFAULT NULL,
  `APTAHUN` int(11) DEFAULT NULL,
  `APBULAN` int(11) DEFAULT NULL,
  `APNILAI` double DEFAULT '0',
  `APCUID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Triggers `baktivapenyusutan`
--
DELIMITER $$
CREATE TRIGGER `baktivapenyusutan_dell` AFTER DELETE ON `baktivapenyusutan` FOR EACH ROW BEGIN
	update baktiva set baktiva.aakumbeban = baktiva.aakumbeban-OLD.apnilai where baktiva.aid=OLD.apida; 
end
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `bbank`
--

CREATE TABLE `bbank` (
  `BID` int(11) NOT NULL,
  `BKODE` varchar(25) NOT NULL,
  `BNAMA` varchar(150) DEFAULT NULL,
  `BCREATEU` int(11) DEFAULT NULL,
  `BMODIFU` int(11) DEFAULT NULL,
  `BAKUNBANK` int(11) DEFAULT NULL,
  `BAKUNKREDIT` int(11) DEFAULT NULL,
  `BAKUNDEBIT` int(11) DEFAULT NULL,
  `BCREATED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `BMODIFD` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bbank`
--

INSERT INTO `bbank` (`BID`, `BKODE`, `BNAMA`, `BCREATEU`, `BMODIFU`, `BAKUNBANK`, `BAKUNKREDIT`, `BAKUNDEBIT`, `BCREATED`, `BMODIFD`) VALUES
(5, 'MANDIRI', 'MANDIRI', 1, 1, NULL, NULL, NULL, '2011-12-17 16:40:53', '2011-12-17 16:40:53'),
(22, 'BCA', 'BANK BCA (MELAWAI)', 1, NULL, NULL, NULL, NULL, '2022-01-04 10:40:11', '2022-01-04 10:40:11'),
(23, 'BNI', 'BANK BNI', 1, NULL, NULL, NULL, NULL, '2022-01-05 01:02:32', '2022-01-05 01:02:32');

-- --------------------------------------------------------

--
-- Table structure for table `bcabang`
--

CREATE TABLE `bcabang` (
  `CID` int(11) NOT NULL,
  `CKODE` varchar(5) DEFAULT NULL,
  `CNAMA` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `bcoa`
--

CREATE TABLE `bcoa` (
  `CID` int(11) NOT NULL,
  `CNOCOA` varchar(25) NOT NULL,
  `CNAMA` varchar(150) DEFAULT NULL,
  `CNAMA2` varchar(150) DEFAULT NULL,
  `CSUBDARI` smallint(6) NOT NULL DEFAULT '0',
  `CPARENT` int(11) DEFAULT NULL,
  `CURUTAN` int(11) DEFAULT '0',
  `CLEVEL` int(11) DEFAULT '0',
  `CUANG` int(11) DEFAULT '1',
  `CDIVISI` int(11) DEFAULT NULL,
  `CTIPE` int(11) DEFAULT '0',
  `CGD` char(1) DEFAULT 'D',
  `CCREATEU` int(11) DEFAULT NULL,
  `CMODIFU` int(11) DEFAULT NULL,
  `CMODIFD` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CSALDOAWALGL` double DEFAULT '0',
  `CBANK` int(11) DEFAULT NULL,
  `CCREATED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CNOAC` varchar(50) DEFAULT NULL,
  `CACTIVE` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bcoa`
--

INSERT INTO `bcoa` (`CID`, `CNOCOA`, `CNAMA`, `CNAMA2`, `CSUBDARI`, `CPARENT`, `CURUTAN`, `CLEVEL`, `CUANG`, `CDIVISI`, `CTIPE`, `CGD`, `CCREATEU`, `CMODIFU`, `CMODIFD`, `CSALDOAWALGL`, `CBANK`, `CCREATED`, `CNOAC`, `CACTIVE`) VALUES
(1, '1-1000', 'Aktiva Lancar', NULL, 0, 0, 0, 0, 1, 0, 0, 'G', 1, 2, '2020-09-30 22:00:00', 0, 0, '0000-00-00 00:00:00', '', 1),
(2, '1-3000', 'Aktiva Tetap', NULL, 0, 0, 0, 0, 1, 0, 5, 'G', 1, 1, '2020-09-30 22:46:36', 0, 0, '0000-00-00 00:00:00', '', 1),
(3, '2-1000', 'Hutang Lancar', NULL, 0, 0, 0, 0, 1, 0, 7, 'G', 1, 1, '2020-11-23 01:25:12', 0, 0, '0000-00-00 00:00:00', '', 1),
(4, '3-1000', 'Modal', NULL, 0, NULL, 0, 0, 1, NULL, 10, 'G', 1, 1, '2020-09-30 22:46:50', 0, NULL, '0000-00-00 00:00:00', NULL, 1),
(5, '4-1000', 'Penjualan', NULL, 0, NULL, 0, 0, 1, NULL, 11, 'G', 1, 1, '2020-09-30 22:47:21', 0, NULL, '0000-00-00 00:00:00', NULL, 1),
(6, '5-1000', 'Biaya', NULL, 0, NULL, 0, 0, 1, NULL, 13, 'G', 1, 1, '2020-09-30 22:47:02', 0, NULL, '0000-00-00 00:00:00', NULL, 1),
(7, '1-1110', 'Kas Kecil', NULL, 1, 111, 0, 0, 1, 0, 0, 'D', 1, 1, '2020-11-23 01:24:44', 0, 0, '0000-00-00 00:00:00', '', 1),
(8, '1-1120', 'Bank Mandiri (IDR)', NULL, 1, 1, 0, 0, 1, 0, 1, 'D', 1, 1, '2020-09-30 22:00:00', 0, 0, '0000-00-00 00:00:00', '', 1),
(11, '1-1150', 'Bank BNI (IDR)', NULL, 1, 1, 0, 0, 1, 0, 1, 'D', 1, 1, '2020-09-30 22:00:00', 0, 0, '0000-00-00 00:00:00', '', 1),
(14, '1-1200', 'Piutang Dagang', NULL, 1, 1, 0, 0, 1, 0, 2, 'D', 1, 1, '2020-09-30 22:00:00', 0, 0, '0000-00-00 00:00:00', '', 1),
(15, '1-1300', 'Piutang Pihak Ketiga', NULL, 1, 1, 0, 0, 1, 0, 2, 'D', 1, 1, '2020-09-30 22:00:00', 0, 0, '0000-00-00 00:00:00', '', 1),
(16, '1-1400', 'Piutang Karyawan', NULL, 1, 1, 0, 0, 1, 0, 2, 'D', 1, 1, '2020-09-30 22:00:00', 0, 0, '0000-00-00 00:00:00', '', 1),
(17, '1-1500', 'Persediaan Barang Dagangan', NULL, 1, 1, 0, 0, 1, 0, 3, 'D', 1, 0, '2020-09-30 22:00:00', 0, 0, '0000-00-00 00:00:00', '', 1),
(18, '1-1510', 'Persediaan Barang Dagangan Lain', NULL, 1, 1, 0, 0, 1, 0, 3, 'D', 1, 0, '2020-09-30 22:00:00', 0, 0, '0000-00-00 00:00:00', '', 1),
(19, '1-1600', 'Prepaid PPh Pasal 25', NULL, 1, 1, 0, 0, 1, 0, 4, 'D', 1, 1, '2020-09-30 22:00:00', 0, 0, '0000-00-00 00:00:00', '', 1),
(20, '1-1710', 'PPN Masukan Dalam Negeri', NULL, 1, 1, 0, 0, 1, 0, 4, 'D', 1, 1, '2020-09-30 22:00:00', 0, 0, '0000-00-00 00:00:00', '', 1),
(21, '1-1711', 'PPN Masukan Import', NULL, 1, 1, 0, 0, 1, 0, 4, 'D', 1, 2, '2020-09-30 22:00:00', 0, 0, '0000-00-00 00:00:00', '', 1),
(22, '1-1800', 'Prepaid PPH Pasal 23', NULL, 1, 1, 0, 1, 1, 0, 4, 'D', 1, 1, '2021-05-18 02:57:35', 0, 0, '0000-00-00 00:00:00', '', 1),
(23, '1-1910', 'Prepaid PPH Pasal 22', NULL, 0, 1, 0, 1, 1, NULL, 4, 'D', 1, 2, '2021-05-18 02:57:50', 0, NULL, '0000-00-00 00:00:00', NULL, 1),
(24, '1-1920', 'Prepaid PPH Pasal 22 Import', NULL, 1, 1, 0, 0, 1, 0, 4, 'D', 1, 1, '2020-09-30 22:00:00', 0, 0, '0000-00-00 00:00:00', '', 1),
(25, '1-2200', 'Biaya Sewa Dibayar Dimuka', NULL, 1, 1, 0, 0, 1, 0, 4, 'D', 1, 1, '2020-09-30 22:00:00', 0, 0, '0000-00-00 00:00:00', '', 1),
(26, '1-2310', 'Biaya Asuransi Dibayar Dimuka', NULL, 1, 1, 0, 0, 1, 0, 4, 'D', 1, 1, '2020-09-30 22:00:00', 0, 0, '0000-00-00 00:00:00', '', 1),
(27, '1-2500', 'Uang Muka Pembelian', NULL, 1, 1, 0, 0, 1, 0, 4, 'D', 1, 1, '2020-09-30 22:00:00', 0, 0, '0000-00-00 00:00:00', '', 1),
(28, '1-2600', 'Uang Muka Lain-Lain', NULL, 1, 1, 0, 0, 1, 0, 4, 'D', 1, 1, '2020-09-30 22:00:00', 0, 0, '0000-00-00 00:00:00', '', 1),
(29, '1-2700', 'Jaminan', NULL, 1, 1, 0, 0, 1, 0, 4, 'D', 1, 1, '2020-09-30 22:00:00', 0, 0, '0000-00-00 00:00:00', '', 1),
(30, '1-3100', 'Kendaraan', NULL, 1, 2, 0, 0, 1, 0, 5, 'D', 1, 1, '2020-09-30 22:00:00', 0, 0, '0000-00-00 00:00:00', '', 1),
(31, '1-3120', 'Akumulasi Penyusutan Kendaraan', NULL, 1, 2, 0, 0, 1, 0, 6, 'D', 1, 1, '2020-09-30 22:00:00', 0, 0, '0000-00-00 00:00:00', '', 1),
(32, '1-3210', 'Peralatan Kantor', NULL, 1, 2, 0, 0, 1, 0, 5, 'D', 1, 1, '2020-09-30 22:00:00', 0, 0, '0000-00-00 00:00:00', '', 1),
(33, '1-3220', 'Akum Penyusutan Peralatan Kantor', NULL, 1, 2, 0, 0, 1, 0, 6, 'D', 1, 1, '2020-09-30 22:00:00', 0, 0, '0000-00-00 00:00:00', '', 1),
(34, '1-3310', 'Furniture', NULL, 1, 2, 0, 0, 1, 0, 5, 'D', 1, 1, '2020-09-30 22:00:00', 0, 0, '0000-00-00 00:00:00', '', 1),
(35, '1-3320', 'Akum Peny Furniture', NULL, 1, 2, 0, 0, 1, 0, 6, 'D', 1, 1, '2020-09-30 22:00:00', 0, 0, '0000-00-00 00:00:00', '', 1),
(36, '1-3410', 'Mesin', NULL, 1, 2, 0, 0, 1, 0, 5, 'D', 1, 1, '2020-09-30 22:00:00', 0, 0, '0000-00-00 00:00:00', '', 1),
(37, '1-3420', 'Akumulasi Penyusutan Mesin', NULL, 1, 2, 0, 0, 1, 0, 6, 'D', 1, 1, '2020-09-30 22:00:00', 0, 0, '0000-00-00 00:00:00', '', 1),
(38, '2-1100', 'Hutang Dagang', NULL, 1, 3, 0, 0, 1, 0, 7, 'D', 1, 1, '2020-09-30 22:00:00', 0, 0, '0000-00-00 00:00:00', '', 1),
(39, '2-1200', 'Hutang Kepada Pihak Ketiga', NULL, 1, 3, 0, 0, 1, 0, 7, 'D', 1, 1, '2020-09-30 22:00:00', 0, 0, '0000-00-00 00:00:00', '', 1),
(40, '2-1310', 'Hutang PPh Pasal 21', NULL, 1, 3, 0, 0, 1, 0, 7, 'D', 1, 1, '2020-09-30 22:00:00', 0, 0, '0000-00-00 00:00:00', '', 1),
(41, '2-1320', 'Hutang PPh Pasal 23', NULL, 0, 3, 0, 0, 1, NULL, 7, 'D', 1, 1, '2020-09-30 22:00:00', 0, NULL, '0000-00-00 00:00:00', NULL, 1),
(42, '2-1330', 'Hutang PPh Final 4 Ayat 2', NULL, 0, 3, 0, 0, 1, NULL, 7, 'D', 1, 1, '2020-09-30 22:00:00', 0, NULL, '0000-00-00 00:00:00', NULL, 1),
(43, '2-1340', 'Hutang PPh Pasal 25', NULL, 0, 3, 0, 0, 1, NULL, 7, 'D', 1, 1, '2020-09-30 22:00:00', 0, NULL, '0000-00-00 00:00:00', NULL, 1),
(44, '2-1350', 'Hutang PPh Pasal 29', NULL, 0, 3, 0, 0, 1, NULL, 7, 'D', 1, 1, '2020-09-30 22:00:00', 0, NULL, '0000-00-00 00:00:00', NULL, 1),
(45, '2-1360', 'PPN Keluaran', NULL, 0, 3, 0, 1, 1, NULL, 8, 'D', 1, 2, '2021-05-18 02:58:14', 0, NULL, '0000-00-00 00:00:00', NULL, 1),
(46, '2-1410', 'Hutang Pokok Leasing', NULL, 0, 3, 0, 0, 1, NULL, 7, 'D', 1, 1, '2020-09-30 22:00:00', 0, NULL, '0000-00-00 00:00:00', NULL, 1),
(48, '2-1600', 'Hutang Lain-Lain', NULL, 1, 3, 0, 0, 1, 0, 7, 'D', 1, 1, '2020-09-30 22:00:00', 0, 0, '0000-00-00 00:00:00', '', 1),
(49, '3-1100', 'Modal Disetor', NULL, 1, 4, 0, 0, 1, 0, 10, 'D', 1, 1, '2020-09-30 22:00:00', 0, 0, '0000-00-00 00:00:00', '', 1),
(50, '3-8000', 'Laba ditahan', NULL, 0, 4, 0, 0, 1, NULL, 10, 'D', 1, 1, '2020-09-30 22:00:00', 0, NULL, '0000-00-00 00:00:00', NULL, 1),
(51, '3-9999', 'Historical Balancing', NULL, 0, 4, 0, 0, 1, NULL, 10, 'D', 1, 1, '2020-09-30 22:00:00', 0, NULL, '0000-00-00 00:00:00', NULL, 1),
(52, '4-1100', 'Penjualan Barang Dagangan', NULL, 1, 5, 0, 0, 1, 0, 11, 'D', 1, 0, '2020-09-30 22:00:00', 0, 0, '0000-00-00 00:00:00', '', 1),
(53, '4-2000', 'Penjualan Barang Dagangan Lain', NULL, 1, 5, 0, 0, 1, 0, 11, 'D', 1, 0, '2020-09-30 22:00:00', 0, 0, '0000-00-00 00:00:00', '', 1),
(54, '4-3000', 'Pendapatan Jasa', NULL, 1, 5, 0, 0, 1, 0, 11, 'D', 1, 1, '2020-09-30 22:00:00', 0, 0, '0000-00-00 00:00:00', '', 1),
(55, '4-4000', 'Pendapatan Lain-Lain', NULL, 1, 5, 0, 0, 1, 0, 11, 'D', 1, 1, '2020-09-30 22:00:00', 0, 0, '0000-00-00 00:00:00', '', 1),
(56, '4-5000', 'Retur Penjualan', NULL, 1, 5, 0, 0, 1, 0, 11, 'D', 1, 1, '2020-09-30 22:00:00', 0, 0, '0000-00-00 00:00:00', '', 1),
(57, '5-2000', 'Harga Pokok Penjualan', NULL, 1, 5, 0, 1, 1, 0, 12, 'D', 1, 0, '2021-05-18 03:05:44', 0, 0, '0000-00-00 00:00:00', '', 1),
(58, '5-4000', 'Biaya Pengiriman', NULL, 0, 5, 0, 1, 1, NULL, 12, 'D', 1, 2, '2021-05-18 02:58:44', 0, NULL, '0000-00-00 00:00:00', NULL, 1),
(59, '5-5000', 'Biaya Import', NULL, 0, 5, 0, 1, 1, NULL, 12, 'D', 1, 2, '2021-05-18 03:06:11', 0, NULL, '0000-00-00 00:00:00', NULL, 1),
(60, '5-6000', 'Biaya Custom Clearance', NULL, 0, 5, 0, 1, 1, NULL, 12, 'D', 1, 2, '2021-05-18 03:06:30', 0, NULL, '0000-00-00 00:00:00', NULL, 1),
(61, '5-7000', 'Barang Sample', NULL, 0, 5, 0, 1, 1, NULL, 12, 'D', 1, 2, '2021-05-18 03:06:54', 0, NULL, '0000-00-00 00:00:00', NULL, 1),
(62, '6-1100', 'Biaya Gaji Karyawan', NULL, 0, 6, 0, 0, 1, NULL, 13, 'D', 1, 1, '2020-09-30 22:00:00', 0, NULL, '0000-00-00 00:00:00', NULL, 1),
(63, '6-1200', 'Biaya Lembur', NULL, 0, 6, 0, 0, 1, NULL, 13, 'D', 1, 1, '2020-09-30 22:00:00', 0, NULL, '0000-00-00 00:00:00', NULL, 1),
(64, '6-1300', 'Biaya PPH Pasal 21', NULL, 0, 6, 0, 0, 1, NULL, 13, 'D', 1, 1, '2020-09-30 22:00:00', 0, NULL, '0000-00-00 00:00:00', NULL, 1),
(65, '6-1400', 'Biaya Asuransi JHT', NULL, 0, 6, 0, 0, 1, NULL, 13, 'D', 1, 1, '2020-09-30 22:00:00', 0, NULL, '0000-00-00 00:00:00', NULL, 1),
(66, '6-1500', 'Biaya Pengobatan', NULL, 0, 6, 0, 0, 1, NULL, 13, 'D', 1, 1, '2020-09-30 22:00:00', 0, NULL, '0000-00-00 00:00:00', NULL, 1),
(67, '6-1600', 'Biaya ATK', NULL, 0, 6, 0, 0, 1, NULL, 13, 'D', 1, 1, '2020-09-30 22:00:00', 0, NULL, '0000-00-00 00:00:00', NULL, 1),
(68, '6-1700', 'Biaya Konsultan', NULL, 0, 6, 0, 0, 1, NULL, 13, 'D', 1, 1, '2020-09-30 22:00:00', 0, NULL, '0000-00-00 00:00:00', NULL, 1),
(69, '6-1800', 'Biaya Keperluan Kantor', NULL, 0, 6, 0, 0, 1, NULL, 13, 'D', 1, 1, '2020-09-30 22:00:00', 0, NULL, '0000-00-00 00:00:00', NULL, 1),
(70, '6-1900', 'Biaya Sewa', NULL, 0, 6, 0, 0, 1, NULL, 13, 'D', 1, 1, '2020-09-30 22:00:00', 0, NULL, '0000-00-00 00:00:00', NULL, 1),
(71, '6-2100', 'Biaya Listrik', NULL, 0, 6, 0, 0, 1, NULL, 13, 'D', 1, 1, '2020-09-30 22:00:00', 0, NULL, '0000-00-00 00:00:00', NULL, 1),
(72, '6-2200', 'Biaya Telepon', NULL, 0, 6, 0, 0, 1, NULL, 13, 'D', 1, 1, '2020-09-30 22:00:00', 0, NULL, '0000-00-00 00:00:00', NULL, 1),
(73, '6-2300', 'Biaya Internet', NULL, 0, 6, 0, 0, 1, NULL, 13, 'D', 1, 1, '2020-09-30 22:00:00', 0, NULL, '0000-00-00 00:00:00', NULL, 1),
(74, '6-2500', 'Biaya Asuransi Pengiriman', NULL, 0, 6, 0, 0, 1, NULL, 13, 'D', 1, 1, '2020-09-30 22:00:00', 0, NULL, '0000-00-00 00:00:00', NULL, 1),
(75, '6-2600', 'Biaya Komisi', NULL, 0, 6, 0, 0, 1, NULL, 13, 'D', 1, 1, '2020-09-30 22:00:00', 0, NULL, '0000-00-00 00:00:00', NULL, 1),
(76, '6-2700', 'Biaya Peralatan Kantor', NULL, 0, 6, 0, 0, 1, NULL, 13, 'D', 1, 1, '2020-09-30 22:00:00', 0, NULL, '0000-00-00 00:00:00', NULL, 1),
(77, '6-2800', 'Biaya Kendaraan', NULL, 0, 6, 0, 0, 1, NULL, 13, 'D', 1, 1, '2020-09-30 22:00:00', 0, NULL, '0000-00-00 00:00:00', NULL, 1),
(78, '6-2900', 'Biaya Bensin', NULL, 0, 6, 0, 0, 1, NULL, 13, 'D', 1, 1, '2020-09-30 22:00:00', 0, NULL, '0000-00-00 00:00:00', NULL, 1),
(79, '6-3000', 'Biaya Transport/Tol/Parkir', NULL, 0, 6, 0, 0, 1, NULL, 13, 'D', 1, 1, '2020-09-30 22:00:00', 0, NULL, '0000-00-00 00:00:00', NULL, 1),
(80, '6-3100', 'Biaya Pengujian/Test Material', NULL, 0, 6, 0, 0, 1, NULL, 13, 'D', 1, 1, '2020-09-30 22:00:00', 0, NULL, '0000-00-00 00:00:00', NULL, 1),
(81, '6-3200', 'Biaya Entertain', NULL, 0, 6, 0, 0, 1, NULL, 13, 'D', 1, 1, '2020-09-30 22:00:00', 0, NULL, '0000-00-00 00:00:00', NULL, 1),
(82, '6-3300', 'Biaya Perjalanan Dinas', NULL, 0, 6, 0, 0, 1, NULL, 13, 'D', 1, 1, '2020-09-30 22:00:00', 0, NULL, '0000-00-00 00:00:00', NULL, 1),
(83, '6-3400', 'Biaya Promosi', NULL, 0, 6, 0, 0, 1, NULL, 13, 'D', 1, 1, '2020-09-30 22:00:00', 0, NULL, '0000-00-00 00:00:00', NULL, 1),
(84, '6-3500', 'Biaya Penyusutan', NULL, 0, 6, 0, 0, 1, NULL, 13, 'D', 1, 1, '2020-09-30 22:00:00', 0, NULL, '0000-00-00 00:00:00', NULL, 1),
(85, '6-3600', 'Biaya Penunjang', NULL, 0, 6, 0, 0, 1, NULL, 13, 'D', 1, 1, '2020-09-30 22:00:00', 0, NULL, '0000-00-00 00:00:00', NULL, 1),
(86, '8-1000', 'Pendapatan Cash Back', NULL, 1, 6, 0, 1, 1, 0, 14, 'D', 1, 0, '2021-05-18 03:07:17', 0, 0, '0000-00-00 00:00:00', '', 1),
(87, '8-2000', 'Selisih Kurs', NULL, 0, 6, 0, 1, 1, NULL, 14, 'D', 1, 2, '2021-05-18 03:07:30', 0, NULL, '0000-00-00 00:00:00', NULL, 1),
(88, '8-3000', 'Pembulatan', NULL, 0, 6, 0, 1, 1, NULL, 14, 'D', 1, 2, '2021-05-18 03:07:42', 0, NULL, '0000-00-00 00:00:00', NULL, 1),
(89, '8-4000', 'Lain-Lain', NULL, 0, 6, 0, 1, 1, NULL, 14, 'D', 1, 2, '2021-05-18 03:07:56', 0, NULL, '0000-00-00 00:00:00', NULL, 1),
(90, '9-1000', 'Biaya Administrasi Bank', NULL, 0, 6, 0, 1, 1, NULL, 15, 'D', 1, 2, '2021-05-18 02:59:29', 0, NULL, '0000-00-00 00:00:00', NULL, 1),
(91, '9-2000', 'Biaya Pajak Jasa Giro', NULL, 0, 6, 0, 1, 1, NULL, 15, 'D', 1, 2, '2021-05-18 03:08:14', 0, NULL, '0000-00-00 00:00:00', NULL, 1),
(92, '9-3000', 'Biaya Bunga Pinjaman', NULL, 0, 6, 0, 1, 1, NULL, 15, 'D', 1, 2, '2021-05-18 03:08:28', 0, NULL, '0000-00-00 00:00:00', NULL, 1),
(93, '9-4000', 'Biaya Discrepancy', NULL, 0, 6, 0, 1, 1, NULL, 15, 'D', 1, 2, '2021-05-18 03:08:39', 0, NULL, '0000-00-00 00:00:00', NULL, 1),
(94, '9-5000', 'Biaya STP Pajak', NULL, 0, 6, 0, 1, 1, NULL, 15, 'D', 1, 2, '2021-05-18 03:08:51', 0, NULL, '0000-00-00 00:00:00', NULL, 1),
(95, '9-7000', 'Biaya Lain-Lain', NULL, 0, 6, 0, 1, 1, NULL, 15, 'D', 1, 2, '2021-05-18 02:59:45', 0, NULL, '0000-00-00 00:00:00', NULL, 1),
(96, '6-3700', 'Biaya Perizinan', NULL, 1, 6, 0, 1, 1, NULL, 13, 'D', 2, 1, '2020-11-15 22:43:50', 0, NULL, '0000-00-00 00:00:00', NULL, 1),
(97, '1-1410', 'Piutang Lain-Lain', NULL, 1, 1, 0, 1, 1, 0, 2, 'D', 2, 1, '2021-02-18 20:00:31', 0, 0, '0000-00-00 00:00:00', '', 1),
(111, '1-1100', 'Kas', NULL, 1, 1, 0, 0, 1, 0, 0, 'G', 1, 1, '2021-09-21 10:08:14', 0, 0, '2021-09-21 10:08:14', '', 1),
(128, '1-1111', 'Kas Besar', NULL, 1, 111, 0, 0, 1, 0, 0, 'D', 1, 1, '2021-10-05 08:24:03', 0, 0, '2021-10-05 08:24:03', '', 1),
(131, '1-1599', 'Penerimaan Barang Belum Difaktur', NULL, 1, 1, 0, 0, 1, 0, 4, 'D', 1, NULL, '2021-10-10 13:22:18', 0, 0, '2021-10-10 13:22:18', '', 1),
(132, '1-1598', 'Pengiriman Barang Belum Faktur', NULL, 1, 1, 0, 0, 1, 0, 4, 'D', 1, NULL, '2021-10-14 19:35:32', 0, 0, '2021-10-14 19:35:32', '', 1),
(133, '1-1420', 'Piutang Direksi', NULL, 1, 1, 0, 0, 1, 0, 2, 'D', 0, NULL, '2021-12-14 00:44:17', 0, 0, '2021-12-14 00:44:17', '', 1),
(134, '6-3800', 'Cash Discount', NULL, 1, 6, 0, 0, 1, 0, 13, 'D', 8, 8, '2021-12-16 00:07:18', 0, 0, '2021-12-16 00:07:18', '', 1),
(135, '6-3900', 'Biaya Operasional Pameran/Event', NULL, 1, 6, 0, 0, 1, 0, 13, 'D', 8, NULL, '2021-12-16 00:46:17', 0, 0, '2021-12-16 00:46:17', '', 1),
(136, '6-4000', 'Biaya Kebersihan & Keamanan', NULL, 1, 6, 0, 0, 1, 0, 13, 'D', 8, NULL, '2021-12-16 01:05:12', 0, 0, '2021-12-16 01:05:12', '', 1),
(137, '6-4100', 'Biaya Pengembangan System', NULL, 1, 6, 0, 0, 1, 0, 13, 'D', 8, NULL, '2021-12-16 01:12:30', 0, 0, '2021-12-16 01:12:30', '', 1),
(138, '6-4200', 'Biaya Peralatan Online', NULL, 1, 6, 0, 0, 1, 0, 13, 'D', 8, NULL, '2021-12-21 01:12:31', 0, 0, '2021-12-21 01:12:31', '', 1),
(139, '9-6000', 'Biaya Pajak Bank', NULL, 1, 6, 0, 0, 1, 0, 13, 'D', 8, NULL, '2021-12-21 01:51:35', 0, 0, '2021-12-21 01:51:35', '', 1),
(140, '6-4300', 'Biaya Pemeliharaan Gedung', NULL, 1, 6, 0, 0, 1, 0, 13, 'D', 8, NULL, '2021-12-21 02:06:05', 0, 0, '2021-12-21 02:06:05', '', 1),
(141, '6-4400', 'Biaya Foto Copy & Print', NULL, 1, 6, 0, 0, 1, 0, 13, 'D', 8, 8, '2021-12-21 02:11:50', 0, 0, '2021-12-21 02:11:50', '', 1),
(142, '6-4500', 'Biaya Beban Direksi', NULL, 1, 6, 0, 0, 1, 0, 13, 'D', 8, NULL, '2021-12-21 02:21:24', 0, 0, '2021-12-21 02:21:24', '', 1),
(143, '6-4600', 'Biaya Matrai', NULL, 1, 6, 0, 0, 1, 0, 13, 'D', 8, NULL, '2021-12-21 02:30:38', 0, 0, '2021-12-21 02:30:38', '', 1),
(144, '6-4700', 'Biaya Makan & Minum', NULL, 1, 6, 0, 0, 1, 0, 13, 'D', 8, NULL, '2021-12-21 02:34:42', 0, 0, '2021-12-21 02:34:42', '', 1),
(145, '6-4800', 'Biaya Operasional Balap', NULL, 1, 6, 0, 0, 1, 0, 13, 'D', 8, NULL, '2021-12-21 20:12:48', 0, 0, '2021-12-21 20:12:48', '', 1),
(146, '6-4900', 'Biaya Operasional Kanvas', NULL, 1, 6, 0, 0, 1, 0, 13, 'D', 8, NULL, '2022-01-03 21:40:10', 0, 0, '2022-01-03 21:40:10', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `bcoagrup`
--

CREATE TABLE `bcoagrup` (
  `CGID` int(11) NOT NULL,
  `CGNAMA` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bcoagrup`
--

INSERT INTO `bcoagrup` (`CGID`, `CGNAMA`) VALUES
(0, 'Kas'),
(1, 'Bank'),
(2, 'Piutang'),
(3, 'Persediaan'),
(4, 'Aktiva Lancar Lainnya'),
(5, 'Aktiva Tetap'),
(6, 'Akumulasi Penyusutan'),
(7, 'Hutang'),
(8, 'Hutang Lancar Lainnya'),
(9, 'Hutang Jangka Panjang'),
(10, 'Modal'),
(11, 'Pendapatan'),
(12, 'Harga Pokok Penjualan'),
(13, 'Biaya'),
(14, 'Pendapatan Lain-Lain'),
(15, 'Biaya Lain-Lain');

-- --------------------------------------------------------

--
-- Table structure for table `bcoasa`
--

CREATE TABLE `bcoasa` (
  `CSID` int(11) NOT NULL,
  `CSCOA` int(11) DEFAULT NULL,
  `CSKONTAK` int(11) DEFAULT NULL,
  `CSJUMLAH` double DEFAULT '0',
  `CSTANGGAL` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `bdivisi`
--

CREATE TABLE `bdivisi` (
  `DID` int(11) NOT NULL,
  `DKODE` varchar(25) NOT NULL,
  `DNAMA` varchar(150) DEFAULT NULL,
  `DCREATEU` int(11) DEFAULT NULL,
  `DMODIFU` int(11) DEFAULT NULL,
  `DCREATED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `DMODIFD` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `bgudang`
--

CREATE TABLE `bgudang` (
  `GID` int(11) NOT NULL,
  `GKODE` varchar(25) NOT NULL,
  `GNAMA` varchar(150) DEFAULT NULL,
  `GDIVISI` int(11) DEFAULT NULL,
  `GALAMAT1` varchar(255) DEFAULT NULL,
  `GALAMAT2` varchar(255) DEFAULT NULL,
  `GKOTA` varchar(25) DEFAULT NULL,
  `GPROPINSI` varchar(25) DEFAULT NULL,
  `GNEGARA` varchar(25) DEFAULT NULL,
  `GTELP` varchar(25) DEFAULT NULL,
  `GFAX` varchar(25) DEFAULT NULL,
  `GKONTAK` varchar(25) DEFAULT NULL,
  `GCREATEU` int(11) DEFAULT NULL,
  `GMODIFU` int(11) DEFAULT NULL,
  `GCREATED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `GDEFAULT` smallint(6) DEFAULT '0',
  `GMODIFD` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `GAKTIF` smallint(6) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bgudang`
--

INSERT INTO `bgudang` (`GID`, `GKODE`, `GNAMA`, `GDIVISI`, `GALAMAT1`, `GALAMAT2`, `GKOTA`, `GPROPINSI`, `GNEGARA`, `GTELP`, `GFAX`, `GKONTAK`, `GCREATEU`, `GMODIFU`, `GCREATED`, `GDEFAULT`, `GMODIFD`, `GAKTIF`) VALUES
(13, 'WAREHOUSE', 'WAREHOUSE', 0, '', NULL, '', '', '', '', '', '', 0, 0, '2021-10-28 12:48:05', 1, '0000-00-00 00:00:00', 0),
(14, 'DEPO-A', 'DEPO A', 0, '', NULL, '', '', '', '', '', '', 0, NULL, '2022-01-04 15:02:59', 0, '2022-01-04 15:02:59', 0);

-- --------------------------------------------------------

--
-- Table structure for table `bitem`
--

CREATE TABLE `bitem` (
  `IID` int(11) NOT NULL,
  `IKODE` varchar(25) NOT NULL,
  `INAMA` varchar(150) DEFAULT NULL,
  `INAMA2` varchar(150) DEFAULT NULL,
  `ISUBITEM` smallint(6) DEFAULT '0',
  `IPARENT` int(11) DEFAULT NULL,
  `IGD` char(1) DEFAULT 'D',
  `IUANG` int(11) DEFAULT NULL,
  `ISATUAN` int(11) NOT NULL,
  `ISATUAND` int(11) NOT NULL,
  `IJENISITEM` int(11) DEFAULT '0',
  `ITIPEITEM` int(11) DEFAULT NULL,
  `ISTATUS` int(11) DEFAULT '1',
  `ISTOCKMINIMAL` double DEFAULT '0',
  `ISTOCKMAKSIMAL` double DEFAULT '0',
  `ISTOCKTOTAL` double DEFAULT '0',
  `ISTOCKREORDER` double DEFAULT '0',
  `IHARGABELI` double DEFAULT '0',
  `IHARGAJUAL1` double DEFAULT '0',
  `IHARGAJUAL2` double DEFAULT '0',
  `IHARGAJUAL3` double DEFAULT '0',
  `IHARGAJUAL4` double DEFAULT '0',
  `IHARGAJUAL5` double DEFAULT '0',
  `IHARGAJUAL6` double DEFAULT '0',
  `IBARCODE` varchar(150) DEFAULT NULL,
  `ICOAPERSEDIAAN` int(11) DEFAULT NULL,
  `ICOAPENDAPATAN` int(11) DEFAULT NULL,
  `ICOAHPP` int(11) DEFAULT NULL,
  `ICUSTOM1` varchar(150) DEFAULT NULL,
  `ICUSTOM2` varchar(150) DEFAULT NULL,
  `ICUSTOM3` varchar(150) DEFAULT NULL,
  `ICUSTOM4` varchar(150) DEFAULT NULL,
  `ICUSTOM5` varchar(150) DEFAULT NULL,
  `ICUSTOM6` varchar(150) DEFAULT NULL,
  `ICUSTOM7` varchar(150) DEFAULT NULL,
  `ICUSTOM8` varchar(150) DEFAULT NULL,
  `ICUSTOM9` varchar(150) DEFAULT NULL,
  `ICUSTOM10` varchar(150) DEFAULT NULL,
  `ICREATEU` int(11) DEFAULT NULL,
  `IMODIFU` int(11) DEFAULT NULL,
  `ICREATED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `IMERK` int(11) DEFAULT NULL,
  `IKATEGORI` int(11) DEFAULT NULL,
  `ISUBKATEGORI` int(11) DEFAULT NULL,
  `IUMUR` double DEFAULT '0',
  `IKELOMPOKHARGA` int(11) DEFAULT NULL,
  `IKECEPATANJUAL` int(11) DEFAULT NULL,
  `ITANGGALRUBAHHARGA` date DEFAULT NULL,
  `IMODEL` varchar(150) DEFAULT NULL,
  `IJENIS` int(11) DEFAULT NULL,
  `INOURUT` int(11) DEFAULT '0',
  `IKODELAMA` varchar(25) DEFAULT NULL,
  `INAMALAMA` varchar(150) DEFAULT NULL,
  `ISALDOAWAL` double DEFAULT '0',
  `IDISKON` double DEFAULT '0',
  `ISALDOX` double DEFAULT '0',
  `IMODIFD` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ISTOKPG` double DEFAULT '0',
  `ISTOKCP` double DEFAULT '0',
  `ISTOKDP` double DEFAULT '0',
  `ISTOKKM` double DEFAULT '0',
  `ISTOKJG` double DEFAULT '0',
  `IPG` smallint(6) DEFAULT '1',
  `ICP` smallint(6) DEFAULT '1',
  `IDP` smallint(6) DEFAULT '1',
  `IKM` smallint(6) DEFAULT '1',
  `IJG` smallint(6) DEFAULT '1',
  `ITANGGAL1` date DEFAULT NULL,
  `ITANGGAL2` date DEFAULT NULL,
  `ISTOKOL` double DEFAULT '0',
  `IOL` smallint(6) DEFAULT '1',
  `ITARIKONLINE` smallint(6) DEFAULT '0',
  `IBA` smallint(6) DEFAULT '1',
  `IBB` smallint(6) DEFAULT '1',
  `ISTOKBA` double DEFAULT '0',
  `ISTOKBB` double DEFAULT '0',
  `IQTYPERBOX` double DEFAULT '1',
  `IKOMISIMARKETING` smallint(6) DEFAULT '0',
  `IKOMISIPAKET` smallint(6) DEFAULT '0',
  `IJENISITEMCOA` int(11) DEFAULT '0',
  `IGG` smallint(6) DEFAULT '1',
  `ISTOKGG` double DEFAULT '0',
  `IBOLEHMINUS` smallint(6) DEFAULT '0',
  `IPAJAKIN` int(11) DEFAULT NULL,
  `IPAJAKOUT` int(11) DEFAULT NULL,
  `IBERAT` double DEFAULT '0',
  `ISERIAL` int(11) DEFAULT NULL,
  `IHARGAPASAR` double DEFAULT '0',
  `ILUAS` double DEFAULT '0',
  `IHPPAKHIR` double DEFAULT '0',
  `IJENISBARANG` int(11) DEFAULT NULL,
  `INAMAA1` varchar(150) DEFAULT NULL,
  `INAMAA2` varchar(150) DEFAULT NULL,
  `INAMAA3` varchar(150) DEFAULT NULL,
  `INAMAA4` varchar(150) DEFAULT NULL,
  `INAMAA5` varchar(150) DEFAULT NULL,
  `INAMAA6` varchar(150) DEFAULT NULL,
  `INAMAA7` varchar(150) DEFAULT NULL,
  `INAMAA8` varchar(150) DEFAULT NULL,
  `INAMAA9` varchar(150) DEFAULT NULL,
  `INAMAA10` varchar(150) DEFAULT NULL,
  `IKELOMPOK` smallint(6) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bitem`
--

INSERT INTO `bitem` (`IID`, `IKODE`, `INAMA`, `INAMA2`, `ISUBITEM`, `IPARENT`, `IGD`, `IUANG`, `ISATUAN`, `ISATUAND`, `IJENISITEM`, `ITIPEITEM`, `ISTATUS`, `ISTOCKMINIMAL`, `ISTOCKMAKSIMAL`, `ISTOCKTOTAL`, `ISTOCKREORDER`, `IHARGABELI`, `IHARGAJUAL1`, `IHARGAJUAL2`, `IHARGAJUAL3`, `IHARGAJUAL4`, `IHARGAJUAL5`, `IHARGAJUAL6`, `IBARCODE`, `ICOAPERSEDIAAN`, `ICOAPENDAPATAN`, `ICOAHPP`, `ICUSTOM1`, `ICUSTOM2`, `ICUSTOM3`, `ICUSTOM4`, `ICUSTOM5`, `ICUSTOM6`, `ICUSTOM7`, `ICUSTOM8`, `ICUSTOM9`, `ICUSTOM10`, `ICREATEU`, `IMODIFU`, `ICREATED`, `IMERK`, `IKATEGORI`, `ISUBKATEGORI`, `IUMUR`, `IKELOMPOKHARGA`, `IKECEPATANJUAL`, `ITANGGALRUBAHHARGA`, `IMODEL`, `IJENIS`, `INOURUT`, `IKODELAMA`, `INAMALAMA`, `ISALDOAWAL`, `IDISKON`, `ISALDOX`, `IMODIFD`, `ISTOKPG`, `ISTOKCP`, `ISTOKDP`, `ISTOKKM`, `ISTOKJG`, `IPG`, `ICP`, `IDP`, `IKM`, `IJG`, `ITANGGAL1`, `ITANGGAL2`, `ISTOKOL`, `IOL`, `ITARIKONLINE`, `IBA`, `IBB`, `ISTOKBA`, `ISTOKBB`, `IQTYPERBOX`, `IKOMISIMARKETING`, `IKOMISIPAKET`, `IJENISITEMCOA`, `IGG`, `ISTOKGG`, `IBOLEHMINUS`, `IPAJAKIN`, `IPAJAKOUT`, `IBERAT`, `ISERIAL`, `IHARGAPASAR`, `ILUAS`, `IHPPAKHIR`, `IJENISBARANG`, `INAMAA1`, `INAMAA2`, `INAMAA3`, `INAMAA4`, `INAMAA5`, `INAMAA6`, `INAMAA7`, `INAMAA8`, `INAMAA9`, `INAMAA10`, `IKELOMPOK`) VALUES
(5, '1-CST-0001', 'COVER SEAT /SARUNG JOK AHRS', NULL, 0, NULL, 'D', NULL, 1, 1, 0, 0, 0, 0, 0, 100, 0, 30000, 80000, 72000, 64000, 56000, 0, 0, '0318506860001', 17, 52, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-11-08 23:28:14', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, '2021-11-08 23:28:14', 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, NULL, NULL, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(6, '1-CST-0002', 'COVER SEAT /SARUNG JOK BESAR AHRS', NULL, 0, NULL, 'D', NULL, 1, 1, 0, 0, 0, 0, 0, 45, 0, 40000, 100000, 90000, 80000, 70000, 0, 0, '0332509620001', 17, 52, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-11-08 23:28:14', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, '2021-11-08 23:28:14', 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, NULL, NULL, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(7, '1-FS-0001', 'FOOT STEP BLADE D/D DURAL SILVER AHRS', NULL, 0, NULL, 'D', NULL, 2, 2, 0, 0, 0, 0, 0, 50, 0, 100000, 185000, 166500, 148000, 129500, 0, 0, '0307501240002', 17, 52, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-11-08 23:28:14', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, '2021-11-08 23:28:14', 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, NULL, NULL, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(8, '1-FS-0002', 'FOOT STEP BLADE DURAL SILVER AHRS', NULL, 0, NULL, 'D', NULL, 2, 2, 0, 0, 0, 0, 0, 50, 0, 100000, 185000, 166500, 148000, 129500, 0, 0, '0307501240001', 17, 52, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-11-08 23:28:14', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, '2021-11-08 23:28:14', 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, NULL, NULL, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(9, '1-FS-0003', 'FOOT STEP F1ZR DURAL SILVER AHRS', NULL, 0, NULL, 'D', NULL, 1, 1, 0, 0, 0, 0, 0, 70, 0, 100000, 185000, 166500, 148000, 129500, 0, 0, '0307503100001', 17, 52, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-11-08 23:28:14', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, '2021-11-08 23:28:14', 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, NULL, NULL, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(10, '1-FS-0004', 'FOOT STEP JUPITER MX 2011 DURAL SILVER AHRS', NULL, 0, NULL, 'D', NULL, 2, 2, 0, 0, 0, 0, 0, 50, 0, 100000, 185000, 166500, 148000, 129500, 0, 0, '0307504450003', 17, 52, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-11-08 23:28:14', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, '2021-11-08 23:28:14', 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, NULL, NULL, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(11, '1-FS-0005', 'FOOT STEP JUPITER MX DURAL SILVER AHRS', NULL, 0, NULL, 'D', NULL, 1, 1, 0, 0, 0, 0, 0, 0, 0, 100000, 185000, 166500, 148000, 129500, 0, 0, '0307504450001', 17, 52, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-11-08 23:28:14', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, '2021-11-08 23:28:14', 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, NULL, NULL, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(12, '1-FS-0006', 'FOOT STEP JUPITER Z DURAL SILVER AHRS', NULL, 0, NULL, 'D', NULL, 1, 1, 0, 0, 0, 0, 0, 0, 0, 100000, 185000, 166500, 148000, 129500, 0, 0, '0307504480002', 17, 52, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-11-08 23:28:14', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, '2021-11-08 23:28:14', 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, NULL, NULL, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(13, '1-FS-0007', 'FOOT STEP JUPITER Z NEW DURAL SILVER AHRS', NULL, 0, NULL, 'D', NULL, 2, 2, 0, 0, 0, 0, 0, 0, 0, 100000, 185000, 166500, 148000, 129500, 0, 0, '0307504490001', 17, 52, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-11-08 23:28:14', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, '2021-11-08 23:28:14', 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, NULL, NULL, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(14, '1-FS-0008', 'FOOT STEP KHARISMA DURAL SILVER AHRS', NULL, 0, NULL, 'D', NULL, 1, 1, 0, 0, 0, 0, 0, 0, 0, 100000, 185000, 166500, 148000, 129500, 0, 0, '0307504670001', 17, 52, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-11-08 23:28:14', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, '2021-11-08 23:28:14', 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, NULL, NULL, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(15, '1-FS-0009', 'FOOT STEP MX KING BILLET AHRS', NULL, 0, NULL, 'D', NULL, 2, 2, 0, 0, 0, 0, 0, 0, 0, 550000, 900000, 720000, 0, 0, 0, 0, '0307511940002', 17, 52, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-11-08 23:28:14', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, '2021-11-08 23:28:14', 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, NULL, NULL, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(16, '1-FS-0010', 'FOOT STEP MX KING DURAL SILVER AHRS', NULL, 0, NULL, 'D', NULL, 2, 2, 0, 0, 0, 0, 0, 0, 0, 100000, 185000, 166500, 148000, 129500, 0, 0, '0307511940001', 17, 52, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-11-08 23:28:14', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, '2021-11-08 23:28:14', 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, NULL, NULL, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(17, '1-FS-0011', 'FOOT STEP RX KING DURAL SILVER AHRS', NULL, 0, NULL, 'D', NULL, 2, 2, 0, 0, 0, 0, 0, 0, 0, 100000, 185000, 166500, 148000, 129500, 0, 0, '0307507290001', 17, 52, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-11-08 23:28:14', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, '2021-11-08 23:28:14', 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, NULL, NULL, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(18, '1-FS-0012', 'FOOT STEP SATRIA D/D DURAL SILVER AHRS', NULL, 0, NULL, 'D', NULL, 1, 1, 0, 0, 0, 0, 0, 0, 0, 100000, 185000, 166500, 148000, 129500, 0, 0, '0307507380003', 17, 52, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-11-08 23:28:14', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, '2021-11-08 23:28:14', 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, NULL, NULL, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(19, '1-FS-0013', 'FOOT STEP SATRIA DURAL SILVER AHRS', NULL, 0, NULL, 'D', NULL, 1, 1, 0, 0, 0, 0, 0, 0, 0, 100000, 185000, 166500, 148000, 129500, 0, 0, '0307507370001', 17, 52, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-11-08 23:28:14', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, '2021-11-08 23:28:14', 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, NULL, NULL, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(20, '1-FS-0014', 'FOOT STEP SATRIA F150 DURAL SILVER AHRS', NULL, 0, NULL, 'D', NULL, 1, 1, 0, 0, 0, 0, 0, 0, 0, 100000, 185000, 166500, 148000, 129500, 0, 0, '0307507390001', 17, 52, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-11-08 23:28:14', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, '2021-11-08 23:28:14', 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, NULL, NULL, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(21, '1-FS-0015', 'FOOT STEP SONIC / GTR BILLET AHRS', NULL, 0, NULL, 'D', NULL, 2, 2, 0, 0, 0, 0, 0, 0, 0, 550000, 950000, 760000, 0, 0, 0, 0, '0307508090002', 17, 52, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-11-08 23:28:14', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, '2021-11-08 23:28:14', 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, NULL, NULL, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(22, '1-FS-0016', 'FOOT STEP SUPRA DURAL SILVER AHRS', NULL, 0, NULL, 'D', NULL, 1, 1, 0, 0, 0, 0, 0, 0, 0, 100000, 185000, 166500, 148000, 129500, 0, 0, '0307508590001', 17, 52, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-11-08 23:28:14', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, '2021-11-08 23:28:14', 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, NULL, NULL, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(23, '1-FS-0017', 'FOOT STEP SUPRA X 125 D/D DURAL SILVER AHRS', NULL, 0, NULL, 'D', NULL, 1, 1, 0, 0, 0, 0, 0, 0, 0, 100000, 185000, 166500, 148000, 129500, 0, 0, '0307508610002', 17, 52, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-11-08 23:28:14', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, '2021-11-08 23:28:14', 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, NULL, NULL, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(24, '1-FS-0018', 'FOOT STEP SUPRA X-125 DURAL SILVER AHRS', NULL, 0, NULL, 'D', NULL, 1, 1, 0, 0, 0, 0, 0, 0, 0, 100000, 185000, 166500, 148000, 129500, 0, 0, '0307508610003', 17, 52, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-11-08 23:28:14', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, '2021-11-08 23:28:14', 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, NULL, NULL, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(25, '1-FS-0019', 'FOOT STEP UNIVERSAL BILLET AHRS', NULL, 0, NULL, 'D', NULL, 2, 2, 0, 0, 0, 0, 0, 0, 0, 550000, 800000, 675000, 0, 0, 0, 0, '0307509620005', 17, 52, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-11-08 23:28:14', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, '2021-11-08 23:28:14', 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, NULL, NULL, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(26, '1-JS-0001', 'JACK STAND CROSS  AHRS', NULL, 0, NULL, 'D', NULL, 1, 1, 0, 0, 0, 0, 0, 0, 0, 120000, 200000, 160000, 140000, 0, 0, 0, '0310602050005', 17, 52, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-11-08 23:28:14', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, '2021-11-08 23:28:14', 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, NULL, NULL, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(27, '1-JS-0002', 'JACK STAND MOTOR AHRS', NULL, 0, NULL, 'D', NULL, 1, 1, 0, 0, 0, 0, 0, 0, 0, 395000, 790000, 632000, 632000, 553000, 0, 0, '0310602050001', 17, 52, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-11-08 23:28:14', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, '2021-11-08 23:28:14', 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, NULL, NULL, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(28, '1-KN-0001', 'KNALPOT F4 STANDBLAST 02 STD CRF 150 AHRS', NULL, 0, NULL, 'D', NULL, 1, 1, 0, 0, 0, 0, 0, 0, 0, 650000, 1300000, 1170000, 1040000, 910000, 0, 0, '0313611990003', 17, 52, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-11-08 23:28:14', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, '2021-11-08 23:28:14', 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, NULL, NULL, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(29, '1-KN-0002', 'KNALPOT F4 STANDBLAST 02 STD KLX 150 AHRS', NULL, 0, NULL, 'D', NULL, 1, 1, 0, 0, 0, 0, 0, 0, 0, 650000, 1300000, 1170000, 1040000, 910000, 0, 0, '0313611990001', 17, 52, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-11-08 23:28:14', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, '2021-11-08 23:28:14', 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, NULL, NULL, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(30, '1-KN-0003', 'KNALPOT F4 STANDBLAST 02 STD WR 155 AHRS', NULL, 0, NULL, 'D', NULL, 1, 1, 0, 0, 0, 0, 0, 0, 0, 650000, 1300000, 1170000, 1040000, 910000, 0, 0, '0313611990005', 17, 52, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-11-08 23:28:14', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, '2021-11-08 23:28:14', 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, NULL, NULL, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(31, '1-KN-0004', 'KNALPOT F4 STANDBLAST 02 UB CRF 150 AHRS', NULL, 0, NULL, 'D', NULL, 1, 1, 0, 0, 0, 0, 0, 0, 0, 650000, 1400000, 1260000, 1120000, 980000, 0, 0, '0313611990004', 17, 52, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-11-08 23:28:14', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, '2021-11-08 23:28:14', 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, NULL, NULL, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(32, '1-KN-0005', 'KNALPOT F4 STANDBLAST 02 UB KLX 150 AHRS', NULL, 0, NULL, 'D', NULL, 1, 1, 0, 0, 0, 0, 0, 0, 0, 650000, 1400000, 1260000, 1120000, 980000, 0, 0, '0313611990002', 17, 52, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-11-08 23:28:14', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, '2021-11-08 23:28:14', 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, NULL, NULL, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(33, '1-KN-0006', 'KNALPOT F4 STANDBLAST 02 UB WR 155 AHRS', NULL, 0, NULL, 'D', NULL, 1, 1, 0, 0, 0, 0, 0, 0, 0, 650000, 1400000, 1260000, 1120000, 980000, 0, 0, '0313611990006', 17, 52, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-11-08 23:28:14', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, '2021-11-08 23:28:14', 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, NULL, NULL, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(34, '1-KN-0007', 'KNALPOT F5 STD CRF 150 AHRS', NULL, 0, NULL, 'D', NULL, 1, 1, 0, 0, 0, 0, 0, 0, 0, 550000, 1100000, 990000, 880000, 770000, 0, 0, '0313612170003', 17, 52, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-11-08 23:28:14', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, '2021-11-08 23:28:14', 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, NULL, NULL, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(35, '1-KN-0008', 'KNALPOT F5 STD KLX 150 AHRS', NULL, 0, NULL, 'D', NULL, 1, 1, 0, 0, 0, 0, 0, 0, 0, 550000, 1100000, 990000, 880000, 770000, 0, 0, '0313612170001', 17, 52, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-11-08 23:28:14', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, '2021-11-08 23:28:14', 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, NULL, NULL, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(36, '1-KN-0009', 'KNALPOT F5 STD WR 155 AHRS', NULL, 0, NULL, 'D', NULL, 1, 1, 0, 0, 0, 0, 0, 0, 0, 550000, 1100000, 990000, 880000, 770000, 0, 0, '0313612170005', 17, 52, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-11-08 23:28:14', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, '2021-11-08 23:28:14', 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, NULL, NULL, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(37, '1-KN-0010', 'KNALPOT F5 UB CRF 150 AHRS', NULL, 0, NULL, 'D', NULL, 1, 1, 0, 0, 0, 0, 0, 0, 0, 550000, 1200000, 1080000, 960000, 840000, 0, 0, '0313612170004', 17, 52, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-11-08 23:28:14', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, '2021-11-08 23:28:14', 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, NULL, NULL, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(38, '1-KN-0011', 'KNALPOT F5 UB KLX 150 AHRS', NULL, 0, NULL, 'D', NULL, 1, 1, 0, 0, 0, 0, 0, 0, 0, 550000, 1200000, 1080000, 960000, 840000, 0, 0, '0313612170002', 17, 52, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-11-08 23:28:14', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, '2021-11-08 23:28:14', 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, NULL, NULL, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(39, '1-KN-0012', 'KNALPOT F5 UB WR 155 AHRS', NULL, 0, NULL, 'D', NULL, 1, 1, 0, 0, 0, 0, 0, 0, 0, 550000, 1200000, 1080000, 960000, 840000, 0, 0, '0313612170006', 17, 52, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-11-08 23:28:14', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, '2021-11-08 23:28:14', 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, NULL, NULL, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(40, '1-MJ-0001', 'MAIN JET F1ZR #130 AHRS', NULL, 0, NULL, 'D', NULL, 1, 1, 0, 0, 0, 0, 0, 0, 0, 7050, 22000, 15400, 13200, 11000, 0, 0, '0314703100001', 17, 52, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-11-08 23:28:14', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, '2021-11-08 23:28:14', 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, NULL, NULL, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(41, '1-MJ-0002', 'MAIN JET F1ZR #135 AHRS', NULL, 0, NULL, 'D', NULL, 1, 1, 0, 0, 0, 0, 0, 0, 0, 7050, 22000, 15400, 13200, 11000, 0, 0, '0314703100002', 17, 52, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-11-08 23:28:14', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, '2021-11-08 23:28:14', 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, NULL, NULL, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(42, '1-MJ-0003', 'MAIN JET MIO # 125 AHRS', NULL, 0, NULL, 'D', NULL, 1, 1, 0, 0, 0, 0, 0, 0, 0, 7050, 22000, 15400, 13200, 11000, 0, 0, '0314705590010', 17, 52, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-11-08 23:28:14', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, '2021-11-08 23:28:14', 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, NULL, NULL, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(43, '1-MJ-0004', 'MAIN JET MIO # 130 AHRS', NULL, 0, NULL, 'D', NULL, 1, 1, 0, 0, 0, 0, 0, 0, 0, 7050, 22000, 15400, 13200, 11000, 0, 0, '0314705590011', 17, 52, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-11-08 23:28:14', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, '2021-11-08 23:28:14', 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, NULL, NULL, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(44, '1-MJ-0005', 'MAIN JET MIO # 135 AHRS', NULL, 0, NULL, 'D', NULL, 1, 1, 0, 0, 0, 0, 0, 0, 0, 7050, 22000, 15400, 13200, 11000, 0, 0, '0314705590006', 17, 52, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-11-08 23:28:14', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, '2021-11-08 23:28:14', 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, NULL, NULL, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(45, '1-MJ-0006', 'MAIN JET MIO # 140 AHRS', NULL, 0, NULL, 'D', NULL, 1, 1, 0, 0, 0, 0, 0, 0, 0, 7050, 22000, 15400, 13200, 11000, 0, 0, '0314705590007', 17, 52, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-11-08 23:28:14', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, '2021-11-08 23:28:14', 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, NULL, NULL, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(46, '1-MJ-0007', 'MAIN JET MIO # 145 AHRS', NULL, 0, NULL, 'D', NULL, 1, 1, 0, 0, 0, 0, 0, 0, 0, 7050, 22000, 15400, 13200, 11000, 0, 0, '0314705590008', 17, 52, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-11-08 23:28:14', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, '2021-11-08 23:28:14', 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, NULL, NULL, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(47, '1-MJ-0008', 'MAIN JET MIO # 150 AHRS', NULL, 0, NULL, 'D', NULL, 1, 1, 0, 0, 0, 0, 0, 0, 0, 7050, 22000, 15400, 13200, 11000, 0, 0, '0314705590009', 17, 52, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-11-08 23:28:14', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, '2021-11-08 23:28:14', 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, NULL, NULL, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(48, '1-MJ-0009', 'MAIN JET MIO #100 AHRS', NULL, 0, NULL, 'D', NULL, 1, 1, 0, 0, 0, 0, 0, 0, 0, 7050, 22000, 15400, 13200, 11000, 0, 0, '0314705590002', 17, 52, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-11-08 23:28:14', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, '2021-11-08 23:28:14', 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, NULL, NULL, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(49, '1-MJ-0010', 'MAIN JET MIO #105 AHRS', NULL, 0, NULL, 'D', NULL, 1, 1, 0, 0, 0, 0, 0, 0, 0, 7050, 22000, 15400, 13200, 11000, 0, 0, '0314705590001', 17, 52, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-11-08 23:28:14', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, '2021-11-08 23:28:14', 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, NULL, NULL, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(50, '1-MJ-0011', 'MAIN JET MIO #110 AHRS', NULL, 0, NULL, 'D', NULL, 1, 1, 0, 0, 0, 0, 0, 0, 0, 7050, 22000, 15400, 13200, 11000, 0, 0, '0314705590005', 17, 52, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-11-08 23:28:14', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, '2021-11-08 23:28:14', 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, NULL, NULL, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `bitemfiles`
--

CREATE TABLE `bitemfiles` (
  `BFID` int(11) NOT NULL,
  `BFIDI` int(11) DEFAULT NULL,
  `BFURUTAN` int(11) DEFAULT NULL,
  `BFNAMAFILE` varchar(255) DEFAULT NULL,
  `BFKETERANGAN` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `bitemkategori`
--

CREATE TABLE `bitemkategori` (
  `IKID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `bitempenyusun`
--

CREATE TABLE `bitempenyusun` (
  `IPID` int(11) NOT NULL,
  `IPIDB` int(11) DEFAULT NULL,
  `IPIDBB` int(11) DEFAULT NULL,
  `IPIDQTY` double DEFAULT '0',
  `IPIDSATUAN` int(11) DEFAULT NULL,
  `IPIDURUTAN` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `bitemrak`
--

CREATE TABLE `bitemrak` (
  `IRID` int(11) NOT NULL,
  `IRITEM` int(11) DEFAULT NULL,
  `IRGUDANG` int(11) DEFAULT NULL,
  `IRRAK` varchar(100) DEFAULT NULL,
  `IRKETERANGAN` varchar(100) DEFAULT NULL,
  `IRURUTAN` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `bitemstoktemp`
--

CREATE TABLE `bitemstoktemp` (
  `ISID` int(11) NOT NULL,
  `ISIDITEM` int(11) DEFAULT NULL,
  `ISKODE` varchar(25) DEFAULT NULL,
  `ISNAMA` varchar(150) DEFAULT NULL,
  `ISTIPE` int(11) DEFAULT NULL,
  `ISGUDANG` int(11) DEFAULT NULL,
  `ISSALDOAWAL` double DEFAULT '0',
  `ISMASUK` double DEFAULT '0',
  `ISKELUAR` double DEFAULT '0',
  `ISAKHIR` double DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `bitemsub`
--

CREATE TABLE `bitemsub` (
  `ISID` int(11) NOT NULL,
  `ISIDITEM` int(11) DEFAULT NULL,
  `ISQTY` double DEFAULT '0',
  `ISURUTAN` int(11) DEFAULT '0',
  `ISIDITEMHEAD` int(11) DEFAULT NULL,
  `ISSATUAN` int(11) DEFAULT NULL,
  `ISPILIHAN` int(11) DEFAULT NULL,
  `ISPILIHAN1` int(11) DEFAULT NULL,
  `ISPILIHAN2` int(11) DEFAULT NULL,
  `ISPILIHAN3` int(11) DEFAULT NULL,
  `ISPILIHAN4` int(11) DEFAULT NULL,
  `ISPILIHAN5` int(11) DEFAULT NULL,
  `ISPILIHAN6` int(11) DEFAULT NULL,
  `ISPILIHAN7` int(11) DEFAULT NULL,
  `ISPILIHAN8` int(11) DEFAULT NULL,
  `ISPILIHAN9` int(11) DEFAULT NULL,
  `ISPILIHAN10` int(11) DEFAULT NULL,
  `ISHARGA` double DEFAULT '0',
  `ISDISCOUNT` double DEFAULT '0',
  `ISDISCOUNT2` double DEFAULT '0',
  `ISDISCMEMBER` smallint(6) DEFAULT '0',
  `ISPILIHANHARGA1` double DEFAULT '0',
  `ISPILIHANHARGA2` double DEFAULT '0',
  `ISPILIHANHARGA3` double DEFAULT '0',
  `ISPILIHANHARGA4` double DEFAULT '0',
  `ISPILIHANHARGA5` double DEFAULT '0',
  `ISPILIHANHARGA6` double DEFAULT '0',
  `ISPILIHANHARGA7` double DEFAULT '0',
  `ISPILIHANHARGA8` double DEFAULT '0',
  `ISPILIHANHARGA9` double DEFAULT '0',
  `ISPILIHANHARGA10` double DEFAULT '0',
  `ISHARGABEDA` smallint(6) DEFAULT '0',
  `ISCETAK` smallint(6) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `bitemsubkategori`
--

CREATE TABLE `bitemsubkategori` (
  `ISKID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `bitem_aset`
--

CREATE TABLE `bitem_aset` (
  `IID` int(11) NOT NULL,
  `IKODE` varchar(25) NOT NULL,
  `INAMA` varchar(150) DEFAULT NULL,
  `INAMA2` varchar(150) DEFAULT NULL,
  `ISUBITEM` smallint(6) DEFAULT '0',
  `IPARENT` int(11) DEFAULT NULL,
  `IGD` char(1) DEFAULT 'D',
  `IUANG` int(11) DEFAULT NULL,
  `ISATUAN` int(11) NOT NULL,
  `ISATUAND` int(11) NOT NULL,
  `IJENISITEM` int(11) DEFAULT '0',
  `ITIPEITEM` int(11) DEFAULT NULL,
  `ISTATUS` int(11) DEFAULT '1',
  `ISTOCKMINIMAL` double DEFAULT '0',
  `ISTOCKMAKSIMAL` double DEFAULT '0',
  `ISTOCKTOTAL` double DEFAULT '0',
  `ISTOCKREORDER` double DEFAULT '0',
  `IHARGABELI` double DEFAULT '0',
  `IHARGAJUAL1` double DEFAULT '0',
  `IHARGAJUAL2` double DEFAULT '0',
  `IHARGAJUAL3` double DEFAULT '0',
  `IHARGAJUAL4` double DEFAULT '0',
  `IHARGAJUAL5` double DEFAULT '0',
  `IHARGAJUAL6` double DEFAULT '0',
  `IBARCODE` varchar(150) DEFAULT NULL,
  `ICOAPERSEDIAAN` int(11) DEFAULT NULL,
  `ICOAPENDAPATAN` int(11) DEFAULT NULL,
  `ICOAHPP` int(11) DEFAULT NULL,
  `ICUSTOM1` varchar(150) DEFAULT NULL,
  `ICUSTOM2` varchar(150) DEFAULT NULL,
  `ICUSTOM3` varchar(150) DEFAULT NULL,
  `ICUSTOM4` varchar(150) DEFAULT NULL,
  `ICUSTOM5` varchar(150) DEFAULT NULL,
  `ICUSTOM6` varchar(150) DEFAULT NULL,
  `ICUSTOM7` varchar(150) DEFAULT NULL,
  `ICUSTOM8` varchar(150) DEFAULT NULL,
  `ICUSTOM9` varchar(150) DEFAULT NULL,
  `ICUSTOM10` varchar(150) DEFAULT NULL,
  `ICREATEU` int(11) DEFAULT NULL,
  `IMODIFU` int(11) DEFAULT NULL,
  `ICREATED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `IMERK` int(11) DEFAULT NULL,
  `IKATEGORI` int(11) DEFAULT NULL,
  `ISUBKATEGORI` int(11) DEFAULT NULL,
  `IUMUR` double DEFAULT '0',
  `IKELOMPOKHARGA` int(11) DEFAULT NULL,
  `IKECEPATANJUAL` int(11) DEFAULT NULL,
  `ITANGGALRUBAHHARGA` date DEFAULT NULL,
  `IMODEL` varchar(150) DEFAULT NULL,
  `IJENIS` int(11) DEFAULT NULL,
  `INOURUT` int(11) DEFAULT '0',
  `IKODELAMA` varchar(25) DEFAULT NULL,
  `INAMALAMA` varchar(150) DEFAULT NULL,
  `ISALDOAWAL` double DEFAULT '0',
  `IDISKON` double DEFAULT '0',
  `ISALDOX` double DEFAULT '0',
  `IMODIFD` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ISTOKPG` double DEFAULT '0',
  `ISTOKCP` double DEFAULT '0',
  `ISTOKDP` double DEFAULT '0',
  `ISTOKKM` double DEFAULT '0',
  `ISTOKJG` double DEFAULT '0',
  `IPG` smallint(6) DEFAULT '1',
  `ICP` smallint(6) DEFAULT '1',
  `IDP` smallint(6) DEFAULT '1',
  `IKM` smallint(6) DEFAULT '1',
  `IJG` smallint(6) DEFAULT '1',
  `ITANGGAL1` date DEFAULT NULL,
  `ITANGGAL2` date DEFAULT NULL,
  `ISTOKOL` double DEFAULT '0',
  `IOL` smallint(6) DEFAULT '1',
  `ITARIKONLINE` smallint(6) DEFAULT '0',
  `IBA` smallint(6) DEFAULT '1',
  `IBB` smallint(6) DEFAULT '1',
  `ISTOKBA` double DEFAULT '0',
  `ISTOKBB` double DEFAULT '0',
  `IQTYPERBOX` double DEFAULT '1',
  `IKOMISIMARKETING` smallint(6) DEFAULT '0',
  `IKOMISIPAKET` smallint(6) DEFAULT '0',
  `IJENISITEMCOA` int(11) DEFAULT '0',
  `IGG` smallint(6) DEFAULT '1',
  `ISTOKGG` double DEFAULT '0',
  `IBOLEHMINUS` smallint(6) DEFAULT '0',
  `IPAJAKIN` int(11) DEFAULT NULL,
  `IPAJAKOUT` int(11) DEFAULT NULL,
  `IBERAT` double DEFAULT '0',
  `ISERIAL` int(11) DEFAULT NULL,
  `IHARGAPASAR` double DEFAULT '0',
  `ILUAS` double DEFAULT '0',
  `IHPPAKHIR` double DEFAULT '0',
  `IJENISBARANG` int(11) DEFAULT NULL,
  `INAMAA1` varchar(150) DEFAULT NULL,
  `INAMAA2` varchar(150) DEFAULT NULL,
  `INAMAA3` varchar(150) DEFAULT NULL,
  `INAMAA4` varchar(150) DEFAULT NULL,
  `INAMAA5` varchar(150) DEFAULT NULL,
  `INAMAA6` varchar(150) DEFAULT NULL,
  `INAMAA7` varchar(150) DEFAULT NULL,
  `INAMAA8` varchar(150) DEFAULT NULL,
  `INAMAA9` varchar(150) DEFAULT NULL,
  `INAMAA10` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `bjenispenyesuaian`
--

CREATE TABLE `bjenispenyesuaian` (
  `JID` int(11) NOT NULL,
  `JKODE` varchar(25) DEFAULT NULL,
  `JNAMA` varchar(150) DEFAULT NULL,
  `JCREATEU` int(11) DEFAULT NULL,
  `JMODIFU` int(11) DEFAULT NULL,
  `JCREATED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `JAKUNBIAYA` int(11) DEFAULT NULL,
  `JMODIFD` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bjenispenyesuaian`
--

INSERT INTO `bjenispenyesuaian` (`JID`, `JKODE`, `JNAMA`, `JCREATEU`, `JMODIFU`, `JCREATED`, `JAKUNBIAYA`, `JMODIFD`) VALUES
(2, 'Kantor', 'Kantor', 1, 1, '2011-09-18 17:53:21', 95, '2018-04-21 18:47:02'),
(9, 'Koreksi HPP', 'Koreksi HPP', 1, 1, '2013-09-26 17:03:12', 95, '2018-04-21 18:47:14'),
(10, 'Opname', 'Stok Opname', 1, 1, '2014-01-27 15:34:38', 95, '2018-04-21 18:47:25');

-- --------------------------------------------------------

--
-- Table structure for table `bkontak`
--

CREATE TABLE `bkontak` (
  `KID` int(11) NOT NULL,
  `KKODE` varchar(25) NOT NULL,
  `KNAMA` varchar(255) DEFAULT NULL,
  `KTIPE` int(11) NOT NULL DEFAULT '0',
  `K1ALAMAT` varchar(255) DEFAULT NULL,
  `K1ALAMAT2` varchar(255) DEFAULT NULL,
  `K1KOTA` varchar(50) DEFAULT NULL,
  `K1KODEPOS` varchar(50) DEFAULT NULL,
  `K1PROPINSI` varchar(50) DEFAULT NULL,
  `K1NEGARA` varchar(50) DEFAULT NULL,
  `K1TELP1` varchar(100) DEFAULT NULL,
  `K1FAX` varchar(50) DEFAULT NULL,
  `K1EMAIL` varchar(100) DEFAULT NULL,
  `K1KONTAK` varchar(50) DEFAULT NULL,
  `KCUSTOM1` varchar(255) DEFAULT NULL,
  `KCUSTOM2` varchar(255) DEFAULT NULL,
  `KCUSTOM3` varchar(255) DEFAULT NULL,
  `KCUSTOM4` varchar(255) DEFAULT NULL,
  `KCUSTOM5` varchar(255) DEFAULT NULL,
  `KCREATEU` int(11) DEFAULT NULL,
  `KMODIFU` int(11) DEFAULT NULL,
  `KCREATED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `KPEMBATASHUTANG` double DEFAULT '0',
  `KPEMTERMIN` int(11) DEFAULT NULL,
  `KPEMKARYAWAN` int(11) DEFAULT NULL,
  `KPENBATASPIUTANG` double DEFAULT '0',
  `KPENTERMIN` int(11) DEFAULT NULL,
  `KPENKARYAWAN` int(11) DEFAULT NULL,
  `KPENLEVELHARGA` int(11) DEFAULT '0',
  `KNPWP` varchar(50) DEFAULT NULL,
  `KPKP` int(11) DEFAULT '0',
  `KJENISKELAMIN` int(11) DEFAULT '0',
  `KMATAUANG` int(11) NOT NULL DEFAULT '1',
  `KBANK` int(11) DEFAULT NULL,
  `KNOAC` varchar(50) DEFAULT NULL,
  `KTGLKONTRAK` date DEFAULT NULL,
  `KTGLLAHIR` date DEFAULT NULL,
  `KPENPENAGIHAN` int(11) DEFAULT NULL,
  `KPENLEVELHARGA2` int(11) DEFAULT '0',
  `KSEKOLAH` int(11) DEFAULT NULL,
  `KKELAS` int(11) DEFAULT NULL,
  `KUM` double DEFAULT '0',
  `KSKS` double DEFAULT '0',
  `KSPP` double DEFAULT '0',
  `KUSER` int(11) DEFAULT NULL,
  `KNOKTP` varchar(50) DEFAULT NULL,
  `KPEMREKHUTANG` int(11) DEFAULT NULL,
  `K2ALAMAT` varchar(255) DEFAULT NULL,
  `K2ALAMAT2` varchar(255) DEFAULT NULL,
  `K2KOTA` varchar(50) DEFAULT NULL,
  `K2KODEPOS` varchar(50) DEFAULT NULL,
  `K2PROPINSI` varchar(50) DEFAULT NULL,
  `K2NEGARA` varchar(50) DEFAULT NULL,
  `K2TELP1` varchar(50) DEFAULT NULL,
  `K2FAX` varchar(50) DEFAULT NULL,
  `K2EMAIL` varchar(50) DEFAULT NULL,
  `K2KONTAK` varchar(50) DEFAULT NULL,
  `K3ALAMAT` varchar(255) DEFAULT NULL,
  `K3ALAMAT2` varchar(255) DEFAULT NULL,
  `K3KOTA` varchar(50) DEFAULT NULL,
  `K3KODEPOS` varchar(50) DEFAULT NULL,
  `K3PROPINSI` varchar(50) DEFAULT NULL,
  `K3NEGARA` varchar(50) DEFAULT NULL,
  `K3TELP1` varchar(50) DEFAULT NULL,
  `K3FAX` varchar(50) DEFAULT NULL,
  `K3EMAIL` varchar(50) DEFAULT NULL,
  `K3KONTAK` varchar(50) DEFAULT NULL,
  `K4ALAMAT` varchar(255) DEFAULT NULL,
  `K4ALAMAT2` varchar(255) DEFAULT NULL,
  `K4KOTA` varchar(50) DEFAULT NULL,
  `K4KODEPOS` varchar(50) DEFAULT NULL,
  `K4PROPINSI` varchar(50) DEFAULT NULL,
  `K4NEGARA` varchar(50) DEFAULT NULL,
  `K4TELP1` varchar(50) DEFAULT NULL,
  `K4FAX` varchar(50) DEFAULT NULL,
  `K4EMAIL` varchar(50) DEFAULT NULL,
  `K4KONTAK` varchar(50) DEFAULT NULL,
  `KMODIFD` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `KTAMPILDIDOKTER` smallint(6) DEFAULT '0',
  `KTAMPILDIPERAWAT` smallint(6) DEFAULT '0',
  `KTAMPILDIRESEP` smallint(6) DEFAULT '0',
  `KCABANG` smallint(6) DEFAULT '0',
  `KIDPASIEN` varchar(255) DEFAULT NULL,
  `KAKTIF` smallint(6) DEFAULT '1',
  `KJENISKARYAWAN` smallint(6) DEFAULT '0',
  `KKODELAMA` varchar(25) DEFAULT NULL,
  `KNOMEMBER` varchar(25) DEFAULT NULL,
  `KKARYAWAN` int(11) DEFAULT NULL,
  `KPEKERJAAN` varchar(100) DEFAULT NULL,
  `KALAMATPAJAK` varchar(255) DEFAULT NULL,
  `KPEMKODETRANS` varchar(2) DEFAULT NULL,
  `KPEMSTATUS` varchar(2) DEFAULT NULL,
  `KPENSTATUS` varchar(2) DEFAULT NULL,
  `KDISKON` double DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bkontak`
--

INSERT INTO `bkontak` (`KID`, `KKODE`, `KNAMA`, `KTIPE`, `K1ALAMAT`, `K1ALAMAT2`, `K1KOTA`, `K1KODEPOS`, `K1PROPINSI`, `K1NEGARA`, `K1TELP1`, `K1FAX`, `K1EMAIL`, `K1KONTAK`, `KCUSTOM1`, `KCUSTOM2`, `KCUSTOM3`, `KCUSTOM4`, `KCUSTOM5`, `KCREATEU`, `KMODIFU`, `KCREATED`, `KPEMBATASHUTANG`, `KPEMTERMIN`, `KPEMKARYAWAN`, `KPENBATASPIUTANG`, `KPENTERMIN`, `KPENKARYAWAN`, `KPENLEVELHARGA`, `KNPWP`, `KPKP`, `KJENISKELAMIN`, `KMATAUANG`, `KBANK`, `KNOAC`, `KTGLKONTRAK`, `KTGLLAHIR`, `KPENPENAGIHAN`, `KPENLEVELHARGA2`, `KSEKOLAH`, `KKELAS`, `KUM`, `KSKS`, `KSPP`, `KUSER`, `KNOKTP`, `KPEMREKHUTANG`, `K2ALAMAT`, `K2ALAMAT2`, `K2KOTA`, `K2KODEPOS`, `K2PROPINSI`, `K2NEGARA`, `K2TELP1`, `K2FAX`, `K2EMAIL`, `K2KONTAK`, `K3ALAMAT`, `K3ALAMAT2`, `K3KOTA`, `K3KODEPOS`, `K3PROPINSI`, `K3NEGARA`, `K3TELP1`, `K3FAX`, `K3EMAIL`, `K3KONTAK`, `K4ALAMAT`, `K4ALAMAT2`, `K4KOTA`, `K4KODEPOS`, `K4PROPINSI`, `K4NEGARA`, `K4TELP1`, `K4FAX`, `K4EMAIL`, `K4KONTAK`, `KMODIFD`, `KTAMPILDIDOKTER`, `KTAMPILDIPERAWAT`, `KTAMPILDIRESEP`, `KCABANG`, `KIDPASIEN`, `KAKTIF`, `KJENISKARYAWAN`, `KKODELAMA`, `KNOMEMBER`, `KKARYAWAN`, `KPEKERJAAN`, `KALAMATPAJAK`, `KPEMKODETRANS`, `KPEMSTATUS`, `KPENSTATUS`, `KDISKON`) VALUES
(1, 'PEL-1', 'PELANGGAN 1', 2, '', NULL, NULL, '', NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, 1, NULL, '2022-01-04 11:20:11', 0, 0, 0, 0, 4, 0, 1, '', 0, 0, 0, 0, '', NULL, NULL, 0, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-04 11:20:11', 0, 0, 0, 0, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(2, 'PEL-2', 'PELANGGAN 2', 2, '', NULL, NULL, '', NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, 1, NULL, '2022-01-04 11:20:40', 0, 0, 0, 0, 0, 0, 2, '', 0, 0, 0, 0, '', NULL, NULL, 0, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-04 11:20:40', 0, 0, 0, 0, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(3, 'PEL-3', 'PELANGGAN 3', 2, '', NULL, NULL, '', NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, 1, NULL, '2022-01-04 11:21:08', 0, 0, 0, 0, 0, 0, 3, '', 0, 0, 0, 0, '', NULL, NULL, 0, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-04 11:21:08', 0, 0, 0, 0, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(4, 'PEL-4', 'PELANGGAN 4', 2, '', NULL, NULL, '', NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, 1, NULL, '2022-01-04 11:21:32', 0, 0, 0, 0, 0, 0, 4, '', 0, 0, 0, 0, '', NULL, NULL, 0, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-04 11:21:32', 0, 0, 0, 0, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(5, 'PEM-UMUM', 'SUPPLIER UMUM', 6, '', NULL, NULL, '', NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-01-04 11:22:08', 0, 4, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, '', NULL, NULL, 0, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-04 11:22:08', 0, 0, 0, 0, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(6, 'KAR-1', 'KARYAWAN 1', 4, '', NULL, NULL, '', NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, 1, NULL, '2022-01-04 11:22:41', 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, '', NULL, NULL, 0, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-04 11:22:41', 0, 0, 0, 0, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(7, 'AGEN-UMUM', 'AGEN UMUM', 3, '', NULL, NULL, '', NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, 1, 1, '2022-01-04 11:23:11', 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, '', NULL, NULL, 0, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-04 11:23:11', 0, 0, 0, 0, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(8, 'UMUM', 'UMUM', 9, '', NULL, NULL, '', NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, 1, NULL, '2022-01-05 00:47:13', 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, '', NULL, NULL, 0, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-05 00:47:13', 0, 0, 0, 0, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `bkontakalamat`
--

CREATE TABLE `bkontakalamat` (
  `BAID` int(11) NOT NULL,
  `BAIDKONTAK` int(11) DEFAULT NULL,
  `BANAMA` varchar(50) DEFAULT NULL,
  `BAALAMAT` varchar(255) DEFAULT NULL,
  `BANOTELP` varchar(50) DEFAULT NULL,
  `BANOFAX` varchar(50) DEFAULT NULL,
  `BAATTENTION` varchar(50) DEFAULT NULL,
  `BAEMAIL` varchar(50) DEFAULT NULL,
  `BADEFAULT` smallint(6) DEFAULT '0',
  `BATIPE` smallint(6) DEFAULT '0',
  `BAKOTA` varchar(100) DEFAULT NULL,
  `BAPROVINSI` varchar(100) DEFAULT NULL,
  `BANEGARA` varchar(100) DEFAULT NULL,
  `BAALAMAT2` varchar(255) DEFAULT NULL,
  `BAKODEPOS` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `bkontakatention`
--

CREATE TABLE `bkontakatention` (
  `KAID` int(11) NOT NULL,
  `KAIDK` int(11) NOT NULL,
  `KAURUTAN` int(11) NOT NULL DEFAULT '0',
  `KANAMA` varchar(50) DEFAULT NULL,
  `KAJABATAN` varchar(50) DEFAULT NULL,
  `KACATATAN` varchar(50) DEFAULT NULL,
  `KAALAMAT1` varchar(255) DEFAULT NULL,
  `KAALAMAT2` varchar(255) DEFAULT NULL,
  `KAKOTA` varchar(25) DEFAULT NULL,
  `KAPROPINSI` varchar(25) DEFAULT NULL,
  `KANEGARA` varchar(25) DEFAULT NULL,
  `KAKODEPOS` varchar(25) DEFAULT NULL,
  `KATELP` varchar(25) DEFAULT NULL,
  `KAFAX` varchar(25) DEFAULT NULL,
  `KAHP` varchar(25) DEFAULT NULL,
  `KAEMAIL` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `bkontakatention2`
--

CREATE TABLE `bkontakatention2` (
  `KAID` int(11) DEFAULT NULL,
  `KAIDK` int(11) DEFAULT NULL,
  `KAURUTAN` int(11) DEFAULT NULL,
  `KANAMA` varchar(100) DEFAULT NULL,
  `KAJABATAN` varchar(50) DEFAULT NULL,
  `KACATATAN` varchar(50) DEFAULT NULL,
  `KAALAMAT1` varchar(255) DEFAULT NULL,
  `KAALAMAT2` varchar(255) DEFAULT NULL,
  `KAKOTA` varchar(25) DEFAULT NULL,
  `KAPROPINSI` varchar(25) DEFAULT NULL,
  `KANEGARA` varchar(25) DEFAULT NULL,
  `KAKODEPOS` varchar(25) DEFAULT NULL,
  `KATELP` varchar(50) DEFAULT NULL,
  `KAFAX` varchar(50) DEFAULT NULL,
  `KAHP` varchar(50) DEFAULT NULL,
  `KAEMAIL` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `bkontakfiles`
--

CREATE TABLE `bkontakfiles` (
  `KFID` int(11) NOT NULL,
  `KFIDK` int(11) NOT NULL,
  `KFURUTAN` int(11) NOT NULL,
  `KFNAMAFILE` varchar(255) DEFAULT NULL,
  `KFKETERANGAN` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `bkontakserial`
--

CREATE TABLE `bkontakserial` (
  `KSID` int(11) NOT NULL,
  `KSKONTAK` int(11) NOT NULL,
  `KSSERIAL` varchar(150) DEFAULT NULL,
  `KSKETERANGAN` varchar(255) DEFAULT NULL,
  `KSURUTAN` int(11) DEFAULT '0',
  `KSBARANG` int(11) DEFAULT NULL,
  `KSALAMAT` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `bkontaktipe`
--

CREATE TABLE `bkontaktipe` (
  `KTID` int(11) NOT NULL,
  `KTNAMA` varchar(25) DEFAULT NULL,
  `KTCREATEU` int(11) DEFAULT NULL,
  `KTMODIFU` int(11) DEFAULT NULL,
  `KTMODIFD` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `KTDISCOUNT` double DEFAULT '0',
  `KTCREATED` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bkontaktipe`
--

INSERT INTO `bkontaktipe` (`KTID`, `KTNAMA`, `KTCREATEU`, `KTMODIFU`, `KTMODIFD`, `KTDISCOUNT`, `KTCREATED`) VALUES
(2, 'PELANGGAN', 1, 1, '2012-12-11 21:30:41', 0, '2011-06-28 12:31:13'),
(3, 'AGENT', 1, 1, '2012-12-03 15:12:29', 0, '2011-08-06 11:10:48'),
(4, 'KARYAWAN', 1, 1, '2012-12-11 21:18:16', 0, '2011-08-06 12:49:30'),
(6, 'SUPPLIER', 1, 1, '2011-12-06 14:24:15', 0, '2011-12-06 14:24:16'),
(9, 'UMUM', 1, 1, '2012-01-05 15:27:37', 0, '2012-01-05 15:27:37'),
(10, 'BANK', 1, 1, '2012-01-05 17:08:40', 0, '2012-01-05 17:08:40'),
(19, 'BLACKLIST', 1, 1, '2018-01-06 17:06:33', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `blain`
--

CREATE TABLE `blain` (
  `LID` int(11) NOT NULL,
  `LKODE` varchar(25) NOT NULL,
  `LNAMA` varchar(150) DEFAULT NULL,
  `LKETERANGAN` varchar(255) DEFAULT NULL,
  `LTIPE` varchar(25) NOT NULL,
  `LCUSTOM1` varchar(255) DEFAULT NULL,
  `LCUSTOM2` varchar(255) DEFAULT NULL,
  `LCUSTOM3` varchar(255) DEFAULT NULL,
  `LCUSTOM4` varchar(255) DEFAULT NULL,
  `LCUSTOM5` varchar(255) DEFAULT NULL,
  `LCREATEU` int(11) DEFAULT NULL,
  `LMODIFU` int(11) DEFAULT NULL,
  `LCREATED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LBARCODE` int(11) DEFAULT NULL,
  `LMODIFD` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `LLUAS` double DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `bpajak`
--

CREATE TABLE `bpajak` (
  `PID` int(11) NOT NULL,
  `PKODE` varchar(25) DEFAULT NULL,
  `PNAMA` varchar(150) DEFAULT NULL,
  `PNILAI` double DEFAULT '0',
  `PTIPE` smallint(6) DEFAULT '0',
  `PCOAIN` int(11) DEFAULT NULL,
  `PCOAOUT` int(11) DEFAULT NULL,
  `PCREATEU` int(11) DEFAULT NULL,
  `PMODIFU` int(11) DEFAULT NULL,
  `PCREATED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `PMODIFD` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bpajak`
--

INSERT INTO `bpajak` (`PID`, `PKODE`, `PNAMA`, `PNILAI`, `PTIPE`, `PCOAIN`, `PCOAOUT`, `PCREATEU`, `PMODIFU`, `PCREATED`, `PMODIFD`) VALUES
(4, 'PPN', 'Pajak Pertambahan Nilai', 10, 1, 20, 45, 1, 1, '2021-10-22 17:02:14', '2021-10-22 17:02:14'),
(5, 'PPH 22 1%', 'Pajak Penghasilan Ps 22', 1, 2, 23, 23, 1, 1, '2021-10-22 17:04:15', '2021-10-22 17:04:15'),
(6, 'PPH 23 2%', 'Pajak Penghasilan Ps 23 2%', 2, 2, 41, 22, 1, 1, '2021-10-24 02:29:50', '2021-10-24 02:29:50'),
(7, 'PPH 4 (2)', 'Pajak Penghasilan Ps 4 (2)', 10, 2, 42, 42, 1, 0, '2021-10-24 15:21:59', '2021-10-24 15:21:59'),
(8, 'PPH 23 4%', 'Pajak Penghasilan Ps 23 4%', 4, 2, 41, 22, 1, 1, '2021-10-24 15:23:22', '2021-10-24 15:23:22'),
(9, 'PPH 22 1,5%', 'Pajak Penghasilan Ps 22 1,5%', 1.5, 2, 23, 23, 2, 1, '2021-10-26 18:21:41', '2021-10-26 18:21:41');

-- --------------------------------------------------------

--
-- Table structure for table `bproyek`
--

CREATE TABLE `bproyek` (
  `PID` int(11) NOT NULL,
  `PKODE` varchar(25) NOT NULL,
  `PNAMA` varchar(150) DEFAULT NULL,
  `PCREATEU` int(11) DEFAULT NULL,
  `PMODIFU` int(11) DEFAULT NULL,
  `PCREATED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `PPELANGGAN` varchar(100) DEFAULT NULL,
  `PFEE` double DEFAULT '0',
  `PMODIFD` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `PINISIAL` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `bsatuan`
--

CREATE TABLE `bsatuan` (
  `SID` int(11) NOT NULL,
  `SKODE` varchar(25) NOT NULL,
  `SNAMA` varchar(150) DEFAULT NULL,
  `SSATUANDASAR` smallint(6) DEFAULT NULL,
  `SNILAI` int(11) DEFAULT NULL,
  `SCUSTOM1` varchar(150) DEFAULT NULL,
  `SCUSTOM2` varchar(150) DEFAULT NULL,
  `SCUSTOM3` varchar(150) DEFAULT NULL,
  `SCREATEU` int(11) DEFAULT NULL,
  `SMODIFU` int(11) DEFAULT NULL,
  `SCREATED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `SMODIFD` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bsatuan`
--

INSERT INTO `bsatuan` (`SID`, `SKODE`, `SNAMA`, `SSATUANDASAR`, `SNILAI`, `SCUSTOM1`, `SCUSTOM2`, `SCUSTOM3`, `SCREATEU`, `SMODIFU`, `SCREATED`, `SMODIFD`) VALUES
(1, 'PCS', 'PCS', 1, 1, NULL, NULL, NULL, 0, 1, '2021-10-28 12:03:36', '0000-00-00 00:00:00'),
(2, 'SET', 'SET', 1, 1, NULL, NULL, NULL, 1, NULL, '2022-01-04 11:09:43', '2022-01-04 11:09:43'),
(3, 'PSG', 'PSG', 1, 1, NULL, NULL, NULL, 1, NULL, '2022-01-04 11:09:57', '2022-01-04 11:09:57'),
(4, 'BTL', 'BTL', 1, 1, NULL, NULL, NULL, 1, NULL, '2022-01-04 11:10:11', '2022-01-04 11:10:11');

-- --------------------------------------------------------

--
-- Table structure for table `bsatuankonfersi`
--

CREATE TABLE `bsatuankonfersi` (
  `SKID` int(11) NOT NULL,
  `SKSATUAN` int(11) DEFAULT NULL,
  `SKSATUANK` int(11) DEFAULT NULL,
  `SKURUTAN` int(11) DEFAULT NULL,
  `SKNILAI` double DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `bstokgudang`
--

CREATE TABLE `bstokgudang` (
  `SGID` int(11) NOT NULL,
  `SGITEM` int(11) DEFAULT NULL,
  `SG1` double DEFAULT '0',
  `SG2` double DEFAULT '0',
  `SG3` double DEFAULT '0',
  `SG4` double DEFAULT '0',
  `SG5` double DEFAULT '0',
  `SG6` double DEFAULT '0',
  `SG7` double DEFAULT '0',
  `SG8` double DEFAULT '0',
  `SG9` double DEFAULT '0',
  `SG10` double DEFAULT '0',
  `SG11` double DEFAULT '0',
  `SG12` double DEFAULT '0',
  `SG13` double DEFAULT '0',
  `SG14` double DEFAULT '0',
  `SG15` double DEFAULT '0',
  `SG16` double DEFAULT '0',
  `SG17` double DEFAULT '0',
  `SG18` double DEFAULT '0',
  `SG19` double DEFAULT '0',
  `SG20` double DEFAULT '0',
  `SG21` double DEFAULT '0',
  `SG22` double DEFAULT '0',
  `SG23` double DEFAULT '0',
  `SG24` double DEFAULT '0',
  `SG25` double DEFAULT '0',
  `SG26` double DEFAULT '0',
  `SG27` double DEFAULT '0',
  `SG28` double DEFAULT '0',
  `SG29` double DEFAULT '0',
  `SG30` double DEFAULT '0',
  `SG31` double DEFAULT '0',
  `SG32` double DEFAULT '0',
  `SG33` double DEFAULT '0',
  `SG34` double DEFAULT '0',
  `SG35` double DEFAULT '0',
  `SG36` double DEFAULT '0',
  `SG37` double DEFAULT '0',
  `SG38` double DEFAULT '0',
  `SG39` double DEFAULT '0',
  `SG40` double DEFAULT '0',
  `SG41` double DEFAULT '0',
  `SG42` double DEFAULT '0',
  `SG43` double DEFAULT '0',
  `SG44` double DEFAULT '0',
  `SG45` double DEFAULT '0',
  `SG46` double DEFAULT '0',
  `SG47` double DEFAULT '0',
  `SG48` double DEFAULT '0',
  `SG49` double DEFAULT '0',
  `SG50` double DEFAULT '0',
  `SG51` double DEFAULT '0',
  `SG52` double DEFAULT '0',
  `SG53` double DEFAULT '0',
  `SG54` double DEFAULT '0',
  `SG55` double DEFAULT '0',
  `SG56` double DEFAULT '0',
  `SG57` double DEFAULT '0',
  `SG58` double DEFAULT '0',
  `SG59` double DEFAULT '0',
  `SG60` double DEFAULT '0',
  `SG61` double DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `btermin`
--

CREATE TABLE `btermin` (
  `TID` int(11) NOT NULL,
  `TKODE` varchar(25) NOT NULL,
  `TNAMA` varchar(150) DEFAULT NULL,
  `TTEMPO` int(11) DEFAULT NULL,
  `TCREATEU` int(11) DEFAULT NULL,
  `TMODIFU` int(11) DEFAULT NULL,
  `TCREATED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `TMODIFD` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `btermin`
--

INSERT INTO `btermin` (`TID`, `TKODE`, `TNAMA`, `TTEMPO`, `TCREATEU`, `TMODIFU`, `TCREATED`, `TMODIFD`) VALUES
(2, 'Tunai', 'Tunai', 0, 1, 1, '2011-09-17 14:52:50', '2011-09-17 14:53:03'),
(4, 'Net 30', 'Net 30', 30, 1, 1, '2011-12-06 15:04:37', '2011-12-06 15:04:37'),
(5, 'Net 45', 'Net 45', 45, 1, 1, '2011-12-06 15:04:47', '2011-12-06 15:04:47'),
(6, 'Net 60', 'Net 60', 60, 1, 1, '2011-12-06 15:04:58', '2011-12-06 15:04:58'),
(8, 'Net 14', 'Net 14', 14, 1, 1, '2013-02-08 11:38:42', '2013-02-08 11:38:42');

-- --------------------------------------------------------

--
-- Table structure for table `buang`
--

CREATE TABLE `buang` (
  `UID` int(11) NOT NULL,
  `UKODE` varchar(25) DEFAULT NULL,
  `UNAMA` varchar(150) DEFAULT NULL,
  `USIMBOL` varchar(5) DEFAULT NULL,
  `UCREATEU` int(11) DEFAULT NULL,
  `UMODIFU` int(11) DEFAULT NULL,
  `UCREATED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UMODIFD` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UCOAPIUTANG` int(11) DEFAULT NULL,
  `UCOAHUTANG` int(11) DEFAULT NULL,
  `UCOADPPENJUALAN` int(11) DEFAULT NULL,
  `UCOADPPEMBELIAN` int(11) DEFAULT NULL,
  `UKURS` double DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `buang`
--

INSERT INTO `buang` (`UID`, `UKODE`, `UNAMA`, `USIMBOL`, `UCREATEU`, `UMODIFU`, `UCREATED`, `UMODIFD`, `UCOAPIUTANG`, `UCOAHUTANG`, `UCOADPPENJUALAN`, `UCOADPPEMBELIAN`, `UKURS`) VALUES
(1, 'IDR', 'Rupiah', 'Rp', 1, 1, '2010-09-16 14:41:16', '2016-02-05 01:28:56', NULL, NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `buangkurs`
--

CREATE TABLE `buangkurs` (
  `UKID` int(11) NOT NULL,
  `UKIDU` int(11) DEFAULT NULL,
  `UKTANNGGAL` date DEFAULT NULL,
  `UKNILAI` double DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `buangkursp`
--

CREATE TABLE `buangkursp` (
  `UKID` int(11) NOT NULL,
  `UKIDU` int(11) DEFAULT NULL,
  `UKTANNGGAL` date DEFAULT NULL,
  `UKNILAI` double DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `bvoucher`
--

CREATE TABLE `bvoucher` (
  `VID` int(11) NOT NULL,
  `VNOMOR` varchar(50) DEFAULT NULL,
  `VTGLTERBIT` date DEFAULT NULL,
  `VTGLGUNA` date DEFAULT NULL,
  `VKONTAK` int(11) DEFAULT NULL,
  `VCREATEU` int(11) DEFAULT NULL,
  `VCREATED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `VMODIFU` int(11) DEFAULT NULL,
  `VMODIFD` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `VPAKAI` smallint(6) DEFAULT '0',
  `VCABANG` int(11) DEFAULT NULL,
  `VNILAI` double DEFAULT '0',
  `VITEM` varchar(255) DEFAULT NULL,
  `VJENIS` int(11) DEFAULT NULL,
  `V1TRANSAKSI` smallint(6) DEFAULT '0',
  `VPRODUKSAJA` smallint(6) DEFAULT '0',
  `VMASABERLAKU` double DEFAULT '0',
  `VTGLEXPIRED` date DEFAULT NULL,
  `VITEM2` varchar(255) DEFAULT NULL,
  `VRUPIAH` smallint(6) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `caktivatransaksi`
--

CREATE TABLE `caktivatransaksi` (
  `CTID` int(11) NOT NULL,
  `CTNOTRANSAKSI` varchar(50) DEFAULT NULL,
  `CTURUTAN` int(11) DEFAULT NULL,
  `CTTANGGAL` date DEFAULT NULL,
  `CTIDA` int(11) DEFAULT NULL,
  `CTJML` double DEFAULT '0',
  `CTNILAIMASUK` double DEFAULT '0',
  `CTNILAIKELUAR` double DEFAULT '0',
  `CTKETERANGAN` varchar(255) DEFAULT NULL,
  `CTNILAIPERBAIKAN` double DEFAULT '0',
  `CTDIVISI` int(11) DEFAULT NULL,
  `CTCREATEU` int(11) DEFAULT NULL,
  `CTCREATED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `CTMODIFU` int(11) DEFAULT NULL,
  `CTMODIFD` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `CTJENIS` smallint(6) DEFAULT NULL,
  `CTURAIAN` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `caktivatransaksi`
--

INSERT INTO `caktivatransaksi` (`CTID`, `CTNOTRANSAKSI`, `CTURUTAN`, `CTTANGGAL`, `CTIDA`, `CTJML`, `CTNILAIMASUK`, `CTNILAIKELUAR`, `CTKETERANGAN`, `CTNILAIPERBAIKAN`, `CTDIVISI`, `CTCREATEU`, `CTCREATED`, `CTMODIFU`, `CTMODIFD`, `CTJENIS`, `CTURAIAN`) VALUES
(109, 'AT19090001', 1, '2019-09-01', 58, 1, 251817900, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:01', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(110, 'AT19120001', 2, '2019-12-31', 58, 1, 0, 10492412.5, NULL, 0, NULL, 1, '2021-02-17 08:17:01', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :4 Bulan'),
(111, 'AT19050001', 3, '2019-05-01', 52, 1, 27684150, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:01', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(112, 'AT19120001', 4, '2019-12-31', 52, 1, 0, 2307012.5, NULL, 0, NULL, 1, '2021-02-17 08:17:01', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :8 Bulan'),
(113, 'AT12040001', 5, '2012-04-01', 78, 1, 2225000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:01', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(114, 'AT19120001', 6, '2019-12-31', 78, 1, 0, 2225000.000000002, NULL, 0, NULL, 1, '2021-02-17 08:17:01', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :48 Bulan'),
(115, 'AT18030001', 7, '2018-03-01', 96, 1, 3600000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:01', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(116, 'AT19120001', 8, '2019-12-31', 96, 1, 0, 1650000, NULL, 0, NULL, 1, '2021-02-17 08:17:01', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :22 Bulan'),
(117, 'AT04100001', 9, '2004-10-20', 71, 1, 2000000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:01', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(118, 'AT19120001', 10, '2019-12-31', 71, 1, 0, 2000000.0000000016, NULL, 0, NULL, 1, '2021-02-17 08:17:01', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :48 Bulan'),
(119, 'AT19090001', 11, '2019-09-01', 103, 1, 5500000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:01', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(120, 'AT19120001', 12, '2019-12-31', 103, 1, 0, 458333.333333332, NULL, 0, NULL, 1, '2021-02-17 08:17:02', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :4 Bulan'),
(121, 'AT04040001', 13, '2004-04-25', 5, 1, 2700000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:02', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(122, 'AT19120001', 14, '2019-12-31', 5, 1, 0, 2700000, NULL, 0, NULL, 1, '2021-02-17 08:17:02', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :48 Bulan'),
(123, 'AT08070001', 15, '2008-07-31', 75, 1, 10290000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:02', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(124, 'AT19120001', 16, '2019-12-31', 75, 1, 0, 10290000, NULL, 0, NULL, 1, '2021-02-17 08:17:02', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :48 Bulan'),
(125, 'AT17020001', 17, '2017-02-01', 89, 1, 10050000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:02', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(126, 'AT19120001', 18, '2019-12-31', 89, 1, 0, 7328125, NULL, 0, NULL, 1, '2021-02-17 08:17:02', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :35 Bulan'),
(127, 'AT04040001', 19, '2004-04-25', 66, 1, 1650000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:02', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(128, 'AT19120001', 20, '2019-12-31', 66, 1, 0, 1650000, NULL, 0, NULL, 1, '2021-02-17 08:17:02', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :48 Bulan'),
(129, 'AT04100001', 21, '2004-10-20', 24, 1, 650000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:02', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(130, 'AT19120001', 22, '2019-12-31', 24, 1, 0, 650000.0000000016, NULL, 0, NULL, 1, '2021-02-17 08:17:02', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :48 Bulan'),
(131, 'AT18090001', 23, '2018-09-01', 45, 1, 50000000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:02', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(132, 'AT19120001', 24, '2019-12-31', 45, 1, 0, 8333333.333333328, NULL, 0, NULL, 1, '2021-02-17 08:17:02', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :16 Bulan'),
(133, 'AT18030001', 25, '2018-03-01', 42, 1, 65000000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:02', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(134, 'AT19120001', 26, '2019-12-31', 42, 1, 0, 14895833.333333327, NULL, 0, NULL, 1, '2021-02-17 08:17:02', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :22 Bulan'),
(135, 'AT19070001', 27, '2019-07-01', 53, 1, 178176600, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:02', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(136, 'AT19120001', 28, '2019-12-31', 53, 1, 0, 11136037.5, NULL, 0, NULL, 1, '2021-02-17 08:17:02', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :6 Bulan'),
(137, 'AT18030001', 29, '2018-03-01', 97, 1, 9800000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:02', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(138, 'AT19120001', 30, '2019-12-31', 97, 1, 0, 4491666.666666674, NULL, 0, NULL, 1, '2021-02-17 08:17:02', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :22 Bulan'),
(139, 'AT19110001', 31, '2019-11-01', 106, 1, 8000000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:02', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(140, 'AT19120001', 32, '2019-12-31', 106, 1, 0, 333333.333333334, NULL, 0, NULL, 1, '2021-02-17 08:17:02', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :2 Bulan'),
(141, 'AT04120001', 33, '2004-12-28', 72, 1, 3422700, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:02', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(142, 'AT19120001', 34, '2019-12-31', 72, 1, 0, 3422700, NULL, 0, NULL, 1, '2021-02-17 08:17:02', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :48 Bulan'),
(143, 'AT19050001', 35, '2019-05-01', 51, 1, 164685200, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:03', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(144, 'AT19120001', 36, '2019-12-31', 51, 1, 0, 13723766.66666664, NULL, 0, NULL, 1, '2021-02-17 08:17:03', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :8 Bulan'),
(145, 'AT17010001', 37, '2017-01-01', 8, 1, 159236925, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:03', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(146, 'AT19120001', 38, '2019-12-31', 8, 1, 0, 59713846.875, NULL, 0, NULL, 1, '2021-02-17 08:17:03', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :36 Bulan'),
(147, 'AT04070001', 39, '2004-07-22', 6, 1, 196000000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:03', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(148, 'AT19120001', 40, '2019-12-31', 6, 1, 0, 196000000.00000033, NULL, 0, NULL, 1, '2021-02-17 08:17:03', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :96 Bulan'),
(149, 'AT07110001', 41, '2007-11-06', 74, 1, 5435800, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:03', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(150, 'AT19120001', 42, '2019-12-31', 74, 1, 0, 5435799.999999983, NULL, 0, NULL, 1, '2021-02-17 08:17:03', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :48 Bulan'),
(151, 'AT17080001', 43, '2017-08-01', 90, 1, 6964000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:03', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(152, 'AT19120001', 44, '2019-12-31', 90, 1, 0, 4207416.666666657, NULL, 0, NULL, 1, '2021-02-17 08:17:03', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :29 Bulan'),
(153, 'AT17100001', 45, '2017-10-01', 91, 1, 1906358, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:03', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(154, 'AT19120001', 46, '2019-12-31', 91, 1, 0, 1072326.375000001, NULL, 0, NULL, 1, '2021-02-17 08:17:03', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :27 Bulan'),
(155, 'AT17110001', 47, '2017-11-01', 92, 1, 8000000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:03', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(156, 'AT19120001', 48, '2019-12-31', 92, 1, 0, 4333333.333333342, NULL, 0, NULL, 1, '2021-02-17 08:17:03', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :26 Bulan'),
(157, 'AT18020001', 49, '2018-02-01', 95, 1, 1599000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:03', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(158, 'AT19120001', 50, '2019-12-31', 95, 1, 0, 766187.5, NULL, 0, NULL, 1, '2021-02-17 08:17:03', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :23 Bulan'),
(159, 'AT15050001', 51, '2015-05-01', 86, 1, 2019200, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:03', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(160, 'AT19120001', 52, '2019-12-31', 86, 1, 0, 2019200.0000000016, NULL, 0, NULL, 1, '2021-02-17 08:17:03', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :48 Bulan'),
(161, 'AT18010001', 53, '2018-01-01', 94, 1, 2678000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:03', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(162, 'AT19120001', 54, '2019-12-31', 94, 1, 0, 1339000.000000001, NULL, 0, NULL, 1, '2021-02-17 08:17:03', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :24 Bulan'),
(163, 'AT10010001', 55, '2010-01-21', 77, 1, 15996500, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:03', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(164, 'AT19120001', 56, '2019-12-31', 77, 1, 0, 15996500.000000015, NULL, 0, NULL, 1, '2021-02-17 08:17:03', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :48 Bulan'),
(165, 'AT14110001', 57, '2014-11-01', 84, 1, 3850000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:03', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(166, 'AT19120001', 58, '2019-12-31', 84, 1, 0, 3849999.999999998, NULL, 0, NULL, 1, '2021-02-17 08:17:03', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :48 Bulan'),
(167, 'AT16030001', 59, '2016-03-01', 88, 1, 4100000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:03', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(168, 'AT19120001', 60, '2019-12-31', 88, 1, 0, 3929166.6666666684, NULL, 0, NULL, 1, '2021-02-17 08:17:04', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :46 Bulan'),
(169, 'AT18100001', 61, '2018-10-01', 98, 1, 4600000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:04', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(170, 'AT19120001', 62, '2019-12-31', 98, 1, 0, 1437499.9999999995, NULL, 0, NULL, 1, '2021-02-17 08:17:04', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :15 Bulan'),
(171, 'AT19030001', 63, '2019-03-01', 101, 1, 4370000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:04', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(172, 'AT19120001', 64, '2019-12-31', 101, 1, 0, 910416.666666667, NULL, 0, NULL, 1, '2021-02-17 08:17:04', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :10 Bulan'),
(173, 'AT04040001', 65, '2004-04-25', 69, 1, 3870000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:04', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(174, 'AT19120001', 66, '2019-12-31', 69, 1, 0, 3870000, NULL, 0, NULL, 1, '2021-02-17 08:17:04', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :48 Bulan'),
(175, 'AT04050001', 67, '2004-05-21', 70, 1, 3950000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:04', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(176, 'AT19120001', 68, '2019-12-31', 70, 1, 0, 3950000.000000002, NULL, 0, NULL, 1, '2021-02-17 08:17:04', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :48 Bulan'),
(177, 'AT11010001', 69, '2011-01-01', 27, 1, 3455056, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:04', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(178, 'AT19120001', 70, '2019-12-31', 27, 1, 0, 3455055.999999998, NULL, 0, NULL, 1, '2021-02-17 08:17:04', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :48 Bulan'),
(179, 'AT17020001', 71, '2017-02-01', 32, 1, 4443000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:04', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(180, 'AT19120001', 72, '2019-12-31', 32, 1, 0, 3239687.5, NULL, 0, NULL, 1, '2021-02-17 08:17:04', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :35 Bulan'),
(181, 'AT04040001', 73, '2004-04-26', 16, 1, 780000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:04', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(182, 'AT19120001', 74, '2019-12-31', 16, 1, 0, 780000, NULL, 0, NULL, 1, '2021-02-17 08:17:04', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :48 Bulan'),
(183, 'AT04040001', 75, '2004-04-26', 18, 1, 750000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:04', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(184, 'AT19120001', 76, '2019-12-31', 18, 1, 0, 750000, NULL, 0, NULL, 1, '2021-02-17 08:17:04', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :48 Bulan'),
(185, 'AT04050001', 77, '2004-05-06', 21, 1, 1900000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:04', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(186, 'AT19120001', 78, '2019-12-31', 21, 1, 0, 1899999.9999999984, NULL, 0, NULL, 1, '2021-02-17 08:17:04', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :48 Bulan'),
(187, 'AT04050001', 79, '2004-05-06', 22, 1, 960000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:04', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(188, 'AT19120001', 80, '2019-12-31', 22, 1, 0, 960000, NULL, 0, NULL, 1, '2021-02-17 08:17:04', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :48 Bulan'),
(189, 'AT15030001', 81, '2015-03-01', 29, 1, 3297000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:04', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(190, 'AT19120001', 82, '2019-12-31', 29, 1, 0, 3297000, NULL, 0, NULL, 1, '2021-02-17 08:17:04', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :48 Bulan'),
(191, 'AT19100001', 83, '2019-10-01', 39, 1, 3935600, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:04', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(192, 'AT19120001', 84, '2019-12-31', 39, 1, 0, 245975.00000000012, NULL, 0, NULL, 1, '2021-02-17 08:17:04', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :3 Bulan'),
(193, 'AT04040001', 85, '2004-04-26', 17, 1, 550000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:04', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(194, 'AT19120001', 86, '2019-12-31', 17, 1, 0, 549999.9999999984, NULL, 0, NULL, 1, '2021-02-17 08:17:04', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :48 Bulan'),
(195, 'AT04040001', 87, '2004-04-26', 14, 1, 400000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:04', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(196, 'AT19120001', 88, '2019-12-31', 14, 1, 0, 399999.9999999999, NULL, 0, NULL, 1, '2021-02-17 08:17:04', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :48 Bulan'),
(197, 'AT04040001', 89, '2004-04-26', 15, 1, 940000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:05', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(198, 'AT19120001', 90, '2019-12-31', 15, 1, 0, 939999.9999999984, NULL, 0, NULL, 1, '2021-02-17 08:17:05', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :48 Bulan'),
(199, 'AT13030001', 91, '2013-03-01', 81, 1, 5820000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:05', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(200, 'AT19120001', 92, '2019-12-31', 81, 1, 0, 5820000, NULL, 0, NULL, 1, '2021-02-17 08:17:05', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :48 Bulan'),
(201, 'AT18110001', 93, '2018-11-01', 100, 1, 9000000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:05', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(202, 'AT19120001', 94, '2019-12-31', 100, 1, 0, 2625000, NULL, 0, NULL, 1, '2021-02-17 08:17:05', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :14 Bulan'),
(203, 'AT04040001', 95, '2004-04-26', 10, 1, 800000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:05', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(204, 'AT19120001', 96, '2019-12-31', 10, 1, 0, 800000.0000000016, NULL, 0, NULL, 1, '2021-02-17 08:17:05', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :48 Bulan'),
(205, 'AT19090001', 97, '2019-09-01', 38, 1, 1680000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:05', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(206, 'AT19120001', 98, '2019-12-31', 38, 1, 0, 140000, NULL, 0, NULL, 1, '2021-02-17 08:17:05', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :4 Bulan'),
(207, 'AT19090001', 99, '2019-09-01', 59, 1, 89630100, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:05', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(208, 'AT19120001', 100, '2019-12-31', 59, 1, 0, 3734587.5, NULL, 0, NULL, 1, '2021-02-17 08:17:05', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :4 Bulan'),
(209, 'AT10010001', 101, '2010-01-12', 26, 1, 5032500, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:05', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(210, 'AT19120001', 102, '2019-12-31', 26, 1, 0, 5032500, NULL, 0, NULL, 1, '2021-02-17 08:17:05', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :48 Bulan'),
(211, 'AT11010001', 103, '2011-01-12', 28, 1, 844753, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:05', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(212, 'AT19120001', 104, '2019-12-31', 28, 1, 0, 844752.9999999984, NULL, 0, NULL, 1, '2021-02-17 08:17:06', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :48 Bulan'),
(213, 'AT04100001', 105, '2004-10-04', 23, 1, 2100000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:06', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(214, 'AT19120001', 106, '2019-12-31', 23, 1, 0, 2100000, NULL, 0, NULL, 1, '2021-02-17 08:17:06', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :48 Bulan'),
(215, 'AT04040001', 107, '2004-04-26', 11, 1, 1400000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:06', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(216, 'AT19120001', 108, '2019-12-31', 11, 1, 0, 1400000.0000000016, NULL, 0, NULL, 1, '2021-02-17 08:17:06', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :48 Bulan'),
(217, 'AT16070001', 109, '2016-07-01', 31, 1, 1615000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:06', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(218, 'AT19120001', 110, '2019-12-31', 31, 1, 0, 1413124.9999999986, NULL, 0, NULL, 1, '2021-02-17 08:17:06', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :42 Bulan'),
(219, 'AT17080001', 111, '2017-08-01', 33, 1, 1850000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:06', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(220, 'AT19120001', 112, '2019-12-31', 33, 1, 0, 1117708.3333333344, NULL, 0, NULL, 1, '2021-02-17 08:17:06', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :29 Bulan'),
(221, 'AT19030001', 113, '2019-03-01', 35, 1, 1599000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:06', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(222, 'AT19120001', 114, '2019-12-31', 35, 1, 0, 333125, NULL, 0, NULL, 1, '2021-02-17 08:17:06', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :10 Bulan'),
(223, 'AT19080001', 115, '2019-08-01', 37, 1, 900000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:06', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(224, 'AT19120001', 116, '2019-12-31', 37, 1, 0, 93750, NULL, 0, NULL, 1, '2021-02-17 08:17:06', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :5 Bulan'),
(225, 'AT04050001', 117, '2004-05-06', 19, 1, 3000000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:06', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(226, 'AT19120001', 118, '2019-12-31', 19, 1, 0, 3000000, NULL, 0, NULL, 1, '2021-02-17 08:17:06', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :48 Bulan'),
(227, 'AT04040001', 119, '2004-04-26', 12, 1, 800000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:06', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(228, 'AT19120001', 120, '2019-12-31', 12, 1, 0, 800000.0000000016, NULL, 0, NULL, 1, '2021-02-17 08:17:06', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :48 Bulan'),
(229, 'AT04040001', 121, '2004-04-26', 13, 1, 700000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:06', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(230, 'AT19120001', 122, '2019-12-31', 13, 1, 0, 699999.9999999984, NULL, 0, NULL, 1, '2021-02-17 08:17:06', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :48 Bulan'),
(231, 'AT04120001', 123, '2004-12-14', 25, 1, 500000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:06', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(232, 'AT19120001', 124, '2019-12-31', 25, 1, 0, 500000.00000000163, NULL, 0, NULL, 1, '2021-02-17 08:17:06', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :48 Bulan'),
(233, 'AT04040001', 125, '2004-04-25', 9, 1, 210000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:06', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(234, 'AT19120001', 126, '2019-12-31', 9, 1, 0, 210000, NULL, 0, NULL, 1, '2021-02-17 08:17:06', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :48 Bulan'),
(235, 'AT04040001', 127, '2004-04-25', 4, 1, 1500000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:06', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(236, 'AT19120001', 128, '2019-12-31', 4, 1, 0, 1500000, NULL, 0, NULL, 1, '2021-02-17 08:17:06', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :48 Bulan'),
(237, 'AT18040001', 129, '2018-04-01', 43, 1, 447182645, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:06', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(238, 'AT19120001', 130, '2019-12-31', 43, 1, 0, 97821203.59374994, NULL, 0, NULL, 1, '2021-02-17 08:17:06', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :21 Bulan'),
(239, 'AT18100001', 131, '2018-10-01', 46, 1, 270502854, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:06', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(240, 'AT19120001', 132, '2019-12-31', 46, 1, 0, 42266070.9375, NULL, 0, NULL, 1, '2021-02-17 08:17:06', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :15 Bulan'),
(241, 'AT19070001', 133, '2019-07-01', 54, 1, 1868800, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:07', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(242, 'AT19120001', 134, '2019-12-31', 54, 1, 0, 116800.0000000002, NULL, 0, NULL, 1, '2021-02-17 08:17:07', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :6 Bulan'),
(243, 'AT17100001', 135, '2017-10-16', 3, 1, 178229533, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:07', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(244, 'AT19120001', 136, '2019-12-31', 3, 1, 0, 50127056.145, NULL, 0, NULL, 1, '2021-02-17 08:17:07', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :27 Bulan'),
(245, 'AT19070001', 137, '2019-07-01', 56, 1, 1448000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:07', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(246, 'AT19120001', 138, '2019-12-31', 56, 1, 0, 90499.9999999998, NULL, 0, NULL, 1, '2021-02-17 08:17:07', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :6 Bulan'),
(247, 'AT19080001', 139, '2019-08-01', 57, 1, 1650000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:07', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(248, 'AT19120001', 140, '2019-12-31', 57, 1, 0, 85937.5, NULL, 0, NULL, 1, '2021-02-17 08:17:07', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :5 Bulan'),
(249, 'AT19090001', 141, '2019-09-01', 60, 1, 1448000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:07', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(250, 'AT19120001', 142, '2019-12-31', 60, 1, 0, 60333.3333333332, NULL, 0, NULL, 1, '2021-02-17 08:17:07', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :4 Bulan'),
(251, 'AT19120001', 143, '2019-12-01', 65, 1, 1550000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:07', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(252, 'AT19120001', 144, '2019-12-31', 65, 1, 0, 16145.8333333333, NULL, 0, NULL, 1, '2021-02-17 08:17:07', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :1 Bulan'),
(253, 'AT19030001', 145, '2019-03-01', 49, 1, 3300000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:07', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(254, 'AT19120001', 146, '2019-12-31', 49, 1, 0, 343750, NULL, 0, NULL, 1, '2021-02-17 08:17:07', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :10 Bulan'),
(255, 'AT19110001', 147, '2019-11-01', 64, 1, 20000000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:07', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(256, 'AT19120001', 148, '2019-12-31', 64, 1, 0, 416666.666666666, NULL, 0, NULL, 1, '2021-02-17 08:17:07', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :2 Bulan'),
(257, 'AT14110001', 149, '2014-11-01', 85, 1, 1786000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:07', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(258, 'AT19120001', 150, '2019-12-31', 85, 1, 0, 1785999.9999999984, NULL, 0, NULL, 1, '2021-02-17 08:17:07', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :48 Bulan'),
(259, 'AT13040001', 151, '2013-04-01', 82, 1, 14545455, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:07', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(260, 'AT19120001', 152, '2019-12-31', 82, 1, 0, 14545455, NULL, 0, NULL, 1, '2021-02-17 08:17:07', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :48 Bulan'),
(261, 'AT18010001', 153, '2018-01-01', 40, 1, 181024893, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:07', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(262, 'AT19120001', 154, '2019-12-31', 40, 1, 0, 45256223.25, NULL, 0, NULL, 1, '2021-02-17 08:17:07', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :24 Bulan'),
(263, 'AT19070001', 155, '2019-07-01', 55, 1, 1384800, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:08', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(264, 'AT19120001', 156, '2019-12-31', 55, 1, 0, 86550, NULL, 0, NULL, 1, '2021-02-17 08:17:08', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :6 Bulan'),
(265, 'AT19090001', 157, '2019-09-01', 61, 1, 2865600, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:08', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(266, 'AT19120001', 158, '2019-12-31', 61, 1, 0, 119400, NULL, 0, NULL, 1, '2021-02-17 08:17:08', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :4 Bulan'),
(267, 'AT19090001', 159, '2019-09-01', 105, 1, 2895722, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:08', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(268, 'AT19120001', 160, '2019-12-31', 105, 1, 0, 241310.1666666668, NULL, 0, NULL, 1, '2021-02-17 08:17:08', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :4 Bulan'),
(269, 'AT18080001', 161, '2018-08-01', 44, 1, 33815230, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:08', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(270, 'AT19120001', 162, '2019-12-31', 44, 1, 0, 5988113.645833339, NULL, 0, NULL, 1, '2021-02-17 08:17:08', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :17 Bulan'),
(271, 'AT18010001', 163, '2018-01-01', 41, 1, 2850000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:08', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(272, 'AT19120001', 164, '2019-12-31', 41, 1, 0, 712500, NULL, 0, NULL, 1, '2021-02-17 08:17:08', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :24 Bulan'),
(273, 'AT08070001', 165, '2008-07-31', 76, 1, 3125000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:08', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(274, 'AT19120001', 166, '2019-12-31', 76, 1, 0, 3125000.000000002, NULL, 0, NULL, 1, '2021-02-17 08:17:08', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :48 Bulan'),
(275, 'AT19040001', 167, '2019-04-01', 102, 1, 1200000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:08', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(276, 'AT19120001', 168, '2019-12-31', 102, 1, 0, 225000, NULL, 0, NULL, 1, '2021-02-17 08:17:08', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :9 Bulan'),
(277, 'AT13010001', 169, '2013-01-01', 80, 1, 3033750, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:08', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(278, 'AT19120001', 170, '2019-12-31', 80, 1, 0, 3033750, NULL, 0, NULL, 1, '2021-02-17 08:17:08', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :48 Bulan'),
(279, 'AT13070001', 171, '2013-07-01', 83, 1, 610000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:08', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(280, 'AT19120001', 172, '2019-12-31', 83, 1, 0, 609999.9999999984, NULL, 0, NULL, 1, '2021-02-17 08:17:08', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :48 Bulan'),
(281, 'AT15090001', 173, '2015-09-01', 87, 1, 1536100, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:08', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(282, 'AT19120001', 174, '2019-12-31', 87, 1, 0, 1536099.9999999984, NULL, 0, NULL, 1, '2021-02-17 08:17:08', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :48 Bulan'),
(283, 'AT18010001', 175, '2018-01-01', 93, 1, 2890000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:08', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(284, 'AT19120001', 176, '2019-12-31', 93, 1, 0, 1444999.999999999, NULL, 0, NULL, 1, '2021-02-17 08:17:08', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :24 Bulan'),
(285, 'AT04040001', 177, '2004-04-25', 68, 1, 1500000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:08', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(286, 'AT19120001', 178, '2019-12-31', 68, 1, 0, 1500000, NULL, 0, NULL, 1, '2021-02-17 08:17:08', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :48 Bulan'),
(287, 'AT12060001', 179, '2012-06-01', 79, 1, 12000000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:08', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(288, 'AT19120001', 180, '2019-12-31', 79, 1, 0, 12000000, NULL, 0, NULL, 1, '2021-02-17 08:17:08', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :48 Bulan'),
(289, 'AT18100001', 181, '2018-10-01', 99, 1, 4000000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:08', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(290, 'AT19120001', 182, '2019-12-31', 99, 1, 0, 1249999.9999999995, NULL, 0, NULL, 1, '2021-02-17 08:17:08', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :15 Bulan'),
(291, 'AT18040001', 183, '2018-04-01', 34, 1, 25000000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:09', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(292, 'AT19120001', 184, '2019-12-31', 34, 1, 0, 10937499.999999993, NULL, 0, NULL, 1, '2021-02-17 08:17:09', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :21 Bulan'),
(293, 'AT16020001', 185, '2016-02-01', 30, 1, 1016720, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:09', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(294, 'AT19120001', 186, '2019-12-31', 30, 1, 0, 995538.3333333349, NULL, 0, NULL, 1, '2021-02-17 08:17:09', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :47 Bulan'),
(295, 'AT19100001', 187, '2019-10-01', 63, 1, 179611060, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:09', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(296, 'AT19120001', 188, '2019-12-31', 63, 1, 0, 5612845.62500001, NULL, 0, NULL, 1, '2021-02-17 08:17:09', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :3 Bulan'),
(297, 'AT19100001', 189, '2019-10-01', 62, 1, 250848940, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:09', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(298, 'AT19120001', 190, '2019-12-31', 62, 1, 0, 7839029.375000009, NULL, 0, NULL, 1, '2021-02-17 08:17:09', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :3 Bulan'),
(299, 'AT04040001', 191, '2004-04-25', 67, 1, 993500, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:09', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(300, 'AT19120001', 192, '2019-12-31', 67, 1, 0, 993500.0000000016, NULL, 0, NULL, 1, '2021-02-17 08:17:09', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :48 Bulan'),
(301, 'AT05010001', 193, '2005-01-20', 73, 1, 835000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:09', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(302, 'AT19120001', 194, '2019-12-31', 73, 1, 0, 834999.9999999984, NULL, 0, NULL, 1, '2021-02-17 08:17:09', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :48 Bulan'),
(303, 'AT04050001', 195, '2004-05-06', 20, 1, 950000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:09', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(304, 'AT19120001', 196, '2019-12-31', 20, 1, 0, 950000.0000000016, NULL, 0, NULL, 1, '2021-02-17 08:17:09', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :48 Bulan'),
(305, 'AT19030001', 197, '2019-03-01', 36, 1, 2258000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:09', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(306, 'AT19120001', 198, '2019-12-31', 36, 1, 0, 470416.666666667, NULL, 0, NULL, 1, '2021-02-17 08:17:09', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :10 Bulan'),
(307, 'AT19090001', 199, '2019-09-01', 104, 1, 9640000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:09', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(308, 'AT19120001', 200, '2019-12-31', 104, 1, 0, 803333.333333332, NULL, 0, NULL, 1, '2021-02-17 08:17:09', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :4 Bulan'),
(309, 'AT04040001', 201, '2004-04-30', 2, 1000, 161374326, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:09', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(310, 'AT19120001', 202, '2019-12-31', 2, 1000, 0, 161374326.048, NULL, 0, NULL, 1, '2021-02-17 08:17:10', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :96 Bulan'),
(311, 'AT10050001', 203, '2010-05-14', 7, 1, 217540455, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:10', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(312, 'AT19120001', 204, '2019-12-31', 7, 1, 0, 217540455, NULL, 0, NULL, 1, '2021-02-17 08:17:10', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :96 Bulan'),
(313, 'AT18100001', 205, '2018-10-01', 47, 1, 8000000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:10', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(314, 'AT19120001', 206, '2019-12-31', 47, 1, 0, 1249999.9999999995, NULL, 0, NULL, 1, '2021-02-17 08:17:10', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :15 Bulan'),
(315, 'AT18120001', 207, '2018-12-01', 48, 1, 54868000, 0, NULL, 0, NULL, 1, '2021-02-17 08:17:10', 1, '0000-00-00 00:00:00', 1, 'Saldo Awal'),
(316, 'AT19120001', 208, '2019-12-31', 48, 1, 0, 7430041.666666671, NULL, 0, NULL, 1, '2021-02-17 08:17:10', 1, '0000-00-00 00:00:00', 1, 'Akumulasi untuk penyusutan tahun lalu sejumlah :13 Bulan');

-- --------------------------------------------------------

--
-- Table structure for table `canggaran`
--

CREATE TABLE `canggaran` (
  `AID` int(11) NOT NULL,
  `ACOA` int(11) DEFAULT NULL,
  `ATAHUN` int(11) DEFAULT NULL,
  `ATAHUN2` int(11) DEFAULT NULL,
  `AR1` double DEFAULT '0',
  `AR2` double DEFAULT '0',
  `AR3` double DEFAULT '0',
  `AR4` double DEFAULT '0',
  `AR5` double DEFAULT '0',
  `AR6` double DEFAULT '0',
  `AR7` double DEFAULT '0',
  `AR8` double DEFAULT '0',
  `AR9` double DEFAULT '0',
  `AR10` double DEFAULT '0',
  `AR11` double DEFAULT '0',
  `AR12` double DEFAULT '0',
  `ARTAHUNLALU` double DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `cconfigcoa`
--

CREATE TABLE `cconfigcoa` (
  `CCID` int(11) NOT NULL,
  `CCKODE` varchar(5) NOT NULL,
  `CCKETERANGAN` varchar(255) DEFAULT NULL,
  `CCCOA` int(11) NOT NULL DEFAULT '0',
  `C` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cconfigcoa`
--

INSERT INTO `cconfigcoa` (`CCID`, `CCKODE`, `CCKETERANGAN`, `CCCOA`, `C`) VALUES
(2, 'PJ', 'PERSEDIAANBARANG', 0, 89),
(3, 'PJ', 'HUTANGDAGANG', 38, 89),
(4, 'PJ', 'PAJAKMASUKAN', 0, 89),
(5, 'PR', 'HUTANGRETUR', 48, 89),
(6, 'PR', 'RETURPEMBELIAN', 89, 89),
(7, 'PR', 'PAJAKMASUKAN', 0, 89),
(8, 'VP', 'DISKONPEMBELIAN', 55, 89),
(9, 'VP', 'KASBANK', 0, 89),
(10, 'VP', 'HUTANGRETUR', 48, 89),
(12, 'VP', 'HUTANGDAGANG', 38, 89),
(13, 'ISP', 'PERSEDIAANBARANG', 0, 89),
(14, 'ISP', 'HUTANGSERVICE', 0, 89),
(15, 'IBK', 'PERSEDIAANBARANG', 0, 89),
(16, 'IBK', 'HUTANGBIAYAKIRIM', 0, 89),
(17, 'BKG', 'HARGAPOKOK', 0, 89),
(18, 'BKG', 'PERSEDIAANBARANG', 0, 89),
(19, 'IV', 'PIUTANGDAGANG', 14, 89),
(20, 'IV', 'PAJAKKELUARAN', 0, 89),
(21, 'IV', 'PENDAPATANPENJUALAN', 52, 89),
(22, 'IBJ', 'PIUTANGBIAYAKIRIM', 0, 89),
(23, 'IBJ', 'PENDAPATANPENGIRIMAN', 0, 89),
(24, 'SR', 'RETURPENJUALAN', 56, 89),
(25, 'SR', 'PAJAKKELUARAN', 0, 89),
(26, 'SR', 'PIUTANGRETUR', 97, 89),
(27, 'CP', 'KASBANK', 0, 89),
(28, 'CP', 'DISKONPENJUALAN', 95, 89),
(29, 'CP', 'PIUTANGRETUR', 97, 89),
(30, 'CP', 'PIUTANGDAGANG', 14, 89),
(31, 'IP', 'KASBANK', 7, 89),
(32, 'IP', 'PIUTANGKARTUKREDIT', 97, 89),
(33, 'IP', 'PIUTANGKARTUDEBIT', 97, 89),
(34, 'IP', 'HUTANGDP', 0, 89),
(35, 'DPP', 'KASDP', 0, 89),
(36, 'DPP', 'HUTANGDP', 0, 89),
(38, 'DPB', 'KASBENGKEL', 0, 89),
(39, 'DPB', 'HUTANGDPBENGKEL', 0, 89),
(40, 'PKB', 'PIUTANGBENGKEL', 0, 89),
(41, 'PKB', 'PENDAPATANSERVICE', 0, 89),
(42, 'PPB', 'KASBENGKEL', 0, 89),
(43, 'PPB', 'PIUTANGBENGKEL', 0, 89),
(45, 'PJ', 'HUTANGKONSINYASI', 0, 89),
(47, 'RL', 'BERJALAN', 51, 89),
(48, 'RL', 'DITAHAN', 50, 89),
(50, 'ITP', 'PERSEDIAAN', 17, 89),
(51, 'ITT', 'PENDAPATAN', 52, 89),
(52, 'ITH', 'HARGAPOKOK', 57, 89),
(61, 'SJ', 'PERSEDIAANTERKIRIM', 0, 89),
(62, 'SJ', 'PERSEDIAANBARANG', 0, 89),
(65, 'DPV', 'UANGMUKAPEMBELIAN', 0, 89),
(66, 'DPV', 'HUTANGUANGMUKA', 48, 89),
(67, 'DPV', 'PAJAKMASUKAN', 20, 89),
(68, 'VP', 'HUTANGUANGMUKA', 48, 89),
(69, 'DPC', 'PIUTANGUANGMUKA', 14, 89),
(70, 'DPC', 'UANGMUKAPENJUALAN', 0, 89),
(71, 'DPC', 'PAJAKKELUARAN', 45, 89),
(72, 'CP', 'PIUTANGUANGMUKA', 14, 89),
(73, 'TB', 'PERSEDIAANBELUMFAKTUR', 0, 89),
(74, '', NULL, 0, 89),
(75, '', NULL, 0, 89),
(76, '', NULL, 0, 89),
(77, '', NULL, 0, 89),
(78, '', NULL, 0, 89),
(79, 'SJ', 'HARGAPOKOK', 0, 89),
(80, '', NULL, 0, 89),
(81, '', NULL, 0, 89),
(82, '', NULL, 0, 89),
(83, '', NULL, 0, 89),
(84, '', NULL, 0, 89),
(85, '', NULL, 0, 89),
(86, '', NULL, 0, 89),
(87, '', NULL, 0, 89),
(88, '', NULL, 0, 89),
(89, '', NULL, 0, 89),
(90, '', NULL, 0, 89),
(91, '', NULL, 0, 89),
(92, '', NULL, 0, 89),
(93, '', NULL, 0, 89),
(94, '', NULL, 0, 89),
(95, '', NULL, 0, 89),
(96, '', NULL, 0, 89),
(97, '', NULL, 0, 89),
(98, '', NULL, 0, 89),
(99, '', NULL, 0, 89),
(100, '', NULL, 0, 89),
(101, '', NULL, 0, 89),
(102, '', NULL, 0, 89),
(103, '', NULL, 0, 89),
(104, '', NULL, 0, 89),
(105, '', NULL, 0, 89),
(106, '', NULL, 0, 89),
(107, '', NULL, 0, 89),
(108, '', NULL, 0, 89),
(109, '', NULL, 0, 89),
(110, '', NULL, 0, 89),
(111, '', NULL, 0, 89),
(112, '', NULL, 0, 89),
(113, '', NULL, 0, 89),
(114, '', NULL, 0, 89),
(115, '', NULL, 0, 89),
(116, '', NULL, 0, 89),
(117, '', NULL, 0, 89),
(118, '', NULL, 0, 89),
(119, '', NULL, 0, 89),
(120, '', NULL, 0, 89),
(121, '', NULL, 0, 89),
(122, '', NULL, 0, 89),
(123, '', NULL, 0, 89),
(124, '', NULL, 0, 89),
(125, '', NULL, 0, 89),
(126, '', NULL, 0, 89),
(127, '', NULL, 0, 89),
(128, '', NULL, 0, 89),
(129, '', NULL, 0, 89),
(130, '', NULL, 0, 89),
(131, '', NULL, 0, 89),
(132, '', NULL, 0, 89),
(133, '', NULL, 0, 89),
(134, '', NULL, 0, 89),
(135, '', NULL, 0, 89),
(136, '', NULL, 0, 89),
(137, '', NULL, 0, 89),
(138, '', NULL, 0, 89),
(139, '', NULL, 0, 89),
(140, '', NULL, 0, 89),
(141, '', NULL, 0, 89),
(142, '', NULL, 0, 89),
(143, '', NULL, 0, 89),
(144, '', NULL, 0, 89),
(145, '', NULL, 0, 89),
(146, '', NULL, 0, 89),
(147, '', NULL, 0, 89),
(148, '', NULL, 0, 89),
(149, '', NULL, 0, 89),
(150, '', NULL, 0, 89),
(151, '', NULL, 0, 89),
(152, '', NULL, 0, 89),
(153, '', NULL, 0, 89),
(154, '', NULL, 0, 89),
(155, '', NULL, 0, 89),
(156, '', NULL, 0, 89),
(157, '', NULL, 0, 89),
(158, '', NULL, 0, 89);

-- --------------------------------------------------------

--
-- Table structure for table `cnoantrian`
--

CREATE TABLE `cnoantrian` (
  `cnaid` int(11) NOT NULL,
  `cnatanggal` datetime DEFAULT CURRENT_TIMESTAMP,
  `cnajenis` int(11) DEFAULT '1',
  `cnanourut` double DEFAULT NULL,
  `cnacomputer` varchar(100) DEFAULT NULL,
  `cnauser` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `cperiode`
--

CREATE TABLE `cperiode` (
  `PID` int(11) NOT NULL,
  `PTAHUN` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cperiode`
--

INSERT INTO `cperiode` (`PID`, `PTAHUN`) VALUES
(1, 2018),
(7, 2019),
(6, 2020),
(8, 2021);

-- --------------------------------------------------------

--
-- Table structure for table `ctransaksid`
--

CREATE TABLE `ctransaksid` (
  `CDID` int(11) NOT NULL,
  `CDIDU` int(11) NOT NULL,
  `CDURUTAN` int(11) NOT NULL DEFAULT '0',
  `CDNOCOA` int(11) NOT NULL,
  `CDDEBIT` double DEFAULT '0',
  `CDKREDIT` double DEFAULT '0',
  `CDDEBITV` double DEFAULT '0',
  `CDKREDITV` double DEFAULT '0',
  `CDUANG` int(11) NOT NULL,
  `CDKURS` double DEFAULT '0',
  `CDDVISI` int(11) DEFAULT NULL,
  `CDCATATAN` varchar(255) DEFAULT NULL,
  `CDCUSTOM1` varchar(255) DEFAULT NULL,
  `CDCUSTOM2` varchar(255) DEFAULT NULL,
  `CDCUSTOM3` varchar(255) DEFAULT NULL,
  `CDCUSTOM4` varchar(255) DEFAULT NULL,
  `CDCUSTOM5` varchar(255) DEFAULT NULL,
  `CDJUMLAH` double DEFAULT '0',
  `CDJUMLAHV` double DEFAULT '0',
  `CDEVENT` int(11) DEFAULT NULL,
  `CDGROUP` int(11) DEFAULT '0',
  `CDKLIRING` smallint(6) DEFAULT '0',
  `CDIDPD` int(11) DEFAULT NULL,
  `CDJUMLAHBAYAR` double DEFAULT '0',
  `CDBKKID` int(11) DEFAULT NULL,
  `CDCABANG` int(11) DEFAULT NULL,
  `CDDIBUATPENGAJUAN` smallint(6) DEFAULT '0',
  `CDPROYEK` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ctransaksid`
--

INSERT INTO `ctransaksid` (`CDID`, `CDIDU`, `CDURUTAN`, `CDNOCOA`, `CDDEBIT`, `CDKREDIT`, `CDDEBITV`, `CDKREDITV`, `CDUANG`, `CDKURS`, `CDDVISI`, `CDCATATAN`, `CDCUSTOM1`, `CDCUSTOM2`, `CDCUSTOM3`, `CDCUSTOM4`, `CDCUSTOM5`, `CDJUMLAH`, `CDJUMLAHV`, `CDEVENT`, `CDGROUP`, `CDKLIRING`, `CDIDPD`, `CDJUMLAHBAYAR`, `CDBKKID`, `CDCABANG`, `CDDIBUATPENGAJUAN`, `CDPROYEK`) VALUES
(1, 1, 1, 17, 5000000, 0, 0, 0, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, 0, NULL, NULL, 0, NULL),
(2, 1, 2, 51, 0, 5000000, 0, 0, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, 0, NULL, NULL, 0, NULL),
(3, 2, 1, 8, 100000000, 0, 0, 0, 0, 1, NULL, 'Setoran Modal Awal', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, 0, NULL, NULL, 0, NULL),
(4, 2, 2, 49, 0, 100000000, 0, 0, 0, 1, NULL, 'Setoran Modal', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, 0, NULL, NULL, 0, NULL),
(5, 3, 1, 17, 2000000, 0, 0, 0, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, 0, NULL, NULL, 0, NULL),
(6, 3, 2, 51, 0, 2000000, 0, 0, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, 0, NULL, NULL, 0, NULL),
(7, 4, 1, 17, 5000000, 0, 0, 0, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, 0, NULL, NULL, 0, NULL),
(8, 4, 2, 51, 0, 5000000, 0, 0, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, 0, NULL, NULL, 0, NULL),
(9, 5, 1, 11, 200000, 0, 0, 0, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, 0, NULL, NULL, 0, NULL),
(10, 5, 1, 28, 0, 200000, 0, 0, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, 0, NULL, NULL, 0, NULL),
(11, 6, 1, 14, 350000, 0, 0, 0, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, 0, NULL, NULL, 0, NULL),
(12, 6, 2, 28, 200000, 0, 0, 0, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, 0, NULL, NULL, 0, NULL),
(13, 6, 4, 45, 0, 50000, 0, 0, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, 0, NULL, NULL, 0, NULL),
(14, 6, 5, 52, 0, 500000, 0, 0, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, 0, NULL, NULL, 0, NULL),
(15, 6, 6, 57, 200000, 0, 0, 0, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, 0, NULL, NULL, 0, NULL),
(16, 6, 7, 17, 0, 200000, 0, 0, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, 0, NULL, NULL, 0, NULL),
(17, 7, 1, 11, 350000, 0, 0, 0, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, 0, NULL, NULL, 0, NULL),
(18, 7, 2, 14, 0, 350000, 0, 0, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, 0, NULL, NULL, 0, NULL),
(19, 8, 1, 17, 3000000, 0, 0, 0, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, 0, NULL, NULL, 0, NULL),
(20, 8, 2, 51, 0, 3000000, 0, 0, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, 0, NULL, NULL, 0, NULL),
(21, 9, 1, 17, 7000000, 0, 0, 0, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, 0, NULL, NULL, 0, NULL),
(22, 9, 2, 51, 0, 7000000, 0, 0, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, 0, NULL, NULL, 0, NULL),
(23, 10, 1, 17, 5000000, 0, 0, 0, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, 0, NULL, NULL, 0, NULL),
(24, 10, 2, 51, 0, 5000000, 0, 0, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, 0, NULL, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ctransaksigiro`
--

CREATE TABLE `ctransaksigiro` (
  `TGID` int(11) NOT NULL,
  `TGIDU` int(11) DEFAULT NULL,
  `TGSUMBER` varchar(6) DEFAULT NULL,
  `TGNOTRANSAKSI` varchar(25) DEFAULT NULL,
  `TGTANGGAL` date DEFAULT NULL,
  `TGKONTAK` int(11) DEFAULT NULL,
  `TGTANGGALTEMPO` date DEFAULT NULL,
  `TGNOGIRO` varchar(25) DEFAULT NULL,
  `TGBANK` int(11) DEFAULT NULL,
  `TGNOAC` varchar(50) DEFAULT NULL,
  `TGAKUNGIRO` int(11) DEFAULT NULL,
  `TGAKUNBANK` int(11) DEFAULT NULL,
  `TGUANG` int(11) DEFAULT NULL,
  `TGTOTALTRANSAKSI` double DEFAULT '0',
  `TGTOTALTRANSAKSIVALAS` double DEFAULT '0',
  `TGSTATUS` int(11) DEFAULT NULL,
  `TGREFF` int(11) DEFAULT NULL,
  `TGJENIS` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `ctransaksipd`
--

CREATE TABLE `ctransaksipd` (
  `CDID` int(11) NOT NULL,
  `CDIDU` int(11) NOT NULL,
  `CDURUTAN` int(11) NOT NULL DEFAULT '0',
  `CDNOCOA` int(11) NOT NULL,
  `CDDEBIT` double DEFAULT '0',
  `CDKREDIT` double DEFAULT '0',
  `CDDEBITV` double DEFAULT '0',
  `CDKREDITV` double DEFAULT '0',
  `CDUANG` int(11) NOT NULL,
  `CDKURS` double DEFAULT '0',
  `CDDVISI` int(11) DEFAULT NULL,
  `CDCATATAN` varchar(255) DEFAULT NULL,
  `CDCUSTOM1` varchar(255) DEFAULT NULL,
  `CDCUSTOM2` varchar(255) DEFAULT NULL,
  `CDCUSTOM3` varchar(255) DEFAULT NULL,
  `CDCUSTOM4` varchar(255) DEFAULT NULL,
  `CDCUSTOM5` varchar(255) DEFAULT NULL,
  `CDJUMLAH` double DEFAULT '0',
  `CDJUMLAHV` double DEFAULT '0',
  `CDEVENT` int(11) DEFAULT NULL,
  `CDGROUP` int(11) DEFAULT '0',
  `CDKLIRING` smallint(6) DEFAULT '0',
  `CDIDPD` int(11) DEFAULT NULL,
  `CDJUMLAHBAYAR` double DEFAULT '0',
  `CDBKKID` int(11) DEFAULT NULL,
  `CDCABANG` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Triggers `ctransaksipd`
--
DELIMITER $$
CREATE TRIGGER `ctransaksipd_add` AFTER INSERT ON `ctransaksipd` FOR EACH ROW begin

  DECLARE tmpVar INTEGER;
  update ctransaksid set CDDIBUATPENGAJUAN = 1 where cdid = NEW.CDBKKID ;
end
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `ctransaksipd_dell` BEFORE DELETE ON `ctransaksipd` FOR EACH ROW begin

  DECLARE tmpVar INTEGER;
  update ctransaksid set CDDIBUATPENGAJUAN = 0 where cdid = old.CDBKKID ;
end
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `ctransaksipu`
--

CREATE TABLE `ctransaksipu` (
  `CUID` int(11) NOT NULL,
  `CUSUMBER` varchar(5) NOT NULL,
  `CUNOTRANSAKSI` varchar(50) DEFAULT NULL,
  `CUTANGGAL` date DEFAULT NULL,
  `CUKONTAK` int(11) NOT NULL,
  `CUURAIAN` varchar(255) DEFAULT NULL,
  `CUNOREFF` varchar(150) DEFAULT NULL,
  `CUDIVISI` int(11) DEFAULT NULL,
  `CUREKKAS` int(11) DEFAULT NULL,
  `CUTOTALTRANS` double DEFAULT '0',
  `CUTOTALTRANSV` double DEFAULT '0',
  `CUSTATUS` int(11) DEFAULT '0',
  `CUUANG` int(11) DEFAULT NULL,
  `CUKURS` double DEFAULT '0',
  `CUTIPE` int(11) DEFAULT '0',
  `CUNOGIRO` varchar(50) DEFAULT NULL,
  `CUBANK` int(11) DEFAULT NULL,
  `CUNOAC` varchar(50) DEFAULT NULL,
  `CUAKUNGIRO` int(11) DEFAULT NULL,
  `CUTGLTEMPO` date DEFAULT NULL,
  `CUNOREFFTIPE` varchar(50) DEFAULT NULL,
  `CUCATATAN` varchar(255) DEFAULT NULL,
  `CUCREATEU` int(11) DEFAULT NULL,
  `CUMODIFU` int(11) DEFAULT NULL,
  `CUCREATED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CUTIPEBM` int(11) DEFAULT '0',
  `CUNOSUMBER` varchar(100) DEFAULT NULL,
  `CUPAKAI` smallint(6) DEFAULT '0',
  `CUMODIFD` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `CUCABANG` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `ctransaksiu`
--

CREATE TABLE `ctransaksiu` (
  `CUID` int(11) NOT NULL,
  `CUSUMBER` varchar(5) NOT NULL,
  `CUNOTRANSAKSI` varchar(50) DEFAULT NULL,
  `CUTANGGAL` date DEFAULT NULL,
  `CUKONTAK` int(11) NOT NULL,
  `CUURAIAN` varchar(255) DEFAULT NULL,
  `CUNOREFF` varchar(150) DEFAULT NULL,
  `CUDIVISI` int(11) DEFAULT NULL,
  `CUREKKAS` int(11) DEFAULT NULL,
  `CUTOTALTRANS` double DEFAULT '0',
  `CUTOTALTRANSV` double DEFAULT '0',
  `CUSTATUS` int(11) DEFAULT '0',
  `CUUANG` int(11) DEFAULT NULL,
  `CUKURS` double DEFAULT '0',
  `CUTIPE` int(11) DEFAULT '0',
  `CUNOGIRO` varchar(50) DEFAULT NULL,
  `CUBANK` int(11) DEFAULT NULL,
  `CUNOAC` varchar(50) DEFAULT NULL,
  `CUAKUNGIRO` int(11) DEFAULT NULL,
  `CUTGLTEMPO` date DEFAULT NULL,
  `CUNOREFFTIPE` varchar(50) DEFAULT NULL,
  `CUCATATAN` varchar(255) DEFAULT NULL,
  `CUCREATEU` int(11) DEFAULT NULL,
  `CUMODIFU` int(11) DEFAULT NULL,
  `CUCREATED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CUTIPEBM` int(11) DEFAULT '0',
  `CUNOSUMBER` varchar(100) DEFAULT NULL,
  `CUPAKAI` smallint(6) DEFAULT '0',
  `CUMODIFD` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CUCABANG` int(11) DEFAULT NULL,
  `CUPROYEK` int(11) DEFAULT NULL,
  `CUPROYEK2` int(11) DEFAULT NULL,
  `CUCATATANPIUTANG` varchar(255) DEFAULT NULL,
  `CUDEPRESIASI` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ctransaksiu`
--

INSERT INTO `ctransaksiu` (`CUID`, `CUSUMBER`, `CUNOTRANSAKSI`, `CUTANGGAL`, `CUKONTAK`, `CUURAIAN`, `CUNOREFF`, `CUDIVISI`, `CUREKKAS`, `CUTOTALTRANS`, `CUTOTALTRANSV`, `CUSTATUS`, `CUUANG`, `CUKURS`, `CUTIPE`, `CUNOGIRO`, `CUBANK`, `CUNOAC`, `CUAKUNGIRO`, `CUTGLTEMPO`, `CUNOREFFTIPE`, `CUCATATAN`, `CUCREATEU`, `CUMODIFU`, `CUCREATED`, `CUTIPEBM`, `CUNOSUMBER`, `CUPAKAI`, `CUMODIFD`, `CUCABANG`, `CUPROYEK`, `CUPROYEK2`, `CUCATATANPIUTANG`, `CUDEPRESIASI`) VALUES
(1, 'SAIT', 'SAIT21120001', '2021-12-31', 6, 'SALDO AWAL', NULL, NULL, NULL, 5000000, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2022-01-05 00:44:57', 0, NULL, 0, '2022-01-05 00:44:57', NULL, NULL, NULL, NULL, 0),
(2, 'BM', 'BM22010001', '2022-01-05', 8, 'Setoran Modal Awal', NULL, NULL, 8, 100000000, 0, 0, 1, 1, 2, '', 5, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2022-01-05 00:48:10', 0, NULL, 0, '2022-01-05 00:48:10', NULL, NULL, NULL, NULL, 0),
(3, 'SAIT', 'SAIT21120002', '2021-12-31', 6, 'SALDO AWAL', NULL, NULL, NULL, 2000000, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2022-01-05 00:49:45', 0, NULL, 0, '2022-01-05 00:49:45', NULL, NULL, NULL, NULL, 0),
(4, 'SAIT', 'SAIT21120003', '2021-12-31', 6, 'SALDO AWAL', NULL, NULL, NULL, 5000000, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2022-01-05 00:50:37', 0, NULL, 0, '2022-01-05 00:50:37', NULL, NULL, NULL, NULL, 0),
(5, 'DPC', 'DPC22010001', '2022-01-05', 1, '', NULL, NULL, NULL, 200000, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2022-01-05 00:54:49', 0, NULL, 0, '2022-01-05 00:54:49', NULL, NULL, NULL, NULL, 0),
(6, 'IV', 'IV22010001', '2022-01-05', 1, 'Tes Sales 1', NULL, NULL, NULL, 550000, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2022-01-05 00:56:40', 0, NULL, 0, '2022-01-05 00:56:40', NULL, NULL, NULL, NULL, 0),
(7, 'CP', 'CP22010001', '2022-01-05', 1, 'Pelunasan Sales 1', NULL, NULL, NULL, 350000, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2022-01-05 01:03:19', 0, NULL, 0, '2022-01-05 01:03:19', NULL, NULL, NULL, NULL, 0),
(8, 'SAIT', 'SAIT21120004', '2021-12-31', 6, 'SALDO AWAL', NULL, NULL, NULL, 3000000, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2022-01-05 01:13:07', 0, NULL, 0, '2022-01-05 01:13:07', NULL, NULL, NULL, NULL, 0),
(9, 'SAIT', 'SAIT21120005', '2021-12-31', 6, 'SALDO AWAL', NULL, NULL, NULL, 7000000, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2022-01-05 01:14:20', 0, NULL, 0, '2022-01-05 01:14:20', NULL, NULL, NULL, NULL, 0),
(10, 'SAIT', 'SAIT21120006', '2021-12-31', 6, 'SALDO AWAL', NULL, NULL, NULL, 5000000, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2022-01-05 01:16:12', 0, NULL, 0, '2022-01-05 01:16:12', NULL, NULL, NULL, NULL, 0);

--
-- Triggers `ctransaksiu`
--
DELIMITER $$
CREATE TRIGGER `ctransaksiu_dell` AFTER DELETE ON `ctransaksiu` FOR EACH ROW begin
  DECLARE tmpVar INTEGER;
   update ctransaksigiro set TGSTATUS=1  where TGREFF =  OLD.CUID;
   delete from ctransaksid where cdidu = OLD.CUID;
end
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `ctransaksiu_dell_depresiasi` AFTER DELETE ON `ctransaksiu` FOR EACH ROW BEGIN
	IF OLD.cudepresiasi=1 THEN
    	delete from baktivapenyusutan where apcuid = OLD.cuid; 
    end if;
end
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `ddp`
--

CREATE TABLE `ddp` (
  `DPID` int(11) NOT NULL,
  `DPNOTRANSAKSI` varchar(25) DEFAULT NULL,
  `DPTANGGAL` date DEFAULT NULL,
  `DPKONTAK` int(11) DEFAULT NULL,
  `DPJUMLAH` double DEFAULT '0',
  `DPJUMLAHV` double DEFAULT '0',
  `DPPAJAKN` double DEFAULT '0',
  `DPPAJAK` int(11) DEFAULT NULL,
  `DPUANG` int(11) DEFAULT NULL,
  `DPKURS` double DEFAULT '1',
  `DPKETERANGAN` varchar(255) DEFAULT NULL,
  `DPCPAJAK` int(11) DEFAULT NULL,
  `DPCKAS` int(11) DEFAULT NULL,
  `DPCDP` int(11) DEFAULT NULL,
  `DPSUMBER` varchar(5) DEFAULT NULL,
  `DPCREATEU` int(11) DEFAULT NULL,
  `DPCREATED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `DPMODIFU` int(11) DEFAULT NULL,
  `DPNOFAKTUPAJAK` varchar(25) DEFAULT NULL,
  `DPJUMLAHPERSEN` double DEFAULT '0',
  `DPPAKAIIV` double NOT NULL DEFAULT '0',
  `DPBAYAR` double DEFAULT '0',
  `DPCLOSE` smallint(6) DEFAULT '0',
  `DPIDJURNAL` int(11) DEFAULT NULL,
  `DPMODIFD` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `DPIDCP` int(11) DEFAULT NULL,
  `DPJUMLAHBAYAR` double DEFAULT '0',
  `DPKARYAWAN` int(11) DEFAULT NULL,
  `DPTIPE` int(11) NOT NULL DEFAULT '0',
  `DPTERMIN` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ddp`
--

INSERT INTO `ddp` (`DPID`, `DPNOTRANSAKSI`, `DPTANGGAL`, `DPKONTAK`, `DPJUMLAH`, `DPJUMLAHV`, `DPPAJAKN`, `DPPAJAK`, `DPUANG`, `DPKURS`, `DPKETERANGAN`, `DPCPAJAK`, `DPCKAS`, `DPCDP`, `DPSUMBER`, `DPCREATEU`, `DPCREATED`, `DPMODIFU`, `DPNOFAKTUPAJAK`, `DPJUMLAHPERSEN`, `DPPAKAIIV`, `DPBAYAR`, `DPCLOSE`, `DPIDJURNAL`, `DPMODIFD`, `DPIDCP`, `DPJUMLAHBAYAR`, `DPKARYAWAN`, `DPTIPE`, `DPTERMIN`) VALUES
(1, 'DPC22010001', '2022-01-05', 1, 200000, 0, 0, NULL, NULL, 1, '', NULL, 11, 28, 'DPC', 1, '2022-01-05 00:54:49', NULL, NULL, 0, 200000, 0, 0, NULL, '2022-01-05 00:54:49', NULL, 200000, NULL, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `einvoicepenjualanbiaya`
--

CREATE TABLE `einvoicepenjualanbiaya` (
  `IPBID` int(11) NOT NULL,
  `IPBCOA` int(11) DEFAULT NULL,
  `IPBJUMLAH` double DEFAULT '0',
  `IPBCATATAN` varchar(255) DEFAULT NULL,
  `IPBIDU` int(11) DEFAULT NULL,
  `IPBKURS` double DEFAULT '0',
  `IPBDIVISI` int(11) DEFAULT NULL,
  `IPBPROYEK` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `einvoicepenjualand`
--

CREATE TABLE `einvoicepenjualand` (
  `IPDID` int(11) NOT NULL,
  `IPDSUMBER` varchar(5) DEFAULT NULL,
  `IPDIDIPU` int(11) DEFAULT NULL,
  `IPDURUTAN` int(11) DEFAULT NULL,
  `IPDITEM` int(11) DEFAULT NULL,
  `IPDMASUK` double DEFAULT '0',
  `IPDKELUAR` double DEFAULT '0',
  `IPDHARGA` double DEFAULT '0',
  `IPDDISKON` double DEFAULT '0',
  `IPDSATUAN` int(11) DEFAULT NULL,
  `IPDSATUAND` int(11) DEFAULT NULL,
  `IPDMASUKD` double DEFAULT '0',
  `IPDKELUARD` double DEFAULT '0',
  `IPDCATATAN` varchar(255) DEFAULT NULL,
  `IPDDIVISI` int(11) DEFAULT NULL,
  `IPDPROYEK` int(11) DEFAULT NULL,
  `IPDRETUR` double DEFAULT '0',
  `IPDBTGD` int(11) DEFAULT NULL,
  `IPDBTGU` int(11) DEFAULT NULL,
  `IPDPOU` int(11) DEFAULT NULL,
  `IPDPAJAKM` int(11) DEFAULT NULL,
  `IPDPAJAKK` int(11) DEFAULT NULL,
  `IPDPAJAKMP` double DEFAULT '0',
  `IPDPAJAKKP` double DEFAULT '0',
  `IPDPAJAKMN` double DEFAULT '0',
  `IPDPAJAKKN` double DEFAULT '0',
  `IPDSJU` int(11) DEFAULT NULL,
  `IPDSJD` int(11) DEFAULT NULL,
  `IPDSUBTOTAL` double DEFAULT '0',
  `IPDTOTAL` double DEFAULT '0',
  `IPDHPP` double DEFAULT '0',
  `IPDDISKON2` double DEFAULT '0',
  `IPDSUBTOTAL2` double DEFAULT '0',
  `IPDDISKONP` double DEFAULT '0',
  `IPDCATATAN2` varchar(255) DEFAULT NULL,
  `IPDGUDANG` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `einvoicepenjualand`
--

INSERT INTO `einvoicepenjualand` (`IPDID`, `IPDSUMBER`, `IPDIDIPU`, `IPDURUTAN`, `IPDITEM`, `IPDMASUK`, `IPDKELUAR`, `IPDHARGA`, `IPDDISKON`, `IPDSATUAN`, `IPDSATUAND`, `IPDMASUKD`, `IPDKELUARD`, `IPDCATATAN`, `IPDDIVISI`, `IPDPROYEK`, `IPDRETUR`, `IPDBTGD`, `IPDBTGU`, `IPDPOU`, `IPDPAJAKM`, `IPDPAJAKK`, `IPDPAJAKMP`, `IPDPAJAKKP`, `IPDPAJAKMN`, `IPDPAJAKKN`, `IPDSJU`, `IPDSJD`, `IPDSUBTOTAL`, `IPDTOTAL`, `IPDHPP`, `IPDDISKON2`, `IPDSUBTOTAL2`, `IPDDISKONP`, `IPDCATATAN2`, `IPDGUDANG`) VALUES
(1, NULL, 1, 1, 6, 0, 5, 100000, 0, 1, 1, 0, 5, 'Tes Sales 1', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 4, NULL, 0, 0, 0, 0, 0, 0, NULL, 13);

--
-- Triggers `einvoicepenjualand`
--
DELIMITER $$
CREATE TRIGGER `EINVOICEPENJUALAND_ADD` AFTER INSERT ON `einvoicepenjualand` FOR EACH ROW begin   
    UPDATE fstoku SET SUSTATUS = 3 WHERE SUID = new.IPDBTGU;
	UPDATE fstokd set SDFAKTUR = SDFAKTUR + ABS(NEW.IPDMASUK-NEW.IPDKELUAR) where sdid = new.IPDBTGD ;
	UPDATE fstokd set SDHARGAINVOICE = NEW.IPDHARGA where sdid = new.IPDBTGD ; 	   UPDATE fstokd set SDDISKONINVOICE = NEW.IPDDISKON where sdid = new.IPDBTGD ;

	UPDATE fstokd set SDFAKTUR = SDFAKTUR + ABS(NEW.IPDMASUK-NEW.IPDKELUAR) where sdid = new.IPDSJD ;
	UPDATE fstokd set SDHARGAINVOICE = NEW.IPDHARGA where sdid = new.IPDSJD ; 	   UPDATE fstokd set SDDISKONINVOICE = NEW.IPDDISKON where sdid = new.IPDSJD ;
end
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `EINVOICEPENJUALAND_DEL` AFTER DELETE ON `einvoicepenjualand` FOR EACH ROW begin
  UPDATE fstoku SET SUSTATUS = 1 WHERE SUID = OLD.IPDBTGU;
  UPDATE fstokd set SDFAKTUR = SDFAKTUR - ABS(OLD.IPDMASUK-OLD.IPDKELUAR) where sdid = OLD.IPDBTGD ;  

  UPDATE fstokd set SDFAKTUR = SDFAKTUR - ABS(OLD.IPDMASUK-OLD.IPDKELUAR) where sdid = OLD.IPDSJD ;  
end
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `EINVOICEPENJUALAND_EDIT` AFTER UPDATE ON `einvoicepenjualand` FOR EACH ROW begin
  UPDATE fstoku SET SUSTATUS = 1 WHERE SUID = OLD.IPDBTGU;  
  UPDATE fstoku SET SUSTATUS = 3 WHERE SUID = new.IPDBTGU;
  UPDATE fstokd set SDFAKTUR = SDFAKTUR - ABS(OLD.IPDMASUK-OLD.IPDKELUAR) where sdid = OLD.IPDBTGD ;
  UPDATE fstokd set SDFAKTUR = SDFAKTUR + ABS(NEW.IPDMASUK-NEW.IPDKELUAR) where sdid = NEW.IPDBTGD ;  

  UPDATE fstokd set SDFAKTUR = SDFAKTUR - ABS(OLD.IPDMASUK-OLD.IPDKELUAR) where sdid = OLD.IPDSJD ;
  UPDATE fstokd set SDFAKTUR = SDFAKTUR + ABS(NEW.IPDMASUK-NEW.IPDKELUAR) where sdid = NEW.IPDSJD ;  
end
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `einvoicepenjualandp`
--

CREATE TABLE `einvoicepenjualandp` (
  `IDPID` int(11) NOT NULL,
  `IDPIDU` int(11) DEFAULT NULL,
  `IDPIDDP` int(11) DEFAULT NULL,
  `IDPJUMLAHDP` double DEFAULT NULL,
  `IDPPAJAKDP` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `einvoicepenjualandp`
--

INSERT INTO `einvoicepenjualandp` (`IDPID`, `IDPIDU`, `IDPIDDP`, `IDPJUMLAHDP`, `IDPPAJAKDP`) VALUES
(1, 1, 1, 200000, 0);

--
-- Triggers `einvoicepenjualandp`
--
DELIMITER $$
CREATE TRIGGER `dp_add` BEFORE INSERT ON `einvoicepenjualandp` FOR EACH ROW begin
   update ddp set dppakaiiv = dppakaiiv+NEW.IDPJUMLAHDP where dpid = new.IDPIDDP;     
end
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `dp_dell` AFTER DELETE ON `einvoicepenjualandp` FOR EACH ROW begin
   update ddp set dppakaiiv = dppakaiiv-OLD.IDPJUMLAHDP where dpid = OLD.IDPIDDP;     
end
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `einvoicepenjualanu`
--

CREATE TABLE `einvoicepenjualanu` (
  `IPUID` int(11) NOT NULL,
  `IPUSUMBER` varchar(5) DEFAULT NULL,
  `IPUNOTRANSAKSI` varchar(50) DEFAULT NULL,
  `IPUTANGGAL` date DEFAULT NULL,
  `IPUKONTAK` int(11) DEFAULT NULL,
  `IPUURAIAN` varchar(255) DEFAULT NULL,
  `IPUUANG` int(11) DEFAULT NULL,
  `IPUVALAS` double DEFAULT '0',
  `IPUKURS` double DEFAULT '0',
  `IPUCREATEU` int(11) DEFAULT NULL,
  `IPUCREATED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `IPUMODIFU` int(11) DEFAULT NULL,
  `IPUMODIFD` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `IPUKARYAWAN` int(11) DEFAULT NULL,
  `IPUCATATAN` varchar(255) DEFAULT NULL,
  `IPUATTENTION` varchar(50) DEFAULT NULL,
  `IPUALAMAT` varchar(255) DEFAULT NULL,
  `IPUSTATUS` smallint(6) DEFAULT '0',
  `IPUTERMIN` int(11) DEFAULT NULL,
  `IPUTIPEPENJUALAN` int(11) NOT NULL DEFAULT '0',
  `IPUJENISPAJAK` smallint(6) DEFAULT '0',
  `IPUNOBKG` int(11) DEFAULT NULL,
  `IPUSUBTOTAL` double DEFAULT '0',
  `IPUTOTALPAJAK` double DEFAULT '0',
  `IPUTOTALTRANSAKSI` double DEFAULT '0',
  `IPUTOTALBAYAR` double DEFAULT '0',
  `IPUTOTALHPSEMENTARA` double DEFAULT '0',
  `IPUTOTALPAJAKM` double DEFAULT '0',
  `IPUTGLJATUHTEMPO` date DEFAULT NULL,
  `IPUDIPAKAIBIAYAKIRIM` smallint(6) DEFAULT '0',
  `IPUCOAPAJAK` int(11) DEFAULT NULL,
  `IPUCOAPIUTANG` int(11) DEFAULT NULL,
  `IPUCOARETUR` int(11) DEFAULT NULL,
  `IPUNOINVOICE` int(11) DEFAULT NULL,
  `IPUEKSPEDISI` int(11) DEFAULT NULL,
  `IPUEKSATTENTION` varchar(50) DEFAULT NULL,
  `IPUEKSALAMAT` varchar(255) DEFAULT NULL,
  `IPUSALDOAWAL` smallint(6) DEFAULT '0',
  `IPUCOAPENDAPATAN` int(11) DEFAULT NULL,
  `IPUNOFAKTURPAJAK` varchar(150) DEFAULT NULL,
  `IPUBUKTIPPN` varchar(255) DEFAULT NULL,
  `IPUBUKTIPPH` varchar(255) DEFAULT NULL,
  `IPUTIPEPAJAK` int(11) DEFAULT NULL,
  `IPUORDER` int(11) DEFAULT NULL,
  `IPUJENISINVOICE` int(11) DEFAULT NULL,
  `IPUDP` double DEFAULT '0',
  `IPUDPPAJAK` double DEFAULT '0',
  `IPUDPPPAKAI` int(11) DEFAULT NULL,
  `IPUDPPERSEN` double DEFAULT '0',
  `IPUTTDPAJAK` varchar(100) DEFAULT NULL,
  `IPUNOIVPEMASOK` varchar(50) DEFAULT NULL,
  `IPUSERIAL` varchar(150) DEFAULT NULL,
  `IPUNILAIBUKTIPOTONG` double DEFAULT '0',
  `IPUONGKIR` double DEFAULT '0',
  `IPUONGKIRAKUN` int(11) DEFAULT NULL,
  `IPUDISKON` double DEFAULT '0',
  `IPUIDDP` varchar(100) DEFAULT NULL,
  `IPUCATATANPIUTANG` varchar(255) DEFAULT NULL,
  `IPUONGKOSPAKING` double DEFAULT '0',
  `IPUONGKOSPAKINGAKUN` int(11) DEFAULT NULL,
  `IPURETITUSI` double DEFAULT '0',
  `IPUGUDANG` int(11) DEFAULT NULL,
  `IPUCETAK` smallint(6) DEFAULT '0',
  `IPUAKUNDP` int(11) DEFAULT NULL,
  `IPUJUMLAHDP` double DEFAULT '0',
  `IPUTGLPAJAK` date DEFAULT NULL,
  `IPUTOTALPPH22` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `einvoicepenjualanu`
--

INSERT INTO `einvoicepenjualanu` (`IPUID`, `IPUSUMBER`, `IPUNOTRANSAKSI`, `IPUTANGGAL`, `IPUKONTAK`, `IPUURAIAN`, `IPUUANG`, `IPUVALAS`, `IPUKURS`, `IPUCREATEU`, `IPUCREATED`, `IPUMODIFU`, `IPUMODIFD`, `IPUKARYAWAN`, `IPUCATATAN`, `IPUATTENTION`, `IPUALAMAT`, `IPUSTATUS`, `IPUTERMIN`, `IPUTIPEPENJUALAN`, `IPUJENISPAJAK`, `IPUNOBKG`, `IPUSUBTOTAL`, `IPUTOTALPAJAK`, `IPUTOTALTRANSAKSI`, `IPUTOTALBAYAR`, `IPUTOTALHPSEMENTARA`, `IPUTOTALPAJAKM`, `IPUTGLJATUHTEMPO`, `IPUDIPAKAIBIAYAKIRIM`, `IPUCOAPAJAK`, `IPUCOAPIUTANG`, `IPUCOARETUR`, `IPUNOINVOICE`, `IPUEKSPEDISI`, `IPUEKSATTENTION`, `IPUEKSALAMAT`, `IPUSALDOAWAL`, `IPUCOAPENDAPATAN`, `IPUNOFAKTURPAJAK`, `IPUBUKTIPPN`, `IPUBUKTIPPH`, `IPUTIPEPAJAK`, `IPUORDER`, `IPUJENISINVOICE`, `IPUDP`, `IPUDPPAJAK`, `IPUDPPPAKAI`, `IPUDPPERSEN`, `IPUTTDPAJAK`, `IPUNOIVPEMASOK`, `IPUSERIAL`, `IPUNILAIBUKTIPOTONG`, `IPUONGKIR`, `IPUONGKIRAKUN`, `IPUDISKON`, `IPUIDDP`, `IPUCATATANPIUTANG`, `IPUONGKOSPAKING`, `IPUONGKOSPAKINGAKUN`, `IPURETITUSI`, `IPUGUDANG`, `IPUCETAK`, `IPUAKUNDP`, `IPUJUMLAHDP`, `IPUTGLPAJAK`, `IPUTOTALPPH22`) VALUES
(1, 'IV', 'IV22010001', '2022-01-05', 1, 'Tes Sales 1', NULL, 0, 0, 1, '2022-01-05 00:56:39', NULL, '2022-01-05 00:56:39', 6, '', '', '', 1, 4, 0, 1, 4, 0, 50000, 550000, 350000, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, '010.000-00.00000001', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, 0, NULL, 0, 0, 0, NULL, 200000, '2022-01-05', 0);

--
-- Triggers `einvoicepenjualanu`
--
DELIMITER $$
CREATE TRIGGER `EINVOICEPENJUALANU_DELL_RETUR` AFTER DELETE ON `einvoicepenjualanu` FOR EACH ROW begin
	IF OLD.IPUTIPEPENJUALAN=1 THEN
		DELETE FROM fstoku 
     	 WHERE suid = OLD.IPUNOBKG;  
    end if;
end
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `EINVOICEPENJUALANU_EDIT` AFTER UPDATE ON `einvoicepenjualanu` FOR EACH ROW begin
  UPDATE fstoku SET  SUSTATUS = 1 WHERE  SUID = OLD.IPUNOBKG; 
  UPDATE fstoku SET  SUSTATUS = 3 WHERE  SUID = NEW.IPUNOBKG;  
end
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `EINVOICEPENJUALANU_FSTOKU` AFTER INSERT ON `einvoicepenjualanu` FOR EACH ROW begin
  UPDATE fstoku SET fstoku.SUSTATUS = 3 WHERE fstoku.SUID = NEW.IPUNOBKG;  
end
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `EINVOICEPENJUALANU_FSTOKU2` AFTER DELETE ON `einvoicepenjualanu` FOR EACH ROW begin
  UPDATE fstoku SET sustatus = 1 WHERE suid = old.IPUNOBKG; 
  delete from einvoicepenjualand where IPDIDIPU = OLD.IPUID;
  delete from einvoicepenjualandp where IDPIDU = OLD.IPUID; 
end
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `epembayaraninvoicei`
--

CREATE TABLE `epembayaraninvoicei` (
  `PIIID` int(11) NOT NULL,
  `PIIIDU` int(11) DEFAULT NULL,
  `PIIURUTAN` int(11) DEFAULT NULL,
  `PIIIDINVOICE` int(11) DEFAULT NULL,
  `PIIIDPEMBELIAN` int(11) DEFAULT NULL,
  `PIIIDDP` int(11) DEFAULT NULL,
  `PIIJMLBAYAR` double DEFAULT '0',
  `PIIJMLBAYARV` decimal(10,0) DEFAULT '0',
  `PIIUANG` int(11) DEFAULT NULL,
  `PIIKURS` double DEFAULT '0',
  `PIIJMLRETUR` double DEFAULT '0',
  `PIIJMLDISKON` double DEFAULT '0',
  `PIIJMLKASBANK` double DEFAULT '0',
  `PIITIPE` int(11) DEFAULT NULL,
  `PIIJMLUANGMUKA` double DEFAULT '0',
  `PIIJMLPAJAK` double DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `epembayaraninvoicei`
--

INSERT INTO `epembayaraninvoicei` (`PIIID`, `PIIIDU`, `PIIURUTAN`, `PIIIDINVOICE`, `PIIIDPEMBELIAN`, `PIIIDDP`, `PIIJMLBAYAR`, `PIIJMLBAYARV`, `PIIUANG`, `PIIKURS`, `PIIJMLRETUR`, `PIIJMLDISKON`, `PIIJMLKASBANK`, `PIITIPE`, `PIIJMLUANGMUKA`, `PIIJMLPAJAK`) VALUES
(1, 1, 1, 1, NULL, NULL, 350000, '0', NULL, 0, 0, 0, 0, 1, 0, 0);

--
-- Triggers `epembayaraninvoicei`
--
DELIMITER $$
CREATE TRIGGER `EPEMBAYARANINVOICEI_EINVOICE3` BEFORE UPDATE ON `epembayaraninvoicei` FOR EACH ROW begin
  UPDATE ddp SET DPJUMLAHBAYAR = DPJUMLAHBAYAR - OLD.PIIJMLBAYAR WHERE DPID = OLD.PIIIDDP;
  UPDATE einvoicepenjualanu SET IPUTOTALBAYAR = IPUTOTALBAYAR - OLD.PIIJMLBAYAR WHERE IPUID = OLD.PIIIDINVOICE;    
  UPDATE fstoku SET SUTOTALBAYAR = SUTOTALBAYAR - OLD.PIIJMLBAYAR WHERE SUID = OLD.PIIIDPEMBELIAN; 
  
  
  
end
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `EPEMBAYARANINVOICEI_EINVOICE4` AFTER UPDATE ON `epembayaraninvoicei` FOR EACH ROW begin
  UPDATE ddp SET DPJUMLAHBAYAR = DPJUMLAHBAYAR + NEW.PIIJMLBAYAR WHERE DPID = NEW.PIIIDDP;
  UPDATE einvoicepenjualanu SET IPUTOTALBAYAR = IPUTOTALBAYAR + NEW.PIIJMLBAYAR WHERE IPUID = NEW.PIIIDINVOICE;   
  UPDATE fstoku SET SUTOTALBAYAR = SUTOTALBAYAR + NEW.PIIJMLBAYAR WHERE SUID = NEW.PIIIDPEMBELIAN; 
   
end
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `EPEMBAYARANINVOICEI_EINVOICEU` AFTER INSERT ON `epembayaraninvoicei` FOR EACH ROW begin
  UPDATE ddp SET DPJUMLAHBAYAR = DPJUMLAHBAYAR + NEW.PIIJMLBAYAR WHERE DPID = NEW.PIIIDDP;
  UPDATE einvoicepenjualanu SET IPUTOTALBAYAR = IPUTOTALBAYAR + NEW.PIIJMLBAYAR WHERE IPUID = NEW.PIIIDINVOICE;   
  UPDATE fstoku SET SUTOTALBAYAR = SUTOTALBAYAR + NEW.PIIJMLBAYAR WHERE SUID = NEW.PIIIDPEMBELIAN;
  
  end
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `epembayaraninvoicei_dell` AFTER DELETE ON `epembayaraninvoicei` FOR EACH ROW begin
  UPDATE ddp SET DPJUMLAHBAYAR = DPJUMLAHBAYAR - OLD.PIIJMLBAYAR WHERE DPID = OLD.PIIIDDP;
  UPDATE einvoicepenjualanu SET IPUTOTALBAYAR = IPUTOTALBAYAR - old.PIIJMLBAYAR WHERE IPUID = old.PIIIDINVOICE;    
  UPDATE fstoku SET SUTOTALBAYAR = SUTOTALBAYAR - old.PIIJMLBAYAR WHERE SUID = old.PIIIDPEMBELIAN; 
end
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `epembayaraninvoicer`
--

CREATE TABLE `epembayaraninvoicer` (
  `PIRID` int(11) NOT NULL,
  `PIRIDU` int(11) DEFAULT NULL,
  `PIRURUTAN` int(11) DEFAULT NULL,
  `PIRIDRETURPENJUALAN` int(11) DEFAULT NULL,
  `PIRIDRETURPEMBELIAN` int(11) DEFAULT NULL,
  `PIRJMLBAYAR` double DEFAULT '0',
  `PIRJMLBAYARV` double DEFAULT '0',
  `PIRKURS` double DEFAULT '0',
  `PIRUANG` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `epembayaraninvoicer`
--

INSERT INTO `epembayaraninvoicer` (`PIRID`, `PIRIDU`, `PIRURUTAN`, `PIRIDRETURPENJUALAN`, `PIRIDRETURPEMBELIAN`, `PIRJMLBAYAR`, `PIRJMLBAYARV`, `PIRKURS`, `PIRUANG`) VALUES
(1, 1, 1, NULL, 0, 0, 0, 0, NULL);

--
-- Triggers `epembayaraninvoicer`
--
DELIMITER $$
CREATE TRIGGER `EPEMBAYARANINVOICER_ADD` BEFORE INSERT ON `epembayaraninvoicer` FOR EACH ROW begin
  UPDATE einvoicepenjualanu SET IPUTOTALBAYAR = IPUTOTALBAYAR + NEW.PIRJMLBAYAR WHERE IPUID = NEW.PIRIDRETURPENJUALAN;
  UPDATE einvoicepenjualanu SET IPUTOTALBAYAR = IPUTOTALBAYAR + NEW.PIRJMLBAYAR WHERE IPUID = NEW.PIRIDRETURPEMBELIAN;

end
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `EPEMBAYARANINVOICER_BUPD` BEFORE UPDATE ON `epembayaraninvoicer` FOR EACH ROW begin
  UPDATE einvoicepenjualanu SET IPUTOTALBAYAR = IPUTOTALBAYAR - OLD.PIRJMLBAYAR WHERE IPUID = OLD.PIRIDRETURPENJUALAN;
  UPDATE einvoicepenjualanu SET IPUTOTALBAYAR = IPUTOTALBAYAR - OLD.PIRJMLBAYAR WHERE IPUID = OLD.PIRIDRETURPEMBELIAN;

end
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `EPEMBAYARANINVOICER_DEL` AFTER DELETE ON `epembayaraninvoicer` FOR EACH ROW begin
  UPDATE einvoicepenjualanu SET IPUTOTALBAYAR = IPUTOTALBAYAR - OLD.PIRJMLBAYAR WHERE IPUID = OLD.PIRIDRETURPENJUALAN;
  UPDATE einvoicepenjualanu SET IPUTOTALBAYAR = IPUTOTALBAYAR - OLD.PIRJMLBAYAR WHERE IPUID = OLD.PIRIDRETURPEMBELIAN;

end
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `EPEMBAYARANINVOICER_UPD` AFTER UPDATE ON `epembayaraninvoicer` FOR EACH ROW begin
  UPDATE einvoicepenjualanu SET IPUTOTALBAYAR = IPUTOTALBAYAR + NEW.PIRJMLBAYAR WHERE IPUID = NEW.PIRIDRETURPENJUALAN;
  UPDATE einvoicepenjualanu SET IPUTOTALBAYAR = IPUTOTALBAYAR + NEW.PIRJMLBAYAR WHERE IPUID = NEW.PIRIDRETURPEMBELIAN;

end
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `epembayaraninvoiceu`
--

CREATE TABLE `epembayaraninvoiceu` (
  `PIUID` int(11) NOT NULL,
  `PIUSUMBER` varchar(5) NOT NULL,
  `PIUNOTRANSAKSI` varchar(25) NOT NULL,
  `PIUTANGGAL` date DEFAULT NULL,
  `PIUKONTAK` int(11) DEFAULT NULL,
  `PIUURAIAN` varchar(255) DEFAULT NULL,
  `PIUCOAKAS` int(11) DEFAULT NULL,
  `PIUJMLKAS` double DEFAULT '0',
  `PIUJMLKASV` double DEFAULT '0',
  `PIUTOTALRETUR` double DEFAULT '0',
  `PIUTOTALPIUTANG` double DEFAULT '0',
  `PIUUANG` int(11) DEFAULT NULL,
  `PIUKURS` double DEFAULT '0',
  `PIUTIPE` int(11) DEFAULT NULL,
  `PIUNOGIRO` varchar(50) DEFAULT NULL,
  `PIUBANK` int(11) DEFAULT NULL,
  `PIUNOAC` varchar(50) DEFAULT NULL,
  `PIUAKUNGIRO` int(11) DEFAULT NULL,
  `PIUTGLTEMPO` date DEFAULT NULL,
  `PIUCREATEU` int(11) DEFAULT NULL,
  `PIUCREATED` datetime DEFAULT NULL,
  `PIUMODIFU` int(11) DEFAULT NULL,
  `PIUMODIFD` datetime DEFAULT NULL,
  `PIUDISKON` double DEFAULT '0',
  `PIUSTATUS` smallint(6) DEFAULT '0',
  `PIUADMINBANK` double DEFAULT '0',
  `PIUPEMBULATAN` double DEFAULT '0',
  `PIUPENDAPATANLAIN` double DEFAULT '0',
  `PIUPPH` int(11) DEFAULT NULL,
  `PIUBUKTIPPH` varchar(255) DEFAULT NULL,
  `PIUNILAIBUKTIPOTONG` double DEFAULT '0',
  `PIUAKUNDISKON` int(11) DEFAULT NULL,
  `PIUKARYAWAN` int(11) DEFAULT NULL,
  `PIUPROYEK` int(11) DEFAULT NULL,
  `PIUADMIN` double DEFAULT '0',
  `PIUDENDA` double DEFAULT '0',
  `PIUPEMBELIAN` double DEFAULT '0',
  `PIUPPNPEMBELIAN` double DEFAULT '0',
  `PIUAKUNADMIN` int(11) DEFAULT NULL,
  `PIUAKUNDENDA` int(11) DEFAULT NULL,
  `PIUAKUNPEMBELIAN` int(11) DEFAULT NULL,
  `PIUAKUNPPNPEMBELIAN` int(11) DEFAULT NULL,
  `PIUIDDP` int(11) DEFAULT NULL,
  `PIUCOADP` int(11) DEFAULT NULL,
  `PIUTOTALDP` int(11) DEFAULT NULL,
  `PIUSTATUSPPH` tinyint(4) NOT NULL DEFAULT '0',
  `PIUSELISIHBAYAR` double NOT NULL DEFAULT '0',
  `PIUAKUNSELISIH` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `epembayaraninvoiceu`
--

INSERT INTO `epembayaraninvoiceu` (`PIUID`, `PIUSUMBER`, `PIUNOTRANSAKSI`, `PIUTANGGAL`, `PIUKONTAK`, `PIUURAIAN`, `PIUCOAKAS`, `PIUJMLKAS`, `PIUJMLKASV`, `PIUTOTALRETUR`, `PIUTOTALPIUTANG`, `PIUUANG`, `PIUKURS`, `PIUTIPE`, `PIUNOGIRO`, `PIUBANK`, `PIUNOAC`, `PIUAKUNGIRO`, `PIUTGLTEMPO`, `PIUCREATEU`, `PIUCREATED`, `PIUMODIFU`, `PIUMODIFD`, `PIUDISKON`, `PIUSTATUS`, `PIUADMINBANK`, `PIUPEMBULATAN`, `PIUPENDAPATANLAIN`, `PIUPPH`, `PIUBUKTIPPH`, `PIUNILAIBUKTIPOTONG`, `PIUAKUNDISKON`, `PIUKARYAWAN`, `PIUPROYEK`, `PIUADMIN`, `PIUDENDA`, `PIUPEMBELIAN`, `PIUPPNPEMBELIAN`, `PIUAKUNADMIN`, `PIUAKUNDENDA`, `PIUAKUNPEMBELIAN`, `PIUAKUNPPNPEMBELIAN`, `PIUIDDP`, `PIUCOADP`, `PIUTOTALDP`, `PIUSTATUSPPH`, `PIUSELISIHBAYAR`, `PIUAKUNSELISIH`) VALUES
(1, 'CP', 'CP22010001', '2022-01-05', 1, 'Pelunasan Sales 1', 11, 350000, 0, 0, 350000, 0, 1, 2, '', 23, NULL, NULL, '0000-00-00', 1, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, '', 0, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0);

--
-- Triggers `epembayaraninvoiceu`
--
DELIMITER $$
CREATE TRIGGER `EPEMBAYARANINVOICEU_DELL` AFTER DELETE ON `epembayaraninvoiceu` FOR EACH ROW begin
  update ddp set DPJUMLAHBAYAR=DPJUMLAHBAYAR-OLD.PIUTOTALDP where DPID= OLD.PIUIDDP;
  delete from epembayaraninvoicei where PIIIDU=OLD.PIUID;
  delete from epembayaraninvoicer where PIRIDU=OLD.PIUID;
end
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `epembayaraninvoiceu_add` BEFORE INSERT ON `epembayaraninvoiceu` FOR EACH ROW begin
  update ddp set DPJUMLAHBAYAR=DPJUMLAHBAYAR+NEW.PIUTOTALDP where DPID= new.PIUIDDP;
end
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `esalesorderd`
--

CREATE TABLE `esalesorderd` (
  `SODID` int(11) NOT NULL,
  `SODIDSOU` int(11) NOT NULL,
  `SODURUTAN` int(11) NOT NULL,
  `SODSUMBER` varchar(2) DEFAULT NULL,
  `SODITEM` int(11) DEFAULT NULL,
  `SODORDER` double DEFAULT '0',
  `SODORDERD` double DEFAULT '0',
  `SODMASUK` double DEFAULT '0',
  `SODKELUAR` double DEFAULT '0',
  `SODHARGA` double DEFAULT '0',
  `SODDISKON` double DEFAULT '0',
  `SODSATUAN` int(11) DEFAULT NULL,
  `SODSATUAND` int(11) DEFAULT NULL,
  `SODMASUKD` double DEFAULT '0',
  `SODKELUARD` double DEFAULT '0',
  `SODCATATAN` varchar(255) DEFAULT NULL,
  `SODDIVISI` int(11) DEFAULT NULL,
  `SODPROYEK` int(11) DEFAULT NULL,
  `SODGUDANG` int(11) DEFAULT NULL,
  `SODIDPB` int(11) DEFAULT NULL,
  `SODBTG` double DEFAULT '0',
  `SODBTGD` double DEFAULT '0',
  `SODSUBTOTAL` double DEFAULT '0',
  `SODPAJAKIN` int(11) DEFAULT NULL,
  `SODPAJAKOUT` int(11) DEFAULT NULL,
  `SODPAJAKINP` double DEFAULT '0',
  `SODPAJAKOUTP` double DEFAULT '0',
  `SODPAJAKINN` double DEFAULT '0',
  `SODPAJAKOUTN` double DEFAULT '0',
  `SODTOTAL` double DEFAULT '0',
  `SODPENAWARANID` int(11) DEFAULT NULL,
  `SODCATATAN2` varchar(255) DEFAULT NULL,
  `SODMASUKM` double DEFAULT '0',
  `SODKELUARM` double DEFAULT '0',
  `SODISIDUS` double DEFAULT '0',
  `SODDUS` double DEFAULT '0',
  `SODDISKONPERSEN` double DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `esalesorderd`
--

INSERT INTO `esalesorderd` (`SODID`, `SODIDSOU`, `SODURUTAN`, `SODSUMBER`, `SODITEM`, `SODORDER`, `SODORDERD`, `SODMASUK`, `SODKELUAR`, `SODHARGA`, `SODDISKON`, `SODSATUAN`, `SODSATUAND`, `SODMASUKD`, `SODKELUARD`, `SODCATATAN`, `SODDIVISI`, `SODPROYEK`, `SODGUDANG`, `SODIDPB`, `SODBTG`, `SODBTGD`, `SODSUBTOTAL`, `SODPAJAKIN`, `SODPAJAKOUT`, `SODPAJAKINP`, `SODPAJAKOUTP`, `SODPAJAKINN`, `SODPAJAKOUTN`, `SODTOTAL`, `SODPENAWARANID`, `SODCATATAN2`, `SODMASUKM`, `SODKELUARM`, `SODISIDUS`, `SODDUS`, `SODDISKONPERSEN`) VALUES
(1, 1, 1, NULL, 6, 5, 5, 0, 5, 100000, 0, 1, 1, 0, 0, 'Tes Sales 1', NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, 0, 0, 0);

--
-- Triggers `esalesorderd`
--
DELIMITER $$
CREATE TRIGGER `ESALESORDERD_ADD` BEFORE INSERT ON `esalesorderd` FOR EACH ROW begin
  UPDATE fpermintaanbarangd SET PBDQTYPAKAI = PBDQTYPAKAI + NEW.SODORDER WHERE PBDID = NEW.SODIDPB ;

end
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `ESALESORDERD_BEDIT` BEFORE UPDATE ON `esalesorderd` FOR EACH ROW begin
  UPDATE fpermintaanbarangd SET PBDQTYPAKAI = PBDQTYPAKAI - OLD.SODORDER WHERE PBDID = OLD.SODIDPB ;

end
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `ESALESORDERD_DEL` AFTER DELETE ON `esalesorderd` FOR EACH ROW begin
  UPDATE fpermintaanbarangd SET PBDQTYPAKAI = PBDQTYPAKAI - OLD.SODORDER WHERE PBDID = OLD.SODIDPB ;

end
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `ESALESORDERD_EDIT` AFTER UPDATE ON `esalesorderd` FOR EACH ROW begin
  UPDATE fpermintaanbarangd SET PBDQTYPAKAI = PBDQTYPAKAI + NEW.SODORDER WHERE PBDID = NEW.SODIDPB ;

end
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `esalesorderu`
--

CREATE TABLE `esalesorderu` (
  `SOUID` int(11) NOT NULL,
  `SOUSUMBER` varchar(5) DEFAULT NULL,
  `SOUNOTRANSAKSI` varchar(25) NOT NULL,
  `SOUTANGGAL` date DEFAULT NULL,
  `SOUKONTAK` int(11) DEFAULT NULL,
  `SOUURAIAN` varchar(255) DEFAULT NULL,
  `SOUUANG` int(11) DEFAULT NULL,
  `SOUVALAS` double DEFAULT '0',
  `SOUKURS` double DEFAULT '0',
  `SOUCREATEU` int(11) DEFAULT NULL,
  `SOUCREATED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `SOUMODIFU` int(11) DEFAULT NULL,
  `SOUMODIFD` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `SOUKARYAWAN` int(11) DEFAULT NULL,
  `SOUNOREF` varchar(25) DEFAULT NULL,
  `SOUTERMIN` int(11) DEFAULT NULL,
  `SOUCATATAN` varchar(255) DEFAULT NULL,
  `SOUALAMAT` varchar(255) DEFAULT NULL,
  `SOUATTENTION` varchar(50) DEFAULT NULL,
  `SOUTIPEPENJUALAN` int(11) DEFAULT NULL,
  `SOUTGL1` date DEFAULT NULL,
  `SOUTGL2` date DEFAULT NULL,
  `SOUDEPARTEMEN` int(11) DEFAULT NULL,
  `SOUPAJAK` int(11) DEFAULT '0',
  `SOUSTATUS` smallint(6) DEFAULT '0',
  `SOUPBID` int(11) DEFAULT NULL,
  `SOUALAMATK` int(11) DEFAULT '0',
  `SOUJENIS` int(11) DEFAULT '0',
  `SOUTIPE` int(11) DEFAULT '0',
  `SOUTOTALPAJAK` double DEFAULT '0',
  `SOUSUBTOTAL` double DEFAULT '0',
  `SOUPAJAKIN` double DEFAULT '0',
  `SOUPAJAKOUT` double DEFAULT '0',
  `SOUTOTALTRANSAKSI` double DEFAULT '0',
  `SOUFORWARDER` int(11) DEFAULT NULL,
  `SOUSERIAL` varchar(150) DEFAULT NULL,
  `SOUINVOICE` smallint(6) DEFAULT '0',
  `SOUDIVISI` int(11) DEFAULT NULL,
  `SOUVESSEL` int(11) DEFAULT NULL,
  `SOUJETTY` int(11) DEFAULT NULL,
  `SOUTB` varchar(255) DEFAULT NULL,
  `SOUBG` varchar(255) DEFAULT NULL,
  `SOUSHIPPER` int(11) DEFAULT NULL,
  `SOUPBID2` int(11) DEFAULT NULL,
  `SOUNOSQ` int(11) DEFAULT NULL,
  `SOUFILE1` varchar(255) DEFAULT NULL,
  `SOUFILE2` varchar(255) DEFAULT NULL,
  `SOUFILE3` varchar(255) DEFAULT NULL,
  `SOUFILE4` varchar(255) DEFAULT NULL,
  `SOUFILE5` varchar(255) DEFAULT NULL,
  `SOUFILE6` varchar(255) DEFAULT NULL,
  `SOUCATATAN1` varchar(255) DEFAULT NULL,
  `SOUCATATAN2` varchar(255) DEFAULT NULL,
  `SOUCATATAN3` varchar(255) DEFAULT NULL,
  `SOUCATATAN4` varchar(255) DEFAULT NULL,
  `SOUCATATAN5` varchar(255) DEFAULT NULL,
  `SOUCATATAN6` varchar(255) DEFAULT NULL,
  `SOUIDPODISTOP` smallint(6) DEFAULT '0',
  `SOUDEAKTIVASI` smallint(6) DEFAULT '0',
  `SOUJUMLAHDP` double DEFAULT '0',
  `SOUJUMLAHONGKIR` double DEFAULT '0',
  `SOUJUMLAHPAKING` double DEFAULT '0',
  `SOUMENGETAHUI` int(11) DEFAULT NULL,
  `SOUMENYETUJUI` int(11) DEFAULT NULL,
  `SOUALAMATID` int(11) DEFAULT NULL,
  `SOUNOPOPELANGGAN` varchar(25) DEFAULT NULL,
  `SOUTOTALPPH22` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `esalesorderu`
--

INSERT INTO `esalesorderu` (`SOUID`, `SOUSUMBER`, `SOUNOTRANSAKSI`, `SOUTANGGAL`, `SOUKONTAK`, `SOUURAIAN`, `SOUUANG`, `SOUVALAS`, `SOUKURS`, `SOUCREATEU`, `SOUCREATED`, `SOUMODIFU`, `SOUMODIFD`, `SOUKARYAWAN`, `SOUNOREF`, `SOUTERMIN`, `SOUCATATAN`, `SOUALAMAT`, `SOUATTENTION`, `SOUTIPEPENJUALAN`, `SOUTGL1`, `SOUTGL2`, `SOUDEPARTEMEN`, `SOUPAJAK`, `SOUSTATUS`, `SOUPBID`, `SOUALAMATK`, `SOUJENIS`, `SOUTIPE`, `SOUTOTALPAJAK`, `SOUSUBTOTAL`, `SOUPAJAKIN`, `SOUPAJAKOUT`, `SOUTOTALTRANSAKSI`, `SOUFORWARDER`, `SOUSERIAL`, `SOUINVOICE`, `SOUDIVISI`, `SOUVESSEL`, `SOUJETTY`, `SOUTB`, `SOUBG`, `SOUSHIPPER`, `SOUPBID2`, `SOUNOSQ`, `SOUFILE1`, `SOUFILE2`, `SOUFILE3`, `SOUFILE4`, `SOUFILE5`, `SOUFILE6`, `SOUCATATAN1`, `SOUCATATAN2`, `SOUCATATAN3`, `SOUCATATAN4`, `SOUCATATAN5`, `SOUCATATAN6`, `SOUIDPODISTOP`, `SOUDEAKTIVASI`, `SOUJUMLAHDP`, `SOUJUMLAHONGKIR`, `SOUJUMLAHPAKING`, `SOUMENGETAHUI`, `SOUMENYETUJUI`, `SOUALAMATID`, `SOUNOPOPELANGGAN`, `SOUTOTALPPH22`) VALUES
(1, 'SO', 'SO22010001', '2022-01-05', 1, 'Tes Sales 1', NULL, 0, 0, 1, '2022-01-05 00:51:51', NULL, '2022-01-05 00:51:51', 6, NULL, 4, '', '', '', NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 0, 0, 50000, 500000, 0, 0, 550000, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, NULL, NULL, '', 0);

--
-- Triggers `esalesorderu`
--
DELIMITER $$
CREATE TRIGGER `esalesorderu_dell` AFTER DELETE ON `esalesorderu` FOR EACH ROW BEGIN
	delete from esalesorderd where SODIDSOU = OLD.SOUID;
end
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `esalesquotationd`
--

CREATE TABLE `esalesquotationd` (
  `SQDID` int(11) NOT NULL,
  `SQDIDSQU` int(11) NOT NULL,
  `SQDURUTAN` int(11) NOT NULL,
  `SQDSUMBER` varchar(2) DEFAULT NULL,
  `SQDITEM` int(11) DEFAULT NULL,
  `SQDORDER` double DEFAULT '0',
  `SQDORDERD` double DEFAULT '0',
  `SQDMASUK` double DEFAULT '0',
  `SQDKELUAR` double DEFAULT '0',
  `SQDHARGA` double DEFAULT '0',
  `SQDDISKON` double DEFAULT '0',
  `SQDSATUAN` int(11) DEFAULT NULL,
  `SQDSATUAND` int(11) DEFAULT NULL,
  `SQDMASUKD` double DEFAULT '0',
  `SQDKELUARD` double DEFAULT '0',
  `SQDCATATAN` varchar(255) DEFAULT NULL,
  `SQDDIVISI` int(11) DEFAULT NULL,
  `SQDPROYEK` int(11) DEFAULT NULL,
  `SQDGUDANG` int(11) DEFAULT NULL,
  `SQDIDPB` int(11) DEFAULT NULL,
  `SQDBTG` double DEFAULT '0',
  `SQDBTGD` double DEFAULT '0',
  `SQDSUBTOTAL` double DEFAULT '0',
  `SQDPAJAKIN` int(11) DEFAULT NULL,
  `SQDPAJAKOUT` int(11) DEFAULT NULL,
  `SQDPAJAKINP` double DEFAULT '0',
  `SQDPAJAKOUTP` double DEFAULT '0',
  `SQDPAJAKINN` double DEFAULT '0',
  `SQDPAJAKOUTN` double DEFAULT '0',
  `SQDTOTAL` double DEFAULT '0',
  `SQDPENAWARANID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `esalesquotationu`
--

CREATE TABLE `esalesquotationu` (
  `SQUID` int(11) NOT NULL,
  `SQUSUMBER` varchar(5) DEFAULT NULL,
  `SQUNOTRANSAKSI` varchar(25) NOT NULL,
  `SQUTANGGAL` date DEFAULT NULL,
  `SQUKONTAK` int(11) DEFAULT NULL,
  `SQUURAIAN` varchar(255) DEFAULT NULL,
  `SQUUANG` int(11) DEFAULT NULL,
  `SQUVALAS` double DEFAULT '0',
  `SQUKURS` double DEFAULT '0',
  `SQUCREATEU` int(11) DEFAULT NULL,
  `SQUCREATED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `SQUMODIFU` int(11) DEFAULT NULL,
  `SQUMODIFD` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `SQUKARYAWAN` int(11) DEFAULT NULL,
  `SQUNOREF` varchar(25) DEFAULT NULL,
  `SQUTERMIN` int(11) DEFAULT NULL,
  `SQUCATATAN` varchar(255) DEFAULT NULL,
  `SQUALAMAT` varchar(255) DEFAULT NULL,
  `SQUATTENTION` varchar(100) DEFAULT NULL,
  `SQUTIPEPENJUALAN` int(11) DEFAULT NULL,
  `SQUTGL1` date DEFAULT NULL,
  `SQUTGL2` date DEFAULT NULL,
  `SQUDEPARTEMEN` int(11) DEFAULT NULL,
  `SQUPAJAK` int(11) DEFAULT '0',
  `SQUSTATUS` smallint(6) DEFAULT '0',
  `SQUPBID` int(11) DEFAULT NULL,
  `SQUALAMATK` int(11) DEFAULT '0',
  `SQUJENIS` int(11) DEFAULT '0',
  `SQUTIPE` int(11) DEFAULT '0',
  `SQUTOTALPAJAK` double DEFAULT '0',
  `SQUSUBTOTAL` double DEFAULT '0',
  `SQUPAJAKIN` double DEFAULT '0',
  `SQUPAJAKOUT` double DEFAULT '0',
  `SQUTOTALTRANSAKSI` double DEFAULT '0',
  `SQUFORWARDER` int(11) DEFAULT NULL,
  `SQUDELIVERY` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `fasetd`
--

CREATE TABLE `fasetd` (
  `SDID` int(11) NOT NULL,
  `SDIDSU` int(11) NOT NULL,
  `SDURUTAN` int(11) NOT NULL,
  `SDSUMBER` varchar(5) DEFAULT NULL,
  `SDITEM` int(11) DEFAULT NULL,
  `SDMASUK` double DEFAULT '0',
  `SDKELUAR` double DEFAULT '0',
  `SDHARGA` double DEFAULT '0',
  `SDDISKON` double DEFAULT '0',
  `SDSATUAN` int(11) DEFAULT NULL,
  `SDSATUAND` int(11) DEFAULT NULL,
  `SDMASUKD` double DEFAULT '0',
  `SDKELUARD` double DEFAULT '0',
  `SDCATATAN` varchar(255) DEFAULT NULL,
  `SDDIVISI` int(11) DEFAULT NULL,
  `SDPROYEK` int(11) DEFAULT NULL,
  `SDGUDANG` int(11) DEFAULT NULL,
  `SDJENIS` int(11) DEFAULT NULL,
  `SDAKUMULASI` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Triggers `fasetd`
--
DELIMITER $$
CREATE TRIGGER `fasetd_delete` AFTER DELETE ON `fasetd` FOR EACH ROW BEGIN
	IF OLD.SDJENIS IN(1,2) THEN
    	UPDATE baktiva SET APIN=0 WHERE aid=OLD.SDITEM;
    end if;
end
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `fasetu`
--

CREATE TABLE `fasetu` (
  `SUID` int(11) NOT NULL,
  `SUNOTRANSAKSI` varchar(50) DEFAULT NULL,
  `SUTANGGAL` date DEFAULT NULL,
  `SUKONTAK` int(11) DEFAULT NULL,
  `SUURAIAN` varchar(255) DEFAULT NULL,
  `SUTOTALTRANSAKSI` double DEFAULT '0',
  `SUUANG` int(11) DEFAULT NULL,
  `SUVALAS` double DEFAULT '0',
  `SUKURS` double DEFAULT '0',
  `SUCREATEU` int(11) DEFAULT NULL,
  `SUCREATED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `SUMODIFU` int(11) DEFAULT NULL,
  `SUMODIFD` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `SUKARYAWAN` int(11) DEFAULT NULL,
  `SUNOREF` varchar(25) DEFAULT NULL,
  `SUSUMBER` varchar(5) DEFAULT NULL,
  `SUJENIS` int(11) DEFAULT NULL,
  `SUNOREFTRANSAKSI` int(11) DEFAULT NULL,
  `SUCATATAN` varchar(255) DEFAULT NULL,
  `SUSTATUS` smallint(6) DEFAULT '0',
  `SUCOA` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Triggers `fasetu`
--
DELIMITER $$
CREATE TRIGGER `fasetu_delete` AFTER DELETE ON `fasetu` FOR EACH ROW BEGIN
	DELETE FROM fasetd WHERE SDIDSU=OLD.SUID;
end
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `fhppin`
--

CREATE TABLE `fhppin` (
  `HIID` int(11) NOT NULL,
  `HITRANSID` int(11) NOT NULL,
  `HITANGGAL` date DEFAULT NULL,
  `HIBARANGID` int(11) NOT NULL,
  `HIGUDANGID` int(11) NOT NULL,
  `HIQTY` double DEFAULT '0',
  `HIHARGA` double DEFAULT '0',
  `HIPAKAI` double DEFAULT '0',
  `HITOTALMASUK` double DEFAULT '0',
  `HIVALAS` double DEFAULT '0',
  `HIKURS` double DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `fhppout`
--

CREATE TABLE `fhppout` (
  `HOID` int(11) NOT NULL,
  `HOTRANSID` int(11) NOT NULL,
  `HOTANGGAL` date DEFAULT NULL,
  `HOBARANGID` int(11) NOT NULL,
  `HOGUDANGID` int(11) NOT NULL,
  `HOQTY` double DEFAULT '0',
  `HOHARGA` double DEFAULT '0',
  `HOTOTALKELUAR` double DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `fhppoutreff`
--

CREATE TABLE `fhppoutreff` (
  `HORID` int(11) NOT NULL,
  `HOHPOUT` int(11) NOT NULL,
  `HOHPIN` int(11) NOT NULL,
  `HOQTY` double DEFAULT '0',
  `HOHARGA` double DEFAULT '0',
  `HOTOTAL` double DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `fpermintaanbarangd`
--

CREATE TABLE `fpermintaanbarangd` (
  `PBDID` int(11) NOT NULL,
  `PBDIDSU` int(11) NOT NULL,
  `PBDURUTAN` int(11) NOT NULL,
  `PBDSUMBER` varchar(5) DEFAULT NULL,
  `PBDITEM` int(11) DEFAULT NULL,
  `PBDQTY` double DEFAULT '0',
  `PBDSATUAN` int(11) DEFAULT NULL,
  `PBDSATUAND` int(11) DEFAULT NULL,
  `PBDQTYD` double DEFAULT '0',
  `PBDCATATAN` varchar(255) DEFAULT NULL,
  `PBDDIVISI` int(11) DEFAULT NULL,
  `PBDPROYEK` int(11) DEFAULT NULL,
  `PBDQTYPAKAI` double DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `fpermintaanbarangu`
--

CREATE TABLE `fpermintaanbarangu` (
  `PBUID` int(11) NOT NULL,
  `PBUNOTRANSAKSI` varchar(50) DEFAULT NULL,
  `PBUTANGGAL` date DEFAULT NULL,
  `PBUKONTAK` int(11) DEFAULT NULL,
  `PBUURAIAN` varchar(255) DEFAULT NULL,
  `PBUCREATED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `PBUCREATEU` int(11) DEFAULT NULL,
  `PBUMODIFU` int(11) DEFAULT NULL,
  `PBUMODIFD` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `PBUKARYAWAN` int(11) DEFAULT NULL,
  `PBUNOREF` varchar(25) DEFAULT NULL,
  `PBUSUMBER` varchar(5) DEFAULT NULL,
  `PBUCATATAN` varchar(255) DEFAULT NULL,
  `PBUGUDANG` int(11) DEFAULT NULL,
  `PBUGUDANGSUMBER` int(11) DEFAULT NULL,
  `PBUDIVISI` int(11) DEFAULT NULL,
  `PBUTIPEPERMINTAAN` int(11) DEFAULT NULL,
  `PBUSTATUS` int(11) DEFAULT NULL,
  `PBUVIA` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `fprob`
--

CREATE TABLE `fprob` (
  `PBID` int(11) NOT NULL,
  `PBIDU` int(11) NOT NULL,
  `PBURUTAN` int(11) DEFAULT '0',
  `PBBARANG` int(11) NOT NULL,
  `PBQTY` double DEFAULT '0',
  `PBSATUAN` int(11) DEFAULT NULL,
  `PBCATATAN` varchar(255) DEFAULT NULL,
  `PBQTYD` int(11) DEFAULT NULL,
  `PBHARGA` double DEFAULT '0',
  `PBTOTAL` double DEFAULT '0',
  `PBNILAIHPP` double DEFAULT '0',
  `PBDIVISI` int(11) DEFAULT NULL,
  `PBPROYEK` int(11) DEFAULT NULL,
  `PBBERAT` double DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `fproc`
--

CREATE TABLE `fproc` (
  `PCID` int(11) NOT NULL,
  `PCIDU` int(11) NOT NULL,
  `PCURUTAN` int(11) DEFAULT '0',
  `PCCOA` int(11) NOT NULL,
  `PCJUMLAH` double DEFAULT '0',
  `PCUANG` int(11) DEFAULT NULL,
  `PCJUMLAHVALAS` double DEFAULT '0',
  `PCKURS` double DEFAULT '0',
  `PCCATATAN` varchar(255) DEFAULT NULL,
  `PCDIVISI` int(11) DEFAULT NULL,
  `PCPROYEK` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `fproj`
--

CREATE TABLE `fproj` (
  `PJID` int(11) NOT NULL,
  `PJIDU` int(11) NOT NULL,
  `PJURUTAN` int(11) DEFAULT '0',
  `PJBARANG` int(11) NOT NULL,
  `PJQTY` double DEFAULT '0',
  `PJSATUAN` int(11) NOT NULL,
  `PJCATATAN` varchar(255) DEFAULT '0',
  `PJHARGA` double DEFAULT '0',
  `PJTOTAL` double DEFAULT '0',
  `PJQTYD` double DEFAULT '0',
  `PJHPP` double DEFAULT '0',
  `PJDIVISI` int(11) DEFAULT NULL,
  `PJPROYEK` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `fprou`
--

CREATE TABLE `fprou` (
  `PUID` int(11) NOT NULL,
  `PUNOTRANSAKSI` varchar(25) NOT NULL,
  `PUSUMBER` varchar(5) DEFAULT 'PRO',
  `PUTANGGAL` date DEFAULT NULL,
  `PUKONTAK` int(11) DEFAULT NULL,
  `PUURAIAN` varchar(255) DEFAULT NULL,
  `PUGUDANG` int(11) DEFAULT NULL,
  `PUGUDANGT` int(11) DEFAULT NULL,
  `PUSTATUS` int(11) DEFAULT '0',
  `PUTOTALTRANS` double DEFAULT '0',
  `PUCREATEU` int(11) DEFAULT NULL,
  `PUCREATED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `PUMODIFU` int(11) DEFAULT NULL,
  `PUMODIFD` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `fstokd`
--

CREATE TABLE `fstokd` (
  `SDID` int(11) NOT NULL,
  `SDIDSU` int(11) NOT NULL,
  `SDURUTAN` int(11) NOT NULL,
  `SDSUMBER` varchar(5) DEFAULT NULL,
  `SDITEM` int(11) DEFAULT NULL,
  `SDMASUK` double DEFAULT '0',
  `SDKELUAR` double DEFAULT '0',
  `SDHARGA` double DEFAULT '0',
  `SDDISKON` double DEFAULT '0',
  `SDSATUAN` int(11) DEFAULT NULL,
  `SDSATUAND` int(11) DEFAULT NULL,
  `SDMASUKD` double DEFAULT '0',
  `SDKELUARD` double DEFAULT '0',
  `SDFAKTUR` double NOT NULL DEFAULT '0',
  `SDCATATAN` varchar(255) DEFAULT NULL,
  `SDDIVISI` int(11) DEFAULT NULL,
  `SDPROYEK` int(11) DEFAULT NULL,
  `SDGUDANG` int(11) DEFAULT NULL,
  `SDCATATANKOLI` varchar(1000) DEFAULT NULL,
  `SDPBDID` int(11) DEFAULT NULL,
  `SDPBDURUTAN` int(11) DEFAULT NULL,
  `SDQTPAKAI` double DEFAULT '0',
  `SDSODID` int(11) DEFAULT NULL,
  `SDSODURUTAN` int(11) DEFAULT NULL,
  `SDPRDID` int(11) DEFAULT NULL,
  `SDRETUR` double DEFAULT '0',
  `SDBIAYAHARGA` double DEFAULT '0',
  `SDBIAYAIDPENERIMAAN` smallint(6) DEFAULT NULL,
  `SDBIAYAQTY` double DEFAULT '0',
  `SDBIAYAHARGABARANG` double DEFAULT '0',
  `SDBELI` double DEFAULT '0',
  `SDHARGAINVOICE` double DEFAULT '0',
  `SDDISKONINVOICE` double DEFAULT '0',
  `SDDISKONPERSEN` double DEFAULT '0',
  `SDKARYAWAN` int(11) DEFAULT NULL,
  `SDTINDAKAN` smallint(6) DEFAULT '0',
  `SDDIPAKAI` double DEFAULT '1',
  `SDIDTINDAKAN1` int(11) DEFAULT NULL,
  `SDPENDING` smallint(6) DEFAULT '0',
  `SDNOREF` varchar(255) DEFAULT NULL,
  `SDKEDATANGAN` int(11) DEFAULT '0',
  `SDDOKTER` int(11) DEFAULT NULL,
  `SDDISKONPERSEN2` double DEFAULT '0',
  `SDDISKON2` double DEFAULT '0',
  `SDPENGULANGAN` smallint(6) DEFAULT '0',
  `SDBAYARDP` double DEFAULT '0',
  `SDQTYBOX` double DEFAULT '0',
  `SDLANTAI2` varchar(5) DEFAULT NULL,
  `SDIDPOTONGSTOK` int(11) DEFAULT NULL,
  `SDCETAK` smallint(6) DEFAULT '1',
  `SDIDPROMO` int(11) DEFAULT NULL,
  `SDCATATAN2` varchar(255) DEFAULT NULL,
  `SDNOSERIALFORKLIFT` varchar(255) DEFAULT NULL,
  `SDPAJAKOUT` int(11) DEFAULT NULL,
  `SDPAJAKIN` int(11) DEFAULT NULL,
  `SDINVOICE` int(11) DEFAULT NULL,
  `SDBERATKELUAR` double DEFAULT '0',
  `SDBERATMASUK` double DEFAULT '0',
  `SDPOSTING` smallint(6) DEFAULT '0',
  `SDPAKAIHPP` double DEFAULT '0',
  `SDHPP` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `fstokd`
--

INSERT INTO `fstokd` (`SDID`, `SDIDSU`, `SDURUTAN`, `SDSUMBER`, `SDITEM`, `SDMASUK`, `SDKELUAR`, `SDHARGA`, `SDDISKON`, `SDSATUAN`, `SDSATUAND`, `SDMASUKD`, `SDKELUARD`, `SDFAKTUR`, `SDCATATAN`, `SDDIVISI`, `SDPROYEK`, `SDGUDANG`, `SDCATATANKOLI`, `SDPBDID`, `SDPBDURUTAN`, `SDQTPAKAI`, `SDSODID`, `SDSODURUTAN`, `SDPRDID`, `SDRETUR`, `SDBIAYAHARGA`, `SDBIAYAIDPENERIMAAN`, `SDBIAYAQTY`, `SDBIAYAHARGABARANG`, `SDBELI`, `SDHARGAINVOICE`, `SDDISKONINVOICE`, `SDDISKONPERSEN`, `SDKARYAWAN`, `SDTINDAKAN`, `SDDIPAKAI`, `SDIDTINDAKAN1`, `SDPENDING`, `SDNOREF`, `SDKEDATANGAN`, `SDDOKTER`, `SDDISKONPERSEN2`, `SDDISKON2`, `SDPENGULANGAN`, `SDBAYARDP`, `SDQTYBOX`, `SDLANTAI2`, `SDIDPOTONGSTOK`, `SDCETAK`, `SDIDPROMO`, `SDCATATAN2`, `SDNOSERIALFORKLIFT`, `SDPAJAKOUT`, `SDPAJAKIN`, `SDINVOICE`, `SDBERATKELUAR`, `SDBERATMASUK`, `SDPOSTING`, `SDPAKAIHPP`, `SDHPP`) VALUES
(1, 1, 1, 'SAIT', 7, 50, 0, 100000, 0, 2, 2, 50, 0, 0, NULL, NULL, NULL, 13, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, NULL, 0, 0, 0, 0, 0, 0, NULL, 0, 1, NULL, 0, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL),
(2, 2, 1, 'SAIT', 6, 50, 0, 40000, 0, 1, 1, 50, 0, 0, NULL, NULL, NULL, 13, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, NULL, 0, 0, 0, 0, 0, 0, NULL, 0, 1, NULL, 0, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 5, NULL),
(3, 3, 1, 'SAIT', 8, 50, 0, 100000, 0, 2, 2, 50, 0, 0, NULL, NULL, NULL, 13, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, NULL, 0, 0, 0, 0, 0, 0, NULL, 0, 1, NULL, 0, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL),
(4, 4, 1, 'IV', 6, 0, 5, 100000, 0, 1, 1, 0, 5, 5, 'Tes Sales 1', NULL, NULL, 13, NULL, NULL, NULL, 0, 1, NULL, NULL, 0, 0, NULL, 0, 0, 0, 100000, 0, 0, NULL, 0, 1, NULL, 0, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL),
(5, 5, 1, 'SAIT', 5, 100, 0, 30000, 0, 1, 1, 100, 0, 0, NULL, NULL, NULL, 13, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, NULL, 0, 0, 0, 0, 0, 0, NULL, 0, 1, NULL, 0, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL),
(6, 6, 1, 'SAIT', 9, 70, 0, 100000, 0, 1, 1, 70, 0, 0, NULL, NULL, NULL, 13, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, NULL, 0, 0, 0, 0, 0, 0, NULL, 0, 1, NULL, 0, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL),
(7, 7, 1, 'SAIT', 10, 50, 0, 100000, 0, 2, 2, 50, 0, 0, NULL, NULL, NULL, 13, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, NULL, 0, 0, 0, 0, 0, 0, NULL, 0, 1, NULL, 0, NULL, 0, NULL, 0, 0, 0, 0, 0, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL);

--
-- Triggers `fstokd`
--
DELIMITER $$
CREATE TRIGGER `fstokd_DELL` AFTER DELETE ON `fstokd` FOR EACH ROW begin

  DECLARE tmpVar INTEGER; 


  update bitem set ISTOCKTOTAL=ISTOCKTOTAL-(old.SDMASUK-old.SDKELUAR) where IID=old.SDITEM;   
   update esalesorderd set SODKELUAR = SODKELUAR - old.SDKELUAR where sodid = OLD.SDSODID ;  
    update esalesorderd set SODMASUK= SODMASUK - OLD.SDMASUK where sodid = OLD.SDSODID ; 
end
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `fstokd_add` BEFORE INSERT ON `fstokd` FOR EACH ROW begin

  DECLARE tmpVar INTEGER; 
  
   update bitem set ISTOCKTOTAL=ISTOCKTOTAL+(new.SDMASUK-new.SDKELUAR) where IID=NEW.SDITEM;    
   update esalesorderd set SODKELUAR = SODKELUAR + NEW.SDKELUAR where sodid = new.SDSODID ;     
   update esalesorderd set SODMASUK= SODMASUK + NEW.SDMASUK where sodid = new.SDSODID ;
end
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `fstokd_edit` AFTER UPDATE ON `fstokd` FOR EACH ROW begin

  DECLARE tmpVar INTEGER;

update bitem set ISTOCKTOTAL=ISTOCKTOTAL-(old.SDMASUK-old.SDKELUAR) where IID=old.SDITEM; 
  
  update bitem set ISTOCKTOTAL=ISTOCKTOTAL+(new.SDMASUK-new.SDKELUAR) where IID=NEW.SDITEM;   
  
    
   update esalesorderd set SODKELUAR = SODKELUAR - OLD.SDKELUAR where sodid = OLD.SDSODID ;
   
     
   update esalesorderd set SODKELUAR = SODKELUAR + NEW.SDKELUAR where sodid = NEW.SDSODID ;  
   
   
   update esalesorderd set SODMASUK= SODMASUK - OLD.SDMASUK where sodid = OLD.SDSODID ;
   
   update esalesorderd set SODMASUK= SODMASUK + NEW.SDMASUK where sodid = new.SDSODID ;

end
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `fstokd_koli`
--

CREATE TABLE `fstokd_koli` (
  `SDKID` int(11) NOT NULL,
  `SDKIDD` int(11) DEFAULT NULL,
  `SDKNOKOLI` varchar(100) DEFAULT NULL,
  `SDKQTY` double DEFAULT '0',
  `SDKURUTAN` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `fstokd_koli`
--

INSERT INTO `fstokd_koli` (`SDKID`, `SDKIDD`, `SDKNOKOLI`, `SDKQTY`, `SDKURUTAN`) VALUES
(7, 479, '7A00186-1', 1175, 1),
(8, 479, '7A00202-1', 633, 2),
(9, 479, '7A00203-1', 1168, 3),
(10, 479, '7A00204-1', 1391, 4),
(11, 479, '7A00205-1', 1193, 5),
(12, 479, '7A00205-2', 1130, 6),
(13, 479, '7A00206-1', 1259, 7),
(14, 479, '7A00207-1', 1206, 8),
(15, 479, '7A00207-2', 1080, 9),
(16, 479, '7A00208-1', 1185, 10),
(17, 479, '7A00208-2', 1123, 11),
(18, 479, '7A00209-1', 1306, 12),
(19, 479, '7A00209-2', 879, 13),
(20, 479, '7A00210-1', 876, 14),
(21, 479, '7A00211-1', 1175, 15),
(22, 479, '7A00212-1', 1188, 16),
(23, 479, '7A00212-2', 1081, 17),
(24, 479, '7A00213-1', 1167, 18),
(25, 479, '7A00213-2', 1185, 19),
(26, 479, '7A00214-1', 1244, 20),
(27, 479, '7A00215-1', 1209, 21),
(28, 479, '7A00215-2', 1130, 22),
(29, 480, '3B90451-6', 3009, 1),
(30, 491, '3B90525-5', 2380, 1),
(31, 491, '3B90526-2', 2225, 2),
(32, 491, '3B90544-1', 2924, 3),
(33, 491, '3B90544-2', 2927, 4),
(34, 492, '4A90436-3', 2695, 1),
(35, 492, '4A90436-4', 2695, 2),
(36, 492, '4A90436-7', 3053, 3),
(58, 496, '3B90450-5', 2888, 1),
(59, 496, '3B90451-1', 2908, 2),
(60, 496, '3B90451-2', 2905, 3),
(61, 496, '3B90451-3', 2904, 4),
(62, 496, '3B90451-4', 2904, 5),
(63, 496, '3B90451-5', 2902, 6),
(78, 499, '3B90482-3', 2214, 1),
(79, 499, '3B90482-4', 2213, 2),
(80, 499, '3B90482-5', 2718, 3),
(81, 499, '3B90482-7', 2583, 4),
(82, 499, '3B90483-2', 3265, 5),
(83, 499, '3B90483-3', 3267, 6),
(84, 499, '3B90483-4', 3267, 7),
(85, 500, '3B90484-6', 2733, 1),
(86, 500, '3B90486-1', 2897, 2),
(87, 500, '3B90486-2', 2896, 3),
(88, 500, '3B90486-3', 2898, 4),
(89, 500, '3B90486-4', 2897, 5),
(90, 500, '3B90486-5', 2898, 6),
(91, 500, '3B90487-6', 3044, 7),
(92, 506, '4A90438-6', 2335, 1),
(93, 506, '4A90440-5', 2311, 2),
(94, 506, '4A90440-6', 2306, 3),
(95, 506, '4A90440-7', 2200, 4),
(96, 506, '4A90442-1', 2203, 5),
(97, 506, '4A90443-2', 3215, 6),
(98, 506, '4A90443-3', 3126, 7),
(99, 506, '4A90443-6', 2100, 8),
(100, 506, '4A90444-1', 2500, 9),
(101, 506, '4A90444-2', 2436, 10),
(102, 507, '4A90450-1', 2923, 1),
(103, 507, '4A90450-2', 2923, 2),
(104, 507, '4A90450-3', 2922, 3),
(105, 507, '4A90450-4', 2923, 4),
(106, 507, '4A90450-5', 2921, 5),
(107, 507, '4A90450-6', 2715, 6),
(108, 507, '4A90451-1', 2926, 7),
(109, 507, '4A90451-2', 2925, 8),
(110, 507, '4A90451-3', 2924, 9),
(111, 508, '4A90444-3', 3126, 1),
(112, 508, '4A90444-4', 3125, 2),
(113, 508, '4A90444-5', 3122, 3),
(114, 508, '4A90444-6', 3003, 4),
(115, 508, '4A90445-1', 2919, 5),
(116, 508, '4A90445-2', 2918, 6),
(117, 508, '4A90445-4', 2912, 7),
(118, 508, '4A90440-4', 2401, 8),
(119, 508, '4A90446-6', 2716, 9),
(120, 509, '4A90457-1', 2903, 1),
(121, 509, '4A90457-2', 2904, 2),
(122, 509, '4A90457-3', 2904, 3),
(123, 509, '4A90457-4', 2903, 4),
(124, 509, '4A90457-5', 2904, 5),
(125, 509, '4A90457-6', 2658, 6),
(126, 509, '4A90458-1', 2909, 7),
(127, 509, '4A90458-2', 2908, 8),
(128, 509, '4A90458-3', 2908, 9),
(129, 510, '4A90451-4', 2923, 1),
(130, 510, '4A90451-5', 2923, 2),
(131, 510, '4A90452-1', 2937, 3),
(132, 510, '4A90452-2', 2940, 4),
(133, 510, '4A90452-3', 2939, 5),
(134, 510, '4A90452-4', 2940, 6),
(135, 510, '4A90452-5', 2938, 7),
(136, 510, '4A90452-6', 2765, 8),
(137, 511, '4A90458-4', 2903, 1),
(138, 511, '4A90458-5', 2903, 2),
(139, 511, '4A90458-6', 2770, 3),
(140, 511, '4A90459-1', 2907, 4),
(141, 511, '4A90459-2', 2907, 5),
(142, 511, '4A90459-3', 2903, 6),
(143, 511, '4A90459-4', 2908, 7),
(144, 511, '4A90459-5', 2909, 8),
(145, 545, '1B00047-6', 2303, 1),
(146, 545, '1B00048-4', 2963, 2),
(147, 545, '1B00048-5', 2965, 3),
(148, 545, '1B00049-1', 2936, 4),
(149, 545, '1B00049-6', 2925, 5),
(150, 545, '1B00051-2', 2929, 6),
(151, 545, '1B00051-5', 2932, 7),
(152, 546, '1B00038-2', 2810, 1),
(153, 546, '1B00038-4', 2886, 2),
(154, 546, '1B00043-2', 2938, 3),
(155, 546, '1B00043-5', 2941, 4),
(156, 546, '1B00046-7', 2413, 5),
(157, 546, '1B00048-2', 2966, 6),
(158, 546, '1B00049-4', 2936, 7),
(159, 551, '3B00481-3', 2879, 1),
(160, 551, '3B00485-1', 2889, 2),
(161, 551, '3B00485-2', 2889, 3),
(162, 551, '3B00485-3', 2888, 4),
(163, 551, '3B00485-4', 2889, 5),
(164, 551, '3B00485-6', 3071, 6),
(165, 551, '3B00486-4', 2897, 7),
(166, 552, '3B00481-2', 2882, 1),
(167, 552, '3B00481-4', 2879, 2),
(168, 552, '3B00484-5', 2890, 3),
(169, 552, '3B00486-2', 2898, 4),
(170, 552, '3B00488-2', 2902, 5),
(171, 552, '3B00488-4', 2901, 6),
(172, 552, '3B00488-5', 2901, 7),
(193, 576, '9B01738-1', 1040, 1),
(194, 576, '9B01739-2', 1018, 2),
(195, 576, '9B01740-2', 1060, 3),
(196, 576, '9B01741-2', 1056, 4),
(197, 576, '9B01742-2', 1057, 5),
(198, 576, '9B01743-2', 1061, 6),
(199, 576, '9B01745-2', 1072, 7),
(200, 576, '9B01781-2', 1058, 8),
(201, 576, '9B01847-1', 1035, 9),
(202, 576, '9B01848-2', 1047, 10),
(203, 576, '9B01849-2', 988, 11),
(204, 576, '9B01905-1', 1007, 12),
(205, 576, '9B01905-2', 1036, 13),
(206, 576, '9B01911-1', 1030, 14),
(207, 576, '9B01912-1', 952, 15),
(208, 576, '9B01913-2', 1053, 16),
(209, 576, '9B01915-2', 1013, 17),
(210, 576, '9D00607-2', 1023, 18),
(211, 576, '9D00666-2', 984, 19),
(212, 576, '9D00667-2', 1023, 20),
(213, 578, '9B01889-2', 1087, 1),
(214, 578, '9B01901-2', 1035, 2),
(215, 578, '9B01908-1', 1031, 3),
(216, 578, '9B01909-2', 1008, 4),
(217, 578, '9B01913-1', 955, 5),
(218, 578, '9B01915-1', 1032, 6),
(219, 578, '9B01919-1', 1053, 7),
(220, 578, '9B01920-1', 941, 8),
(221, 578, '9B01920-2', 1030, 9),
(222, 578, '9B01921-1', 973, 10),
(223, 578, '9B01921-2', 1068, 11),
(224, 578, '9B01922-1', 991, 12),
(225, 578, '9B01922-2', 1058, 13),
(226, 578, '9B01932-1', 990, 14),
(227, 578, '9D00667-1', 1008, 15),
(228, 578, '9D00668-2', 938, 16),
(229, 578, '9D00669-1', 982, 17),
(230, 578, '9D00673-2', 895, 18),
(231, 578, '9D00674-2', 851, 19),
(232, 578, '9B01713-2', 1072, 20),
(233, 579, '9B01783-2', 1063, 1),
(234, 579, '9B01785-2', 1061, 2),
(235, 579, '9B01786-2', 1067, 3),
(236, 579, '9B01795-2', 1053, 4),
(237, 579, '9B01796-2', 1045, 5),
(238, 579, '9B01797-2', 1065, 6),
(239, 579, '9B01799-2', 1060, 7),
(240, 579, '9B01800-2', 1070, 8),
(241, 579, '9B01906-1', 1029, 9),
(242, 579, '9B01908-2', 1009, 10),
(243, 579, '9B01909-1', 1034, 11),
(244, 579, '9B01917-1', 999, 12),
(245, 579, '9B01919-2', 981, 13),
(246, 579, '9B01925-1', 1010, 14),
(247, 579, '9B01926-1', 1080, 15),
(248, 579, '9B01930-1', 1011, 16),
(249, 579, '9B01937-1', 991, 17),
(250, 579, '9D00660-2', 906, 18),
(251, 579, '9D00661-2', 949, 19),
(252, 579, '9B01717-2', 1063, 20),
(253, 580, '3B00098-6', 3041, 1),
(254, 580, '3B00099-2', 3066, 2),
(255, 580, '3B00099-4', 3067, 3),
(256, 580, '3B00100-3', 3066, 4),
(257, 581, '9B01333-1', 1078, 1),
(258, 581, '9B01334-2', 1070, 2),
(259, 581, '9B01753-2', 1067, 3),
(260, 581, '9B01761-2', 1050, 4),
(261, 581, '9B01762-2', 1044, 5),
(262, 581, '9B01765-1', 1068, 6),
(263, 581, '9B01767-2', 1060, 7),
(264, 581, '9B01780-2', 1044, 8),
(265, 581, '9B01784-2', 1063, 9),
(266, 581, '9B01828-2', 1059, 10),
(267, 581, '9B01829-1', 1023, 11),
(268, 581, '9B01830-2', 1078, 12),
(269, 581, '9B01831-2', 1066, 13),
(270, 581, '9B01833-2', 1073, 14),
(271, 581, '9B01834-2', 1071, 15),
(272, 581, '9B01835-2', 1078, 16),
(273, 581, '9B01836-2', 1067, 17),
(274, 581, '9D00385-1', 993, 18),
(275, 581, '9D00618-1', 946, 19),
(276, 581, '9B01168-2', 1064, 20),
(277, 582, '9B01794-1', 1028, 1),
(278, 582, '9B01802-2', 1047, 2),
(279, 582, '9B01832-2', 1058, 3),
(280, 582, '9B01903-2', 976, 4),
(281, 582, '9B01906-2', 1009, 5),
(282, 582, '9B01907-2', 1004, 6),
(283, 582, '9B01910-2', 1037, 7),
(284, 582, '9B01918-1', 969, 8),
(285, 582, '9B01918-2', 991, 9),
(286, 582, '9B01929-1', 1036, 10),
(287, 582, '9B01931-1', 994, 11),
(288, 582, '9B01931-2', 1044, 12),
(289, 582, '9B01932-2', 1009, 13),
(290, 582, '9B01934-1', 1032, 14),
(291, 582, '9B01937-2', 1007, 15),
(292, 582, '9B01938-2', 1011, 16),
(293, 582, '9D00670-1', 1024, 17),
(294, 582, '9D00670-2', 1025, 18),
(295, 582, '9B01801-2', 1066, 19),
(296, 582, '9B01173-2', 928, 20),
(297, 583, '1A00005-1', 2516, 1),
(298, 583, '1A00005-7', 2336, 2),
(299, 583, '1A00011-2', 3165, 3),
(300, 583, '1A00011-3', 3168, 4),
(301, 583, '1A00022-3', 3047, 5),
(302, 583, '1A00022-4', 3048, 6),
(303, 583, '1A00026-3', 2670, 7),
(304, 584, '9B01707-2', 1065, 1),
(305, 584, '9B01789-2', 1044, 2),
(306, 584, '9B01790-2', 1052, 3),
(307, 584, '9B01791-2', 1061, 4),
(308, 584, '9B01793-2', 1073, 5),
(309, 584, '9B01867-2', 1057, 6),
(310, 584, '9B01868-2', 1080, 7),
(311, 584, '9B01870-2', 1039, 8),
(312, 584, '9B01872-2', 1053, 9),
(313, 584, '9B01873-2', 1061, 10),
(314, 584, '9B01874-2', 1053, 11),
(315, 584, '9B01912-2', 1064, 12),
(316, 584, '9B01916-1', 1018, 13),
(317, 584, '9B01916-2', 1025, 14),
(318, 584, '9B01917-2', 1017, 15),
(319, 584, '9B01936-1', 1022, 16),
(320, 584, '9B01938-1', 1027, 17),
(321, 584, '9B01939-1', 982, 18),
(322, 584, '9D00632-3', 950, 19),
(323, 584, '9B01897-2', 1088, 20),
(324, 585, '9B01636-2', 1063, 1),
(325, 585, '9B01637-1', 916, 2),
(326, 585, '9B01716-2', 1012, 3),
(327, 585, '9B01754-2', 1050, 4),
(328, 585, '9B01771-2', 1042, 5),
(329, 585, '9B01773-2', 1037, 6),
(330, 585, '9B01775-2', 1059, 7),
(331, 585, '9B01792-2', 936, 8),
(332, 585, '9B01846-2', 1051, 9),
(333, 585, '9B01847-2', 1060, 10),
(334, 585, '9B01864-1', 1082, 11),
(335, 585, '9B01864-2', 1061, 12),
(336, 585, '9B01866-2', 1056, 13),
(337, 585, '9B01898-1', 1050, 14),
(338, 585, '9B01902-2', 1021, 15),
(339, 585, '9B01933-2', 1011, 16),
(340, 585, '9B01935-2', 1015, 17),
(341, 585, '9D00622-2', 984, 18),
(342, 585, '9B01899-2', 1058, 19),
(343, 585, '9D00647-2', 900, 20),
(344, 586, '9B01856-2', 1024, 1),
(345, 586, '9B01900-2', 1043, 2),
(346, 586, '9B01904-2', 1046, 3),
(347, 586, '9B01914-1', 1032, 4),
(348, 586, '9B01914-2', 1013, 5),
(349, 586, '9B01933-1', 1035, 6),
(350, 586, '9B01934-2', 1014, 7),
(351, 586, '9B01935-1', 1018, 8),
(352, 586, '9B01936-2', 1018, 9),
(353, 586, '9B01940-1', 1054, 10),
(354, 586, '9B01941-1', 1024, 11),
(355, 586, '9B01941-2', 1015, 12),
(356, 586, '9B01942-2', 1015, 13),
(357, 586, '9B01943-1', 1033, 14),
(358, 586, '9B01943-2', 1008, 15),
(359, 586, '9B01946-2', 917, 16),
(360, 586, '9B01947-1', 996, 17),
(361, 586, '9B01947-2', 1044, 18),
(362, 586, '9D00657-2', 868, 19),
(363, 586, '9B01942-1', 1014, 20),
(364, 587, '9B01544-2', 1064, 1),
(365, 587, '9B01758-2', 1055, 2),
(366, 587, '9B01760-1', 1037, 3),
(367, 587, '9B01766-2', 1055, 4),
(368, 587, '9B01768-2', 1018, 5),
(369, 587, '9B01804-2', 976, 6),
(370, 587, '9B01807-2', 1057, 7),
(371, 587, '9B01827-2', 1061, 8),
(372, 587, '9B01837-2', 1067, 9),
(373, 587, '9B01839-2', 1047, 10),
(374, 587, '9B01841-1', 1045, 11),
(375, 587, '9B01842-2', 1052, 12),
(376, 587, '9B01843-2', 1053, 13),
(377, 587, '9B01844-2', 1057, 14),
(378, 587, '9B01851-2', 1041, 15),
(379, 587, '9B01852-2', 1073, 16),
(380, 587, '9B01860-2', 1077, 17),
(381, 587, '9B01861-2', 1073, 18),
(382, 587, '9B01862-2', 1065, 19),
(383, 588, '9B01777-2', 1056, 1),
(384, 588, '9B01787-2', 1043, 2),
(385, 588, '9B01813-2', 1059, 3),
(386, 588, '9B01814-2', 1059, 4),
(387, 588, '9B01815-2', 1043, 5),
(388, 588, '9B01816-2', 1045, 6),
(389, 588, '9B01818-2', 1040, 7),
(390, 588, '9B01819-2', 1063, 8),
(391, 588, '9B01820-2', 1064, 9),
(392, 588, '9B01821-2', 1051, 10),
(393, 588, '9B01822-2', 1073, 11),
(394, 588, '9B01825-2', 1053, 12),
(395, 588, '9B01838-2', 1083, 13),
(396, 588, '9B01840-2', 1019, 14),
(397, 588, '9B01845-2', 1060, 15),
(398, 588, '9B01875-2', 1051, 16),
(399, 588, '9B01876-2', 1051, 17),
(400, 588, '9B01882-2', 1057, 18),
(401, 588, '9B01885-2', 1058, 19),
(402, 589, '9B02047-2', 1050, 1),
(403, 589, '9B02046-2', 1021, 2),
(404, 589, '9B02048-2', 1026, 3),
(405, 589, '9B02048-1', 1010, 4),
(406, 589, '9B02049-2', 1029, 5),
(407, 589, '9B01972-2', 1060, 6),
(408, 589, '9B01971-1', 1035, 7),
(409, 589, '9B01970-2', 1008, 8),
(410, 589, '9B01965-2', 1060, 9),
(411, 589, '9B01963-1', 1037, 10),
(412, 589, '9B01960-2', 1033, 11),
(413, 589, '9B01956-2', 1052, 12),
(414, 589, '9B01952-2', 1005, 13),
(415, 589, '9B01949-1', 902, 14),
(416, 589, '9B01948-1', 983, 15),
(417, 589, '9B01948-2', 1058, 16),
(418, 589, '9B01956-1', 953, 17),
(419, 589, '9B01939-2', 1058, 18),
(420, 589, '9B02031-2', 980, 19),
(421, 590, '9B01957-2', 1060, 1),
(422, 590, '9B01958-1', 1026, 2),
(423, 590, '9B01954-1', 1026, 3),
(424, 590, '9B01963-2', 1008, 4),
(425, 590, '9B01960-1', 1007, 5),
(426, 590, '9B01985-1', 1016, 6),
(427, 590, '9B01984-2', 1049, 7),
(428, 590, '9B01985-2', 1020, 8),
(429, 590, '9B01980-1', 1045, 9),
(430, 590, '9B01981-2', 1033, 10),
(431, 590, '9B02012-1', 959, 11),
(432, 590, '9B02012-2', 1052, 12),
(433, 590, '9B02013-2', 1041, 13),
(434, 590, '9B02002-1', 906, 14),
(435, 590, '9B02004-2', 960, 15),
(436, 590, '9B02005-2', 1011, 16),
(437, 590, '9B02000-1', 1013, 17),
(438, 590, '9B02004-1', 995, 18),
(439, 590, '9B01981-1', 917, 19),
(440, 591, '4A00033-3', 2928, 1),
(441, 591, '4A00033-5', 2931, 2),
(442, 591, '4B00014-2', 3144, 3),
(443, 591, '4B00014-3', 3066, 4),
(444, 591, '4B00014-2', 2717, 5),
(445, 591, '4B00014-5', 2717, 6),
(446, 591, '4B00023-1', 2914, 7),
(447, 592, '4A00030-4', 2935, 1),
(448, 592, '4A00032-3', 2923, 2),
(449, 592, '4A00039-2', 2948, 3),
(450, 592, '4A00041-3', 2951, 4),
(451, 592, '4B00012-1', 2918, 5),
(452, 592, '4B00019-5', 2936, 6),
(453, 592, '4B00021-3', 2920, 7),
(454, 593, '4A00040-4', 2945, 1),
(455, 593, '4B00011-7', 2351, 2),
(456, 593, '4B00020-4', 3146, 3),
(457, 593, '4B00020-5', 2517, 4),
(458, 593, '4B00022-2', 2932, 5),
(459, 593, '4B00022-4', 2933, 6),
(460, 593, '4B00023-6', 2771, 7),
(461, 613, '3B00032-4', 3067, 1),
(462, 613, '3B00033-6', 3041, 2),
(463, 613, '3B00034-4', 2536, 3),
(464, 613, '3B00035-1', 2876, 4),
(465, 613, '3B00035-3', 2875, 5),
(466, 613, '3B00038-4', 2714, 6),
(467, 613, '3B00038-6', 2712, 7),
(468, 617, '4A90496-3', 2928, 1),
(469, 617, '4A00003-5', 2914, 2),
(470, 617, '4A00004-1', 2918, 3),
(471, 617, '4A00024-1', 2916, 4),
(472, 617, '4A00025-4', 2904, 5),
(473, 617, '4A00026-1', 2901, 6),
(474, 617, '4A00023-6', 2788, 7),
(475, 618, '3B90574-4', 2897, 1),
(476, 618, '3B90584-5', 2900, 2),
(477, 618, '3B90603-5', 2918, 3),
(482, 620, '1A00001-2', 2904, 1),
(483, 620, '1A90033-1', 2888, 2),
(484, 620, '1A00004-5', 2888, 3),
(485, 620, '1A00006-1', 2785, 4),
(486, 620, '1A00006-2', 2784, 5),
(487, 620, '1A00006-3', 3168, 6),
(488, 620, '1A00006-6', 2845, 7),
(489, 621, '1A00001-1', 2907, 1),
(490, 621, '1A00001-3', 2901, 2),
(491, 621, '1A00001-5', 2900, 3),
(492, 621, '1A00001-6', 2858, 4),
(493, 621, '1A00002-1', 2930, 5),
(494, 621, '1A00002-2', 2925, 6),
(495, 621, '1A00002-3', 2922, 7),
(496, 622, '1A00002-4', 2924, 1),
(497, 622, '1A00002-5', 2926, 2),
(498, 622, '1A00002-6', 2902, 3),
(499, 622, '1A00003-1', 2901, 4),
(500, 622, '1A00003-2', 2933, 5),
(501, 622, '1A00003-3', 2926, 6),
(502, 622, '1A00003-4', 2930, 7),
(503, 623, '1A00003-5', 2929, 1),
(504, 623, '1A00003-6', 2901, 2),
(505, 623, '1A00004-1', 2871, 3),
(506, 623, '1A00004-2', 2890, 4),
(507, 623, '1A00004-3', 2891, 5),
(508, 623, '1A00004-4', 2889, 6),
(509, 623, '1A00004-6', 2840, 7),
(510, 632, '3B00043-6', 2035, 1),
(511, 632, '3B00052-6', 3018, 2),
(512, 632, '3B00053-1', 2890, 3),
(513, 632, '3B00055-2', 2887, 4),
(514, 632, '3B00055-5', 2888, 5),
(515, 632, '3B00057-5', 2895, 6),
(516, 632, '3B00057-6', 3056, 7),
(517, 635, '4B00100-1', 2318, 1),
(518, 635, '4B00101-3', 2905, 2),
(519, 635, '4B00101-6', 2810, 3),
(520, 635, '4B00103-1', 2111, 4),
(521, 635, '4B00103-3', 2707, 5),
(522, 635, '4B00103-5', 2706, 6),
(523, 635, '4B00104-4', 2905, 7),
(524, 635, '4B00104-5', 2904, 8),
(525, 635, '4B00105-1', 3100, 9),
(526, 636, '4A00065-3', 2913, 1),
(527, 636, '4A00073-1', 2915, 2),
(528, 636, '4A00082-6', 2864, 3),
(529, 636, '4A00084-1', 2899, 4),
(530, 636, '4A00084-2', 2896, 5),
(531, 636, '4A00084-3', 2894, 6),
(532, 636, '4A00086-1', 2929, 7),
(533, 637, '4A00096-1', 2910, 1),
(534, 637, '4B00060-2', 2905, 2),
(535, 637, '4B00079-6', 2829, 3),
(536, 637, '4B00084-1', 2945, 4),
(537, 637, '4B00084-4', 2947, 5),
(538, 637, '4B00085-5', 2928, 6),
(539, 637, '4B00086-2', 2958, 7),
(540, 638, '4B00090-1', 2925, 1),
(541, 638, '4B00090-3', 2925, 2),
(542, 638, '4B00090-4', 2926, 3),
(543, 638, '4B00090-5', 2901, 4),
(544, 638, '4B00090-6', 2831, 5),
(545, 638, '4B00071-4', 2854, 6),
(546, 638, '4B00071-5', 2865, 7),
(547, 639, '4B00071-6', 3108, 1),
(548, 639, '4B00073-6', 3154, 2),
(549, 639, '4B00074-2', 2896, 3),
(550, 639, '4B00075-5', 2904, 4),
(551, 639, '4B00085-6', 2808, 5),
(552, 639, '4B00090-2', 2925, 6),
(553, 639, '4B00050-7', 2439, 7),
(554, 640, '4A00057-4', 2929, 1),
(555, 640, '4A00072-6', 2631, 2),
(556, 640, '4A00074-3', 2919, 3),
(557, 640, '4A00074-4', 2911, 4),
(558, 640, '4A00074-5', 2710, 5),
(559, 640, '4A00074-6', 2901, 6),
(560, 640, '4A00075-2', 2914, 7),
(561, 640, '4A00076-3', 2902, 8),
(562, 640, '4A00076-5', 2906, 9),
(563, 641, '4A00086-2', 2922, 1),
(564, 641, '4A00089-1', 2914, 2),
(565, 641, '4A00091-2', 2912, 3),
(566, 641, '4A00092-2', 2924, 4),
(567, 641, '4A00093-1', 2925, 5),
(568, 641, '4A00093-6', 2922, 6),
(569, 641, '4A00094-2', 2922, 7),
(570, 642, '4A00056-1', 2940, 1),
(571, 642, '4A00057-3', 2930, 2),
(572, 642, '4A00067-5', 2706, 3),
(573, 642, '4A00070-5', 2915, 4),
(574, 642, '4A00071-2', 2912, 5),
(575, 642, '4A00088-5', 2919, 6),
(576, 642, '4A00089-4', 2911, 7),
(577, 643, '3B90543-2', 2910, 1),
(578, 643, '3B00114-6', 3076, 2),
(579, 643, '3B00173-2', 2889, 3),
(580, 644, '3A00052-6', 3348, 1),
(581, 644, '3B00164-4', 2930, 2),
(582, 644, '3B00171-4', 2904, 3),
(583, 645, '9B01002-1', 1141, 1),
(584, 645, '9B01002-2', 1188, 2),
(585, 645, '9B01003-1', 1188, 3),
(586, 645, '9B01003-2', 1173, 4),
(587, 645, '9B01004-2', 1119, 5),
(588, 645, '9B01005-2', 1103, 6),
(589, 645, '9B01006-1', 1288, 7),
(590, 645, '9B01006-2', 1072, 8),
(591, 645, '9B01007-1', 1203, 9),
(592, 645, '9B01007-2', 1157, 10),
(593, 645, '9D00245-1', 1153, 11),
(594, 645, '9D00246-1', 1214, 12),
(595, 645, '9D00246-2', 1149, 13),
(596, 645, '9D00247-1', 1065, 14),
(597, 645, '9D00247-2', 1321, 15),
(598, 645, '9D00248-1', 1270, 16),
(599, 645, '9D00248-2', 1097, 17),
(600, 645, '9D00249-1', 1207, 18),
(601, 645, '9D00249-2', 1152, 19),
(602, 645, '9D00250-1', 1139, 20),
(603, 645, '9D00250-2', 1221, 21),
(604, 646, '3A00001-2', 2709, 1),
(605, 646, '3A00047-6', 2602, 2),
(606, 646, '3A00058-3', 2883, 3),
(607, 646, '3A00059-1', 2894, 4),
(608, 646, '3A00060-3', 2887, 5),
(609, 646, '3B00187-1', 2897, 6),
(610, 646, '3B00197-3', 2870, 7),
(611, 647, '3A00058-4', 2883, 1),
(612, 647, '3A00063-1', 2856, 2),
(613, 647, '3A00064-1', 2868, 3),
(614, 647, '3A00064-2', 2866, 4),
(615, 647, '3B90573-6', 2980, 5),
(616, 647, '3B00101-3', 2884, 6),
(617, 647, '3B00200-2', 2904, 7),
(618, 648, '3B00203-6', 3022, 1),
(619, 648, '3B00210-4', 2915, 2),
(620, 649, '4B00098-3', 2950, 1),
(621, 649, '4A00112-5', 2929, 2),
(622, 649, '4A00113-3', 2926, 3),
(623, 649, '4B00106-5', 2926, 4),
(624, 649, '4B00106-1', 2923, 5),
(625, 649, '4B00112-4', 2924, 6),
(626, 649, '4A00111-3', 2922, 7),
(627, 649, '4A00111-2', 2922, 8),
(628, 649, '4A00108-1', 2921, 9),
(629, 649, '4A00111-1', 2921, 10),
(630, 650, '4A00105-1', 2915, 1),
(631, 650, '4A00110-3', 2921, 2),
(632, 650, '4A00110-5', 2921, 3),
(633, 650, '4A00111-4', 2921, 4),
(634, 650, '4A00113-6', 2856, 5),
(635, 650, '4B00107-6', 2810, 6),
(636, 650, '4A00108-2', 2917, 7),
(637, 650, '4A00111-6', 2854, 8),
(638, 650, '4A00104-6', 2505, 9),
(639, 650, '4A00112-6', 2841, 10),
(640, 650, '4A00114-1', 2507, 11),
(641, 651, '11B00001-2', 1127, 1),
(642, 651, '11B00002-1', 924, 2),
(643, 651, '11B00003-1', 887, 3),
(644, 651, '11B00003-2', 1009, 4),
(645, 651, '11B00004-1', 1042, 5),
(646, 651, '11B00004-2', 950, 6),
(647, 651, '11B00005-1', 958, 7),
(648, 651, '11B00005-2', 1095, 8),
(649, 651, '11B80247-2', 992, 9),
(650, 651, '11B80248-2', 1038, 10),
(651, 651, '11B90029-1', 1013, 11),
(652, 651, '11B90030-2', 1001, 12),
(653, 651, '11B00002-2', 1068, 13),
(654, 651, '11B00006-1', 1290, 14),
(655, 652, '11B90014-2', 653, 1),
(656, 652, '11B00007-1', 988, 2),
(657, 652, '11B00007-2', 968, 3),
(658, 652, '11B00008-1', 1052, 4),
(659, 652, '11B00008-2', 1006, 5),
(660, 652, '11B00009-1', 688, 6),
(661, 652, '11B00009-2', 892, 7),
(662, 652, '11B00010-1', 1157, 8),
(663, 652, '11B00010-2', 896, 9),
(664, 652, '11B00011-1', 953, 10),
(665, 652, '11B00011-2', 1098, 11),
(666, 652, '11B00012-1', 941, 12),
(667, 652, '11B00012-2', 1109, 13),
(668, 652, '11B00013-1', 971, 14),
(669, 652, '11B00013-2', 1083, 15),
(670, 653, '4A00117-2', 2924, 1),
(671, 653, '4A00117-4', 2924, 2),
(672, 653, '4A00117-5', 2922, 3),
(673, 653, '4A00118-2', 2929, 4),
(674, 655, '4A00117-3', 2919, 1),
(675, 655, '4A00118-3', 2929, 2),
(676, 655, '4A00118-4', 2923, 3),
(677, 655, '4A00119-1', 2924, 4),
(678, 655, '4A00119-2', 2923, 5),
(679, 655, '4A00119-6', 2849, 6),
(680, 655, '4A00117-1', 2922, 7),
(681, 656, '4A00119-5', 2920, 1),
(682, 656, '4B00109-1', 2123, 2),
(683, 656, '4B00109-5', 2723, 3),
(684, 657, '4B00048-2', 2948, 1),
(685, 657, '4B00084-3', 2942, 2),
(686, 657, '4B00084-5', 2949, 3),
(687, 657, '4N00085-2', 2950, 4),
(688, 657, '4B00072-2', 2865, 5),
(689, 657, '4B00072-5', 2861, 6),
(690, 657, '4B00073-3', 2865, 7),
(691, 664, '4B0083-2', 2734, 1),
(692, 664, '4B0083-6', 2982, 2),
(693, 664, '4B0085-1', 2952, 3),
(694, 664, '4B0085-3', 2954, 4),
(695, 664, '4B00100-3', 2724, 5),
(696, 664, '4B00102-1', 2704, 6),
(697, 664, '4B00102-2', 2707, 7),
(698, 664, '4B00102-3', 2891, 8),
(699, 664, '4B00102-4', 3119, 9),
(700, 665, '4B00102-5', 3123, 1),
(701, 665, '4B00102-6', 2755, 2),
(702, 665, '4B00103-4', 2708, 3),
(703, 665, '4B00104-2', 2908, 4),
(704, 665, '4B00105-5', 2711, 5),
(705, 665, '4B00071-2', 2871, 6),
(706, 665, '4B00071-3', 2869, 7),
(707, 665, '4B00072-3', 2865, 8),
(708, 665, '4B00073-1', 2872, 9),
(709, 666, '4A00066-1', 2915, 1),
(710, 666, '4A00069-4', 2911, 2),
(711, 666, '4A00077-4', 2902, 3),
(712, 666, '4A00081-3', 2912, 4),
(713, 666, '4A00083-1', 2924, 5),
(714, 666, '4A00087-3', 2907, 6),
(715, 666, '4A00092-5', 2921, 7),
(716, 666, '4A00095-3', 2720, 8),
(717, 667, '4B00101-4', 2909, 1),
(718, 667, '4B00104-1', 2907, 2),
(719, 667, '4B00104-6', 2802, 3),
(720, 667, '4B00072-1', 2865, 4),
(721, 667, '4B00073-2', 2865, 5),
(722, 667, '4A00050-1', 2920, 6),
(723, 667, '4A00062-2', 2903, 7),
(724, 667, '4A00062-4', 2902, 8),
(725, 667, '4A00063-4', 2902, 9),
(726, 668, '4A00096-2', 2912, 1),
(727, 668, '4B00098-4', 2951, 2),
(728, 668, '4B00103-6', 2504, 3),
(729, 669, '4A00100-3', 2928, 1),
(730, 669, '4B00067-5', 2948, 2),
(731, 669, '4B00068-3', 2948, 3),
(732, 669, '4B00069-5', 2941, 4),
(733, 669, '4B00080-1', 2709, 5),
(734, 669, '4B00084-6', 2802, 6),
(735, 669, '4B00092-3', 2926, 7),
(736, 669, '4B00096-1', 2930, 8),
(737, 669, '4A00100-5', 2932, 9),
(738, 671, '9B01031-1', 1215, 1),
(739, 671, '9B01031-2', 1146, 2),
(740, 671, '9B01032-2', 1190, 3),
(741, 671, '9B01033-2', 1172, 4),
(742, 671, '9B01034-1', 1185, 5),
(743, 671, '9B01034-2', 1174, 6),
(744, 671, '9B01035-2', 1180, 7),
(745, 671, '9B01041-2', 1180, 8),
(746, 671, '9B01048-1', 1150, 9),
(747, 671, '9B01048-2', 1168, 10),
(748, 671, '9B01049-2', 1177, 11),
(749, 671, '9B01051-1', 1191, 12),
(750, 671, '9B01052-1', 1195, 13),
(751, 671, '9B01053-1', 1187, 14),
(752, 671, '9B01053-2', 1174, 15),
(753, 671, '9D00274-2', 1143, 16),
(754, 671, '9D00275-2', 1160, 17),
(755, 671, '9D00276-1', 1207, 18),
(756, 671, '9D00290-1', 1177, 19),
(757, 671, '9D00294-1', 1228, 20),
(758, 671, '9D00294-2', 1135, 21),
(759, 671, '9D00318-2', 1178, 22),
(760, 672, '11B00014-1', 1143, 1),
(761, 672, '11B00015-1', 936, 2),
(762, 672, '11B00015-2', 1106, 3),
(763, 672, '11B00016-1', 1035, 4),
(764, 672, '11B00016-2', 1025, 5),
(765, 672, '11B00017-1', 1082, 6),
(766, 672, '11B00017-2', 967, 7),
(767, 672, '11B00018-1', 966, 8),
(768, 672, '11B00018-2', 1086, 9),
(769, 672, '11B00019-1', 1067, 10),
(770, 672, '11B00019-2', 981, 11),
(771, 672, '11B00020-1', 1199, 12),
(772, 672, '11B00020-2', 807, 13),
(773, 672, '11B00021-1', 1066, 14),
(774, 672, '11N00021-2', 990, 15),
(775, 682, '11B00025-2', 747, 1),
(776, 682, '11B00025-3', 754, 2),
(777, 682, '11B00028-1', 964, 3),
(778, 682, '11B00028-2', 1081, 4),
(779, 682, '11B00029-1', 1080, 5),
(780, 683, '11B00022-1', 1068, 1),
(781, 683, '11B00022-2', 996, 2),
(782, 683, '11B00023-1', 1075, 3),
(783, 683, '11B00023-2', 994, 4),
(784, 683, '11B00024-1', 993, 5),
(785, 683, '11B00024-2', 1061, 6),
(786, 683, '11B00026-1', 803, 7),
(787, 683, '11B00026-2', 941, 8),
(788, 683, '11B00027-1', 1020, 9),
(789, 683, '11B00027-2', 1033, 10),
(790, 689, '3B00230-6', 3085, 1),
(791, 689, '3B00228-6', 3046, 2),
(792, 700, '7A00297-2', 1144, 1),
(793, 700, '7A00299-2', 1163, 2),
(794, 700, '7A00303-2', 1108, 3),
(795, 700, '7A00305-2', 1101, 4),
(796, 700, '7A00308-2', 1171, 5),
(797, 700, '7A00309-2', 1183, 6),
(798, 700, '7A00310-1', 780, 7),
(799, 700, '7A00310-2', 600, 8),
(800, 700, '7A00311-1', 1183, 9),
(801, 700, '7A00311-2', 1179, 10),
(802, 700, '7A00312-1', 1150, 11),
(803, 700, '7A00312-2', 1133, 12),
(804, 700, '7A00313-1', 1160, 13),
(805, 700, '7A00313-2', 1173, 14),
(806, 700, '7A00314-2', 881, 15),
(807, 700, '7A00315-1', 760, 16),
(808, 700, '7A00316-1', 1141, 17),
(809, 700, '7A00318-1', 1141, 18),
(810, 700, '7A00319-1', 1188, 19),
(811, 700, '7A00319-2', 1140, 20),
(812, 700, '7A00321-1', 1192, 21),
(813, 700, '7A00323-1', 1222, 22),
(814, 700, '7A00318-2', 1203, 23),
(815, 701, '4A00047-6', 3133, 1),
(816, 701, '4A00077-6', 3233, 2),
(817, 701, '4A00101-2', 3058, 3),
(818, 701, '4A00101-5', 3064, 4),
(819, 701, '4B00082-4', 2926, 5),
(820, 702, '7A00334-1', 1119, 1),
(821, 702, '7A00334-2', 1108, 2),
(822, 702, '7A00373-2', 1180, 3),
(823, 702, '7A00374-2', 1163, 4),
(824, 702, '7A00376-2', 1166, 5),
(825, 702, '7A00384-1', 1177, 6),
(826, 702, '7A00398-1', 1235, 7),
(827, 702, '7A00398-2', 1078, 8),
(828, 702, '7A00399-1', 1144, 9),
(829, 702, '7A00400-1', 1137, 10),
(830, 702, '7A00400-2', 1094, 11),
(831, 702, '7A00401-1', 1151, 12),
(832, 702, '7A00401-2', 1143, 13),
(833, 702, '7A00402-1', 1114, 14),
(834, 702, '7A00402-2', 1193, 15),
(835, 702, '7A00403-1', 1149, 16),
(836, 702, '7A00403-2', 1204, 17),
(837, 702, '7A00404-1', 1170, 18),
(838, 702, '7A00404-2', 1180, 19),
(839, 702, '7A00405-1', 1138, 20),
(840, 702, '7A00406-1', 1172, 21),
(841, 702, '7A00406-2', 1143, 22),
(842, 704, '3B00105-3', 2883, 1),
(843, 704, '3B00105-4', 2888, 2),
(844, 704, '3B00105-5', 2888, 3),
(845, 704, '3B00109-5', 2895, 4),
(846, 704, '3B00109-6', 2757, 5),
(847, 704, '3B00110-6', 3059, 6),
(848, 704, '3B00111-2', 2891, 7),
(849, 705, '4B00086-1', 2960, 1),
(850, 705, '4B00096-2', 2923, 2),
(851, 705, '4B00098-5', 2951, 3),
(852, 705, '4B00099-1', 2922, 4),
(853, 705, '4B00099-2', 2922, 5),
(854, 705, '4B00101-5', 2910, 6),
(855, 705, '4B00105-6', 2281, 7),
(856, 706, '3A00074-5', 2896, 1),
(857, 706, '3A00076-5', 2542, 2),
(858, 706, '3A00078-7', 2711, 3),
(859, 706, '3A00079-4', 2892, 4),
(860, 706, '3A00079-5', 2890, 5),
(861, 706, '3B00216-4', 2936, 6),
(862, 706, '3B00218-6', 3106, 7),
(863, 707, '4B00029-1', 2951, 1),
(864, 707, '4B00039-6', 2515, 2),
(865, 707, '4A00037-3', 3157, 3),
(866, 707, '4A00039-3', 2947, 4),
(867, 707, '4A00045-1', 2520, 5),
(868, 707, '4B00032-5', 2944, 6),
(869, 707, '4B00042-5', 2310, 7),
(870, 708, '3A00078-2', 2374, 1),
(871, 708, '3A00079-1', 2895, 2),
(872, 708, '3B00211-2', 2921, 3),
(873, 708, '3B00211-4', 2919, 4),
(874, 708, '3B00212-5', 2923, 5),
(875, 708, '3B00214-5', 2663, 6),
(876, 708, '3B00217-3', 2914, 7),
(877, 714, '3A00077-6', 3056, 1),
(878, 714, '3B00203-4', 2904, 2),
(879, 714, '3B00205-3', 2897, 3),
(880, 714, '3B00207-2', 2904, 4),
(881, 714, '3B00207-5', 2908, 5),
(882, 714, '3B00208-2', 2913, 6),
(883, 714, '3B00212-4', 2921, 7),
(884, 715, '4A00106-1', 2915, 1),
(885, 715, '4A00106-5', 2920, 2),
(886, 715, '4A00108-3', 2920, 3),
(887, 715, '4A00110-4', 2922, 4),
(888, 715, '4A00110-6', 2840, 5),
(889, 715, '4A00127-5', 2907, 6),
(890, 715, '4B00118-6', 2800, 7),
(891, 716, '9B01084-1', 1266, 1),
(892, 716, '9B01085-1', 1231, 2),
(893, 716, '9B01149-1', 1267, 3),
(894, 716, '9B01153-2', 1272, 4),
(895, 716, '9B01175-2', 1263, 5),
(896, 716, '9B01176-1', 1390, 6),
(897, 716, '9B01177-1', 1266, 7),
(898, 716, '9B01178-1', 1265, 8),
(899, 716, '9B01180-1', 1258, 9),
(900, 716, '9B01183-1', 1306, 10),
(901, 716, '9B01189-1', 1273, 11),
(902, 716, '9B01190-1', 1263, 12),
(903, 716, '9B01192-2', 1231, 13),
(904, 716, '9B01193-1', 1285, 14),
(905, 716, '9B01194-1', 1133, 15),
(906, 716, '9B01195-2', 1287, 16),
(907, 722, '4B00032-4', 2944, 1),
(908, 722, '4B00035-2', 2952, 2),
(909, 722, '4B00058-1', 3191, 3),
(910, 722, '4B00048-3', 2859, 4),
(911, 722, '4B00048-4', 2870, 5),
(912, 722, '4B00034-2', 2947, 6),
(913, 723, '4A00040-6', 2862, 1),
(914, 723, '4A00043-4', 2938, 2),
(915, 723, '4B00026-5', 2944, 3),
(916, 723, '4B00026-6', 2762, 4),
(917, 723, '4B00027-4', 2954, 5),
(918, 723, '4B00029-3', 2951, 6),
(919, 723, '4B00037-1', 2937, 7),
(920, 731, '11B00029-2', 992, 1),
(921, 731, '11B00030-1', 987, 2),
(922, 731, '11B00030-2', 979, 3),
(923, 731, '11B00031-1', 855, 4),
(924, 731, '11B00032-1', 1107, 5),
(925, 731, '11B00032-2', 943, 6),
(926, 731, '11B00033-1', 1058, 7),
(927, 731, '11B00035-2', 1152, 8),
(928, 731, '11B00036-1', 1104, 9),
(929, 731, '11B00036-2', 949, 10),
(930, 731, '11B00037-2', 949, 11),
(931, 731, '11B00038-2', 940, 12),
(932, 731, '11B00039-1', 945, 13),
(933, 731, '11B00043-2', 977, 14),
(934, 731, '11B00046-1', 1102, 15),
(935, 733, '11B00045-1', 928, 1),
(936, 733, '11B00045-2', 1125, 2),
(937, 733, '11B00046-2', 954, 3),
(938, 733, '11B00051-1', 1010, 4),
(939, 733, '11B00051-2', 1022, 5),
(940, 735, '11B00031-2', 1202, 1),
(941, 735, '11B00033-2', 993, 2),
(942, 735, '11B00034-1', 1132, 3),
(943, 735, '11B00034-2', 920, 4),
(944, 735, '11B00035-1', 894, 5),
(945, 735, '11B00037-1', 1103, 6),
(946, 735, '11B00038-1', 1050, 7),
(947, 735, '11B00041-2', 1168, 8),
(948, 735, '11B00042-1', 1067, 9),
(949, 735, '11B00044-1', 1024, 10),
(950, 736, '3B00231-4', 2813, 1),
(951, 736, '3B00232-5', 2914, 2),
(952, 736, '3B00234-1', 2891, 3),
(953, 736, '3B00234-2', 2891, 4),
(954, 736, '3B00234-3', 2891, 5),
(955, 736, '3B00234-6', 3065, 6),
(956, 736, '3B00235-1', 2916, 7),
(957, 737, '3A00078-1', 2377, 1),
(958, 737, '3A00081-1', 2387, 2),
(959, 737, '3B00216-1', 2936, 3),
(960, 737, '3B00232-4', 2912, 4),
(961, 737, '3B00234-4', 2891, 5),
(962, 737, '3B00235-4', 2913, 6),
(963, 737, '3B00237-1', 2904, 7),
(964, 738, '4A00054-1', 2937, 1),
(965, 738, '4A00092-6', 2892, 2),
(966, 738, '4A00139-6', 2708, 3),
(967, 738, '4A00140-3', 2723, 4),
(968, 738, '4B00067-6', 2697, 5),
(969, 738, '4B00068-4', 2946, 6),
(970, 738, '4B00070-3', 2727, 7),
(971, 739, '4A00061-2', 2912, 1),
(972, 739, '4B00070-6', 2729, 2),
(973, 739, '4B00073-4', 2864, 3),
(974, 739, '4B00085-4', 2951, 4),
(975, 739, '4B00086-3', 2958, 5),
(976, 739, '4B00086-4', 2957, 6),
(977, 739, '4B00086-5', 2960, 7),
(978, 740, '4A00143-2', 3108, 1),
(979, 740, '4A00143-3', 3082, 2),
(980, 740, '4A00061-3', 2910, 3),
(981, 740, '4A00108-4', 2921, 4),
(982, 740, '4A00139-5', 2711, 5),
(983, 740, '4A00140-2', 2721, 6),
(984, 740, '4B00096-4', 2926, 7),
(985, 741, '4A00070-1', 2916, 1),
(986, 741, '4A00070-6', 2877, 2),
(987, 741, '4A00106-2', 2915, 3),
(988, 741, '4A00106-3', 2912, 4),
(989, 741, '4A00107-2', 2921, 5),
(990, 741, '4A00109-3', 2924, 6),
(991, 741, '4A00138-6', 2661, 7),
(992, 742, '3B00218-2', 2930, 1),
(993, 742, '3B00220-3', 2928, 2),
(994, 742, '3B00247-3', 2923, 3),
(995, 742, '3B00247-4', 2922, 4),
(996, 742, '3B00247-6', 3091, 5),
(997, 742, '3B00246-4', 2925, 6),
(998, 742, '3B00248-6', 3081, 7),
(999, 742, '3B00249-4', 2882, 8),
(1000, 742, '3B00251-2', 2900, 9),
(1001, 742, '3B00251-4', 3078, 10),
(1002, 743, '3B00251-3', 3065, 1),
(1003, 744, '11B00039-2', 957, 1),
(1004, 744, '11B00040-1', 1010, 2),
(1005, 744, '11B00040-2', 995, 3),
(1006, 744, '11B00041-1', 882, 4),
(1007, 744, '11B00042-2', 985, 5),
(1008, 744, '11B00043-1', 1077, 6),
(1009, 744, '11B00044-2', 1045, 7),
(1010, 744, '11B00047-1', 974, 8),
(1011, 744, '11B00047-2', 995, 9),
(1012, 744, '11B00048-1', 1042, 10),
(1013, 744, '11B00048-2', 962, 11),
(1014, 744, '11B00049-1', 1011, 12),
(1015, 744, '11B00049-2', 965, 13),
(1016, 744, '11B00050-1', 962, 14),
(1017, 744, '11B00050-2', 1072, 15),
(1018, 745, '9B00454-2', 1365, 1),
(1019, 745, '9B00455-2', 1324, 2),
(1020, 745, '9B00457-2', 1321, 3),
(1021, 745, '9B00459-2', 1328, 4),
(1022, 745, '9B00461-2', 1314, 5),
(1023, 745, '9B00553-2', 1329, 6),
(1024, 745, '9B00554-2', 1261, 7),
(1025, 745, '9B00557-1', 1335, 8),
(1026, 745, '9B00558-1', 1045, 9),
(1027, 745, '9B00558-2', 1336, 10),
(1028, 745, '9B00559-2', 1294, 11),
(1029, 745, '9D00213-1', 870, 12),
(1030, 745, '9D00213-2', 770, 13),
(1031, 745, '9D00214-2', 1380, 14),
(1032, 745, '9B00556-1', 1310, 15),
(1033, 745, '9B00560-2', 1307, 16),
(1034, 746, '9B00437-2', 1359, 1),
(1035, 746, '9B00441-2', 1298, 2),
(1036, 746, '9B00442-2', 1262, 3),
(1037, 746, '9B00444-2', 1301, 4),
(1038, 746, '9B00493-2', 1311, 5),
(1039, 746, '9B00494-2', 1314, 6),
(1040, 746, '9B00504-1', 1312, 7),
(1041, 746, '9B00522-1', 1232, 8),
(1042, 746, '9B00523-2', 1324, 9),
(1043, 746, '9B00532-2', 1317, 10),
(1044, 746, '9B00543-2', 1283, 11),
(1045, 746, '9B00544-2', 1328, 12),
(1046, 746, '9D00203-2', 1171, 13),
(1047, 746, '9D00204-1', 1169, 14),
(1048, 746, '9D00204-2', 1181, 15),
(1049, 746, '9B00443-2', 1300, 16),
(1050, 747, '4A00046-4', 2646, 1),
(1051, 747, '4A00046-7', 2699, 2),
(1052, 747, '4B00034-4', 2950, 3),
(1053, 747, '4B00036-4', 2962, 4),
(1054, 747, '4B00037-2', 2933, 5),
(1055, 747, '4B00043-1', 2696, 6),
(1056, 747, '4B00044-5', 2852, 7),
(1057, 748, '4B00036-3', 2963, 1),
(1058, 748, '4B00036-5', 2962, 2),
(1059, 748, '4B00044-2', 2902, 3),
(1060, 748, '4B00046-4', 2867, 4),
(1061, 748, '4B00046-6', 2729, 5),
(1062, 748, '4B00047-4', 2871, 6),
(1063, 748, '4B00047-5', 2871, 7),
(1064, 752, '7A00561-2', 1159, 1),
(1065, 752, '7A00562-1', 1146, 2),
(1066, 752, '7A00563-1', 1129, 3),
(1067, 752, '7A00563-2', 1158, 4),
(1068, 753, '1A00010-3', 3285, 1),
(1069, 753, '1A00011-6', 1631, 2),
(1070, 756, '3B00092-3', 2877, 1),
(1071, 756, '3B00094-3', 2866, 2),
(1072, 756, '3B00097-2', 2888, 3),
(1073, 756, '3B00099-5', 2418, 4),
(1074, 756, '3B00120-3', 2873, 5),
(1075, 756, '3B00132-1', 2865, 6),
(1076, 756, '3B00134-4', 2867, 7),
(1077, 756, '3B00134-5', 2869, 8),
(1078, 756, '3B00137-2', 2321, 9),
(1079, 757, '3B00135-5', 2872, 1),
(1080, 757, '3B00136-5', 2871, 2),
(1081, 757, '3B00136-6', 3050, 3),
(1082, 757, '3B00137-1', 2532, 4),
(1083, 757, '3B00138-5', 2871, 5),
(1084, 757, '3B00138-6', 2402, 6),
(1085, 757, '3B00139-3', 2868, 7),
(1086, 757, '3B00140-1', 2561, 8),
(1087, 757, '3B00140-6', 2633, 9),
(1088, 758, '3B00096-3', 2865, 1),
(1089, 758, '3B00117-1', 2887, 2),
(1090, 758, '3B00132-5', 2863, 3),
(1091, 758, '3B00133-5', 2867, 4),
(1092, 758, '3B00135-1', 2873, 5),
(1093, 758, '3B00136-3', 2872, 6),
(1094, 758, '3B00137-6', 2850, 7),
(1095, 758, '3B00138-1', 2872, 8),
(1096, 758, '3B00138-2', 2870, 9),
(1097, 762, '7A00823-2', 1162, 1),
(1098, 762, '7A00828-2', 1228, 2),
(1099, 762, '7A00837-1', 1143, 3),
(1100, 762, '7A00837-2', 1178, 4),
(1101, 762, '7A00838-2', 1170, 5),
(1102, 762, '7A00839-2', 898, 6),
(1103, 762, '7A00840-2', 1174, 7),
(1104, 762, '7A00841-1', 1113, 8),
(1105, 762, '7A00851-2', 1131, 9),
(1106, 762, '7A00852-2', 1102, 10),
(1107, 762, '7A00853-1', 1143, 11),
(1108, 762, '7A00853-2', 1130, 12),
(1109, 762, '7A00854-1', 1156, 13),
(1110, 762, '7A00858-2', 1133, 14),
(1111, 762, '7A00869-1', 1151, 15),
(1112, 762, '7A00869-2', 1145, 16),
(1113, 762, '7A00870-1', 1117, 17),
(1114, 762, '7A00876-2', 1186, 18),
(1115, 762, '7A00877-1', 1138, 19),
(1116, 762, '7A00877-2', 1143, 20),
(1117, 762, '7A00879-2', 1197, 21),
(1118, 762, '7A00881-1', 1137, 22),
(1119, 763, '7A00841-2', 1161, 1),
(1120, 763, '7A00842-1', 1112, 2),
(1121, 763, '7A00843-1', 1139, 3),
(1122, 763, '7A00843-2', 1196, 4),
(1123, 763, '7A00844-1', 1133, 5),
(1124, 763, '7A00844-2', 1218, 6),
(1125, 763, '7A00846-1', 1156, 7),
(1126, 763, '7A00848-2', 1096, 8),
(1127, 763, '7A00849-1', 1126, 9),
(1128, 763, '7A00849-2', 1163, 10),
(1129, 763, '7A00851-1', 1154, 11),
(1130, 763, '7A00852-1', 1166, 12),
(1131, 763, '7A00871-1', 1208, 13),
(1132, 763, '7A00871-2', 1120, 14),
(1133, 763, '7A00878-1', 1142, 15),
(1134, 763, '7A00878-2', 1182, 16),
(1135, 763, '7A00879-1', 1118, 17),
(1136, 763, '7A00881-2', 1147, 18),
(1137, 763, '7A00882-1', 1166, 19),
(1138, 763, '7A00884-2', 1153, 20),
(1139, 763, '7A00885-2', 1150, 21),
(1140, 763, '7A00889-2', 1143, 22),
(1141, 764, '7A00643-1', 1121, 1),
(1142, 764, '7A00643-2', 912, 2),
(1143, 764, '7A00644-1', 1139, 3),
(1144, 764, '7A00644-2', 1139, 4),
(1145, 764, '7A00645-1', 1147, 5),
(1146, 764, '7A00646-1', 1148, 6),
(1147, 764, '7A00646-2', 1206, 7),
(1148, 764, '7A00647-1', 1121, 8),
(1149, 764, '7A00647-2', 1154, 9),
(1150, 764, '7A00648-2', 1137, 10),
(1151, 764, '7A00649-1', 1123, 11),
(1152, 764, '7A00649-2', 896, 12),
(1153, 764, '7A00650-1', 1109, 13),
(1154, 764, '7A00651-1', 1118, 14),
(1155, 764, '7A00651-2', 1161, 15),
(1156, 764, '7A00653-1', 1119, 16),
(1157, 764, '7A00653-2', 1145, 17),
(1158, 764, '7A00654-1', 1152, 18),
(1159, 764, '7A00654-2', 1128, 19),
(1160, 764, '7A00656-1', 1151, 20),
(1161, 764, '7A00656-2', 1093, 21),
(1162, 764, '7A00657-1', 1150, 22),
(1163, 765, '7A00637-2', 1030, 1),
(1164, 765, '7A00640-1', 1083, 2),
(1165, 765, '7A00658-1', 1153, 3),
(1166, 765, '7A00658-2', 1115, 4),
(1167, 765, '7A00659-1', 1180, 5),
(1168, 765, '7A00660-1', 1114, 6),
(1169, 765, '7A00661-2', 1144, 7),
(1170, 765, '7A00663-1', 1005, 8),
(1171, 765, '7A00663-2', 885, 9),
(1172, 765, '7A00665-2', 1149, 10),
(1173, 765, '7A00667-1', 1129, 11),
(1174, 765, '7A00667-2', 1152, 12),
(1175, 765, '7A00668-1', 1122, 13),
(1176, 765, '7A00668-2', 1166, 14),
(1177, 765, '7A00669-1', 1145, 15),
(1178, 765, '7A00669-2', 1125, 16),
(1179, 765, '7A00670-1', 1160, 17),
(1180, 765, '7A00670-2', 1121, 18),
(1181, 765, '7A00671-1', 1138, 19),
(1182, 765, '7A00671-2', 1095, 20),
(1183, 765, '7A00657-2', 1131, 21),
(1184, 765, '7A00645-2', 1136, 22),
(1185, 765, '7A00665-1', 1118, 23),
(1186, 766, '4A00046-2', 2722, 1),
(1187, 766, '4A00046-5', 2510, 2),
(1188, 767, '1A00012-1', 2385, 1),
(1189, 767, '1A00012-3', 2382, 2),
(1190, 767, '1A00012-5', 2514, 3),
(1191, 767, '1A00012-7', 2637, 4),
(1192, 767, '1A00016-2', 2896, 5),
(1193, 767, '1A00016-3', 2898, 6),
(1194, 767, '1A00024-1', 2171, 7),
(1195, 767, '1A00024-2', 2093, 8),
(1196, 768, '590059-2', 612, 1),
(1197, 768, '590093-2', 903, 2),
(1198, 768, '590095-2', 815, 3),
(1199, 768, '590097-1', 1304, 4),
(1200, 768, '590098-1', 667, 5),
(1201, 768, '590122-1', 656, 6),
(1202, 769, '1B00011-6', 2825, 1),
(1203, 769, '1B00012-4', 2802, 2),
(1204, 769, '1B00012-7', 2416, 3),
(1205, 769, '1B00015-6', 2650, 4),
(1206, 770, '3A00006-1', 3240, 1),
(1207, 770, '3B00097-6', 3067, 2),
(1208, 770, '3B00108-2', 3260, 3),
(1209, 771, '4A00050-6', 2883, 1),
(1210, 771, '4A00051-4', 2504, 2),
(1211, 771, '4A00052-5', 2917, 3),
(1212, 771, '4A00053-1', 2925, 4),
(1213, 771, '4A00053-2', 2921, 5),
(1214, 771, '4A00089-6', 2803, 6),
(1215, 771, '4A00091-5', 2909, 7),
(1216, 772, '4A00092-3', 2924, 1),
(1217, 772, '4A00092-4', 2922, 2),
(1218, 772, '4A00055-1', 2950, 3),
(1219, 772, '4A00068-3', 2908, 4),
(1220, 772, '4A00068-4', 2913, 5),
(1221, 772, '4A00083-2', 2916, 6),
(1222, 772, '4A00085-1', 2900, 7),
(1223, 773, '4A00085-2', 2900, 1),
(1224, 773, '4A00085-3', 2900, 2),
(1225, 773, '4A00085-6', 2905, 3),
(1226, 773, '4A00087-6', 2905, 4),
(1227, 773, '5A00055-3', 2948, 5),
(1228, 773, '4A00073-5', 2910, 6),
(1229, 773, '4A00077-1', 2703, 7),
(1230, 774, '4A00077-5', 2910, 1),
(1231, 774, '4A-00078-3', 2905, 2),
(1232, 774, '4A00078-5', 3114, 3),
(1233, 774, '4A00079-1', 2911, 4),
(1234, 774, '4A00079-6', 2877, 5),
(1235, 774, '4A00081-6', 2834, 6),
(1236, 774, '4A00053-4', 2920, 7),
(1237, 776, '4A00087-2', 2893, 1),
(1238, 776, '4A00095-4', 2720, 2),
(1239, 776, '4B00049-5', 2939, 3),
(1240, 776, '4B00051-6', 2820, 4),
(1241, 776, '4B00050-1', 2135, 5),
(1242, 776, '4B00052-2', 2928, 6),
(1243, 776, '4B00053-5', 2920, 7),
(1244, 776, '4B00053-6', 2700, 8),
(1245, 776, '4B00054-6', 2766, 9),
(1246, 777, '3A00015-6', 3139, 1),
(1247, 777, '3A00021-1', 2904, 2),
(1248, 777, '3B00163-4', 2922, 3),
(1249, 778, '4B00062-2', 2928, 1),
(1250, 778, '4B00065-4', 2720, 2),
(1251, 778, '4B00066-1', 2926, 3),
(1252, 778, '4B00066-3', 2925, 4),
(1253, 784, '7A00688-1', 1143, 1),
(1254, 784, '7A00678-2', 1152, 2),
(1255, 784, '7A00683-2', 1120, 3),
(1256, 784, '7A00686-2', 1020, 4),
(1257, 784, '7A00687-1', 1144, 5),
(1258, 784, '7A00691-1', 1152, 6),
(1259, 784, '7A00692-1', 1131, 7),
(1260, 784, '7A00692-2', 1126, 8),
(1261, 784, '7A00694-1', 1163, 9),
(1262, 784, '7A00695-1', 1134, 10),
(1263, 784, '7A00695-2', 1154, 11),
(1264, 784, '7A00696-1', 1140, 12),
(1265, 784, '7A00696-2', 1135, 13),
(1266, 784, '7A00699-1', 1168, 14),
(1267, 784, '7A00699-2', 1098, 15),
(1268, 784, '7A00700-1', 1150, 16),
(1269, 784, '7A00702-2', 1096, 17),
(1270, 784, '7A00706-2', 1110, 18),
(1271, 784, '7A00707-2', 1128, 19),
(1272, 784, '7A00708-2', 1102, 20),
(1273, 784, '7A00709-1', 1123, 21),
(1274, 784, '7A00711-1', 1120, 22),
(1275, 784, '7A00713-2', 1108, 23),
(1276, 785, '7A00714-1', 1117, 1),
(1277, 785, '7A00716-1', 1039, 2),
(1278, 785, '7A00716-2', 989, 3),
(1279, 785, '7A00717-2', 1157, 4),
(1280, 785, '7A00718-2', 1202, 5),
(1281, 785, '7A00719-2', 1109, 6),
(1282, 785, '7A00720-1', 1202, 7),
(1283, 785, '7A00720-2', 823, 8),
(1284, 785, '7A00721-1', 1114, 9),
(1285, 785, '7A00722-2', 1225, 10),
(1286, 785, '7A00723-2', 1158, 11),
(1287, 785, '7A00724-1', 1128, 12),
(1288, 785, '7A00724-2', 893, 13),
(1289, 785, '7A00725-1', 1211, 14),
(1290, 785, '7A00725-2', 1073, 15),
(1291, 785, '7A00726-1', 1136, 16),
(1292, 785, '7A00726-2', 1132, 17),
(1293, 785, '7A00727-1', 1198, 18),
(1294, 785, '7A00727-2', 1148, 19),
(1295, 785, '7A00728-1', 1119, 20),
(1296, 785, '7A00728-2', 1155, 21),
(1297, 785, '7A00729-1', 1120, 22),
(1298, 788, '7A00738-2', 1037, 1),
(1299, 788, '7A00793-1', 670, 2),
(1300, 788, '7A00885-1', 1128, 3),
(1301, 788, '7A00886-1', 1145, 4),
(1302, 788, '7A00890-1', 1132, 5),
(1303, 788, '7A00892-1', 1161, 6),
(1304, 788, '7A00892-2', 1099, 7),
(1305, 788, '7A00893-1', 1124, 8),
(1306, 788, '7A00893-2', 1147, 9),
(1307, 788, '7A00894-1', 1154, 10),
(1308, 788, '7A00896-1', 1023, 11),
(1309, 788, '7A00896-2', 1011, 12),
(1310, 788, '7A00898-1', 1159, 13),
(1311, 788, '7A00898-2', 868, 14),
(1312, 788, '7A00900-2', 911, 15),
(1313, 788, '7A00903-2', 1137, 16),
(1314, 788, '7A00905-2', 1144, 17),
(1315, 788, '7A00910-1', 1116, 18),
(1316, 788, '7A00910-2', 1168, 19),
(1317, 788, '7A00913-1', 1187, 20),
(1318, 788, '7A00913-2', 1096, 21),
(1319, 788, '7A00915-1', 1141, 22),
(1320, 788, '7A00917-2', 1170, 23),
(1321, 789, '7A00761-2', 1137, 1),
(1322, 789, '7A00765-1', 1123, 2),
(1323, 789, '7A00765-2', 1144, 3),
(1324, 789, '7A00895-1', 1118, 4),
(1325, 789, '7A00895-2', 1166, 5),
(1326, 789, '7A00899-1', 1160, 6),
(1327, 789, '7A00904-2', 1130, 7),
(1328, 789, '7A00905-1', 1131, 8),
(1329, 789, '7A00907-1', 1123, 9),
(1330, 789, '7A00907-2', 1151, 10),
(1331, 789, '7A00908-2', 1165, 11),
(1332, 789, '7A00914-1', 1154, 12),
(1333, 789, '7A00915-2', 1133, 13),
(1334, 789, '7A00918-2', 1027, 14),
(1335, 789, '7A00919-1', 1131, 15),
(1336, 789, '7A00919-2', 1146, 16),
(1337, 789, '7A00920-1', 1131, 17),
(1338, 789, '7A00920-2', 1149, 18),
(1339, 789, '7A00921-1', 1080, 19),
(1340, 789, '7A00921-2', 958, 20),
(1341, 789, '7A00922-1', 1133, 21),
(1342, 789, '7A00922-2', 1128, 22),
(1343, 793, '7A00847-2', 1125, 1),
(1344, 793, '7A00848-1', 1183, 2),
(1345, 793, '7A00850-1', 1154, 3),
(1346, 793, '7A00855-2', 1183, 4),
(1347, 793, '7A00859-2', 1136, 5),
(1348, 793, '7A00862-1', 1175, 6),
(1349, 793, '7A00864-1', 1178, 7),
(1350, 793, '7A00865-2', 1218, 8),
(1351, 793, '7A00870-2', 1171, 9),
(1352, 793, '7A00873-2', 1123, 10),
(1353, 793, '7A00875-2', 1165, 11),
(1354, 793, '7A00887-1', 1160, 12),
(1355, 793, '7A00887-2', 1120, 13),
(1356, 793, '7A00888-1', 1138, 14),
(1357, 793, '7A00903-1', 1135, 15),
(1358, 793, '7A00906-1', 1129, 16),
(1359, 793, '7A00909-1', 1113, 17),
(1360, 793, '7A00926-1', 1121, 18),
(1361, 793, '7A00932-1', 1115, 19),
(1362, 793, '7A00932-2', 1115, 20),
(1363, 793, '7A00864-2', 1132, 21),
(1364, 793, '', 0, 22),
(1365, 794, '9B01101-1', 1296, 1),
(1366, 794, '9B01105-1', 1268, 2),
(1367, 794, '9B01106-1', 1300, 3),
(1368, 794, '9B01143-1', 1261, 4),
(1369, 794, '9B01213-1', 1269, 5),
(1370, 794, '9B01214-1', 1176, 6),
(1371, 794, '9B01216-1', 1265, 7),
(1372, 794, '9B01218-1', 1279, 8),
(1373, 794, '9B01227-1', 1101, 9),
(1374, 794, '9B01228-1', 1268, 10),
(1375, 795, '9B01229-1', 1254, 1),
(1376, 795, '9B01236-1', 1252, 2),
(1377, 795, '9B01237-1', 1215, 3),
(1378, 795, '9B01238-1', 1250, 4),
(1379, 795, '9B01239-1', 1253, 5),
(1380, 795, '9B01240-1', 1110, 6),
(1381, 795, '9B01240-2', 1123, 7),
(1382, 795, '9B01241-1', 1326, 8),
(1383, 795, '9B01242-1', 1300, 9),
(1384, 795, '9B01244-1', 1217, 10),
(1385, 796, '4A00145-5', 2920, 1),
(1386, 796, '4A00145-6', 2898, 2),
(1387, 796, '4A00146-6', 2880, 3),
(1388, 796, '4A00147-2', 2941, 4),
(1389, 796, '4A00147-3', 2944, 5),
(1390, 796, '4A00147-6', 2896, 6),
(1391, 796, '4A00148-2', 2505, 7),
(1392, 796, '4A00149-2', 2721, 8),
(1393, 796, '4A00149-1', 2928, 9),
(1394, 797, '3A00075-3', 2754, 1),
(1395, 797, '3A00075-4', 2755, 2),
(1396, 797, '3A00075-5', 2750, 3),
(1397, 797, '3A00076-7', 2709, 4),
(1398, 797, '3B00213-3', 2923, 5),
(1399, 797, '3B00214-1', 2925, 6),
(1400, 797, '3B00215-3', 2931, 7),
(1401, 798, '3A00077-5', 2890, 1),
(1402, 798, '3B00205-2', 2897, 2),
(1403, 798, '3B00213-6', 3080, 3),
(1404, 798, '3B00214-2', 2927, 4),
(1405, 798, '3B00231-6', 2380, 5),
(1406, 798, '3B00233-3', 3101, 6),
(1407, 798, '3B00233-4', 3097, 7),
(1408, 799, '4A00148-1', 2503, 1),
(1409, 799, '4A00145-4', 2916, 2),
(1410, 799, '4A00147-4', 2944, 3),
(1411, 799, '4A00147-5', 2942, 4),
(1412, 799, '4A00155-3', 2936, 5),
(1413, 799, '4A00155-6', 2889, 6),
(1414, 799, '4A00158-2', 2913, 7),
(1415, 799, '4A00166-6', 2720, 8),
(1416, 799, '4A00167-6', 2885, 9),
(1417, 800, '4A00142-6', 3083, 1),
(1418, 800, '4A00146-5', 2939, 2),
(1419, 800, '4A00149-6', 3144, 3),
(1420, 800, '4A00150-6', 2884, 4),
(1421, 800, '4A00158-3', 2916, 5),
(1422, 800, '4A00158-6', 2884, 6),
(1423, 800, '4A00159-5', 2926, 7),
(1424, 800, '4A00143-4', 2696, 8),
(1425, 800, '4A00168-3', 2812, 9),
(1426, 800, '4A00144-3', 3322, 10),
(1427, 801, '4A00168-2', 2921, 1),
(1428, 801, '4A00169-6', 2706, 2),
(1429, 802, '3B00226-6', 3070, 1),
(1430, 803, '7A00994-2', 1299, 1),
(1431, 803, '7A00995-1', 974, 2),
(1432, 803, '7A00995-2', 1361, 3),
(1433, 803, '7A00996-2', 1293, 4),
(1434, 803, '7A01034-1', 998, 5),
(1435, 803, '7A01035-1', 994, 6),
(1436, 803, '7A01136-1', 977, 7),
(1437, 803, '7A01137-1', 1007, 8),
(1438, 803, '7A01141-1', 976, 9),
(1439, 803, '7A01142-1', 994, 10),
(1440, 803, '7A01143-1', 993, 11),
(1441, 803, '7A01144-1', 979, 12),
(1442, 803, '7A01157-2', 1103, 13),
(1443, 803, '7A01197-1', 1004, 14),
(1444, 803, '7A01247-1', 1056, 15),
(1445, 803, '7A01247-2', 1128, 16),
(1446, 803, '7A01248-1', 1192, 17),
(1447, 803, '7A01249-1', 1101, 18),
(1448, 803, '7A01249-2', 1224, 19),
(1449, 803, '7A01250-1', 1123, 20),
(1450, 803, '7A01250-2', 1209, 21),
(1451, 803, '7A01251-1', 1032, 22),
(1452, 803, '7A01251-2', 1076, 23),
(1453, 808, '3B00250-5', 2719, 1),
(1454, 808, '3B00253-4', 2923, 2),
(1455, 808, '3B00254-3', 2911, 3),
(1456, 808, '3B00255-1', 2890, 4),
(1457, 808, '3B00255-2', 2890, 5),
(1458, 808, '3B00265-5', 2893, 6),
(1459, 808, '3B00257-7', 2629, 7),
(1460, 809, '3A00076-2', 2161, 1),
(1461, 809, '3B00204-6', 3061, 2),
(1462, 809, '3B00210-6', 3079, 3),
(1463, 809, '3B00224-6', 3066, 4),
(1464, 809, '3B00225-5', 2919, 5),
(1465, 809, '3B00226-1', 2928, 6),
(1466, 809, '3B00226-4', 2927, 7),
(1467, 810, '7A00999-2', 1193, 1),
(1468, 810, '7A01002-2', 1296, 2),
(1469, 810, '7A01003-2', 1360, 3),
(1470, 810, '7A01063-2', 1284, 4),
(1471, 810, '7A01064-1', 1000, 5),
(1472, 810, '7A01064-2', 1278, 6),
(1473, 810, '7A01106-2', 1288, 7),
(1474, 810, '7A01127-2', 1298, 8),
(1475, 810, '7A01128-2', 1281, 9),
(1476, 810, '7A01146-1', 1052, 10),
(1477, 810, '7A01152-1', 1276, 11),
(1478, 810, '7A01160-2', 1318, 12),
(1479, 810, '7A01161-1', 1280, 13),
(1480, 810, '7A01173-2', 1356, 14),
(1481, 810, '7A01206-1', 1005, 15),
(1482, 810, '7A01212-2', 1313, 16),
(1483, 810, '7A01214-2', 1304, 17),
(1484, 810, '7A01215-2', 1279, 18),
(1485, 810, '7A01221-1', 1254, 19),
(1486, 810, '7A00439-1', 1185, 20),
(1487, 811, '7A01026-2', 1284, 1),
(1488, 811, '7A01036-2', 1300, 2),
(1489, 811, '7A01037-1', 1303, 3),
(1490, 811, '7A01042-2', 1286, 4),
(1491, 811, '7A01049-2', 1278, 5),
(1492, 811, '7A01059-2', 1254, 6),
(1493, 811, '7A01067-1', 1000, 7),
(1494, 811, '7A01067-2', 1251, 8),
(1495, 811, '7A01098-2', 1244, 9),
(1496, 811, '7A01112-2', 1292, 10),
(1497, 811, '7A01113-2', 1285, 11),
(1498, 811, '7A01115-1', 1273, 12),
(1499, 811, '7A01121-2', 754, 13),
(1500, 811, '7A01122-2', 1309, 14),
(1501, 811, '7A01137-2', 1321, 15),
(1502, 811, '7A01138-1', 1021, 16),
(1503, 811, '7A01138-2', 1299, 17),
(1504, 811, '7A01219-1', 1023, 18),
(1505, 811, '7A01238-2', 1263, 19),
(1506, 811, '7A01244-1', 1003, 20),
(1507, 811, '7A01244-2', 1310, 21),
(1508, 812, '7A00258-1', 1004, 1),
(1509, 812, '7A00610-1', 1123, 2),
(1510, 812, '7A00731-1', 1155, 3),
(1511, 812, '7A00731-1', 1118, 4),
(1512, 812, '7A01023-2', 1280, 5),
(1513, 812, '7A01024-2', 1338, 6),
(1514, 812, '7A01044-2', 1284, 7),
(1515, 812, '7A01052-1', 1001, 8),
(1516, 812, '7A01053-1', 1002, 9),
(1517, 812, '7A01060-2', 1242, 10),
(1518, 812, '7A01061-2', 1280, 11),
(1519, 812, '7A01062-1', 1009, 12),
(1520, 812, '7A01062-2', 1264, 13),
(1521, 812, '7A01069-2', 1318, 14),
(1522, 812, '7A01070-1', 1276, 15),
(1523, 812, '7A01072-2', 1281, 16),
(1524, 812, '7A01073-1', 1024, 17),
(1525, 812, '7A01073-2', 1258, 18),
(1526, 812, '7A01111-1', 811, 19),
(1527, 812, '7A01166-2', 1214, 20),
(1528, 812, '7A01200-2', 1278, 21),
(1529, 812, '7A01225-1', 671, 22),
(1530, 815, '11B00116-1', 995, 1),
(1531, 815, '11B00136-1', 942, 2),
(1532, 815, '11B00111-2', 1012, 3),
(1533, 815, '11B00118-2', 1013, 4),
(1534, 815, '11B00149-1', 1018, 5),
(1535, 816, '3B00371-1', 2552, 1),
(1536, 816, '3B00375-5', 2729, 2),
(1537, 817, '11B00111-1', 1038, 1),
(1538, 817, '11B00116-2', 1030, 2),
(1539, 817, '11B00119-2', 1000, 3),
(1540, 817, '11B00124-1', 1030, 4),
(1541, 817, '11B00124-2', 1026, 5),
(1542, 817, '11B00125-1', 1028, 6),
(1543, 817, '11B00125-2', 1027, 7),
(1544, 817, '11B00126-2', 1039, 8),
(1545, 817, '11B00128-1', 1033, 9),
(1546, 817, '11B00149-2', 1238, 10),
(1547, 817, '11B00150-2', 1070, 11),
(1548, 817, '11B00151-2', 1001, 12),
(1549, 817, '11B00152-1', 1239, 13),
(1550, 817, '11B00152-2', 1024, 14),
(1551, 820, '11B00117-1', 1029, 1),
(1552, 820, '11B00117-2', 1022, 2),
(1553, 820, '11B00129-1', 1049, 3),
(1554, 820, '11B00129-2', 1022, 4),
(1555, 820, '11B00130-2', 1040, 5),
(1556, 820, '11B00147-1', 1230, 6),
(1557, 820, '11B00147-2', 1035, 7),
(1558, 820, '11B00148-2', 1155, 8),
(1559, 820, '11B00150-1', 1125, 9),
(1560, 821, '9B01511-1', 1327, 1),
(1561, 821, '9B01514-1', 1313, 2),
(1562, 821, '9B01515-1', 1318, 3),
(1563, 821, '9B01517-1', 1314, 4),
(1564, 821, '9B01566-1', 1147, 5),
(1565, 821, '9B01567-1', 1296, 6),
(1566, 821, '9B01609-2', 1181, 7),
(1567, 821, '9B01618-2', 1152, 8),
(1568, 821, '9B01621-1', 1196, 9),
(1569, 821, '9B01623-1', 1179, 10),
(1570, 821, '9B01624-1', 1169, 11),
(1571, 821, '9B01625-1', 1187, 12),
(1572, 821, '9B01627-2', 1175, 13),
(1573, 821, '9B01632-1', 1136, 14),
(1574, 821, '9B01633-1', 1250, 15),
(1575, 821, '9B01634-1', 1187, 16),
(1576, 821, '9B01653-1', 1171, 17),
(1577, 821, '9B01658-1', 1154, 18),
(1578, 821, '9B01660-2', 1210, 19),
(1579, 821, '9B01668-2', 1321, 20),
(1580, 821, '9B01679-1', 1312, 21),
(1581, 822, '3B00288-1', 2928, 1),
(1582, 822, '3B00288-2', 2928, 2),
(1583, 822, '3B00288-3', 2927, 3),
(1584, 822, '3B00291-3', 2936, 4),
(1585, 822, '3B00291-4', 2936, 5),
(1586, 822, '3B00291-5', 2936, 6),
(1587, 822, '3B00292-3', 2897, 7),
(1588, 823, '9B01365-1', 1248, 1),
(1589, 823, '9B01366-2', 1125, 2),
(1590, 823, '9B01379-2', 1141, 3),
(1591, 823, '9B01628-1', 1133, 4),
(1592, 823, '9D00478-1', 1120, 5),
(1593, 823, '9D00481-2', 1161, 6),
(1594, 823, '9D00493-1', 1112, 7);
INSERT INTO `fstokd_koli` (`SDKID`, `SDKIDD`, `SDKNOKOLI`, `SDKQTY`, `SDKURUTAN`) VALUES
(1595, 824, '11B00101-1', 903, 1),
(1596, 824, '11B00142-2', 1141, 2),
(1597, 824, '11B00144-1', 1187, 3),
(1598, 824, '11B00160-1', 958, 4),
(1599, 824, '11B00165-1', 870, 5),
(1600, 825, '7A00996-1', 976, 1),
(1601, 825, '7A01000-1', 971, 2),
(1602, 825, '7A01003-1', 991, 3),
(1603, 825, '7A01080-1', 979, 4),
(1604, 825, '7A01101-2', 739, 5),
(1605, 825, '7A01106-1', 987, 6),
(1606, 825, '7A01140-1', 995, 7),
(1607, 825, '7A01145-1', 989, 8),
(1608, 825, '7A01146-2', 1230, 9),
(1609, 825, '7A01147-1', 1302, 10),
(1610, 825, '7A01147-2', 983, 11),
(1611, 825, '7A01155-1', 999, 12),
(1612, 825, '7A01161-2', 818, 13),
(1613, 825, '7A01188-1', 997, 14),
(1614, 825, '7A01205-1', 1023, 15),
(1615, 825, '7A01207-2', 1301, 16),
(1616, 825, '7A01208-1', 1042, 17),
(1617, 825, '7A01210-1', 979, 18),
(1618, 825, '7A01210-2', 840, 19),
(1619, 825, '7A01212-1', 997, 20),
(1620, 825, '7A01216-2', 1316, 21),
(1621, 825, '7A01222-3', 961, 22),
(1622, 825, '7A01223-1', 938, 23),
(1623, 826, '11B00055-1', 1059, 1),
(1624, 826, '11B00062-1', 910, 2),
(1625, 826, '11B00063-1', 1122, 3),
(1626, 826, '11B00063-2', 934, 4),
(1627, 826, '11B00067-1', 1086, 5),
(1628, 826, '11B00069-1', 1003, 6),
(1629, 826, '11B00070-1', 1054, 7),
(1630, 826, '11B00072-1', 1074, 8),
(1631, 826, '11B00073-1', 920, 9),
(1632, 826, '11B00087-1', 1024, 10),
(1633, 826, '11B00087-2', 1038, 11),
(1634, 826, '11B00088-1', 1032, 12),
(1635, 826, '11B00088-2', 1015, 13),
(1636, 826, '11B00132-2', 997, 14),
(1637, 826, '11B00140-2', 703, 15),
(1638, 827, '11B00056-1', 693, 1),
(1639, 827, '11B00068-2', 986, 2),
(1640, 827, '11B00070-2', 983, 3),
(1641, 827, '11B00071-2', 1074, 4),
(1642, 827, '11B00072-2', 986, 5),
(1643, 827, '11B00073-2', 1121, 6),
(1644, 827, '11B00089-2', 991, 7),
(1645, 827, '11B00097-1', 1039, 8),
(1646, 827, '11B00097-2', 1025, 9),
(1647, 827, '11B00098-2', 1040, 10),
(1648, 828, '7A00737-1', 1148, 1),
(1649, 828, '7A00992-1', 985, 2),
(1650, 828, '7A00992-2', 1284, 3),
(1651, 828, '7A00993-1', 1277, 4),
(1652, 828, '7A00997-1', 1343, 5),
(1653, 828, '7A01004-1', 1083, 6),
(1654, 828, '7A01104-2', 1265, 7),
(1655, 828, '7A01105-2', 1000, 8),
(1656, 828, '7A01123-2', 1251, 9),
(1657, 828, '7A01141-2', 1332, 10),
(1658, 828, '7A01149-1', 1272, 11),
(1659, 828, '7A01150-1', 1388, 12),
(1660, 828, '7A01151-1', 1292, 13),
(1661, 828, '7A01163-2', 1283, 14),
(1662, 828, '7A01164-1', 1006, 15),
(1663, 828, '7A01165-1', 1002, 16),
(1664, 828, '7A01180-1', 1057, 17),
(1665, 828, '7A01180-2', 1223, 18),
(1666, 828, '7A01181-2', 1352, 19),
(1667, 828, '7A01182-1', 1004, 20),
(1668, 828, '7A01211-1', 1004, 21),
(1669, 829, '7A00733-1', 1138, 1),
(1670, 829, '7A00733-2', 1127, 2),
(1671, 829, '7A01004-2', 1199, 3),
(1672, 829, '7A01080-2', 1301, 4),
(1673, 829, '7A01092-2', 1305, 5),
(1674, 829, '7A01102-2', 1267, 6),
(1675, 829, '7A01103-2', 1285, 7),
(1676, 829, '7A01109-2', 1259, 8),
(1677, 829, '7A01123-1', 1006, 9),
(1678, 829, '7A01140-2', 1292, 10),
(1679, 829, '7A01158-2', 1302, 11),
(1680, 829, '7A01186-2', 1330, 12),
(1681, 829, '7A01188-2', 1320, 13),
(1682, 829, '7A01201-2', 1316, 14),
(1683, 829, '7A01205-2', 1292, 15),
(1684, 829, '7A01206-2', 1321, 16),
(1685, 829, '7A01207-1', 1012, 17),
(1686, 829, '7A01209-2', 1016, 18),
(1687, 829, '7A01217-1', 1024, 19),
(1688, 829, '7A01217-2', 1282, 20),
(1689, 829, '7A01218-1', 1050, 21),
(1690, 830, '3A00084-7', 2381, 1),
(1691, 830, '3A00085-1', 2917, 2),
(1692, 831, '3B00282-1', 2384, 1),
(1693, 831, '3B00282-2', 2456, 2),
(1694, 832, '3B00272-5', 2556, 1),
(1695, 832, '3B00272-6', 2514, 2),
(1696, 834, '3A00083-1', 2893, 1),
(1697, 834, '3A00083-3', 2874, 2),
(1698, 834, '3A00084-5', 2213, 3),
(1699, 834, '3A00084-6', 2213, 4),
(1700, 834, '3B00281-6', 3080, 5),
(1701, 834, '3B00282-3', 3260, 6),
(1702, 834, '3B00283-6', 3062, 7),
(1703, 835, '3A00083-2', 2878, 1),
(1704, 835, '3A00083-4', 2874, 2),
(1705, 835, '3B00268-6', 2150, 3),
(1706, 835, '3B00281-4', 2921, 4),
(1707, 835, '3B00282-5', 3260, 5),
(1708, 835, '3B00283-4', 2910, 6),
(1709, 835, '3B00284-1', 2916, 7),
(1710, 838, '3A00099-3', 3076, 1),
(1711, 838, '3A00099-4', 3076, 2),
(1712, 839, '3A00103-2', 3061, 1),
(1713, 839, '3A00103-5', 3060, 2),
(1714, 839, '3A00104-1', 2892, 3),
(1715, 839, '3B00327-4', 2902, 4),
(1716, 840, '11B00127-1', 1030, 1),
(1717, 840, '11B00130-1', 1013, 2),
(1718, 840, '11B00134-2', 1047, 3),
(1719, 840, '11B00145-2', 1094, 4),
(1720, 840, '11B00153-2', 1071, 5),
(1721, 840, '11B00154-1', 1201, 6),
(1722, 840, '11B00154-2', 998, 7),
(1723, 840, '11B00155-2', 1194, 8),
(1724, 840, '11B00156-1', 1132, 9),
(1725, 840, '11B00156-2', 1120, 10),
(1726, 840, '11B00158-1', 1058, 11),
(1727, 840, '11B00159-2', 1041, 12),
(1728, 840, '11B00162-1', 1053, 13),
(1729, 840, '11B00163-1', 1050, 14),
(1730, 841, '3B00309-5', 2910, 1),
(1731, 841, '3B00311-3', 2925, 2),
(1732, 841, '3B00311-4', 2922, 3),
(1733, 841, '3B00311-5', 2925, 4),
(1734, 841, '3B00312-4', 2916, 5),
(1735, 841, '3B00312-5', 2915, 6),
(1736, 841, '3B00312-6', 3079, 7),
(1737, 842, '3A00088-4', 2936, 1),
(1738, 842, '3A00088-5', 2940, 2),
(1739, 842, '3A00089-3', 2890, 3),
(1740, 842, '3A00089-4', 2891, 4),
(1741, 842, '3A00090-2', 2907, 5),
(1742, 842, '3A00090-4', 2903, 6),
(1743, 842, '3A00090-6', 3076, 7),
(1744, 843, '3A00077-4', 2892, 1),
(1745, 843, '3A00092-1', 2889, 2),
(1746, 843, '3A00092-2', 2892, 3),
(1747, 843, '3A00092-4', 2889, 4),
(1748, 843, '3A00092-5', 2891, 5),
(1749, 843, '3A00092-6', 3069, 6),
(1750, 843, '3B00311-2', 2925, 7),
(1751, 844, '3B00272-1', 3260, 1),
(1752, 844, '3B00272-2', 3260, 2),
(1753, 846, '11B00114-1', 1020, 1),
(1754, 846, '11B00115-1', 980, 2),
(1755, 846, '11B00115-2', 1027, 3),
(1756, 846, '11B00118-1', 1038, 4),
(1757, 846, '11B00122-1', 974, 5),
(1758, 846, '11B00157-2', 1178, 6),
(1759, 846, '11B00168-2', 1004, 7),
(1760, 846, '11B00169-2', 1100, 8),
(1761, 846, '11B00170-1', 1010, 9),
(1762, 846, '11B00170-2', 1004, 10),
(1763, 846, '11B00171-1', 1032, 11),
(1764, 846, '11B00171-2', 1028, 12),
(1765, 846, '11B00172-1', 935, 13),
(1766, 846, '11B00177-1', 984, 14),
(1767, 846, '11B00161-2', 1059, 15),
(1768, 847, '4A00169-4', 2905, 1),
(1769, 847, '4A00177-1', 2936, 2),
(1770, 847, '4A00187-2', 2911, 3),
(1771, 847, '4A00190-3', 2921, 4),
(1772, 847, '4A00198-1', 2501, 5),
(1773, 847, '4B00098-6', 2834, 6),
(1774, 847, '4B00103-7', 2595, 7),
(1775, 848, '3B00315-6', 3063, 1),
(1776, 848, '3B00317-4', 2906, 2),
(1777, 848, '3B00319-2', 2921, 3),
(1778, 848, '3B00319-3', 2918, 4),
(1779, 848, '3B00319-4', 2921, 5),
(1780, 848, '3B00319-5', 2922, 6),
(1781, 848, '3A00095-1', 2972, 7),
(1782, 849, '3A00082-1', 2881, 1),
(1783, 849, '3A00082-3', 2881, 2),
(1784, 849, '3A00082-4', 2880, 3),
(1785, 849, '3A00082-5', 2882, 4),
(1786, 849, '3A00082-6', 2853, 5),
(1787, 849, '3B00268-4', 2732, 6),
(1788, 849, '3B00281-3', 2919, 7),
(1789, 850, '3B00282-6', 2927, 1),
(1790, 850, '3B00301-6', 3055, 2),
(1791, 850, '3B00309-4', 2912, 3),
(1792, 850, '3B00312-1', 2915, 4),
(1793, 850, '3B00315-1', 2903, 5),
(1794, 850, '3B00315-2', 2900, 6),
(1795, 850, '3B00315-3', 2903, 7),
(1796, 851, '3A00101-1', 2717, 1),
(1797, 851, '3A00101-3', 2891, 2),
(1798, 851, '3B00322-2', 2910, 3),
(1799, 851, '3B00323-6', 3078, 4),
(1800, 851, '3B00324-2', 2748, 5),
(1801, 851, '3B00326-1', 2920, 6),
(1802, 851, '3B00326-4', 2919, 7),
(1803, 852, '3A00100-4', 2917, 1),
(1804, 852, '3A00101-2', 2713, 2),
(1805, 852, '3B00320-2', 2897, 3),
(1806, 852, '3B00320-3', 2898, 4),
(1807, 852, '3B00321-3', 2906, 5),
(1808, 852, '3B00324-1', 2748, 6),
(1809, 852, '3B00325-5', 2912, 7),
(1810, 854, '3A00098-5', 2906, 1),
(1811, 854, '3A00100-2', 2913, 2),
(1812, 854, '3B00320-4', 2898, 3),
(1813, 854, '3B00321-1', 2905, 4),
(1814, 854, '3B00321-2', 2905, 5),
(1815, 854, '3B00321-5', 2906, 6),
(1816, 854, '3B00324-3', 2672, 7),
(1817, 855, '4A00172-1', 2916, 1),
(1818, 855, '4A00174-3', 2920, 2),
(1819, 855, '4A00185-3', 2912, 3),
(1820, 855, '4A00186-3', 2903, 4),
(1821, 855, '4A00188-5', 2914, 5),
(1822, 855, '4A00189-6', 2865, 6),
(1823, 855, '4A00190-4', 2918, 7),
(1824, 856, '4A00169-3', 2902, 1),
(1825, 856, '4A00176-4', 2923, 2),
(1826, 856, '4A00180-1', 2919, 3),
(1827, 856, '4A00189-1', 2925, 4),
(1828, 856, '4A00193-2', 2930, 5),
(1829, 856, '4A00194-4', 2915, 6),
(1830, 856, '4A00194-6', 2861, 7),
(1831, 857, '3A00112-1', 2893, 1),
(1832, 857, '3A00112-4', 2890, 2),
(1833, 857, '3B00341-2', 2910, 3),
(1834, 857, '3B00343-5', 2926, 4),
(1835, 857, '3B00344-2', 2904, 5),
(1836, 857, '3B00344-3', 2908, 6),
(1837, 857, '3B00344-4', 2905, 7),
(1838, 858, '3A00112-2', 2891, 1),
(1839, 858, '3A00112-5', 2888, 2),
(1840, 858, '3A00112-6', 2959, 3),
(1841, 858, '3B00341-3', 2911, 4),
(1842, 858, '3B00341-6', 3084, 5),
(1843, 858, '3B00343-1', 2920, 6),
(1844, 858, '3B00344-5', 2906, 7),
(1845, 859, '7A00736-1', 1184, 1),
(1846, 859, '7A00737-2', 1136, 2),
(1847, 859, '7A00738-1', 1240, 3),
(1848, 859, '7A00739-1', 1185, 4),
(1849, 859, '7A00739-2', 1113, 5),
(1850, 859, '7A00740-1', 1202, 6),
(1851, 859, '7A00741-1', 1151, 7),
(1852, 859, '7A00742-1', 1162, 8),
(1853, 859, '7A00742-2', 1123, 9),
(1854, 859, '7A00949-2', 1157, 10),
(1855, 859, '7A01230-1', 995, 11),
(1856, 859, '7A01237-2', 1309, 12),
(1857, 859, '7A01238-1', 1016, 13),
(1858, 859, '7A01239-2', 1213, 14),
(1859, 859, '7A01240-1', 1041, 15),
(1860, 859, '7A01253-1', 1168, 16),
(1861, 859, '7A01254-1', 1117, 17),
(1862, 859, '7A01254-2', 1168, 18),
(1863, 859, '7A01256-2', 1108, 19),
(1864, 859, '7A01257-1', 1139, 20),
(1865, 859, '7A01257-2', 1131, 21),
(1866, 862, '11B00133-1', 1027, 1),
(1867, 862, '11B00135-1', 1037, 2),
(1868, 862, '11B00137-2', 947, 3),
(1869, 862, '11B00141-1', 1051, 4),
(1870, 862, '11B00142-1', 1093, 5),
(1871, 862, '11B00148-1', 1107, 6),
(1872, 862, '11B00155-1', 1057, 7),
(1873, 862, '11B00157-1', 1030, 8),
(1874, 862, '11B00159-1', 1022, 9),
(1875, 862, '11B01160-2', 1045, 10),
(1876, 862, '11B00161-1', 984, 11),
(1877, 862, '11B00162-2', 1000, 12),
(1878, 862, '11B00164-1', 1047, 13),
(1879, 862, '11B00164-2', 1019, 14),
(1880, 862, '11B00165-2', 1033, 15),
(1881, 865, '4B00135-1', 2939, 1),
(1882, 865, '4B00144-2', 2955, 2),
(1883, 865, '4B00146-4', 2515, 3),
(1884, 865, '3B00147-1', 2939, 4),
(1885, 865, '4B00147-5', 2864, 5),
(1886, 865, '4B00148-3', 2928, 6),
(1887, 865, '3B00148-6', 2800, 7),
(1888, 867, '4B00156-3', 2926, 1),
(1889, 867, '4B00157-5', 2934, 2),
(1890, 867, '4B00161-3', 2926, 3),
(1891, 867, '4B00161-4', 2933, 4),
(1892, 867, '4B00162-1', 2924, 5),
(1893, 868, '3B00310-7', 2267, 1),
(1894, 868, '3B00331-2', 2222, 2),
(1895, 872, '7A01133-2', 1239, 1),
(1896, 872, '7A01134-1', 979, 2),
(1897, 872, '7A01156-1', 990, 3),
(1898, 872, '7A01158-1', 974, 4),
(1899, 872, '7A01167-1', 992, 5),
(1900, 872, '7A01174-1', 987, 6),
(1901, 872, '7A01185-1', 965, 7),
(1902, 872, '7A01189-1', 996, 8),
(1903, 872, '7A01196-2', 1009, 9),
(1904, 872, '7A01201-1', 998, 10),
(1905, 872, '7A01202-2', 1322, 11),
(1906, 872, '7A01242-1', 1000, 12),
(1907, 872, '7A01242-2', 1310, 13),
(1908, 872, '7A01243-1', 1007, 14),
(1909, 872, '7A01002-1', 970, 15),
(1910, 872, '7A01013-2', 976, 16),
(1911, 872, '7A01016-1', 976, 17),
(1912, 872, '7A01027-1', 984, 18),
(1913, 872, '7A01048-1', 997, 19),
(1914, 872, '7A01082-1', 987, 20),
(1915, 872, '7A01101-1', 793, 21),
(1916, 872, '7A01127-1', 978, 22),
(1917, 872, '7A01132-1', 988, 23),
(1918, 872, '7A01148-2', 925, 24),
(1919, 872, '7A01222-1', 737, 25),
(1920, 873, '1B00013-8', 1645, 1),
(1921, 873, '1B00032-2', 2940, 2),
(1922, 874, '3A00124-3', 3260, 1),
(1923, 875, '7A01155-2', 1324, 1),
(1924, 875, '7A01156-2', 1319, 2),
(1925, 875, '7A01157-1', 1003, 3),
(1926, 875, '7A01172-2', 1247, 4),
(1927, 875, '7A01184-2', 1257, 5),
(1928, 875, '7A01185-2', 1360, 6),
(1929, 875, '7A01219-2', 1300, 7),
(1930, 875, '7A01234-1', 1004, 8),
(1931, 875, '7A01259-2', 1116, 9),
(1932, 875, '7A01262-2', 1144, 10),
(1933, 875, '7A01302-1', 1151, 11),
(1934, 875, '7A01302-2', 1026, 12),
(1935, 875, '7A01303-1', 1123, 13),
(1936, 875, '7A01331-1', 1139, 14),
(1937, 875, '7A01331-2', 1062, 15),
(1938, 875, '7A01332-1', 1169, 16),
(1939, 875, '7A01332-2', 1189, 17),
(1940, 875, '7A01334-1', 1187, 18),
(1941, 875, '7A01339-1', 1266, 19),
(1942, 875, '7A01340-1', 1195, 20),
(1943, 875, '7A01340-2', 1127, 21),
(1944, 876, '11B00074-1', 988, 1),
(1945, 876, '11B00074-2', 1067, 2),
(1946, 876, '11B00076-2', 1014, 3),
(1947, 876, '11B00077-1', 1026, 4),
(1948, 876, '11B00077-2', 1040, 5),
(1949, 876, '11B00078-1', 872, 6),
(1950, 876, '11B00078-2', 1170, 7),
(1951, 876, '11B00079-1', 966, 8),
(1952, 876, '11B00079-2', 1022, 9),
(1953, 876, '11B00080-2', 1090, 10),
(1954, 876, '11B00081-1', 1419, 11),
(1955, 876, '11B00082-2', 1078, 12),
(1956, 876, '11B00101-2', 1001, 13),
(1957, 876, '11B00102-1', 787, 14),
(1958, 877, '4B00133-5', 2929, 1),
(1959, 877, '4B00134-4', 2930, 2),
(1960, 877, '4B00134-5', 2930, 3),
(1961, 877, '4B00137-2', 2942, 4),
(1962, 877, '4B00137-3', 2940, 5),
(1963, 877, '4B00144-6', 2891, 6),
(1964, 877, '4B00172-3', 2945, 7),
(1965, 878, '7A01256-1', 1100, 1),
(1966, 878, '7A01298-1', 1133, 2),
(1967, 878, '7A01327-1', 1022, 3),
(1968, 878, '7A01328-1', 1139, 4),
(1969, 878, '7A01328-2', 1144, 5),
(1970, 878, '7A01329-1', 1124, 6),
(1971, 878, '7A01329-2', 1151, 7),
(1972, 878, '7A01330-1', 1105, 8),
(1973, 878, '7A01333-1', 1256, 9),
(1974, 878, '7A01333-2', 1032, 10),
(1975, 878, '7A01334-2', 1161, 11),
(1976, 878, '7A01335-1', 1115, 12),
(1977, 878, '7A01335-2', 1161, 13),
(1978, 878, '7A01336-1', 1119, 14),
(1979, 878, '7A01336-2', 1224, 15),
(1980, 878, '7A01337-1', 1157, 16),
(1981, 878, '7A01337-2', 1109, 17),
(1982, 878, '7A01338-1', 1159, 18),
(1983, 878, '7A01338-2', 1071, 19),
(1984, 879, '3B00369-1', 2103, 1),
(1985, 879, '3B00371-2', 2551, 2),
(1986, 880, '11B00053-1', 902, 1),
(1987, 880, '11B00053-2', 894, 2),
(1988, 880, '11B00057-1', 1015, 3),
(1989, 880, '11B00057-2', 1038, 4),
(1990, 881, '3A00131-2', 3075, 1),
(1991, 881, '3A00131-3', 3073, 2),
(1992, 881, '3B00361-4', 3090, 3),
(1993, 881, '3B00363-6', 3073, 4),
(1994, 881, '3B00364-6', 3050, 5),
(1995, 881, '3B00367-6', 3044, 6),
(1996, 881, '3B00375-3', 3084, 7),
(1997, 882, '3A00101-4', 3070, 1),
(1998, 882, '3A00124-5', 3262, 2),
(1999, 882, '3B00322-6', 3073, 3),
(2000, 882, '3B00334-6', 3086, 4),
(2001, 882, '3B00361-5', 3270, 5),
(2002, 882, '3B00377-6', 3090, 6),
(2003, 882, '3B00379-6', 3083, 7),
(2004, 883, '3A00142-5', 3080, 1),
(2005, 883, '3A00142-6', 3118, 2),
(2006, 883, '3B00361-2', 3090, 3),
(2007, 883, '3B00361-3', 3090, 4),
(2008, 883, '3B00366-6', 3051, 5),
(2009, 883, '3B00368-6', 3059, 6),
(2010, 883, '3B00382-3', 3076, 7),
(2011, 884, '3A00124-6', 3354, 1),
(2012, 884, '3A00125-6', 3120, 2),
(2013, 884, '3B00372-4', 3366, 3),
(2014, 884, '3B00261-6', 3089, 4),
(2015, 884, '3B00354-6', 3077, 5),
(2016, 884, '3B00365-6', 3077, 6),
(2017, 884, '3B00369-3', 3079, 7),
(2018, 885, '3A00134-1', 3085, 1),
(2019, 885, '3A00137-6', 3059, 2),
(2020, 885, '3B00373-1', 3435, 3),
(2021, 885, '3B00374-6', 3088, 4),
(2022, 885, '3B00375-1', 3263, 5),
(2023, 885, '3B00376-6', 3116, 6),
(2024, 885, '3B00378-6', 3074, 7),
(2025, 887, '7A00732-1', 1135, 1),
(2026, 887, '7A01005-1', 978, 2),
(2027, 887, '7A01038-1', 979, 3),
(2028, 887, '7A01056-1', 996, 4),
(2029, 887, '7A01074-1', 986, 5),
(2030, 887, '7A01075-1', 987, 6),
(2031, 887, '7A01083-1', 978, 7),
(2032, 887, '7A01087-1', 994, 8),
(2033, 887, '7A01097-1', 988, 9),
(2034, 887, '7A01108-1', 982, 10),
(2035, 887, '7A01115-2', 981, 11),
(2036, 887, '7A01116-1', 988, 12),
(2037, 887, '7A01118-1', 994, 13),
(2038, 887, '7A01119-1', 984, 14),
(2039, 887, '7A01129-1', 993, 15),
(2040, 887, '7A01139-1', 989, 16),
(2041, 887, '7A01160-1', 975, 17),
(2042, 887, '7A01171-2', 986, 18),
(2043, 887, '7A01183-2', 984, 19),
(2044, 887, '7A01186-1', 995, 20),
(2045, 887, '7A01191-1', 992, 21),
(2046, 887, '7A01229-1', 997, 22),
(2047, 887, '7A01233-2', 1268, 23),
(2048, 887, '7A01268-3', 932, 24),
(2049, 887, '7A01315-2', 1017, 25),
(2050, 888, '7A01007-1', 978, 1),
(2051, 888, '7A01009-1', 984, 2),
(2052, 888, '7A01010-1', 993, 3),
(2053, 888, '7A01028-1', 983, 4),
(2054, 888, '7A01029-1', 980, 5),
(2055, 888, '7A01042-1', 987, 6),
(2056, 888, '7A01076-1', 986, 7),
(2057, 888, '7A01036-1', 991, 8),
(2058, 888, '7A01088-1', 998, 9),
(2059, 888, '7A01089-1', 989, 10),
(2060, 888, '7A01090-1', 995, 11),
(2061, 888, '7A01091-1', 981, 12),
(2062, 888, '7A01126-1', 977, 13),
(2063, 888, '7A01130-1', 984, 14),
(2064, 888, '7A01131-1', 986, 15),
(2065, 888, '7A01151-2', 972, 16),
(2066, 888, '7A01159-1', 979, 17),
(2067, 888, '7A01176-1', 999, 18),
(2068, 888, '7A01178-1', 996, 19),
(2069, 888, '7A01179-1', 999, 20),
(2070, 888, '7A01181-1', 999, 21),
(2071, 888, '7A01190-1', 975, 22),
(2072, 888, '7A01195-1', 998, 23),
(2073, 888, '7A01264-2', 1058, 24),
(2074, 888, '7A01280-2', 1087, 25),
(2075, 889, '4B00165-1', 2940, 1),
(2076, 889, '4B00169-4', 2950, 2),
(2077, 889, '4B00176-3', 2939, 3),
(2078, 890, '9B01680-1', 1317, 1),
(2079, 890, '9B01681-1', 1277, 2),
(2080, 890, '9D00501-2', 1211, 3),
(2081, 890, '9D00504-2', 1105, 4),
(2082, 890, '9D00505-1', 1122, 5),
(2083, 890, '9D00540-1', 1158, 6),
(2084, 890, '9D00540-2', 1118, 7),
(2085, 890, '9D00558-2', 1083, 8),
(2086, 890, '9D00567-1', 1176, 9),
(2087, 890, '9B01088-2', 697, 10),
(2088, 890, '9D00331-2', 690, 11),
(2089, 890, '9D00332-2', 687, 12),
(2090, 890, '9D00570-2', 1191, 13),
(2091, 891, '1B00033-1', 2931, 1),
(2092, 891, '1B00033-2', 2931, 2),
(2093, 891, '1B00033-4', 2928, 3),
(2094, 891, '1B00033-5', 2930, 4),
(2095, 891, '1B00034-1', 2920, 5),
(2096, 891, '1B00034-2', 2921, 6),
(2097, 891, '1B00034-3', 2922, 7),
(2098, 892, '4A00208-2', 2384, 1),
(2099, 892, '4B00183-1', 2953, 2),
(2100, 892, '4B00193-2', 2731, 3),
(2101, 892, '4B00196-4', 2957, 4),
(2102, 892, '4B00201-6', 3196, 5),
(2103, 892, '4B00207-6', 2788, 6),
(2104, 892, '4B00209-5', 2729, 7),
(2105, 893, '4B00123-3', 2515, 1),
(2106, 893, '4B00186-6', 2819, 2),
(2107, 893, '4B00203-1', 2931, 3),
(2108, 893, '4B00203-2', 2929, 4),
(2109, 893, '4B00205-6', 2838, 5),
(2110, 893, '4B00209-3', 2911, 6),
(2111, 894, '11B00076-1', 1033, 1),
(2112, 894, '11B00090-2', 1039, 2),
(2113, 894, '11B00091-1', 950, 3),
(2114, 894, '11B00092-1', 1014, 4),
(2115, 894, '11B00093-1', 920, 5),
(2116, 894, '11B00093-2', 990, 6),
(2117, 894, '11B00096-2', 995, 7),
(2118, 894, '11B00098-1', 1017, 8),
(2119, 894, '11B00099-1', 1015, 9),
(2120, 894, '11B00099-2', 1040, 10),
(2121, 894, '11B00103-1', 1025, 11),
(2122, 894, '11B00106-2', 1031, 12),
(2123, 894, '11B00110-1', 969, 13),
(2124, 894, '11B00126-1', 1007, 14),
(2125, 894, '11B00112-2', 1010, 15),
(2126, 896, '3B00343-2', 2922, 1),
(2127, 896, '3B00405-5', 2904, 2),
(2128, 897, '1B00038-5', 3212, 1),
(2129, 898, '9B01341-2', 914, 1),
(2130, 898, '9B01345-2', 1047, 2),
(2131, 898, '9B01348-2', 1054, 3),
(2132, 898, '9B01359-1', 1047, 4),
(2133, 898, '9B01362-2', 1057, 5),
(2134, 898, '9B01378-2', 1062, 6),
(2135, 898, '9B01380-2', 1068, 7),
(2136, 898, '9B01386-2', 996, 8),
(2137, 898, '9B01393-1', 1043, 9),
(2138, 898, '9B01397-1', 1020, 10),
(2139, 898, '9B01404-1', 1066, 11),
(2140, 898, '9B01430-2', 1083, 12),
(2141, 898, '9B01506-2', 1050, 13),
(2142, 898, '9B01522-1', 1020, 14),
(2143, 898, '9B01523-1', 1093, 15),
(2144, 898, '9B01668-1', 925, 16),
(2145, 898, '9D00474-2', 1012, 17),
(2146, 898, '9D00480-2', 915, 18),
(2147, 898, '9D00514-2', 1018, 19),
(2148, 898, '9D00515-2', 980, 20),
(2149, 899, '9B01423-1', 1047, 1),
(2150, 899, '9B01424-1', 1047, 2),
(2151, 899, '9B01497-2', 1050, 3),
(2152, 899, '9B01504-2', 1053, 4),
(2153, 899, '9B01526-1', 1001, 5),
(2154, 899, '9B01552-1', 1071, 6),
(2155, 899, '9B01627-1', 1060, 7),
(2156, 899, '9B01628-2', 1070, 8),
(2157, 899, '9B01632-2', 974, 9),
(2158, 899, '9B01669-2', 1018, 10),
(2159, 899, '9B01670-2', 1091, 11),
(2160, 899, '9B01678-1', 1055, 12),
(2161, 899, '9B01679-2', 1055, 13),
(2162, 899, '9B01680-2', 1048, 14),
(2163, 899, '9B01681-2', 1051, 15),
(2164, 899, '9B01696-2', 1053, 16),
(2165, 899, '9B01698-2', 1064, 17),
(2166, 899, '9B01718-2', 1060, 18),
(2167, 899, '9D00539-2', 997, 19),
(2168, 899, '9D00548-2', 970, 20),
(2169, 900, '9B01320-2', 1055, 1),
(2170, 900, '9B01324-2', 1062, 2),
(2171, 900, '9B01327-2', 1057, 3),
(2172, 900, '9B01328-2', 1053, 4),
(2173, 900, '9B01329-2', 1055, 5),
(2174, 900, '9B01338-2', 1065, 6),
(2175, 900, '9B01344-2', 1065, 7),
(2176, 900, '9B01548-2', 1026, 8),
(2177, 900, '9B01551-1', 1077, 9),
(2178, 900, '9B01553-1', 1051, 10),
(2179, 900, '9B01617-1', 1042, 11),
(2180, 900, '9B01633-2', 979, 12),
(2181, 900, '9B01634-2', 1059, 13),
(2182, 900, '9B01653-2', 1065, 14),
(2183, 900, '9B01656-2', 1053, 15),
(2184, 900, '9B01658-2', 1079, 16),
(2185, 900, '9B01659-2', 1059, 17),
(2186, 900, '9B01660-1', 1038, 18),
(2187, 900, '9B01661-2', 1073, 19),
(2188, 900, '9D00449-2', 993, 20),
(2189, 901, '9B01321-2', 1054, 1),
(2190, 901, '9B01322-2', 1056, 2),
(2191, 901, '9B01323-2', 1059, 3),
(2192, 901, '9B01325-2', 1088, 4),
(2193, 901, '9B01326-2', 1062, 5),
(2194, 901, '9B01331-2', 1055, 6),
(2195, 901, '9B01421-2', 1046, 7),
(2196, 901, '9B01422-2', 1030, 8),
(2197, 901, '9B01429-2', 1064, 9),
(2198, 901, '9B01430-1', 1052, 10),
(2199, 901, '9B01431-2', 1053, 11),
(2200, 901, '9B01435-2', 994, 12),
(2201, 901, '9B01437-2', 1030, 13),
(2202, 901, '9B01494-2', 1055, 14),
(2203, 901, '9B01495-2', 946, 15),
(2204, 901, '9B01496-1', 1069, 16),
(2205, 901, '9B01503-2', 962, 17),
(2206, 901, '9B01505-2', 1058, 18),
(2207, 901, '9B01507-2', 1043, 19),
(2208, 901, '9D00441-2', 951, 20),
(2209, 902, '9B01283-2', 1044, 1),
(2210, 902, '9B01292-2', 1058, 2),
(2211, 902, '9B01297-2', 1004, 3),
(2212, 902, '9B01307-2', 1051, 4),
(2213, 902, '9B01308-2', 1059, 5),
(2214, 902, '9B01315-2', 1065, 6),
(2215, 902, '9B01337-2', 1061, 7),
(2216, 902, '9B01347-2', 1058, 8),
(2217, 902, '9B01351-2', 1078, 9),
(2218, 902, '9B01512-2', 1046, 10),
(2219, 902, '9B01542-1', 975, 11),
(2220, 902, '9B01554-2', 1051, 12),
(2221, 902, '9B01559-2', 1045, 13),
(2222, 902, '9B01560-1', 1072, 14),
(2223, 902, '9B01561-2', 1058, 15),
(2224, 902, '9B01567-2', 1063, 16),
(2225, 902, '9B01568-1', 1038, 17),
(2226, 902, '9B01570-2', 1047, 18),
(2227, 902, '9B01662-2', 1062, 19),
(2228, 902, '9B01663-2', 1017, 20),
(2229, 903, '9B01336-2', 1060, 1),
(2230, 903, '9B01363-2', 1062, 2),
(2231, 903, '9B01364-2', 1081, 3),
(2232, 903, '9B01365-2', 1080, 4),
(2233, 903, '9B01417-2', 1044, 5),
(2234, 903, '9B01418-2', 981, 6),
(2235, 903, '9B01419-1', 823, 7),
(2236, 903, '9B01419-2', 967, 8),
(2237, 903, '9B01425-1', 1042, 9),
(2238, 903, '9B01571-2', 1055, 10),
(2239, 903, '9B01592-2', 1061, 11),
(2240, 903, '9B01606-2', 1075, 12),
(2241, 903, '9B01624-2', 1070, 13),
(2242, 903, '9B01625-2', 1068, 14),
(2243, 903, '9B01626-2', 1075, 15),
(2244, 903, '9B01629-2', 1062, 16),
(2245, 903, '9B01683-2', 1064, 17),
(2246, 903, '9B01684-1', 1061, 18),
(2247, 903, '9B01710-2', 1060, 19),
(2248, 903, '9D00527-1', 1038, 20),
(2249, 905, '7D00150-2', 679, 1),
(2250, 905, '7D00780-2', 669, 2),
(2251, 905, '7D01049-2', 772, 3),
(2252, 905, '7D01091-3', 679, 4),
(2253, 905, '7D01127-1', 1186, 5),
(2254, 905, '7D01127-2', 1151, 6),
(2255, 905, '7D01130-2', 1293, 7),
(2256, 905, '7D01131-1', 1140, 8),
(2257, 905, '7D01131-2', 1188, 9),
(2258, 905, '7D01135-1', 1108, 10),
(2259, 905, '7D01143-2', 1107, 11),
(2260, 905, '7D01150-1', 1123, 12),
(2261, 905, '7D01150-2', 1233, 13),
(2262, 905, '7D01162-1', 1080, 14),
(2263, 905, '7D01162-2', 1190, 15),
(2264, 905, '7D01163-1', 1086, 16),
(2265, 905, '7D01163-2', 1255, 17),
(2266, 905, '7D01164-1', 1145, 18),
(2267, 905, '7D01164-2', 1193, 19),
(2268, 905, '7D01165-1', 1113, 20),
(2269, 905, '7D01168-2', 1138, 21),
(2270, 905, '7D01172-1', 1083, 22),
(2271, 905, '7D01183-1', 1064, 23),
(2272, 905, '7D00918-2', 742, 24),
(2273, 906, '11B00054-1', 1189, 1),
(2274, 906, '11B00054-2', 864, 2),
(2275, 906, '11B00055-2', 1000, 3),
(2276, 906, '11B00056-2', 1302, 4),
(2277, 906, '11B00060-1', 1035, 5),
(2278, 906, '11B00060-2', 1028, 6),
(2279, 906, '11B00062-2', 1042, 7),
(2280, 906, '11B00065-1', 1016, 8),
(2281, 906, '11B00065-2', 1039, 9),
(2282, 906, '11B00066-1', 931, 10),
(2283, 906, '11B00066-2', 1115, 11),
(2284, 906, '11B00067-2', 980, 12),
(2285, 906, '11B00089-1', 1057, 13),
(2286, 906, '11B00100-1', 967, 14),
(2287, 906, '11B00100-2', 1044, 15),
(2288, 907, '11B00080-1', 968, 1),
(2289, 907, '11B00082-1', 976, 2),
(2290, 907, '11B00083-1', 984, 3),
(2291, 907, '11B00083-2', 1065, 4),
(2292, 907, '11B00084-2', 990, 5),
(2293, 907, '11B00104-2', 990, 6),
(2294, 907, '11B00105-1', 1036, 7),
(2295, 907, '11B00106-1', 1034, 8),
(2296, 907, '11B00107-1', 1029, 9),
(2297, 907, '11B00107-2', 1034, 10),
(2298, 907, '11B00113-1', 1040, 11),
(2299, 907, '11B00113-2', 752, 12),
(2300, 907, '11B00114-2', 1034, 13),
(2301, 907, '11B00163-2', 1028, 14),
(2302, 907, '11B00176-1', 991, 15),
(2303, 908, '7A01392-1', 1133, 1),
(2304, 908, '7A01395-1', 1179, 2),
(2305, 908, '7A01398-1', 1147, 3),
(2306, 908, '7A01398-2', 1123, 4),
(2307, 908, '7A01399-2', 1134, 5),
(2308, 908, '7A01400-1', 1115, 6),
(2309, 908, '7A01401-1', 1176, 7),
(2310, 908, '7A01401-2', 1048, 8),
(2311, 908, '7A01402-1', 1220, 9),
(2312, 908, '7A01402-2', 1052, 10),
(2313, 908, '7A01406-1', 1149, 11),
(2314, 908, '7A01406-2', 1127, 12),
(2315, 908, '7A01407-1', 1095, 13),
(2316, 908, '7A01407-2', 1170, 14),
(2317, 908, '7A01408-1', 1115, 15),
(2318, 908, '7A01408-2', 1167, 16),
(2319, 908, '7A01409-1', 1088, 17),
(2320, 908, '7A01409-2', 1247, 18),
(2321, 908, '7A01410-1', 1121, 19),
(2322, 908, '7A01415-2', 1157, 20),
(2323, 908, '7A01416-1', 1099, 21),
(2324, 908, '7A01417-1', 1145, 22),
(2325, 909, '7A01366-1', 1136, 1),
(2326, 909, '7A01366-2', 1132, 2),
(2327, 909, '7A01367-1', 1160, 3),
(2328, 909, '7A01378-1', 1144, 4),
(2329, 909, '7A01378-2', 1137, 5),
(2330, 909, '7A01379-1', 1148, 6),
(2331, 909, '7A01390-1', 1091, 7),
(2332, 909, '7A01391-2', 1111, 8),
(2333, 909, '7A01392-2', 1132, 9),
(2334, 909, '7A01403-1', 1113, 10),
(2335, 909, '7A01403-2', 1151, 11),
(2336, 909, '7A01404-1', 1137, 12),
(2337, 909, '7A01410-2', 1132, 13),
(2338, 910, '7A01411-1', 1146, 1),
(2339, 910, '7A01412-1', 1152, 2),
(2340, 910, '7A01412-2', 1123, 3),
(2341, 910, '7A01413-1', 1144, 4),
(2342, 910, '7A01413-2', 1126, 5),
(2343, 910, '7A01414-1', 1177, 6),
(2344, 910, '7A01414-2', 848, 7),
(2345, 910, '7A01415-1', 1114, 8),
(2346, 910, '7A01416-2', 1179, 9),
(2347, 910, '7A01418-2', 1161, 10),
(2348, 910, '7A01419-1', 1111, 11),
(2349, 910, '7A01423-2', 1213, 12),
(2350, 910, '7A01424-1', 1278, 13),
(2351, 910, '7A01424-2', 1048, 14),
(2352, 912, '7A01454-1', 1022, 1),
(2353, 912, '7A01454-2', 1014, 2),
(2354, 912, '7A01341-1', 1143, 3),
(2355, 912, '7A01344-2', 1073, 4),
(2356, 912, '7A01364-2', 1143, 5),
(2357, 912, '7A01365-1', 1129, 6),
(2358, 912, '7A01365-2', 1145, 7),
(2359, 912, '7A01368-2', 1126, 8),
(2360, 912, '7A01369-1', 1168, 9),
(2361, 912, '7A01369-2', 1122, 10),
(2362, 912, '7A01370-1', 1171, 11),
(2363, 912, '7A01370-2', 1116, 12),
(2364, 912, '7A01381-1', 1128, 13),
(2365, 912, '7A01386-2', 1128, 14),
(2366, 912, '7A01395-2', 1098, 15),
(2367, 912, '7A01396-1', 1142, 16),
(2368, 912, '7A01397-1', 1111, 17),
(2369, 912, '7A00994-1', 987, 18),
(2370, 912, '7A01427-1', 1186, 19),
(2371, 912, '7A01450-1', 1109, 20),
(2372, 912, '7A01394-2', 1140, 21),
(2373, 912, '7A01451-1', 1066, 22),
(2374, 913, '7A01220-2', 898, 1),
(2375, 913, '7A01426-6', 1022, 2),
(2376, 913, '7A01455-1', 1030, 3),
(2377, 913, '7A01460-2', 1022, 4),
(2378, 913, '7A01461-1', 1029, 5),
(2379, 913, '7A01464-1', 1020, 6),
(2380, 913, '7A01450-2', 896, 7),
(2381, 913, '7A01453-2', 1052, 8),
(2382, 914, '3A00112-3', 3012, 1),
(2383, 914, '3A00137-3', 2889, 2),
(2384, 914, '3B00362-1', 2900, 3),
(2385, 914, '3B00363-5', 2893, 4),
(2386, 914, '3B00377-3', 2904, 5),
(2387, 914, '3B00382-2', 3080, 6),
(2388, 914, '3B00382-4', 3079, 7),
(2389, 915, '3B00362-3', 2899, 1),
(2390, 915, '3B00362-5', 2900, 2),
(2391, 915, '3B00371-3', 2532, 3),
(2392, 915, '3B00375-6', 2491, 4),
(2393, 915, '3B00381-4', 2905, 5),
(2394, 915, '3B00390-5', 2905, 6),
(2395, 915, '3B00400-1', 2907, 7),
(2396, 916, '3B00227-1', 2909, 1),
(2397, 916, '3B00337-5', 2906, 2),
(2398, 916, '3B00399-5', 2914, 3),
(2399, 916, '3B00415-3', 2892, 4),
(2400, 916, '3B00415-5', 2890, 5),
(2401, 916, '3B00418-3', 2897, 6),
(2402, 916, '3B00418-4', 2897, 7),
(2403, 917, '3B00411-3', 2896, 1),
(2404, 917, '3B00414-6', 3165, 2),
(2405, 917, '3B00415-1', 2890, 3),
(2406, 917, '3B00415-2', 2892, 4),
(2407, 917, '3B00415-4', 2890, 5),
(2408, 917, '3B00418-2', 2894, 6),
(2409, 917, '3B00418-6', 3040, 7),
(2410, 918, '7A01011-1', 984, 1),
(2411, 918, '7A01014-1', 976, 2),
(2412, 918, '7A01019-1', 982, 3),
(2413, 918, '7A01128-1', 991, 4),
(2414, 918, '7A01351-2', 1154, 5),
(2415, 918, '7A01360-1', 1171, 6),
(2416, 918, '7A01360-2', 1111, 7),
(2417, 918, '7A01388-1', 1180, 8),
(2418, 918, '7A01389-1', 1082, 9),
(2419, 918, '7A01389-2', 1177, 10),
(2420, 918, '7A01427-2', 1080, 11),
(2421, 918, '7A01430-2', 1087, 12),
(2422, 918, '7A01342-2', 1163, 13),
(2423, 918, '7A01433-2', 1164, 14),
(2424, 918, '7A01434-1', 1157, 15),
(2425, 918, '7A01434-2', 1128, 16),
(2426, 918, '7A01435-2', 1067, 17),
(2427, 918, '7A01442-1', 1010, 18),
(2428, 918, '7A01442-2', 1013, 19),
(2429, 918, '7A01443-1', 1124, 20),
(2430, 918, '7A01443-2', 908, 21),
(2431, 918, '7A01445-2', 1029, 22),
(2432, 918, '7A01446-1', 1033, 23),
(2433, 919, '7A01043-1', 988, 1),
(2434, 919, '7A01330-2', 1139, 2),
(2435, 919, '7A01346-1', 619, 3),
(2436, 919, '7A01346-2', 698, 4),
(2437, 919, '7A01346-3', 708, 5),
(2438, 919, '7A01352-2', 1143, 6),
(2439, 919, '7A01393-1', 1119, 7),
(2440, 919, '7A01394-1', 1111, 8),
(2441, 919, '7A01400-2', 925, 9),
(2442, 919, '7A01431-1', 1141, 10),
(2443, 919, '7A01431-2', 1132, 11),
(2444, 919, '7A01432-1', 1118, 12),
(2445, 919, '7A01436-1', 1004, 13),
(2446, 919, '7A01436-2', 1249, 14),
(2447, 919, '7A01449-2', 960, 15),
(2448, 919, '7A01455-2', 987, 16),
(2449, 919, '7A01456-1', 1013, 17),
(2450, 919, '7A01457-2', 995, 18),
(2451, 919, '7A01459-2', 957, 19),
(2452, 919, '7A01461-2', 983, 20),
(2453, 919, '7A01462-1', 1003, 21),
(2454, 919, '7A01462-2', 1033, 22),
(2455, 919, '7A01463-1', 1000, 23),
(2456, 919, '7A01463-2', 1002, 24),
(2457, 919, '7A01391-1', 1147, 25),
(2458, 920, '9B01682-1', 1167, 1),
(2459, 920, '9B01748-1', 1276, 2),
(2460, 920, '9B01755-2', 1221, 3),
(2461, 920, '9B01756-1', 1278, 4),
(2462, 920, '9B01757-2', 1260, 5),
(2463, 920, '9B01776-2', 1127, 6),
(2464, 920, '9B01801-1', 1255, 7),
(2465, 920, '9B01803-1', 1260, 8),
(2466, 920, '9B01804-1', 1309, 9),
(2467, 920, '9B01806-1', 1244, 10),
(2468, 920, '9B01810-2', 1270, 11),
(2469, 920, '9B01789-1', 1210, 12),
(2470, 920, '9B01817-2', 702, 13),
(2471, 920, '9B01821-1', 1270, 14),
(2472, 920, '9B01826-1', 1267, 15),
(2473, 920, '9B01854-1', 1145, 16),
(2474, 920, '9B01923-1', 673, 17),
(2475, 920, '9D00611-2', 1195, 18),
(2476, 920, '9D00612-1', 1127, 19),
(2477, 920, '9B01781-1', 1262, 20),
(2478, 920, '9D00638-2', 1096, 21),
(2479, 920, '9B01809-1', 1250, 22),
(2480, 921, '4B00224-2', 2920, 1),
(2481, 921, '4B00225-4', 2920, 2),
(2482, 922, '9B02016-1', 1279, 1),
(2483, 922, '9B01812-1', 1294, 2),
(2484, 922, '9B01570-1', 1317, 3),
(2485, 922, '9B01805-1', 1292, 4),
(2486, 922, '9B01782-1', 1284, 5),
(2487, 922, '9B01863-1', 1264, 6),
(2488, 922, '9B01859-1', 1281, 7),
(2489, 923, '9B02015-1', 1266, 1),
(2490, 923, '9B01807-1', 1221, 2),
(2491, 923, '9D00621-2', 1156, 3),
(2492, 923, '9B01780-1', 1271, 4),
(2493, 923, '9B01779-1', 1275, 5),
(2494, 923, '9B01778-1', 1261, 6),
(2495, 923, '9B01775-1', 1213, 7),
(2496, 923, '9D00637-2', 1094, 8),
(2497, 923, '9B01837-1', 1209, 9),
(2498, 923, '9B01842-1', 1272, 10),
(2499, 923, '9B01838-1', 1235, 11),
(2500, 923, '9B01820-1', 1255, 12),
(2501, 923, '9B01819-1', 1264, 13),
(2502, 923, '9D00637-1', 1184, 14),
(2503, 924, '3B00380-2', 2911, 1),
(2504, 924, '3B00429-2', 2742, 2),
(2505, 924, '3B00435-3', 2901, 3),
(2506, 924, '3B00436-5', 2910, 4),
(2507, 924, '3B00438-1', 2709, 5),
(2508, 924, '3B00439-2', 2876, 6),
(2509, 924, '3B00439-4', 2877, 7),
(2510, 925, '3B00436-3', 2909, 1),
(2511, 925, '3B00440-5', 2883, 2),
(2512, 925, '3B00443-4', 2883, 3),
(2513, 925, '3B00445-2', 2892, 4),
(2514, 925, '3B00446-2', 2701, 5),
(2515, 925, '3B00446-4', 2703, 6),
(2516, 925, '3B00420-6', 3048, 7),
(2517, 926, '3B00213-5', 2924, 1),
(2518, 926, '3B00416-3', 2891, 2),
(2519, 926, '3B00416-5', 2895, 3),
(2520, 926, '3B00424-6', 3077, 4),
(2521, 926, '3B00423-6', 3086, 5),
(2522, 926, '3B00438-5', 2494, 6),
(2523, 926, '3B00444-3', 2548, 7),
(2524, 931, '9B01590-1', 1163, 1),
(2525, 931, '9B01598-1', 601, 2),
(2526, 931, '9B01636-1', 1193, 3),
(2527, 931, '9B01705-1', 1312, 4),
(2528, 931, '9B01706-1', 1257, 5),
(2529, 931, '9B01712-1', 1316, 6),
(2530, 931, '9B01713-1', 1295, 7),
(2531, 931, '9B01762-1', 1276, 8),
(2532, 931, '9B01763-1', 1125, 9),
(2533, 931, '9B01763-2', 1144, 10),
(2534, 931, '9B01764-1', 1285, 11),
(2535, 931, '9B01765-2', 1255, 12),
(2536, 931, '9B01766-1', 1270, 13),
(2537, 931, '9B01767-1', 1229, 14),
(2538, 931, '9B01771-1', 1253, 15),
(2539, 931, '9B01772-1', 1285, 16),
(2540, 931, '9D00578-1', 1178, 17),
(2541, 931, '9D00578-2', 1189, 18),
(2542, 931, '9D00579-1', 1082, 19),
(2543, 931, '9D00581-2', 1208, 20),
(2544, 931, '9D00596-1', 1125, 21),
(2545, 932, '4B00050-2', 1945, 1),
(2546, 932, '4B00055-1', 2318, 2),
(2547, 932, '4B00056-1', 2313, 3),
(2548, 932, '4B00057-2', 2914, 4),
(2549, 932, '4B00067-4', 2946, 5),
(2550, 944, '3B90614-1', 2474, 1),
(2551, 944, '3B00002-3', 2926, 2),
(2552, 944, '3B00002-4', 2927, 3),
(2553, 959, '4A00007-3', 2916, 1),
(2554, 959, '4A00011-6', 2711, 2),
(2555, 959, '4A00013-5', 2873, 3),
(2556, 959, '4A00014-3', 2923, 4),
(2557, 980, '3B90599-6', 2993, 1),
(2558, 980, '3B90603-3', 2916, 2),
(2559, 980, '3B90603-4', 2917, 3),
(2560, 983, '11B00058-1', 983, 1),
(2561, 983, '11B00058-2', 1070, 2),
(2562, 983, '11B00061-2', 1004, 3),
(2563, 983, '11B00064-1', 973, 4),
(2564, 983, '11B00064-2', 1088, 5),
(2565, 983, '11B00069-2', 1057, 6),
(2566, 983, '11B00090-1', 1015, 7),
(2567, 983, '11B00091-2', 1047, 8),
(2568, 983, '11B00092-2', 1035, 9),
(2569, 983, '11B00138-1', 894, 10),
(2570, 983, '11B00138-2', 1021, 11),
(2571, 985, '4B00157-1', 2938, 1),
(2572, 985, '4B00162-3', 2924, 2),
(2573, 995, '9B00370-1', 1029, 1),
(2574, 995, '9B00372-1', 1067, 2),
(2575, 995, '9B01678-2', 1290, 3),
(2576, 995, '9B01759-1', 1117, 4),
(2577, 995, '9B01788-1', 1237, 5),
(2578, 995, '9B01854-2', 1180, 6),
(2579, 995, '9B01867-1', 1282, 7),
(2580, 995, '9B01868-1', 1240, 8),
(2581, 995, '9B01870-1', 1192, 9),
(2582, 995, '9B01871-1', 1212, 10),
(2583, 995, '9B01872-1', 1269, 11),
(2584, 995, '9B01873-1', 1253, 12),
(2585, 995, '9D00329-2', 693, 13),
(2586, 995, '9D00330-1', 694, 14),
(2587, 995, '9D00623-1', 1161, 15),
(2588, 995, '9D00623-2', 1134, 16),
(2589, 995, '9D00624-1', 1234, 17),
(2590, 995, '9D00626-1', 1108, 18),
(2591, 995, '9D00627-2', 1193, 19),
(2592, 995, '9D00629-1', 1114, 20),
(2593, 995, '9D00634-1', 1110, 21),
(2594, 995, '9D00634-2', 1211, 22),
(2595, 996, '11B90016-1', 1031, 1),
(2596, 996, '11B90018-2', 1045, 2),
(2597, 996, '11B90020-2', 1013, 3),
(2598, 996, '11B90021-1', 1039, 4),
(2599, 996, '11B90022-2', 960, 5),
(2600, 996, '11B90023-1', 1000, 6),
(2601, 996, '11B90023-2', 1050, 7),
(2602, 996, '11B90024-1', 1067, 8),
(2603, 996, '11B90024-2', 985, 9),
(2604, 996, '11B90025-2', 1027, 10),
(2605, 996, '11B90026-1', 1017, 11),
(2606, 996, '11B90026-2', 1023, 12),
(2607, 996, '11B90027-1', 1028, 13),
(2608, 996, '11B90027-2', 964, 14),
(2609, 996, '11B90030-1', 1006, 15),
(2610, 998, '11B90002-1', 1010, 1),
(2611, 998, '11B90004-2', 1022, 2),
(2612, 998, '11B90016-2', 1020, 3),
(2613, 998, '11B90017-1', 1030, 4),
(2614, 998, '11B90017-2', 1020, 5),
(2615, 998, '11B90018-1', 1007, 6),
(2616, 998, '11B90025-1', 1022, 7),
(2617, 998, '11B90028-1', 1017, 8),
(2618, 998, '11B90028-2', 1024, 9),
(2619, 999, '11B90001-1', 817, 1),
(2620, 999, '11B90001-2', 664, 2),
(2621, 999, '11B90002-2', 1037, 3),
(2622, 999, '11B90003-1', 1005, 4),
(2623, 999, '11B90003-2', 966, 5),
(2624, 999, '11B90004-1', 1030, 6),
(2625, 999, '11B90005-1', 1104, 7),
(2626, 999, '11B90005-2', 950, 8),
(2627, 999, '11B90006-1', 1010, 9),
(2628, 999, '11B90006-2', 1027, 10),
(2629, 999, '11B90012-1', 1019, 11),
(2630, 999, '11B90019-2', 1030, 12),
(2631, 999, '11B90021-2', 1009, 13),
(2632, 999, '11B90022-1', 1004, 14),
(2633, 999, '11B90029-2', 1036, 15),
(2634, 1006, '4B00118-3', 2932, 1),
(2635, 1006, '4B00121-2', 2929, 2),
(2636, 1009, '7A01029-2', 1292, 1),
(2637, 1009, '7A01030-1', 1039, 2),
(2638, 1009, '7A01030-2', 1234, 3),
(2639, 1009, '7A01038-2', 1249, 4),
(2640, 1009, '7A01045-2', 1290, 5),
(2641, 1009, '7A01048-2', 1274, 6),
(2642, 1009, '7A01049-1', 1004, 7),
(2643, 1009, '7A01087-2', 1287, 8),
(2644, 1009, '7A01088-2', 1283, 9),
(2645, 1009, '7A01090-2', 1348, 10),
(2646, 1009, '7A01091-2', 1304, 11),
(2647, 1009, '7A01046-2', 1286, 12),
(2648, 1009, '7A01130-2', 1291, 13),
(2649, 1009, '7A01131-2', 1290, 14),
(2650, 1009, '7A01132-2', 1336, 15),
(2651, 1009, '7A01133-1', 1077, 16),
(2652, 1009, '7A01191-2', 1329, 17),
(2653, 1009, '7A01192-1', 1057, 18),
(2654, 1009, '7A01192-2', 1260, 19),
(2655, 1009, '7A01204-2', 1313, 20),
(2656, 1011, '7A01005-2', 1376, 1),
(2657, 1011, '7A01006-2', 1343, 2),
(2658, 1011, '7A01050-2', 1276, 3),
(2659, 1011, '7A01051-1', 1002, 4),
(2660, 1011, '7A01051-2', 1276, 5),
(2661, 1011, '7A01052-2', 1229, 6),
(2662, 1011, '7A01053-2', 1275, 7),
(2663, 1011, '7A01054-1', 1003, 8),
(2664, 1011, '7A01068-1', 1001, 9),
(2665, 1011, '7A01068-2', 1263, 10),
(2666, 1011, '7A01069-1', 1004, 11),
(2667, 1011, '7A01074-2', 1297, 12),
(2668, 1011, '7A01075-2', 1277, 13),
(2669, 1011, '7A01076-2', 1292, 14),
(2670, 1011, '7A01162-2', 1322, 15),
(2671, 1011, '7A01163-1', 1000, 16),
(2672, 1011, '7A01170-1', 1042, 17),
(2673, 1011, '7A01171-1', 1289, 18),
(2674, 1011, '7A01215-1', 1037, 19),
(2675, 1011, '7A01236-2', 1321, 20),
(2676, 1011, '7A01237-1', 1014, 21),
(2677, 1019, '7A01261-2', 941, 1),
(2678, 1019, '7A01285-2', 1153, 2),
(2679, 1019, '7A01286-1', 1127, 3),
(2680, 1019, '7A01286-2', 1149, 4),
(2681, 1019, '7A01303-2', 1142, 5),
(2682, 1019, '7A01304-1', 1171, 6),
(2683, 1019, '7A01304-2', 1088, 7),
(2684, 1019, '7A01308-1', 1165, 8),
(2685, 1019, '7A01308-2', 1103, 9),
(2686, 1019, '7A01309-2', 1131, 10),
(2687, 1019, '7A01311-2', 1119, 11),
(2688, 1019, '7A01312-1', 1153, 12),
(2689, 1019, '7A01312-2', 1109, 13),
(2690, 1019, '7A01314-1', 1145, 14),
(2691, 1019, '7A01314-2', 1193, 15),
(2692, 1019, '7A01315-1', 1262, 16),
(2693, 1019, '7A01316-2', 1138, 17),
(2694, 1019, '7A01321-2', 1193, 18),
(2695, 1019, '7A01322-1', 1164, 19),
(2696, 1019, '7A01371-2', 1155, 20),
(2697, 1019, '7A01321-1', 1165, 21),
(2698, 1019, '7A01260-2', 1063, 22),
(2699, 1020, '7A01072-1', 989, 1),
(2700, 1020, '7A01078-2', 1292, 2),
(2701, 1020, '7A01079-1', 984, 3),
(2702, 1020, '7A01079-2', 1280, 4),
(2703, 1020, '7A01182-2', 1252, 5),
(2704, 1020, '7A01203-1', 999, 6),
(2705, 1020, '7A01274-2', 1101, 7),
(2706, 1020, '7A01275-1', 1185, 8),
(2707, 1020, '7A01275-2', 1095, 9),
(2708, 1020, '7A01276-2', 1038, 10),
(2709, 1020, '7A01277-1', 1122, 11),
(2710, 1020, '7A01277-2', 1172, 12),
(2711, 1020, '7A01293-1', 1177, 13),
(2712, 1020, '7A01293-2', 1028, 14),
(2713, 1020, '7A01294-1', 1191, 15),
(2714, 1020, '7A01295-2', 1130, 16),
(2715, 1020, '7A01296-1', 1149, 17),
(2716, 1020, '7A01297-1', 1142, 18),
(2717, 1020, '7A01305-1', 1100, 19),
(2718, 1020, '7A01305-2', 1144, 20),
(2719, 1020, '7A01306-1', 1101, 21),
(2720, 1020, '7A01143-2', 1279, 22),
(2721, 1021, '7A00985-1', 1084, 1),
(2722, 1021, '7A00987-2', 1197, 2),
(2723, 1021, '7A00988-1', 1050, 3),
(2724, 1021, '7A00988-2', 1241, 4),
(2725, 1021, '7A00989-2', 1075, 5),
(2726, 1021, '7A00990-1', 1058, 6),
(2727, 1021, '7A00998-2', 1344, 7),
(2728, 1021, '7A01014-2', 1286, 8),
(2729, 1021, '7A01018-2', 1296, 9),
(2730, 1021, '7A01019-2', 1292, 10),
(2731, 1021, '7A01020-2', 1254, 11),
(2732, 1021, '7A01022-1', 1157, 12),
(2733, 1021, '7A01039-1', 1308, 13),
(2734, 1021, '7A01040-2', 1278, 14),
(2735, 1021, '7A01041-1', 1001, 15),
(2736, 1021, '7A01213-2', 1306, 16),
(2737, 1021, '7A01258-1', 1117, 17),
(2738, 1021, '7A01258-2', 1230, 18),
(2739, 1021, '7A01259-1', 1149, 19),
(2740, 1021, '7A01134-2', 1209, 20),
(2741, 1021, '7A01216-1', 1013, 21),
(2742, 1022, '7A01093-1', 984, 1),
(2743, 1022, '7A01103-1', 995, 2),
(2744, 1022, '7A01228-2', 1317, 3),
(2745, 1022, '7A01230-2', 1326, 4),
(2746, 1022, '7A01231-1', 1004, 5),
(2747, 1022, '7A01233-1', 1009, 6),
(2748, 1022, '7A01234-2', 1314, 7),
(2749, 1022, '7A01235-1', 1005, 8),
(2750, 1022, '7A01235-2', 1269, 9),
(2751, 1022, '7A01236-1', 1008, 10),
(2752, 1022, '7A01239-1', 1033, 11),
(2753, 1022, '7A01299-2', 1136, 12),
(2754, 1022, '7A01300-1', 1138, 13),
(2755, 1022, '7A01310-2', 1201, 14),
(2756, 1022, '7A01313-1', 1142, 15),
(2757, 1022, '7A01316-1', 1141, 16),
(2758, 1022, '7A01318-1', 1106, 17),
(2759, 1022, '7A01318-2', 1237, 18),
(2760, 1022, '7A01319-1', 1162, 19),
(2761, 1022, '7A01322-2', 1171, 20),
(2762, 1022, '7A01323-1', 1143, 21),
(2763, 1022, '7A01323-2', 1176, 22),
(2764, 1030, '4B00131-3', 2717, 1),
(2765, 1030, '4B00157-6', 2734, 2);

-- --------------------------------------------------------

--
-- Table structure for table `fstokh`
--

CREATE TABLE `fstokh` (
  `SHID` int(11) NOT NULL,
  `SHIDSD` int(11) DEFAULT NULL,
  `SHIDHPP` int(11) DEFAULT NULL,
  `SHQTY` double DEFAULT '0',
  `SHHARGA` double DEFAULT '0',
  `SHURUTAN` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `fstokh`
--

INSERT INTO `fstokh` (`SHID`, `SHIDSD`, `SHIDHPP`, `SHQTY`, `SHHARGA`, `SHURUTAN`) VALUES
(1, 4, 2, 5, 40000, NULL);

--
-- Triggers `fstokh`
--
DELIMITER $$
CREATE TRIGGER `fstokh_dell` BEFORE DELETE ON `fstokh` FOR EACH ROW begin 
  DECLARE tmpVar INTEGER; 

          update fstokd set SDPAKAIHPP=SDPAKAIHPP-OLD.SHQTY where sdid = OLD.SHIDHPP ;
end
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `fstokopnamed`
--

CREATE TABLE `fstokopnamed` (
  `SODID` int(11) NOT NULL,
  `SODIDSOU` int(11) NOT NULL,
  `SODURUTAN` int(11) NOT NULL,
  `SODITEM` int(11) DEFAULT NULL,
  `SODQTY` double NOT NULL DEFAULT '0',
  `SODSATUAN` int(11) DEFAULT NULL,
  `SODQTYD` double NOT NULL DEFAULT '0',
  `SODSATUAND` int(11) DEFAULT NULL,
  `SODCATATAN` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `fstokopnameu`
--

CREATE TABLE `fstokopnameu` (
  `SOUID` int(11) NOT NULL,
  `SOUSUMBER` varchar(5) NOT NULL,
  `SOUNOTRANSAKSI` varchar(50) DEFAULT NULL,
  `SOUTANGGAL` date DEFAULT NULL,
  `SOUKONTAK` int(11) NOT NULL,
  `SOUURAIAN` varchar(255) DEFAULT NULL,
  `SOUGUDANG` int(11) DEFAULT NULL,
  `SOUNOREF` varchar(25) DEFAULT NULL,
  `SOUCREATED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `SOUCREATEU` int(11) DEFAULT NULL,
  `SOUMODIFD` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `SOUMODIFU` int(11) DEFAULT NULL,
  `SOUSTATUS` int(11) DEFAULT NULL,
  `SOUTOTALTRANSAKSI` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `fstokpergudang`
--

CREATE TABLE `fstokpergudang` (
  `SPGID` int(11) NOT NULL,
  `SPGITEM` int(11) DEFAULT NULL,
  `SPGGUDANG` int(11) DEFAULT NULL,
  `SPGQTY` double DEFAULT '0',
  `SPGNILAI` double DEFAULT '0',
  `SPGGUDANG1` double DEFAULT '0',
  `SPGGUDANG2` double DEFAULT '0',
  `SPGGUDANG3` double DEFAULT '0',
  `SPGGUDANG4` double DEFAULT '0',
  `SPGGUDANG5` double DEFAULT '0',
  `SPGGUDANG6` double DEFAULT '0',
  `SPGGUDANG7` double DEFAULT '0',
  `SPGGUDANG8` double DEFAULT '0',
  `SPGGUDANG9` double DEFAULT '0',
  `SPGGUDANG10` double DEFAULT '0',
  `SPGGUDANG11` double DEFAULT '0',
  `SPGGUDANG12` double DEFAULT '0',
  `SPGGUDANG13` double DEFAULT '0',
  `SPGGUDANG14` double DEFAULT '0',
  `SPGGUDANG15` double DEFAULT '0',
  `SPGGUDANG16` double DEFAULT '0',
  `SPGGUDANG17` double DEFAULT '0',
  `SPGGUDANG18` double DEFAULT '0',
  `SPGGUDANG19` double DEFAULT '0',
  `SPGGUDANG20` double DEFAULT '0',
  `SPGGUDANG21` double DEFAULT '0',
  `SPGGUDANG22` double DEFAULT '0',
  `SPGGUDANG23` double DEFAULT '0',
  `SPGGUDANG24` double DEFAULT '0',
  `SPGGUDANG25` double DEFAULT '0',
  `SPGGUDANG26` double DEFAULT '0',
  `SPGGUDANG27` double DEFAULT '0',
  `SPGGUDANG28` double DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `fstoksjd`
--

CREATE TABLE `fstoksjd` (
  `SDID` int(11) NOT NULL,
  `SDIDSU` int(11) NOT NULL,
  `SDURUTAN` int(11) NOT NULL,
  `SDSUMBER` varchar(5) DEFAULT NULL,
  `SDITEM` int(11) DEFAULT NULL,
  `SDMASUK` double DEFAULT '0',
  `SDKELUAR` double DEFAULT '0',
  `SDHARGA` double DEFAULT '0',
  `SDDISKON` double DEFAULT '0',
  `SDSATUAN` int(11) DEFAULT NULL,
  `SDSATUAND` int(11) DEFAULT NULL,
  `SDMASUKD` double DEFAULT '0',
  `SDKELUARD` double DEFAULT '0',
  `SDCATATAN` varchar(255) DEFAULT NULL,
  `SDDIVISI` int(11) DEFAULT NULL,
  `SDPROYEK` int(11) DEFAULT NULL,
  `SDGUDANG` int(11) DEFAULT NULL,
  `SDCATATANKOLI` varchar(255) DEFAULT NULL,
  `SDPBDID` int(11) DEFAULT NULL,
  `SDPBDURUTAN` int(11) DEFAULT NULL,
  `SDQTPAKAI` double DEFAULT '0',
  `SDSODID` int(11) DEFAULT NULL,
  `SDSODURUTAN` int(11) DEFAULT NULL,
  `SDPRDID` int(11) DEFAULT NULL,
  `SDRETUR` double DEFAULT '0',
  `SDBIAYAHARGA` double DEFAULT '0',
  `SDBIAYAIDPENERIMAAN` smallint(6) DEFAULT NULL,
  `SDBIAYAQTY` double DEFAULT '0',
  `SDBIAYAHARGABARANG` double DEFAULT '0',
  `SDBELI` double DEFAULT '0',
  `SDHARGAINVOICE` double DEFAULT '0',
  `SDDISKONINVOICE` double DEFAULT '0',
  `SDDISKONPERSEN` double DEFAULT '0',
  `SDKARYAWAN` int(11) DEFAULT NULL,
  `SDTINDAKAN` smallint(6) DEFAULT '0',
  `SDDIPAKAI` smallint(6) DEFAULT '0',
  `SDIDTINDAKAN1` int(11) DEFAULT NULL,
  `SDPENDING` smallint(6) DEFAULT '0',
  `SDNOREF` varchar(255) DEFAULT NULL,
  `SDKEDATANGAN` int(11) DEFAULT '0',
  `SDDOKTER` int(11) DEFAULT NULL,
  `SDDISKONPERSEN2` double DEFAULT '0',
  `SDDISKON2` double DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `fstoksju`
--

CREATE TABLE `fstoksju` (
  `SUID` int(11) NOT NULL,
  `SUNOTRANSAKSI` varchar(50) DEFAULT NULL,
  `SUTANGGAL` date DEFAULT NULL,
  `SUKONTAK` int(11) DEFAULT NULL,
  `SUURAIAN` varchar(255) DEFAULT NULL,
  `SUTOTALTRANSAKSI` double DEFAULT '0',
  `SUUANG` int(11) DEFAULT NULL,
  `SUVALAS` double DEFAULT '0',
  `SUKURS` double DEFAULT '0',
  `SUCREATEU` int(11) DEFAULT NULL,
  `SUMODIFU` int(11) DEFAULT NULL,
  `SUMODIFD` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `SUKARYAWAN` int(11) DEFAULT NULL,
  `SUNOREF` varchar(25) DEFAULT NULL,
  `SUSUMBER` varchar(5) DEFAULT NULL,
  `SUJENISPENYESUAIAN` int(11) DEFAULT NULL,
  `SUNOREFTRANSAKSI` int(11) DEFAULT NULL,
  `SUCATATAN` varchar(255) DEFAULT NULL,
  `SUATTENTION` varchar(50) DEFAULT NULL,
  `SUALAMAT` varchar(255) DEFAULT NULL,
  `SUEKSPEDISI` int(11) DEFAULT NULL,
  `SUSTATUSKIRIM` int(11) DEFAULT '0',
  `SUCATATANEKSPEDISI` varchar(255) DEFAULT NULL,
  `SUPBUID` int(11) DEFAULT NULL,
  `SUSTATUS` smallint(6) DEFAULT '0',
  `SUSOUID` int(11) DEFAULT NULL,
  `SUTGLKIRIM` date DEFAULT NULL,
  `SUPAJAK` int(11) DEFAULT '0',
  `SUNOFAKTURPAJAK` varchar(50) DEFAULT NULL,
  `SUPRUID` int(11) DEFAULT NULL,
  `SUTOTALKAS` double DEFAULT '0',
  `SUTOTALKARTUDEBIT` double DEFAULT '0',
  `SUTOTALKARTUKREDIT` double DEFAULT '0',
  `SUNOKARTUDEBIT` varchar(50) DEFAULT NULL,
  `SUNAMADEBIT` varchar(50) DEFAULT NULL,
  `SUBANKDEBIT` varchar(50) DEFAULT NULL,
  `SUNOKARTUKREDIT` varchar(50) DEFAULT NULL,
  `SUNAMAKREDIT` varchar(50) DEFAULT NULL,
  `SUBANKKREDIT` varchar(50) DEFAULT NULL,
  `SUTOTALBAYAR` double DEFAULT '0',
  `SUTOTALSISA` double DEFAULT '0',
  `SUDPID` int(11) DEFAULT NULL,
  `SUTOTALDP` double DEFAULT '0',
  `SUBIAYAKIRIM` smallint(6) DEFAULT '0',
  `SUREKHUTANG` int(11) DEFAULT NULL,
  `SUNOPENERIMAAN` int(11) DEFAULT NULL,
  `SUCOAPIUTANG` int(11) DEFAULT NULL,
  `SUKONSINYASI` smallint(6) DEFAULT '0',
  `SUGUDANGTUJUAN` int(11) DEFAULT NULL,
  `SUGUDANGASAL` int(11) DEFAULT NULL,
  `SUTERMIN` int(11) DEFAULT NULL,
  `SUTGLJATUHTEMPO` date DEFAULT NULL,
  `SUJUMLAHDP` double DEFAULT '0',
  `SUAKUNDP` int(11) DEFAULT NULL,
  `SUIDDP` int(11) DEFAULT NULL,
  `SUSALDOAWAL` smallint(6) DEFAULT '0',
  `SUTOTALPAJAK` double DEFAULT '0',
  `SUTGLPAJAK` date DEFAULT NULL,
  `SUTOTALTRANSFER` double DEFAULT '0',
  `SUNOTRANSFER` varchar(50) DEFAULT NULL,
  `SUBANKTRANSFER` varchar(50) DEFAULT NULL,
  `SUNAMATRANSFER` varchar(50) DEFAULT NULL,
  `SUTOTALVOUCHER` double DEFAULT '0',
  `SUNOVOUCHER` varchar(50) DEFAULT NULL,
  `SUPROGRAMVOUCHER` varchar(50) DEFAULT NULL,
  `SUNAMAVOUCHER` varchar(50) DEFAULT NULL,
  `SUIDUPOS` int(11) DEFAULT NULL,
  `SUNOSO` int(11) DEFAULT NULL,
  `SUDOKTER` varchar(255) DEFAULT NULL,
  `SUCREATED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `SUCABANG` int(11) DEFAULT '0',
  `SUTIDAKBAWAKARTU` smallint(6) DEFAULT '0',
  `SUNILAIPAJAK` smallint(6) DEFAULT '0',
  `SUNOTRANSAKSIPUSAT` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `fstoku`
--

CREATE TABLE `fstoku` (
  `SUID` int(11) NOT NULL,
  `SUNOTRANSAKSI` varchar(50) DEFAULT NULL,
  `SUTANGGAL` date DEFAULT NULL,
  `SUKONTAK` int(11) DEFAULT NULL,
  `SUURAIAN` varchar(255) DEFAULT NULL,
  `SUTOTALTRANSAKSI` double DEFAULT '0',
  `SUUANG` int(11) DEFAULT NULL,
  `SUVALAS` double DEFAULT '0',
  `SUKURS` double DEFAULT '0',
  `SUCREATEU` int(11) DEFAULT NULL,
  `SUMODIFU` int(11) DEFAULT NULL,
  `SUMODIFD` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `SUKARYAWAN` int(11) DEFAULT NULL,
  `SUNOREF` varchar(25) DEFAULT NULL,
  `SUSUMBER` varchar(5) DEFAULT NULL,
  `SUJENISPENYESUAIAN` int(11) DEFAULT NULL,
  `SUNOREFTRANSAKSI` int(11) DEFAULT NULL,
  `SUCATATAN` varchar(255) DEFAULT NULL,
  `SUATTENTION` varchar(50) DEFAULT NULL,
  `SUALAMAT` varchar(255) DEFAULT NULL,
  `SUEKSPEDISI` int(11) DEFAULT NULL,
  `SUSTATUSKIRIM` int(11) DEFAULT '0',
  `SUCATATANEKSPEDISI` varchar(255) DEFAULT NULL,
  `SUPBUID` int(11) DEFAULT NULL,
  `SUSTATUS` smallint(6) DEFAULT '0',
  `SUSOUID` int(11) DEFAULT NULL,
  `SUTGLKIRIM` date DEFAULT NULL,
  `SUPAJAK` int(11) DEFAULT '0',
  `SUNOFAKTURPAJAK` varchar(50) DEFAULT NULL,
  `SUPRUID` int(11) DEFAULT NULL,
  `SUTOTALKAS` double DEFAULT '0',
  `SUTOTALKARTUDEBIT` double DEFAULT '0',
  `SUTOTALKARTUKREDIT` double DEFAULT '0',
  `SUNOKARTUDEBIT` varchar(50) DEFAULT NULL,
  `SUNAMADEBIT` varchar(50) DEFAULT NULL,
  `SUBANKDEBIT` varchar(50) DEFAULT NULL,
  `SUNOKARTUKREDIT` varchar(50) DEFAULT NULL,
  `SUNAMAKREDIT` varchar(50) DEFAULT NULL,
  `SUBANKKREDIT` varchar(50) DEFAULT NULL,
  `SUTOTALBAYAR` double DEFAULT '0',
  `SUTOTALSISA` double DEFAULT '0',
  `SUDPID` int(11) DEFAULT NULL,
  `SUTOTALDP` double DEFAULT '0',
  `SUBIAYAKIRIM` smallint(6) DEFAULT '0',
  `SUREKHUTANG` int(11) DEFAULT NULL,
  `SUNOPENERIMAAN` int(11) DEFAULT NULL,
  `SUCOAPIUTANG` int(11) DEFAULT NULL,
  `SUKONSINYASI` smallint(6) DEFAULT '0',
  `SUGUDANGTUJUAN` int(11) DEFAULT NULL,
  `SUGUDANGASAL` int(11) DEFAULT NULL,
  `SUTERMIN` int(11) DEFAULT NULL,
  `SUTGLJATUHTEMPO` date DEFAULT NULL,
  `SUJUMLAHDP` double DEFAULT '0',
  `SUAKUNDP` int(11) DEFAULT NULL,
  `SUIDDP` int(11) DEFAULT NULL,
  `SUSALDOAWAL` smallint(6) DEFAULT '0',
  `SUTOTALPAJAK` double DEFAULT '0',
  `SUTGLPAJAK` date DEFAULT NULL,
  `SUTOTALTRANSFER` double DEFAULT '0',
  `SUNOTRANSFER` varchar(50) DEFAULT NULL,
  `SUBANKTRANSFER` varchar(50) DEFAULT NULL,
  `SUNAMATRANSFER` varchar(50) DEFAULT NULL,
  `SUTOTALVOUCHER` double DEFAULT '0',
  `SUNOVOUCHER` varchar(50) DEFAULT NULL,
  `SUPROGRAMVOUCHER` varchar(50) DEFAULT NULL,
  `SUNAMAVOUCHER` varchar(50) DEFAULT NULL,
  `SUIDUPOS` int(11) DEFAULT NULL,
  `SUNOSO` int(11) DEFAULT NULL,
  `SUDOKTER` varchar(255) DEFAULT NULL,
  `SUCREATED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `SUCABANG` int(11) DEFAULT '0',
  `SUTIDAKBAWAKARTU` smallint(6) DEFAULT '0',
  `SUNILAIPAJAK` smallint(6) DEFAULT '0',
  `SUNOTRANSAKSIPUSAT` varchar(50) DEFAULT NULL,
  `SUKREDITJUMLAHCICILAN` double DEFAULT '0',
  `SUKREDITCICILANPERBULAN` double DEFAULT '0',
  `SUKREDITJENIS` varchar(100) DEFAULT NULL,
  `SUDEBITJENIS` varchar(100) DEFAULT NULL,
  `SUDPID2` int(11) DEFAULT NULL,
  `SUDPID3` int(11) DEFAULT NULL,
  `SUDP1` double DEFAULT '0',
  `SUDP2` double DEFAULT '0',
  `SUDP3` double DEFAULT '0',
  `SUIDPOTONGSTOK` int(11) DEFAULT NULL,
  `SUPROMO` varchar(255) DEFAULT NULL,
  `SUTOTALBIAYA` double DEFAULT '0',
  `SUPENGIRIM` int(11) DEFAULT NULL,
  `SUVIA` int(11) DEFAULT NULL,
  `SUHP` int(11) DEFAULT NULL,
  `SUJENIS` int(11) DEFAULT '0',
  `SUPOSTING` int(11) NOT NULL DEFAULT '0',
  `SUDIPAKAIHPP` int(11) DEFAULT NULL,
  `SUTOTALPPH22` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `fstoku`
--

INSERT INTO `fstoku` (`SUID`, `SUNOTRANSAKSI`, `SUTANGGAL`, `SUKONTAK`, `SUURAIAN`, `SUTOTALTRANSAKSI`, `SUUANG`, `SUVALAS`, `SUKURS`, `SUCREATEU`, `SUMODIFU`, `SUMODIFD`, `SUKARYAWAN`, `SUNOREF`, `SUSUMBER`, `SUJENISPENYESUAIAN`, `SUNOREFTRANSAKSI`, `SUCATATAN`, `SUATTENTION`, `SUALAMAT`, `SUEKSPEDISI`, `SUSTATUSKIRIM`, `SUCATATANEKSPEDISI`, `SUPBUID`, `SUSTATUS`, `SUSOUID`, `SUTGLKIRIM`, `SUPAJAK`, `SUNOFAKTURPAJAK`, `SUPRUID`, `SUTOTALKAS`, `SUTOTALKARTUDEBIT`, `SUTOTALKARTUKREDIT`, `SUNOKARTUDEBIT`, `SUNAMADEBIT`, `SUBANKDEBIT`, `SUNOKARTUKREDIT`, `SUNAMAKREDIT`, `SUBANKKREDIT`, `SUTOTALBAYAR`, `SUTOTALSISA`, `SUDPID`, `SUTOTALDP`, `SUBIAYAKIRIM`, `SUREKHUTANG`, `SUNOPENERIMAAN`, `SUCOAPIUTANG`, `SUKONSINYASI`, `SUGUDANGTUJUAN`, `SUGUDANGASAL`, `SUTERMIN`, `SUTGLJATUHTEMPO`, `SUJUMLAHDP`, `SUAKUNDP`, `SUIDDP`, `SUSALDOAWAL`, `SUTOTALPAJAK`, `SUTGLPAJAK`, `SUTOTALTRANSFER`, `SUNOTRANSFER`, `SUBANKTRANSFER`, `SUNAMATRANSFER`, `SUTOTALVOUCHER`, `SUNOVOUCHER`, `SUPROGRAMVOUCHER`, `SUNAMAVOUCHER`, `SUIDUPOS`, `SUNOSO`, `SUDOKTER`, `SUCREATED`, `SUCABANG`, `SUTIDAKBAWAKARTU`, `SUNILAIPAJAK`, `SUNOTRANSAKSIPUSAT`, `SUKREDITJUMLAHCICILAN`, `SUKREDITCICILANPERBULAN`, `SUKREDITJENIS`, `SUDEBITJENIS`, `SUDPID2`, `SUDPID3`, `SUDP1`, `SUDP2`, `SUDP3`, `SUIDPOTONGSTOK`, `SUPROMO`, `SUTOTALBIAYA`, `SUPENGIRIM`, `SUVIA`, `SUHP`, `SUJENIS`, `SUPOSTING`, `SUDIPAKAIHPP`, `SUTOTALPPH22`) VALUES
(1, 'SAIT21120001', '2021-12-31', 6, 'SALDO AWAL', 5000000, NULL, 0, 0, 1, NULL, '2022-01-05 00:44:55', NULL, NULL, 'SAIT', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, NULL, 0, 13, NULL, NULL, NULL, 0, NULL, NULL, 1, 0, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-05 00:44:55', 0, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, NULL, 0),
(2, 'SAIT21120002', '2021-12-31', 6, 'SALDO AWAL', 2000000, NULL, 0, 0, 1, NULL, '2022-01-05 00:49:45', NULL, NULL, 'SAIT', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, NULL, 0, 13, NULL, NULL, NULL, 0, NULL, NULL, 1, 0, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-05 00:49:45', 0, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, NULL, 0),
(3, 'SAIT21120003', '2021-12-31', 6, 'SALDO AWAL', 5000000, NULL, 0, 0, 1, NULL, '2022-01-05 00:50:37', NULL, NULL, 'SAIT', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, NULL, 0, 13, NULL, NULL, NULL, 0, NULL, NULL, 1, 0, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-05 00:50:37', 0, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, NULL, 0),
(4, 'IV22010001', '2022-01-05', 1, 'Tes Sales 1', 550000, NULL, 0, 0, 1, NULL, '2022-01-05 00:56:39', 6, NULL, 'IV', NULL, NULL, NULL, '', '', NULL, 0, NULL, NULL, 3, NULL, NULL, 1, '010.000-00.00000001', NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 350000, NULL, 200000, 0, NULL, NULL, NULL, 0, 0, NULL, 4, NULL, 0, NULL, NULL, 0, 50000, '2022-01-05', 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-05 00:56:39', 0, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, NULL, 0),
(5, 'SAIT21120004', '2021-12-31', 6, 'SALDO AWAL', 3000000, NULL, 0, 0, 1, NULL, '2022-01-05 01:13:06', NULL, NULL, 'SAIT', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, NULL, 0, 13, NULL, NULL, NULL, 0, NULL, NULL, 1, 0, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-05 01:13:06', 0, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, NULL, 0),
(6, 'SAIT21120005', '2021-12-31', 6, 'SALDO AWAL', 7000000, NULL, 0, 0, 1, NULL, '2022-01-05 01:14:20', NULL, NULL, 'SAIT', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, NULL, 0, 13, NULL, NULL, NULL, 0, NULL, NULL, 1, 0, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-05 01:14:20', 0, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, NULL, 0),
(7, 'SAIT21120006', '2021-12-31', 6, 'SALDO AWAL', 5000000, NULL, 0, 0, 1, NULL, '2022-01-05 01:16:12', NULL, NULL, 'SAIT', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, NULL, 0, 13, NULL, NULL, NULL, 0, NULL, NULL, 1, 0, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-05 01:16:12', 0, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, NULL, 0);

--
-- Triggers `fstoku`
--
DELIMITER $$
CREATE TRIGGER `fstoku_dell` AFTER DELETE ON `fstoku` FOR EACH ROW BEGIN
	delete from fstokd where SDIDSU=OLD.SUID;
end
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `gstokd`
--

CREATE TABLE `gstokd` (
  `SDCETAK` smallint(6) DEFAULT '0',
  `SDID` int(11) NOT NULL,
  `SDIDSU` int(11) NOT NULL,
  `SDURUTAN` int(11) NOT NULL,
  `SDSUMBER` varchar(5) DEFAULT NULL,
  `SDITEM` int(11) DEFAULT NULL,
  `SDMASUK` double DEFAULT '0',
  `SDKELUAR` double DEFAULT '0',
  `SDHARGA` double DEFAULT '0',
  `SDDISKON` double DEFAULT '0',
  `SDSATUAN` int(11) DEFAULT NULL,
  `SDSATUAND` int(11) DEFAULT NULL,
  `SDMASUKD` double DEFAULT '0',
  `SDKELUARD` double DEFAULT '0',
  `SDCATATAN` varchar(255) DEFAULT NULL,
  `SDDIVISI` int(11) DEFAULT NULL,
  `SDPROYEK` int(11) DEFAULT NULL,
  `SDGUDANG` int(11) DEFAULT NULL,
  `SDCATATANKOLI` varchar(255) DEFAULT NULL,
  `SDPBDID` int(11) DEFAULT NULL,
  `SDPBDURUTAN` int(11) DEFAULT NULL,
  `SDQTPAKAI` double DEFAULT '0',
  `SDSODID` int(11) DEFAULT NULL,
  `SDSODURUTAN` int(11) DEFAULT NULL,
  `SDPRDID` int(11) DEFAULT NULL,
  `SDRETUR` double DEFAULT '0',
  `SDBIAYAHARGA` double DEFAULT '0',
  `SDBIAYAIDPENERIMAAN` smallint(6) DEFAULT NULL,
  `SDBIAYAQTY` double DEFAULT '0',
  `SDBIAYAHARGABARANG` double DEFAULT '0',
  `SDBELI` double DEFAULT '0',
  `SDHARGAINVOICE` double DEFAULT '0',
  `SDDISKONINVOICE` double DEFAULT '0',
  `SDDISKONPERSEN` double DEFAULT '0',
  `SDKARYAWAN` int(11) DEFAULT NULL,
  `SDTINDAKAN` smallint(6) DEFAULT '0',
  `SDDIPAKAI` double DEFAULT '1',
  `SDIDTINDAKAN1` int(11) DEFAULT NULL,
  `SDPENDING` smallint(6) DEFAULT '0',
  `SDNOREF` varchar(255) DEFAULT NULL,
  `SDKEDATANGAN` int(11) DEFAULT '0',
  `SDDOKTER` int(11) DEFAULT NULL,
  `SDDISKONPERSEN2` double DEFAULT '0',
  `SDDISKON2` double DEFAULT '0',
  `SDPENGULANGAN` smallint(6) DEFAULT '0',
  `SDBAYARDP` double DEFAULT '0',
  `SDQTYBOX` double DEFAULT '0',
  `SDLANTAI2` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `gstoku`
--

CREATE TABLE `gstoku` (
  `SUID` int(11) NOT NULL,
  `SUNOTRANSAKSI` varchar(50) DEFAULT NULL,
  `SUTANGGAL` date DEFAULT NULL,
  `SUKONTAK` int(11) DEFAULT NULL,
  `SUURAIAN` varchar(255) DEFAULT NULL,
  `SUTOTALTRANSAKSI` double DEFAULT '0',
  `SUUANG` int(11) DEFAULT NULL,
  `SUVALAS` double DEFAULT '0',
  `SUKURS` double DEFAULT '0',
  `SUCREATEU` int(11) DEFAULT NULL,
  `SUMODIFU` int(11) DEFAULT NULL,
  `SUMODIFD` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `SUKARYAWAN` int(11) DEFAULT NULL,
  `SUNOREF` varchar(25) DEFAULT NULL,
  `SUSUMBER` varchar(5) DEFAULT NULL,
  `SUJENISPENYESUAIAN` int(11) DEFAULT NULL,
  `SUNOREFTRANSAKSI` int(11) DEFAULT NULL,
  `SUCATATAN` varchar(255) DEFAULT NULL,
  `SUATTENTION` varchar(50) DEFAULT NULL,
  `SUALAMAT` varchar(255) DEFAULT NULL,
  `SUEKSPEDISI` int(11) DEFAULT NULL,
  `SUSTATUSKIRIM` int(11) DEFAULT '0',
  `SUCATATANEKSPEDISI` varchar(255) DEFAULT NULL,
  `SUPBUID` int(11) DEFAULT NULL,
  `SUSTATUS` smallint(6) DEFAULT '0',
  `SUSOUID` int(11) DEFAULT NULL,
  `SUTGLKIRIM` date DEFAULT NULL,
  `SUPAJAK` int(11) DEFAULT '0',
  `SUNOFAKTURPAJAK` varchar(50) DEFAULT NULL,
  `SUPRUID` int(11) DEFAULT NULL,
  `SUTOTALKAS` double DEFAULT '0',
  `SUTOTALKARTUDEBIT` double DEFAULT '0',
  `SUTOTALKARTUKREDIT` double DEFAULT '0',
  `SUNOKARTUDEBIT` varchar(50) DEFAULT NULL,
  `SUNAMADEBIT` varchar(50) DEFAULT NULL,
  `SUBANKDEBIT` varchar(50) DEFAULT NULL,
  `SUNOKARTUKREDIT` varchar(50) DEFAULT NULL,
  `SUNAMAKREDIT` varchar(50) DEFAULT NULL,
  `SUBANKKREDIT` varchar(50) DEFAULT NULL,
  `SUTOTALBAYAR` double DEFAULT '0',
  `SUTOTALSISA` double DEFAULT '0',
  `SUDPID` int(11) DEFAULT NULL,
  `SUTOTALDP` double DEFAULT '0',
  `SUBIAYAKIRIM` smallint(6) DEFAULT '0',
  `SUREKHUTANG` int(11) DEFAULT NULL,
  `SUNOPENERIMAAN` int(11) DEFAULT NULL,
  `SUCOAPIUTANG` int(11) DEFAULT NULL,
  `SUKONSINYASI` smallint(6) DEFAULT '0',
  `SUGUDANGTUJUAN` int(11) DEFAULT NULL,
  `SUGUDANGASAL` int(11) DEFAULT NULL,
  `SUTERMIN` int(11) DEFAULT NULL,
  `SUTGLJATUHTEMPO` date DEFAULT NULL,
  `SUJUMLAHDP` double DEFAULT '0',
  `SUAKUNDP` int(11) DEFAULT NULL,
  `SUIDDP` int(11) DEFAULT NULL,
  `SUSALDOAWAL` smallint(6) DEFAULT '0',
  `SUTOTALPAJAK` double DEFAULT '0',
  `SUTGLPAJAK` date DEFAULT NULL,
  `SUTOTALTRANSFER` double DEFAULT '0',
  `SUNOTRANSFER` varchar(50) DEFAULT NULL,
  `SUBANKTRANSFER` varchar(50) DEFAULT NULL,
  `SUNAMATRANSFER` varchar(50) DEFAULT NULL,
  `SUTOTALVOUCHER` double DEFAULT '0',
  `SUNOVOUCHER` varchar(50) DEFAULT NULL,
  `SUPROGRAMVOUCHER` varchar(50) DEFAULT NULL,
  `SUNAMAVOUCHER` varchar(50) DEFAULT NULL,
  `SUIDUPOS` int(11) DEFAULT NULL,
  `SUNOSO` int(11) DEFAULT NULL,
  `SUDOKTER` varchar(255) DEFAULT NULL,
  `SUCREATED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `SUCABANG` int(11) DEFAULT '0',
  `SUTIDAKBAWAKARTU` smallint(6) DEFAULT '0',
  `SUNILAIPAJAK` smallint(6) DEFAULT '0',
  `SUNOTRANSAKSIPUSAT` varchar(50) DEFAULT NULL,
  `SUKREDITJUMLAHCICILAN` double DEFAULT '0',
  `SUKREDITCICILANPERBULAN` double DEFAULT '0',
  `SUKREDITJENIS` varchar(100) DEFAULT NULL,
  `SUDEBITJENIS` varchar(100) DEFAULT NULL,
  `SUDPID2` int(11) DEFAULT NULL,
  `SUDPID3` int(11) DEFAULT NULL,
  `SUDP1` double DEFAULT '0',
  `SUDP2` double DEFAULT '0',
  `SUDP3` double DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tmp_bb`
--

CREATE TABLE `tmp_bb` (
  `id` int(11) DEFAULT NULL,
  `link` varchar(150) DEFAULT NULL,
  `captionlink` varchar(255) DEFAULT NULL,
  `nomor` varchar(100) DEFAULT NULL,
  `sumber` varchar(20) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `kontak` varchar(255) DEFAULT NULL,
  `uraian` varchar(255) DEFAULT NULL,
  `debit` double DEFAULT NULL,
  `kredit` double DEFAULT NULL,
  `saldo` double DEFAULT NULL,
  `idcoa` varchar(150) DEFAULT NULL,
  `coa` varchar(255) DEFAULT NULL,
  `cid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ainfo`
--
ALTER TABLE `ainfo`
  ADD PRIMARY KEY (`iid`);

--
-- Indexes for table `amenu`
--
ALTER TABLE `amenu`
  ADD PRIMARY KEY (`MID`);

--
-- Indexes for table `anomor`
--
ALTER TABLE `anomor`
  ADD PRIMARY KEY (`NID`),
  ADD UNIQUE KEY `U_anomor` (`NKODE`);

--
-- Indexes for table `areport`
--
ALTER TABLE `areport`
  ADD PRIMARY KEY (`ARID`);

--
-- Indexes for table `arole`
--
ALTER TABLE `arole`
  ADD PRIMARY KEY (`ARID`),
  ADD KEY `FK_arole_u` (`ARIDMENU`);

--
-- Indexes for table `aserial`
--
ALTER TABLE `aserial`
  ADD PRIMARY KEY (`AID`);

--
-- Indexes for table `auser`
--
ALTER TABLE `auser`
  ADD PRIMARY KEY (`uid`),
  ADD UNIQUE KEY `idx_kode_user` (`ukode`);

--
-- Indexes for table `ausercoa`
--
ALTER TABLE `ausercoa`
  ADD PRIMARY KEY (`AUCID`);

--
-- Indexes for table `auserlog`
--
ALTER TABLE `auserlog`
  ADD PRIMARY KEY (`ULID`);

--
-- Indexes for table `ausermenu`
--
ALTER TABLE `ausermenu`
  ADD PRIMARY KEY (`AUID`),
  ADD KEY `FK_ausermenu_auser` (`AUIDUSER`);

--
-- Indexes for table `auuserrole`
--
ALTER TABLE `auuserrole`
  ADD PRIMARY KEY (`AURID`),
  ADD KEY `FK_auuserrole_menu` (`AURIDMENU`),
  ADD KEY `FK_auuserrole_user` (`AURIDUSER`);

--
-- Indexes for table `baktiva`
--
ALTER TABLE `baktiva`
  ADD PRIMARY KEY (`AID`),
  ADD KEY `FK_baktiva_baktivakelompok` (`AKELOMPOK`);

--
-- Indexes for table `baktivafiles`
--
ALTER TABLE `baktivafiles`
  ADD PRIMARY KEY (`AFID`),
  ADD KEY `FK_baktivafiles_baktiva` (`AFIDA`);

--
-- Indexes for table `baktivakelompok`
--
ALTER TABLE `baktivakelompok`
  ADD PRIMARY KEY (`AKID`),
  ADD UNIQUE KEY `U_baktivakelompok` (`AKKODE`),
  ADD KEY `FK_baktivakelompok_bcoa` (`AKCOADEPRESIASI`),
  ADD KEY `FK_baktivakelompok_coaaktiva` (`AKCOAAKTIVA`),
  ADD KEY `FK_baktivakelompok_coaakum` (`AKCOADEPRESIASIAKUM`),
  ADD KEY `FK_baktivakelompok_coa_writeoff` (`AKCOAWRITEOFF`);

--
-- Indexes for table `baktivapenyusutan`
--
ALTER TABLE `baktivapenyusutan`
  ADD PRIMARY KEY (`APID`),
  ADD UNIQUE KEY `idx_apida_tahun_bulan` (`APIDA`,`APTAHUN`,`APBULAN`);

--
-- Indexes for table `bbank`
--
ALTER TABLE `bbank`
  ADD PRIMARY KEY (`BID`),
  ADD UNIQUE KEY `U_bbank` (`BKODE`),
  ADD KEY `FK_bbank_create` (`BCREATEU`),
  ADD KEY `FK_bbank_modif` (`BMODIFU`);

--
-- Indexes for table `bcabang`
--
ALTER TABLE `bcabang`
  ADD PRIMARY KEY (`CID`);

--
-- Indexes for table `bcoa`
--
ALTER TABLE `bcoa`
  ADD PRIMARY KEY (`CID`),
  ADD UNIQUE KEY `U_bcoa` (`CNOCOA`),
  ADD KEY `FK_bcoa_create` (`CCREATEU`),
  ADD KEY `FK_bcoa_modif` (`CMODIFU`);

--
-- Indexes for table `bcoagrup`
--
ALTER TABLE `bcoagrup`
  ADD PRIMARY KEY (`CGID`);

--
-- Indexes for table `bcoasa`
--
ALTER TABLE `bcoasa`
  ADD PRIMARY KEY (`CSID`);

--
-- Indexes for table `bdivisi`
--
ALTER TABLE `bdivisi`
  ADD PRIMARY KEY (`DID`),
  ADD UNIQUE KEY `idx_dkode` (`DKODE`);

--
-- Indexes for table `bgudang`
--
ALTER TABLE `bgudang`
  ADD PRIMARY KEY (`GID`);

--
-- Indexes for table `bitem`
--
ALTER TABLE `bitem`
  ADD PRIMARY KEY (`IID`),
  ADD UNIQUE KEY `U_bitem` (`IKODE`),
  ADD KEY `FK_bitem_create` (`ICREATEU`),
  ADD KEY `FK_bitem_modif` (`IMODIFU`);

--
-- Indexes for table `bitemfiles`
--
ALTER TABLE `bitemfiles`
  ADD PRIMARY KEY (`BFID`),
  ADD KEY `FK_bitemfiles_bitem` (`BFIDI`);

--
-- Indexes for table `bitemkategori`
--
ALTER TABLE `bitemkategori`
  ADD PRIMARY KEY (`IKID`);

--
-- Indexes for table `bitempenyusun`
--
ALTER TABLE `bitempenyusun`
  ADD PRIMARY KEY (`IPID`),
  ADD KEY `FK_bitempenyusun_bitem` (`IPIDB`),
  ADD KEY `FK_bitempenyusun_bitem_penyusun` (`IPIDBB`);

--
-- Indexes for table `bitemrak`
--
ALTER TABLE `bitemrak`
  ADD PRIMARY KEY (`IRID`),
  ADD KEY `FK_bitemrak_bgudang` (`IRGUDANG`),
  ADD KEY `FK_bitemrak_bitem` (`IRITEM`);

--
-- Indexes for table `bitemstoktemp`
--
ALTER TABLE `bitemstoktemp`
  ADD PRIMARY KEY (`ISID`);

--
-- Indexes for table `bitemsub`
--
ALTER TABLE `bitemsub`
  ADD PRIMARY KEY (`ISID`);

--
-- Indexes for table `bitemsubkategori`
--
ALTER TABLE `bitemsubkategori`
  ADD PRIMARY KEY (`ISKID`);

--
-- Indexes for table `bitem_aset`
--
ALTER TABLE `bitem_aset`
  ADD PRIMARY KEY (`IID`);

--
-- Indexes for table `bjenispenyesuaian`
--
ALTER TABLE `bjenispenyesuaian`
  ADD PRIMARY KEY (`JID`),
  ADD UNIQUE KEY `idx_jkode` (`JKODE`);

--
-- Indexes for table `bkontak`
--
ALTER TABLE `bkontak`
  ADD PRIMARY KEY (`KID`),
  ADD UNIQUE KEY `U_bkontak` (`KKODE`),
  ADD KEY `FK_bkontak_bkontaktipe` (`KTIPE`),
  ADD KEY `FK_bkontak_createu` (`KCREATEU`),
  ADD KEY `FK_bkontak_modifu` (`KMODIFU`);

--
-- Indexes for table `bkontakalamat`
--
ALTER TABLE `bkontakalamat`
  ADD PRIMARY KEY (`BAID`),
  ADD KEY `FK_bkontakalamat_bkontak` (`BAIDKONTAK`);

--
-- Indexes for table `bkontakatention`
--
ALTER TABLE `bkontakatention`
  ADD PRIMARY KEY (`KAID`),
  ADD KEY `FK_bkontakatention_bkontak` (`KAIDK`);

--
-- Indexes for table `bkontakfiles`
--
ALTER TABLE `bkontakfiles`
  ADD PRIMARY KEY (`KFID`),
  ADD KEY `FK_bkontakfiles_bkontak` (`KFIDK`);

--
-- Indexes for table `bkontakserial`
--
ALTER TABLE `bkontakserial`
  ADD PRIMARY KEY (`KSID`),
  ADD KEY `FK_BKONTAKSERIAL_BKONTAK` (`KSKONTAK`),
  ADD KEY `FK_BKONTAKSERI_BKONTAKALAM` (`KSALAMAT`);

--
-- Indexes for table `bkontaktipe`
--
ALTER TABLE `bkontaktipe`
  ADD PRIMARY KEY (`KTID`),
  ADD UNIQUE KEY `U_bkontaktipe` (`KTNAMA`);

--
-- Indexes for table `blain`
--
ALTER TABLE `blain`
  ADD PRIMARY KEY (`LID`),
  ADD UNIQUE KEY `U_blain` (`LKODE`,`LTIPE`),
  ADD KEY `FK_blain_auser` (`LCREATEU`),
  ADD KEY `FK_blain_auserm` (`LMODIFU`);

--
-- Indexes for table `bpajak`
--
ALTER TABLE `bpajak`
  ADD PRIMARY KEY (`PID`),
  ADD KEY `FK_bpajak_auser` (`PCREATEU`),
  ADD KEY `FK_bpajak_ausermodif` (`PMODIFU`);

--
-- Indexes for table `bproyek`
--
ALTER TABLE `bproyek`
  ADD PRIMARY KEY (`PID`),
  ADD KEY `FK_bproyek_auser` (`PCREATEU`),
  ADD KEY `FK_bproyek_ausermodif` (`PMODIFU`);

--
-- Indexes for table `bsatuan`
--
ALTER TABLE `bsatuan`
  ADD PRIMARY KEY (`SID`),
  ADD UNIQUE KEY `U_bsatuan` (`SKODE`),
  ADD KEY `FK_bsatuan_crateu` (`SCREATEU`),
  ADD KEY `FK_bsatuan_modifu` (`SMODIFU`);

--
-- Indexes for table `bsatuankonfersi`
--
ALTER TABLE `bsatuankonfersi`
  ADD PRIMARY KEY (`SKID`),
  ADD KEY `FK_bsatuankonfersi_bsatuan` (`SKSATUAN`),
  ADD KEY `FK_bsatuankonfersi_bsatuank` (`SKSATUANK`);

--
-- Indexes for table `bstokgudang`
--
ALTER TABLE `bstokgudang`
  ADD PRIMARY KEY (`SGID`);

--
-- Indexes for table `btermin`
--
ALTER TABLE `btermin`
  ADD PRIMARY KEY (`TID`),
  ADD UNIQUE KEY `U_btermin` (`TKODE`),
  ADD KEY `FK_btermin_auser` (`TCREATEU`),
  ADD KEY `FK_btermin_auserm` (`TMODIFU`);

--
-- Indexes for table `buang`
--
ALTER TABLE `buang`
  ADD PRIMARY KEY (`UID`),
  ADD UNIQUE KEY `U_buang` (`UKODE`),
  ADD KEY `FK_buang_auser` (`UCREATEU`),
  ADD KEY `FK_buang_auserm` (`UMODIFU`);

--
-- Indexes for table `buangkurs`
--
ALTER TABLE `buangkurs`
  ADD PRIMARY KEY (`UKID`);

--
-- Indexes for table `buangkursp`
--
ALTER TABLE `buangkursp`
  ADD PRIMARY KEY (`UKID`);

--
-- Indexes for table `bvoucher`
--
ALTER TABLE `bvoucher`
  ADD PRIMARY KEY (`VID`);

--
-- Indexes for table `caktivatransaksi`
--
ALTER TABLE `caktivatransaksi`
  ADD PRIMARY KEY (`CTID`),
  ADD KEY `FK_caktivatransaksi_baktiva` (`CTIDA`);

--
-- Indexes for table `canggaran`
--
ALTER TABLE `canggaran`
  ADD PRIMARY KEY (`AID`);

--
-- Indexes for table `cconfigcoa`
--
ALTER TABLE `cconfigcoa`
  ADD PRIMARY KEY (`CCID`);

--
-- Indexes for table `cnoantrian`
--
ALTER TABLE `cnoantrian`
  ADD PRIMARY KEY (`cnaid`);

--
-- Indexes for table `cperiode`
--
ALTER TABLE `cperiode`
  ADD PRIMARY KEY (`PID`),
  ADD UNIQUE KEY `idx_ptahun` (`PTAHUN`);

--
-- Indexes for table `ctransaksid`
--
ALTER TABLE `ctransaksid`
  ADD PRIMARY KEY (`CDID`),
  ADD KEY `FK_ctransaksid_bcoa` (`CDNOCOA`),
  ADD KEY `FK_ctransaksid_bdivisi` (`CDDVISI`),
  ADD KEY `FK_ctransaksid_ctransaksiu` (`CDIDU`);

--
-- Indexes for table `ctransaksigiro`
--
ALTER TABLE `ctransaksigiro`
  ADD PRIMARY KEY (`TGID`),
  ADD UNIQUE KEY `U_ctransaksigiro` (`TGNOTRANSAKSI`),
  ADD KEY `FK_ctransaksigiro_bbank` (`TGBANK`),
  ADD KEY `FK_ctransaksigiro_bkontak` (`TGKONTAK`),
  ADD KEY `FK_ctransaksigiro_ctransaksiu` (`TGIDU`);

--
-- Indexes for table `ctransaksipd`
--
ALTER TABLE `ctransaksipd`
  ADD PRIMARY KEY (`CDID`);

--
-- Indexes for table `ctransaksipu`
--
ALTER TABLE `ctransaksipu`
  ADD PRIMARY KEY (`CUID`);

--
-- Indexes for table `ctransaksiu`
--
ALTER TABLE `ctransaksiu`
  ADD PRIMARY KEY (`CUID`),
  ADD UNIQUE KEY `U_ctransaksiu` (`CUNOTRANSAKSI`),
  ADD KEY `FK_ctransaksiu_bkontak` (`CUKONTAK`),
  ADD KEY `FK_ctransaksiu_ausermodif` (`CUMODIFU`),
  ADD KEY `FK_ctransaksiu_auser` (`CUCREATEU`),
  ADD KEY `FK_ctransaksiu_bbank` (`CUBANK`),
  ADD KEY `FK_ctransaksiu_bcoa` (`CUREKKAS`),
  ADD KEY `FK_ctransaksiu_bcoagiro` (`CUAKUNGIRO`);

--
-- Indexes for table `ddp`
--
ALTER TABLE `ddp`
  ADD PRIMARY KEY (`DPID`),
  ADD UNIQUE KEY `U_ddp` (`DPNOTRANSAKSI`),
  ADD KEY `FK_ddp_bkontak` (`DPKONTAK`);

--
-- Indexes for table `einvoicepenjualanbiaya`
--
ALTER TABLE `einvoicepenjualanbiaya`
  ADD PRIMARY KEY (`IPBID`),
  ADD KEY `FK_einvoicepenjualanbiaya_bcoa` (`IPBCOA`);

--
-- Indexes for table `einvoicepenjualand`
--
ALTER TABLE `einvoicepenjualand`
  ADD PRIMARY KEY (`IPDID`),
  ADD UNIQUE KEY `U_EINVOICEPENJUALAND` (`IPDIDIPU`,`IPDURUTAN`),
  ADD KEY `FK_EINVOICEPENJUALAND_BITEM` (`IPDITEM`),
  ADD KEY `FK_EINVOICEPENJUALAND_BKG` (`IPDSJD`),
  ADD KEY `FK_EINVOICEPENJUALA_BDIVISI` (`IPDDIVISI`),
  ADD KEY `FK_EINVOICEPENJUALA_BSATUAN` (`IPDSATUAN`),
  ADD KEY `FK_EINVOICEPENJUALA_BSATUAN2` (`IPDSATUAND`);

--
-- Indexes for table `einvoicepenjualandp`
--
ALTER TABLE `einvoicepenjualandp`
  ADD PRIMARY KEY (`IDPID`),
  ADD KEY `FK_EINVOICEPENJUALANDP_U` (`IDPIDDP`),
  ADD KEY `FK_EINVOICEPENJUALANDP_U2` (`IDPIDU`);

--
-- Indexes for table `einvoicepenjualanu`
--
ALTER TABLE `einvoicepenjualanu`
  ADD PRIMARY KEY (`IPUID`),
  ADD UNIQUE KEY `U_EINVOICEPENJUALANU` (`IPUNOTRANSAKSI`),
  ADD KEY `FK_EINVOICEPENJUALANU_BUANG` (`IPUUANG`),
  ADD KEY `FK_EINVOICEPENJUALANU_ORDER` (`IPUORDER`),
  ADD KEY `FK_EINVOICEPENJUALANU_US` (`IPUCREATEU`),
  ADD KEY `FK_EINVOICEPENJUALANU_US2` (`IPUMODIFU`),
  ADD KEY `FK_EINVOICEPENJUALAN_FSTOKU` (`IPUNOBKG`),
  ADD KEY `FK_EINVOICEPENJUALA_BKONTAK` (`IPUKONTAK`),
  ADD KEY `FK_EINVOICEPENJUALA_BTERMIN` (`IPUTERMIN`);

--
-- Indexes for table `epembayaraninvoicei`
--
ALTER TABLE `epembayaraninvoicei`
  ADD PRIMARY KEY (`PIIID`),
  ADD UNIQUE KEY `U_epembayaraninvoicei` (`PIIIDU`,`PIIIDINVOICE`),
  ADD KEY `FK_EPEMBAYARANINVOICE_BUANG` (`PIIUANG`),
  ADD KEY `FK_EPEMBAYARAN_EINVOICEPEN` (`PIIIDINVOICE`),
  ADD KEY `FK_epembayaraninvoicei_fstoku` (`PIIIDPEMBELIAN`);

--
-- Indexes for table `epembayaraninvoicer`
--
ALTER TABLE `epembayaraninvoicer`
  ADD PRIMARY KEY (`PIRID`),
  ADD UNIQUE KEY `U_EPEMBAYARANINVOICER` (`PIRIDU`,`PIRURUTAN`),
  ADD KEY `FK_EPEMBAYARAN_EIP` (`PIRIDRETURPENJUALAN`),
  ADD KEY `FK_EPEMBAYARAN_EIP2` (`PIRIDRETURPEMBELIAN`);

--
-- Indexes for table `epembayaraninvoiceu`
--
ALTER TABLE `epembayaraninvoiceu`
  ADD PRIMARY KEY (`PIUID`),
  ADD UNIQUE KEY `U_EPEMBAYARANINVOICEU` (`PIUNOTRANSAKSI`),
  ADD KEY `FK_EPEMBAYARANINVOICEU_C` (`PIUCOAKAS`),
  ADD KEY `FK_EPEMBAYARANINVOICEU_C2` (`PIUAKUNGIRO`),
  ADD KEY `FK_EPEMBAYARANINVOICE_C1` (`PIUBANK`),
  ADD KEY `FK_EPEMBAYARANINVOICE_U` (`PIUUANG`),
  ADD KEY `FK_EPEMBAYARANINVOICE_US` (`PIUCREATEU`),
  ADD KEY `FK_EPEMBAYARANINVOICE_US2` (`PIUMODIFU`),
  ADD KEY `FK_EPEMBAYARANINVOI_BKONTAK` (`PIUKONTAK`);

--
-- Indexes for table `esalesorderd`
--
ALTER TABLE `esalesorderd`
  ADD PRIMARY KEY (`SODID`),
  ADD UNIQUE KEY `U_ESALESORDERD` (`SODIDSOU`,`SODURUTAN`),
  ADD KEY `FK_ESALESORDERD_BDIVISI` (`SODDIVISI`),
  ADD KEY `FK_ESALESORDERD_BGUDANG` (`SODGUDANG`),
  ADD KEY `FK_ESALESORDERD_BITEM` (`SODITEM`),
  ADD KEY `FK_ESALESORDERD_BSATUAN` (`SODSATUAN`),
  ADD KEY `FK_ESALESORDERD_BSATUAN2` (`SODSATUAND`);

--
-- Indexes for table `esalesorderu`
--
ALTER TABLE `esalesorderu`
  ADD PRIMARY KEY (`SOUID`),
  ADD KEY `FK_esalesorderu_auser` (`SOUCREATEU`),
  ADD KEY `FK_esalesorderu_auserm` (`SOUMODIFU`),
  ADD KEY `FK_esalesorderu_bkontak` (`SOUKONTAK`),
  ADD KEY `FK_esalesorderu_bkontakkaryawan` (`SOUKARYAWAN`),
  ADD KEY `FK_esalesorderu_buang` (`SOUUANG`),
  ADD KEY `FK_esalesorderu_mengetahui` (`SOUMENGETAHUI`),
  ADD KEY `FK_esalesorderu_menyetujui` (`SOUMENYETUJUI`);

--
-- Indexes for table `esalesquotationd`
--
ALTER TABLE `esalesquotationd`
  ADD PRIMARY KEY (`SQDID`);

--
-- Indexes for table `esalesquotationu`
--
ALTER TABLE `esalesquotationu`
  ADD PRIMARY KEY (`SQUID`);

--
-- Indexes for table `fasetd`
--
ALTER TABLE `fasetd`
  ADD PRIMARY KEY (`SDID`),
  ADD UNIQUE KEY `U_fasetd` (`SDIDSU`,`SDURUTAN`),
  ADD KEY `FK_fasetd_gudang` (`SDGUDANG`),
  ADD KEY `FK_fasetd_item` (`SDITEM`),
  ADD KEY `FK_fasetd_satuan` (`SDSATUAN`),
  ADD KEY `FK_fasetd_satuand` (`SDSATUAND`);

--
-- Indexes for table `fasetu`
--
ALTER TABLE `fasetu`
  ADD PRIMARY KEY (`SUID`),
  ADD UNIQUE KEY `U_fasetu_sunotransaksi` (`SUNOTRANSAKSI`),
  ADD KEY `FK_fasetu_kontak` (`SUKONTAK`),
  ADD KEY `FK_fasetu_user_add` (`SUCREATEU`),
  ADD KEY `FK_fasetu_user_edit` (`SUMODIFU`);

--
-- Indexes for table `fhppin`
--
ALTER TABLE `fhppin`
  ADD PRIMARY KEY (`HIID`),
  ADD KEY `FK_FHPPIN_GUDANG` (`HIGUDANGID`),
  ADD KEY `FK_FHPPIN_ITEM` (`HIBARANGID`),
  ADD KEY `FK_FHPPIN_TRANS` (`HITRANSID`);

--
-- Indexes for table `fhppout`
--
ALTER TABLE `fhppout`
  ADD PRIMARY KEY (`HOID`),
  ADD KEY `FK_FHPPOUT_GUDANG` (`HOGUDANGID`),
  ADD KEY `FK_FHPPOUT_ITEM` (`HOBARANGID`),
  ADD KEY `FK_FHPPOUT_TRANS` (`HOTRANSID`);

--
-- Indexes for table `fhppoutreff`
--
ALTER TABLE `fhppoutreff`
  ADD PRIMARY KEY (`HORID`),
  ADD KEY `FK_FHPPOUTREFF_IN` (`HOHPIN`),
  ADD KEY `FK_FHPPOUTREFF_OUT` (`HOHPOUT`);

--
-- Indexes for table `fpermintaanbarangd`
--
ALTER TABLE `fpermintaanbarangd`
  ADD PRIMARY KEY (`PBDID`),
  ADD UNIQUE KEY `U_fpermintaanbarangd` (`PBDIDSU`,`PBDURUTAN`),
  ADD KEY `FK_fpermintaanbarangd_bitem` (`PBDITEM`),
  ADD KEY `FK_fpermintaanbarangd_bsatuan` (`PBDSATUAN`);

--
-- Indexes for table `fpermintaanbarangu`
--
ALTER TABLE `fpermintaanbarangu`
  ADD PRIMARY KEY (`PBUID`),
  ADD UNIQUE KEY `U_fpermintaanbarangu` (`PBUNOTRANSAKSI`),
  ADD KEY `FK_fpermintaanbarangu_auser` (`PBUCREATEU`),
  ADD KEY `FK_fpermintaanbarangu_auserm` (`PBUMODIFU`),
  ADD KEY `FK_fpermintaanbarangu_bkontak` (`PBUKONTAK`),
  ADD KEY `FK_fpermintaanbarangu_karyawan` (`PBUKARYAWAN`);

--
-- Indexes for table `fprob`
--
ALTER TABLE `fprob`
  ADD PRIMARY KEY (`PBID`),
  ADD KEY `FK_FPROB_ITEM` (`PBBARANG`),
  ADD KEY `FK_FPROB_U` (`PBIDU`),
  ADD KEY `FK_FPROB_UOM` (`PBSATUAN`);

--
-- Indexes for table `fproc`
--
ALTER TABLE `fproc`
  ADD PRIMARY KEY (`PCID`),
  ADD KEY `FK_FPROC_COA` (`PCCOA`),
  ADD KEY `FK_FPROC_U` (`PCIDU`),
  ADD KEY `FK_FPROC_UANG` (`PCUANG`);

--
-- Indexes for table `fproj`
--
ALTER TABLE `fproj`
  ADD PRIMARY KEY (`PJID`);

--
-- Indexes for table `fprou`
--
ALTER TABLE `fprou`
  ADD PRIMARY KEY (`PUID`),
  ADD KEY `FK_FPROU_GUDANG` (`PUGUDANG`),
  ADD KEY `FK_FPROU_KONTAK` (`PUKONTAK`),
  ADD KEY `FK_FPROU_USER` (`PUCREATEU`),
  ADD KEY `FK_FPROU_USER2` (`PUMODIFU`);

--
-- Indexes for table `fstokd`
--
ALTER TABLE `fstokd`
  ADD PRIMARY KEY (`SDID`),
  ADD UNIQUE KEY `U_fstokd` (`SDIDSU`,`SDURUTAN`),
  ADD KEY `FK_fstokd_dokter` (`SDDOKTER`),
  ADD KEY `FK_fstokd_gudang` (`SDGUDANG`),
  ADD KEY `FK_fstokd_item` (`SDITEM`),
  ADD KEY `FK_fstokd_perawat` (`SDKARYAWAN`),
  ADD KEY `FK_fstokd_satuan` (`SDSATUAN`),
  ADD KEY `FK_fstokd_satuand` (`SDSATUAND`);

--
-- Indexes for table `fstokd_koli`
--
ALTER TABLE `fstokd_koli`
  ADD PRIMARY KEY (`SDKID`),
  ADD KEY `FK_fstokd_koli_fstokd` (`SDKIDD`);

--
-- Indexes for table `fstokh`
--
ALTER TABLE `fstokh`
  ADD PRIMARY KEY (`SHID`),
  ADD KEY `fk_fstokh_fstokd` (`SHIDSD`);

--
-- Indexes for table `fstokopnamed`
--
ALTER TABLE `fstokopnamed`
  ADD PRIMARY KEY (`SODID`),
  ADD UNIQUE KEY `U_fstokopnamed` (`SODIDSOU`,`SODURUTAN`),
  ADD KEY `FK_fstokopnamed_bitem` (`SODITEM`),
  ADD KEY `FK_fstokopnamed_bsatuan` (`SODSATUAN`);

--
-- Indexes for table `fstokopnameu`
--
ALTER TABLE `fstokopnameu`
  ADD PRIMARY KEY (`SOUID`),
  ADD UNIQUE KEY `U_fstokopnameu` (`SOUNOTRANSAKSI`);

--
-- Indexes for table `fstokpergudang`
--
ALTER TABLE `fstokpergudang`
  ADD PRIMARY KEY (`SPGID`);

--
-- Indexes for table `fstoksjd`
--
ALTER TABLE `fstoksjd`
  ADD PRIMARY KEY (`SDID`),
  ADD UNIQUE KEY `U_fstoksjd` (`SDIDSU`,`SDURUTAN`),
  ADD KEY `FK_fstoksjd_bitem` (`SDITEM`);

--
-- Indexes for table `fstoksju`
--
ALTER TABLE `fstoksju`
  ADD PRIMARY KEY (`SUID`);

--
-- Indexes for table `fstoku`
--
ALTER TABLE `fstoku`
  ADD PRIMARY KEY (`SUID`),
  ADD UNIQUE KEY `U_fstoku_sunotransaksi` (`SUNOTRANSAKSI`),
  ADD KEY `FK_fstoku_kasir` (`SUKARYAWAN`),
  ADD KEY `FK_fstoku_kontak` (`SUKONTAK`),
  ADD KEY `FK_fstoku_user_add` (`SUCREATEU`),
  ADD KEY `FK_fstoku_user_edit` (`SUMODIFU`);

--
-- Indexes for table `gstoku`
--
ALTER TABLE `gstoku`
  ADD UNIQUE KEY `U_gstoku` (`SUNOTRANSAKSI`);

--
-- Indexes for table `tmp_bb`
--
ALTER TABLE `tmp_bb`
  ADD KEY `idx_cid_tmp_bb` (`cid`) USING BTREE,
  ADD KEY `idx_tgl_tmp_bb` (`tanggal`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ainfo`
--
ALTER TABLE `ainfo`
  MODIFY `iid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `amenu`
--
ALTER TABLE `amenu`
  MODIFY `MID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=658;

--
-- AUTO_INCREMENT for table `anomor`
--
ALTER TABLE `anomor`
  MODIFY `NID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=725;

--
-- AUTO_INCREMENT for table `areport`
--
ALTER TABLE `areport`
  MODIFY `ARID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=154;

--
-- AUTO_INCREMENT for table `arole`
--
ALTER TABLE `arole`
  MODIFY `ARID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `aserial`
--
ALTER TABLE `aserial`
  MODIFY `AID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auser`
--
ALTER TABLE `auser`
  MODIFY `uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `ausercoa`
--
ALTER TABLE `ausercoa`
  MODIFY `AUCID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auserlog`
--
ALTER TABLE `auserlog`
  MODIFY `ULID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `ausermenu`
--
ALTER TABLE `ausermenu`
  MODIFY `AUID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7913;

--
-- AUTO_INCREMENT for table `auuserrole`
--
ALTER TABLE `auuserrole`
  MODIFY `AURID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `baktiva`
--
ALTER TABLE `baktiva`
  MODIFY `AID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `baktivafiles`
--
ALTER TABLE `baktivafiles`
  MODIFY `AFID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `baktivakelompok`
--
ALTER TABLE `baktivakelompok`
  MODIFY `AKID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `baktivapenyusutan`
--
ALTER TABLE `baktivapenyusutan`
  MODIFY `APID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bbank`
--
ALTER TABLE `bbank`
  MODIFY `BID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `bcabang`
--
ALTER TABLE `bcabang`
  MODIFY `CID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bcoa`
--
ALTER TABLE `bcoa`
  MODIFY `CID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=148;

--
-- AUTO_INCREMENT for table `bcoasa`
--
ALTER TABLE `bcoasa`
  MODIFY `CSID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bdivisi`
--
ALTER TABLE `bdivisi`
  MODIFY `DID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bgudang`
--
ALTER TABLE `bgudang`
  MODIFY `GID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `bitem`
--
ALTER TABLE `bitem`
  MODIFY `IID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `bitemfiles`
--
ALTER TABLE `bitemfiles`
  MODIFY `BFID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bitemkategori`
--
ALTER TABLE `bitemkategori`
  MODIFY `IKID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bitempenyusun`
--
ALTER TABLE `bitempenyusun`
  MODIFY `IPID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bitemrak`
--
ALTER TABLE `bitemrak`
  MODIFY `IRID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bitemstoktemp`
--
ALTER TABLE `bitemstoktemp`
  MODIFY `ISID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bitemsub`
--
ALTER TABLE `bitemsub`
  MODIFY `ISID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bitemsubkategori`
--
ALTER TABLE `bitemsubkategori`
  MODIFY `ISKID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bitem_aset`
--
ALTER TABLE `bitem_aset`
  MODIFY `IID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bjenispenyesuaian`
--
ALTER TABLE `bjenispenyesuaian`
  MODIFY `JID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `bkontak`
--
ALTER TABLE `bkontak`
  MODIFY `KID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `bkontakalamat`
--
ALTER TABLE `bkontakalamat`
  MODIFY `BAID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bpajak`
--
ALTER TABLE `bpajak`
  MODIFY `PID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `bproyek`
--
ALTER TABLE `bproyek`
  MODIFY `PID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bsatuan`
--
ALTER TABLE `bsatuan`
  MODIFY `SID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `btermin`
--
ALTER TABLE `btermin`
  MODIFY `TID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `buang`
--
ALTER TABLE `buang`
  MODIFY `UID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cconfigcoa`
--
ALTER TABLE `cconfigcoa`
  MODIFY `CCID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=159;

--
-- AUTO_INCREMENT for table `cperiode`
--
ALTER TABLE `cperiode`
  MODIFY `PID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `ctransaksid`
--
ALTER TABLE `ctransaksid`
  MODIFY `CDID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `ctransaksiu`
--
ALTER TABLE `ctransaksiu`
  MODIFY `CUID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `ddp`
--
ALTER TABLE `ddp`
  MODIFY `DPID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `einvoicepenjualand`
--
ALTER TABLE `einvoicepenjualand`
  MODIFY `IPDID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `einvoicepenjualandp`
--
ALTER TABLE `einvoicepenjualandp`
  MODIFY `IDPID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `einvoicepenjualanu`
--
ALTER TABLE `einvoicepenjualanu`
  MODIFY `IPUID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `epembayaraninvoicei`
--
ALTER TABLE `epembayaraninvoicei`
  MODIFY `PIIID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `epembayaraninvoicer`
--
ALTER TABLE `epembayaraninvoicer`
  MODIFY `PIRID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `epembayaraninvoiceu`
--
ALTER TABLE `epembayaraninvoiceu`
  MODIFY `PIUID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `esalesorderd`
--
ALTER TABLE `esalesorderd`
  MODIFY `SODID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `esalesorderu`
--
ALTER TABLE `esalesorderu`
  MODIFY `SOUID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `fasetd`
--
ALTER TABLE `fasetd`
  MODIFY `SDID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fasetu`
--
ALTER TABLE `fasetu`
  MODIFY `SUID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fstokd`
--
ALTER TABLE `fstokd`
  MODIFY `SDID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `fstokh`
--
ALTER TABLE `fstokh`
  MODIFY `SHID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `fstokopnamed`
--
ALTER TABLE `fstokopnamed`
  MODIFY `SODID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fstokopnameu`
--
ALTER TABLE `fstokopnameu`
  MODIFY `SOUID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fstoku`
--
ALTER TABLE `fstoku`
  MODIFY `SUID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
