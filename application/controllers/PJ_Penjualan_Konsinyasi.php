<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PJ_Penjualan_Konsinyasi extends CI_Controller {

   function __construct() { 
  		parent::__construct();
      if(!$this->session->has_userdata('nama')){
        redirect(base_url('exception'));
      }          
  		$this->load->model('M_transaksi');
      $this->load->model('M_PJ_Penjualan_Konsinyasi');
   }

   function savedata(){
      if($_POST['id']==''){
        echo $this->M_PJ_Penjualan_Konsinyasi->tambahTransaksi();
      }else{
        echo $this->M_PJ_Penjualan_Konsinyasi->ubahTransaksi();      
      }
   }

   function deletedata(){
      echo $this->M_PJ_Penjualan_Konsinyasi->hapusTransaksi();          
   }   

   function get_item() {
        $query  = "SELECT A.isatuan AS 'idsatuan', B.snama 'namasatuan', 
                        (SELECT kpenlevelharga FROM bkontak WHERE kid='".$_POST['kontak']."') 'level', 
                        A.ihargajual1 'hargajual',A.ihargajual2 'hargajual2',
                        A.ihargajual3 'hargajual3',A.ihargajual4 'hargajual4',      
                        (SELECT gid FROM bgudang WHERE gdefault=1 LIMIT 1) 'idgudang',
                        (SELECT gnama FROM bgudang WHERE gdefault=1 LIMIT 1) 'gudang'       
                   FROM bitem A LEFT JOIN bsatuan B ON A.isatuan=B.sid
                  WHERE A.iid='".$this->input->post('id')."'";
        header('Content-Type: application/json');
        echo $this->M_transaksi->get_data_query($query);
    }                      

   function getdata(){
   		if(empty($_POST['id'])) {
   			echo _pesanError("Id transaksi tidak ditemukan !");
  			exit;
   		}

      $transcode = $this->M_transaksi->prefixtrans(element('PJ_Order_Penjualan',NID));        
   		$query = "SELECT A.souid 'id', A.sounotransaksi 'nomor', DATE_FORMAT(A.soutanggal,'%d-%m-%Y') 'tanggal',
           						 A.soukontak 'kontakid', B.kkode 'kontakkode', B.knama 'kontak', A.souuraian 'uraian',
           						 A.soukaryawan 'idkaryawan', C.kkode 'kodekaryawan', C.knama 'namakaryawan', A.sounopopelanggan 'noref',
                       A.soutermin 'idtermin', D.tkode 'termin', A.soucatatan 'catatan', A.soualamat 'alamat', A.souattention 'idperson',
                       E.kanama 'person', A.soustatus 'status', 
                       F.soditem 'iditem', G.ikode 'kditem', G.inama 'namaitem', F.sodcatatan 'catdetil',
                       F.sodsatuan 'idsatuan', H.skode 'satuan', F.sodgudangasal 'idgudangasal',F.sodgudangtujuan 'idgudangtujuan',
                       IFNULL(F.sodorder,0) 'qtydetil',
												I.gnama 'gudangasal',
												J.gnama 'gudangtujuan'			
                    FROM epenjualankonsinyasiu A 
               LEFT JOIN bkontak B ON A.soukontak=B.kid
               LEFT JOIN bkontak C ON A.soukaryawan=C.kid 
               LEFT JOIN btermin D ON A.soutermin=D.tid
               LEFT JOIN bkontakatention E ON A.souattention=E.kaid 
               LEFT JOIN epenjualankonsinyasid F ON A.souid=F.sodidsou 
               LEFT JOIN bitem G ON F.soditem=G.iid 
               LEFT JOIN bsatuan H ON F.sodsatuan=H.sid 
               LEFT JOIN bgudang I ON F.sodgudangasal=I.gid
							 LEFT JOIN bgudang J ON F.sodgudangtujuan=J.gid
                   WHERE A.sousumber='".$transcode."' AND A.souid='".$_POST['id']."' ORDER BY F.sodurutan ASC ";
       
        header('Content-Type: application/json');
        echo $this->M_transaksi->get_data_query($query);
   }

  function get_Termin()
  {
    $query  = "SELECT * FROM `btermin`
                WHERE TID='" . $_POST['id'] . "'";
    header('Content-Type: application/json');
    echo $this->M_transaksi->get_data_query($query);
  }  

}