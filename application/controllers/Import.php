<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'phpspreadsheet/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\Reader\Csv;

class Import extends CI_Controller {

	function __construct() {
	    header('Access-Control-Allow-Origin: *');
	    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");	 
		parent::__construct();
	}


	function importcoa(){
		if (!empty($_FILES)) {
		    $tempFile = $_FILES['file']['tmp_name'];
		    $fileName = $_FILES['file']['name'];
		    $fileType = $_FILES['file']['type'];
		    $fileSize = $_FILES['file']['size'];
		    $targetPath = './assets/uploads/';
		    $targetFile = $targetPath . $fileName ;
			
			$arr_file = explode('.', $fileName);
			$extension = end($arr_file);

			if('csv' == $extension) {
				$reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
			} else {
			    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
			}

			$spreadsheet = $reader->load($tempFile);
			$sheetData = $spreadsheet->getActiveSheet()->toArray();

        	$this->load->model('M_Master_Akun');

			for($i = 1;$i < count($sheetData);$i++)
			{
			    $kode = $sheetData[$i]['0'];
			    $nama = $sheetData[$i]['1'];
			    $tipe = $sheetData[$i]['2'];
			    $gd = $sheetData[$i]['3'];

			    $tipecoa = array(
			    		'cgnama' => trim($tipe) 
			    );
				$tipe = $this->M_Master_Akun->getcoatipe($tipecoa);

		        $data = array(
		                'cnocoa' => $kode,
		                'cnama' => $nama,
		                'ctipe' => $tipe,
		                'cgd' => $gd,
		                'cactive' => 1,                                
		                'ccreateu' => $this->session->id                
		        );  

				$this->M_Master_Akun->importData($data);
			}

		    // Upload file ke server
		    move_uploaded_file($tempFile, $targetFile);

			echo "sukses";
	    }		
	}


	function importkontak(){
		if (!empty($_FILES)) {
		    $tempFile = $_FILES['file']['tmp_name'];
		    $fileName = $_FILES['file']['name'];
		    $fileType = $_FILES['file']['type'];
		    $fileSize = $_FILES['file']['size'];
		    $targetPath = './assets/uploads/';
		    $targetFile = $targetPath . $fileName ;
			
			$arr_file = explode('.', $fileName);
			$extension = end($arr_file);

			if('csv' == $extension) {
				$reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
			} else {
			    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
			}

			$spreadsheet = $reader->load($tempFile);
			$sheetData = $spreadsheet->getActiveSheet()->toArray();

        	$this->load->model('M_Master_Kontak');
        	$this->load->model('M_Master_Termin');        	

			for($i = 1;$i < count($sheetData);$i++)
			{
			    $kode = $sheetData[$i]['0'];
			    $nama = $sheetData[$i]['1'];
			    $tipe = $sheetData[$i]['2'];
			    $alamat = $sheetData[$i]['3'];
			    $kodepos = $sheetData[$i]['4'];			    
			    $telp = $sheetData[$i]['5'];			    			    
			    $faks = $sheetData[$i]['6'];			    
			    $email = $sheetData[$i]['7'];
			    $person = $sheetData[$i]['8'];
			    $limithutang = $sheetData[$i]['9'];
			    $limitpiutang = $sheetData[$i]['10'];
			    $terminbeli = $sheetData[$i]['11'];
			    $terminjual = $sheetData[$i]['12'];			    			    			    			    			    			    	
			    $lvlhargajual = $sheetData[$i]['13'];			    	    			    
			    $diskonjual = $sheetData[$i]['14'];

			    $tipekontak = array(
			    		'ktnama' => trim($tipe) 
			    );
				$tipe = $this->M_Master_Kontak->getkontaktipe($tipekontak);

			    $datatermin = array(
			    		'tkode' => trim($terminbeli) 
			    );
				$terminbeli = $this->M_Master_Termin->getterminid($datatermin);				

			    $datatermin = array(
			    		'tkode' => trim($terminjual) 
			    );
				$terminjual = $this->M_Master_Termin->getterminid($datatermin);				


		        $data = array(
		                'kkode' => $kode,
		                'knama' => $nama,
		                'ktipe' => $tipe,
		                'k1alamat' => $alamat,
		                'k1kodepos' => $kodepos,   
		                'k1telp1' => $telp,    
		                'k1fax' => $faks,  
		                'k1email' => $email,
		                'k1kontak' => $person,    
		                'kpemtermin' => $terminbeli,
		                'kpentermin' => $terminjual,
		                'kpenlevelharga' => $lvlhargajual, 
		                'kpembatashutang' => $limithutang,                
		                'kpenbataspiutang' => $limitpiutang,
		                'kdiskon' => $diskonjual,
		                'kcreateu' => $this->session->id                
		        );        

				$this->M_Master_Kontak->importData($data);
			}

		    // Upload file ke server
		    move_uploaded_file($tempFile, $targetFile);

			echo "sukses";
	    }		
	}

	function importitem(){
		if (!empty($_FILES)) {
		    $tempFile = $_FILES['file']['tmp_name'];
		    $fileName = $_FILES['file']['name'];
		    $fileType = $_FILES['file']['type'];
		    $fileSize = $_FILES['file']['size'];
		    $targetPath = './assets/uploads/';
		    $targetFile = $targetPath . $fileName ;
			
			$arr_file = explode('.', $fileName);
			$extension = end($arr_file);

			if('csv' == $extension) {
				$reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
			} else {
			    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
			}

			$spreadsheet = $reader->load($tempFile);
			$sheetData = $spreadsheet->getActiveSheet()->toArray();

        	$this->load->model('M_Master_Item');

			for($i = 1;$i < count($sheetData);$i++)
			{
			    $kode = $sheetData[$i]['0'];
			    $barcode = $sheetData[$i]['1'];
			    $nama = $sheetData[$i]['2'];
			    $satuan = $sheetData[$i]['3'];
			    $jenis = $sheetData[$i]['4'];			    			    
			    $tipe = $sheetData[$i]['5'];			    
			    $hargabeli = $sheetData[$i]['6'];
			    $hargajual1 = $sheetData[$i]['7'];
			    $hargajual2 = $sheetData[$i]['8'];			    
			    $hargajual3 = $sheetData[$i]['9'];
			    $hargajual4 = $sheetData[$i]['10'];
			    $stokmin = $sheetData[$i]['11'];
			    $stokmaks = $sheetData[$i]['12'];			    			    			    			    			    			    	
			    $stokreorder = $sheetData[$i]['13'];			    	    			    
			    $coapersediaan = $sheetData[$i]['14'];
			    $coapendapatan = $sheetData[$i]['15'];			    
			    $coahpp = $sheetData[$i]['16'];

		        $data = array(
		                'ikode' => $kode,
		                'ibarcode' => $barcode,                                
		                'inama' => $nama,
		                'isatuan' => $satuan,
		                'isatuand' => $satuan,                
		                'ijenisitem' => $jenis,
		                'itipeitem' => $tipe,
		                'istockminimal' => $stokmin,
		                'istockmaksimal' => $stokmaks,
		                'istockreorder' => $stokreorder,
		                'ihargabeli' => $hargabeli,
		                'ihargajual1' => $hargajual1,                                                                                
		                'ihargajual2' => $hargajual2,
		                'ihargajual3' => $hargajual3,
		                'ihargajual4' => $hargajual4,
		                'icoapersediaan' => $coapersediaan,
		                'icoapendapatan' => $coapendapatan,
		                'icoahpp' => $coahpp,
		                'icreateu' => $this->session->id                
		        );        

				$this->M_Master_Item->importData($data);
			}

		    // Upload file ke server
		    move_uploaded_file($tempFile, $targetFile);

			echo "sukses";
	    }		
	}

}

