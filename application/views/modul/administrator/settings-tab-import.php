<script src="<? echo base_url('assets/plugins/dropzone/dropzone.min.js'); ?>"></script>
<style>
    .dropzone .dz-preview {
        background: #f8f9fa !important;
    }

    .dz-message {
        margin-top: 0 !important;
        top: auto !important;
        cursor: pointer;
    }

    .dz-details {
        background: none !important;
        text-align: center !important;
    }

    .dropzone.dz-clickable {
        padding-bottom: 100px !important;
    }

    .dropzone.dz-started .dz-message {
        opacity: 1 !important;
    }

    .dropzone-disabled {
        cursor: no-drop;
    }

    .col-centered{
        float: none;
        margin: 0 auto;
    }

    .dz-message_disabled{
        font-size: 1rem;
        position: absolute;
        top: 50%;
        left: 0;
        width: 100%;
        height: 100px;
        margin-top: -30px;
        color: #5A8DEE;
        text-align: center; 
    }
</style>

<div class="tab-pane text-sm px-2 mx-4" id="tab-import" role="tabpanel" aria-labelledby="btn-tab-import">
<section class="row">
                <div class="col-12 col-md-6 col-xs-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <h1 class="text-md ml-4">Chart Of Account</h1>
                                <div style="position: absolute;top:10px;right: 25px;z-index:1">
                                    <button onclick="download('template_coa.xlsx')" title="download template COA" type="button" class="download_template btn btn-icon rounded-circle btn-light">
                                        <i class="fas fa-download text-md" style="color:#5A8DEE"></i>
                                    </button>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="card">
                                            <div class="card-content">
                                                <div class="card-body">
                                                    <div class="dropzone dropzone-area bg-white" id="dropzone-coa">
                                                        <div class="dz-message">
                                                        Upload File
                                                        </div>
                                                    </div>
                                                    <div class="dropzone dropzone-disabled bg-white" id="dropzone-coa-disabled" style="display: none">
                                                        <div class="dz-message_disabled">
                                                            <div style="">
                                                                <i style="font-size: 2.5rem !important;" class="fas fa-times"></i>
                                                            </div>
                                                            Tidak Tersedia
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                  
                <div class="col-12 col-md-6 col-xs-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <h1 class="text-md ml-4">Kontak</h1>
                                <div style="position: absolute;top:10px;right: 25px;z-index:1">
                                    <button onclick="download('template_kontak.xlsx')" title="download template data kontak" type="button" class="download_template btn btn-icon rounded-circle btn-light">
                                        <i class="fas fa-download text-md" style="color:#5A8DEE"></i>
                                    </button>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="card">
                                            <div class="card-content">
                                                <div class="card-body">
                                                    <div class="dropzone dropzone-area bg-white" id="dropzone-kontak">
                                                        <div class="dz-message">
                                                        Upload File
                                                        </div>
                                                    </div>
                                                    <div class="dropzone dropzone-disabled bg-white" id="dropzone-kontak-disabled" style="display: none;">
                                                        <div class="dz-message_disabled">
                                                            <div style="">
                                                                <i style="font-size: 3rem !important;" class="fas fa-times"></i>
                                                            </div>
                                                            Tidak Tersedia
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

</section>
<section class="row">
                <div class="col-12 col-md-6 col-xs-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <h1 class="text-md ml-4">Item Barang & Jasa</h1>
                                <div style="position: absolute;top:10px;right: 25px;z-index:1">
                                    <button onclick="download('template_item.xlsx')" title="download template data item" type="button" class="download_template btn btn-icon rounded-circle btn-light">
                                        <i class="fas fa-download text-md" style="color:#5A8DEE"></i>
                                    </button>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="card">
                                            <div class="card-content">
                                                <div class="card-body">
                                                    <div class="dropzone dropzone-area bg-white" id="dropzone-item">
                                                        <div class="dz-message">
                                                        Upload File
                                                        </div>
                                                    </div>
                                                    <div class="dropzone dropzone-disabled bg-white" id="dropzone-item-disabled" style="display: none">
                                                        <div class="dz-message_disabled">
                                                            <div style="">
                                                                <i style="font-size: 2.5rem !important;" class="fas fa-times"></i>
                                                            </div>
                                                            Tidak Tersedia
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                
                <div class="col-12 col-md-6 col-xs-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <h1 class="text-md ml-4">Aktiva Tetap</h1>
                                <div style="position: absolute;top:10px;right: 25px;z-index:1">
                                    <button data-type="template_fa" title="download template aktiva tetap" type="button" class="download_template btn btn-icon rounded-circle btn-light">
                                        <i class="fas fa-download text-md" style="color:#5A8DEE"></i>
                                    </button>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="card">
                                            <div class="card-content">
                                                <div class="card-body">
                                                    <div class="dropzone dropzone-area bg-white" id="dropzone-fa">
                                                        <div class="dz-message">
                                                        Upload File
                                                        </div>
                                                    </div>
                                                    <div class="dropzone dropzone-disabled bg-white" id="dropzone-fa-disabled" style="display: none">
                                                        <div class="dz-message_disabled">
                                                            <div style="">
                                                                <i style="font-size: 2.5rem !important;" class="fas fa-times"></i>
                                                            </div>
                                                            Tidak Tersedia
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                                  
</section>
</div>

<script>
  function download(url){
    window.open(base_url+'assets/download/'+url, '_blank');
  }
</script>

<script>
  // DropzoneJS Demo Code Start
  Dropzone.autoDiscover = false

  // Get the template HTML and remove it from the doumenthe template HTML and remove it from the doument
//  var previewNode = document.querySelector("#template")
//  previewNode.id = ""
//  var previewTemplate = previewNode.parentNode.innerHTML
//  previewNode.parentNode.removeChild(previewNode)

// Dropzone COA
  let upload_coa = new Dropzone("#dropzone-coa", {
        addRemoveLinks: true,
        init: function() {
            this.on("success", function(file,response) {
                    if (response=="sukses") {
                    } else {
                      //alert(response);
                      console.log (response);
                      file.previewElement.remove();
                    }
            }); 

            this.on("maxfilesexceeded", function(file){
              this.removeAllFiles();
              this.addFile(file);
            });           
        },
        url: base_url + "Import/importcoa",
        maxFilesize: 5,
        method: "POST",
        data : "",
        uploadMultiple: false,
        maxFiles: 1,
        paramName: "file",
        dictInvalidFileType: "Type file ini tidak dizinkan",
        acceptedFiles: '.xls,.xlsx',
        removedfile: function(file) {
            //var file_ = file.name;
            file.previewElement.remove();
            // tambahkan ajax untuk menghapus file di server
            //.....
        },
  });

// Dropzone Kontak
  let upload_kontak = new Dropzone("#dropzone-kontak", {
        addRemoveLinks: true,
        init: function() {
            this.on("success", function(file,response) {
                    if (response=="sukses") {
                    } else {
                      //alert(response);
                      console.log (response);
                      file.previewElement.remove();
                    }
            }); 

            this.on("maxfilesexceeded", function(file){
              this.removeAllFiles();
              this.addFile(file);
            });           
        },
        url: base_url + "Import/importkontak",
        maxFilesize: 5,
        method: "POST",
        data : "",
        uploadMultiple: false,
        maxFiles: 1,
        paramName: "file",
        dictInvalidFileType: "Type file ini tidak dizinkan",
        acceptedFiles: '.xls,.xlsx',
        removedfile: function(file) {
            //var file_ = file.name;
            file.previewElement.remove();
            // tambahkan ajax untuk menghapus file di server
            //.....
        },
  });

// Dropzone Item
  let upload_item = new Dropzone("#dropzone-item", {
        addRemoveLinks: true,
        init: function() {
            this.on("success", function(file,response) {
                    if (response=="sukses") {
                    } else {
                      //alert(response);
                      console.log (response);
                      file.previewElement.remove();
                    }
            }); 

            this.on("maxfilesexceeded", function(file){
              this.removeAllFiles();
              this.addFile(file);
            });           
        },
        url: base_url + "Import/importitem",
        maxFilesize: 5,
        method: "POST",
        data : "",
        uploadMultiple: false,
        maxFiles: 1,
        paramName: "file",
        dictInvalidFileType: "Type file ini tidak dizinkan",
        acceptedFiles: '.xls,.xlsx',
        removedfile: function(file) {
            //var file_ = file.name;
            file.previewElement.remove();
            // tambahkan ajax untuk menghapus file di server
            //.....
        },
  });

/*
  $('#dropzone-coa').on('click', '.dz-preview.dz-file-preview', function(e) {
      var file = $(this).children().children().children('span').html();
      $('#preview_file .file_name').html(file);
      var url = base_url + 'Dasbor/getmenutransaksi';
      $.ajax({
          type: "POST",
          url: url,
          data: {
              file: file
          },
          dataType: "JSON",
          success: function(data) {
              if (data.status == 'success') {
                  console.log(data.file_lokasi);
                  var embed = "https://view.officeapps.live.com/op/embed.aspx?src=" + data.file_lokasi;
                  $('#preview_file .frame_').attr('src', embed);
                  $('#preview_file').modal('show')
              }
          }
      });
  });
*/
/*
  var myDropzone = new Dropzone("#dropzone-coa", { // Make the whole body a dropzone
    url: "/target-url", // Set the url
    thumbnailWidth: 80,
    thumbnailHeight: 80,
    parallelUploads: 20,
    previewTemplate: previewTemplate,
    autoQueue: false, // Make sure the files aren't queued until manually added
    previewsContainer: "#previews", // Define the container to display the previews
    clickable: "#dropzone-coa" // Define the element that should be used as click trigger to select files.    
  })

  myDropzone.on("addedfile", function(file) {
    // Hookup the start button
    file.previewElement.querySelector(".start").onclick = function() { myDropzone.enqueueFile(file) }
  })

  // Update the total progress bar
  myDropzone.on("totaluploadprogress", function(progress) {
    document.querySelector("#total-progress .progress-bar").style.width = progress + "%"
  })

  myDropzone.on("sending", function(file) {
    // Show the total progress bar when upload starts
    document.querySelector("#total-progress").style.opacity = "1"
    // And disable the start button
    file.previewElement.querySelector(".start").setAttribute("disabled", "disabled")
  })

  // Hide the total progress bar when nothing's uploading anymore
  myDropzone.on("queuecomplete", function(progress) {
    document.querySelector("#total-progress").style.opacity = "0"
  })

  // Setup the buttons for all transfers
  // The "add files" button doesn't need to be setup because the config
  // `clickable` has already been specified.
  document.querySelector("#actions .start").onclick = function() {
    myDropzone.enqueueFiles(myDropzone.getFilesWithStatus(Dropzone.ADDED))
  }
  document.querySelector("#actions .cancel").onclick = function() {
    myDropzone.removeAllFiles(true)
  }
  // DropzoneJS Demo Code End  
*/
</script>