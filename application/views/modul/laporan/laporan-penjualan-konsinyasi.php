<?php
include('style.php');
$date1 = $_POST['tgldari'];
$date2 = $_POST['tglsampai'];
if (isset($_POST['idkontak'])) {
    $kontak = $_POST['idkontak'];
} else {
    $kontak = "";
}

$CI = &get_instance();
$transcode = element('PJ_Order_Penjualan', NID); // Lihat di global_helper
$transcode = $CI->M_transaksi->prefixtrans($transcode);
$query  = "SELECT A.souid 'id', A.sounotransaksi 'nomor', DATE_FORMAT(A.soutanggal,'%d-%m-%Y') 'tanggal',
           						 A.soukontak 'kontakid', B.kkode 'kontakkode', B.knama 'kontak', A.souuraian 'uraian',
           						 A.soukaryawan 'idkaryawan', C.kkode 'kodekaryawan', C.knama 'namakaryawan', A.sounopopelanggan 'noref',
                       A.soutermin 'idtermin', D.tkode 'termin', A.soucatatan 'catatan', A.soualamat 'alamat', A.souattention 'idperson',
                       E.kanama 'person', A.soustatus 'status', 
                       F.soditem 'iditem', G.ikode 'kditem', G.inama 'namaitem', F.sodcatatan 'catdetil',
                       F.sodsatuan 'idsatuan', H.skode 'satuan', F.sodgudangasal 'idgudangasal',F.sodgudangtujuan 'idgudangtujuan',
                       IFNULL(F.sodorder,0) 'qtydetil',
												I.gnama 'gudangasal',
												J.gnama 'gudangtujuan'			
                    FROM epenjualankonsinyasiu A 
               LEFT JOIN bkontak B ON A.soukontak=B.kid
               LEFT JOIN bkontak C ON A.soukaryawan=C.kid 
               LEFT JOIN btermin D ON A.soutermin=D.tid
               LEFT JOIN bkontakatention E ON A.souattention=E.kaid 
               LEFT JOIN epenjualankonsinyasid F ON A.souid=F.sodidsou 
               LEFT JOIN bitem G ON F.soditem=G.iid 
               LEFT JOIN bsatuan H ON F.sodsatuan=H.sid 
               LEFT JOIN bgudang I ON F.sodgudangasal=I.gid
							 LEFT JOIN bgudang J ON F.sodgudangtujuan=J.gid
	                WHERE A.sousumber = '" . $transcode . "'  
	                  AND A.soutanggal BETWEEN '" . tgl_database($date1) . "' 
	                  AND '" . tgl_database($date2) . "'";

if ($kontak != "") {
    $query .= " AND A.soukontak='" . $kontak . "'";
}

$query .= " GROUP BY A.souid,A.sounotransaksi,A.soutanggal";

$datareport = $CI->M_transaksi->get_data_query($query);
$datareport = json_decode($datareport);

?>
<div class="header-report">
    <h4 class="text-blue"><?= $company_name; ?></h4>
    <h3><?= $title; ?></h3>
    <span>Periode : <?= $date1; ?> s/d <?= $date2; ?></span>
</div>
<div class="content-report">
    <table class="table">
        <thead>
            <tr class="bg-dark">
                <th class="left px-1" width="11%">Tanggal</th>
                <th class="left px-1" width="12%">Nomor</th>
                <th class="left px-1">Kontak</th>
                <th class="left px-1">Keterangan</th>
                <th class="left px-1">Gudang Asal</th>
                <th class="left px-1">Gudang Tujuan</th>
            </tr>
        </thead>
        <tbody>
            <?
            $total = 0;
            foreach ($datareport->data as $row) {
                echo "<tr>";
                echo "<td>" . $row->tanggal . "</td>";
                echo "<td>" . $row->nomor . "</td>";
                echo "<td>" . $row->kontak . "</td>";
                echo "<td>" . $row->uraian . "</td>";
                echo "<td>" . $row->gudangasal . "</td>";
                echo "<td>" . $row->gudangtujuan . "</td>";
                echo "</tr>";
            }
            ?>
        </tbody>
    </table>
    <div class="clear">&nbsp;</div>
</div>