<?php
	include ('style.php');
	$date1 = $_POST['tgldari'];
	$date2 = $_POST['tglsampai'];	
    if(isset($_POST['item'])){
    	$item = $_POST['item'];
    } else {
    	$item = "";
    }	

    $CI =& get_instance();
    $transcode = element('PJ_Faktur_Penjualan',NID); // Lihat di global_helper
    $transcode = $CI->M_transaksi->prefixtrans($transcode);        
    $query  = "SELECT
				B.knama as 'kontak',
				B.kid as id,
				SUM(A.IPUTOTALTRANSAKSI) as total
                     FROM einvoicepenjualanu A 
                LEFT JOIN bkontak B ON A.IPUKARYAWAN=B.kid 
                LEFT JOIN einvoicepenjualand C ON A.ipuid=C.ipdidipu
                LEFT JOIN bitem D ON C.ipditem=D.iid  
                WHERE A.ipusumber = 'IV'  
	                  AND A.iputanggal BETWEEN '".tgl_database($date1)."' 
	                  AND '".tgl_database($date2)."'
	                  GROUP BY
					B.kid, B.knama 
				ORDER BY
					B.knama ASC
					";            
    $datareport = $CI->M_transaksi->get_data_query($query);
    $datareport = json_decode($datareport);

?>
<div class="header-report">
	<h4 class="text-blue"><?= $company_name; ?></h4>		
	<h3><?= $title; ?></h3>
	<span>Periode : <?= $date1; ?> s/d <?= $date2; ?></span>
</div>
<div class="content-report">
	<table class="table">
		<thead>
			<tr class="bg-dark">
				<th class="left px-1" width="80%">Nama Marketing</th>
				<th class="right px-1" width="20%" align="center">Nominal</th>						
			</tr>
		</thead>
		<tbody>
			<?
				$subtotal = 0;
				foreach ($datareport->data as $rowdetil) {
					echo "<tr>";
					echo "<td>".$rowdetil->kontak."</td>";
					echo "<td class='right px-1'>".eFormatNumber($rowdetil->total,2)."</td>";
					echo "</tr>";								
					$subtotal += $rowdetil->total;	
				}				
			?>
		</tbody>
		<tfoot>
			<tr>
				<td class="px-1">Jumlah</td>
				<td class="right px-1"><?= eFormatNumber($subtotal,2); ?></td>				
			</tr>			
		</tfoot>
	</table>
	<div class="clear">&nbsp;</div>	
</div>