<?php
	include ('style.php');
	$date1 = $_POST['tgldari'];
	$date2 = $_POST['tglsampai'];	
    if(isset($_POST['item'])){
    	$item = $_POST['item'];
    } else {
    	$item = "";
    }	

    $CI =& get_instance();
    $transcode = element('PJ_Faktur_Penjualan',NID); // Lihat di global_helper
    $transcode = $CI->M_transaksi->prefixtrans($transcode);        
    $query  = "SELECT
				B.knama as 'kontak',
				B.kid as id
                     FROM einvoicepenjualanu A 
                LEFT JOIN bkontak B ON A.IPUKARYAWAN=B.kid 
                LEFT JOIN einvoicepenjualand C ON A.ipuid=C.ipdidipu
                LEFT JOIN bitem D ON C.ipditem=D.iid  
                WHERE A.ipusumber = 'IV'  
	                  AND A.iputanggal BETWEEN '".tgl_database($date1)."' 
	                  AND '".tgl_database($date2)."'
	                  AND A.IPUKARYAWAN > 0
	                  GROUP BY
					B.kid, B.knama 
				ORDER BY
					B.knama  ASC
					";            
    $datareport = $CI->M_transaksi->get_data_query($query);
    $datareport = json_decode($datareport);

?>
<div class="header-report">
	<h4 class="text-blue"><?= $company_name; ?></h4>		
	<h3><?= $title; ?></h3>
	<span>Periode : <?= $date1; ?> s/d <?= $date2; ?></span>
</div>
<div class="content-report">
	<?php foreach ($datareport->data as $rowdetil) : ?>
			<br><h3 style="font-weight: bold"><?= $rowdetil->kontak ?></h3>
	<table class="table">
		<thead>
			<tr class="bg-dark">
				<th class="left px-1" width="25%" style="font-weight: bold">Tanggal</th>
				<th class="left px-1" width="25%" style="font-weight: bold">No Invoice</th>
				<th class="left px-1" width="25%" style="font-weight: bold">Pelanggan</th>
				<th class="right px-1" width="25%" style="font-weight: bold">Total Nominal</th>						
			</tr>
		</thead>
		<tbody>
			<?
			 $query  = "SELECT
				A.*, 
				B.knama as 'pelanggan'
                     FROM einvoicepenjualanu A 
                LEFT JOIN bkontak B ON A.IPUKONTAK=B.kid 
                LEFT JOIN einvoicepenjualand C ON A.ipuid=C.ipdidipu
                LEFT JOIN bitem D ON C.ipditem=D.iid  
                WHERE A.ipusumber = 'IV'  
	                  AND A.iputanggal BETWEEN '".tgl_database($date1)."' 
	                  AND '".tgl_database($date2)."'
	                AND A.IPUKARYAWAN = '".$rowdetil->id."'
					ORDER BY A.IPUNOTRANSAKSI ASC
					";            
			    $detail = $CI->M_transaksi->get_data_query($query);
			    $detail = json_decode($detail);

				$subtotal = 0;

				foreach ($detail->data as $rowdetil) {
					echo "<tr>";
					echo "<td class='left px-1'>".$rowdetil->IPUTANGGAL."</td>";
					echo "<td class='left px-1'>".$rowdetil->IPUNOTRANSAKSI."</td>";
					echo "<td class='left px-1'>".$rowdetil->pelanggan."</td>";
					echo "<td class='right px-1'>".eFormatNumber($rowdetil->IPUTOTALTRANSAKSI,2)."</td>";
					echo "</tr>";								
					$subtotal += $rowdetil->IPUTOTALTRANSAKSI;	

					echo "<tr>";
					echo "<td class='right px-1' colspan='4'></td>";
					echo "</tr>";	
				}				
			?>
		</tbody>

		
		<tfoot>
			<tr>
				<td class="px-1" colspan="3" style="font-weight: bold">Jumlah</td>
				<td class="right px-1" style="font-weight: bold"><?= eFormatNumber($subtotal,2); ?></td>				
			</tr>

		</tfoot>
	</table>
	<?php endforeach; ?>
	<div class="clear">&nbsp;</div>	
</div>