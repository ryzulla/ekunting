<?php
	include ('style.php');
	$date1 = $_POST['tgldari'];
	$date2 = $_POST['tglsampai'];
	$tampilNol =  $_POST['saldo'];

    $CI =& get_instance();

    $query  = "SELECT caruskas FROM bcoa WHERE caruskas IS NOT NULL GROUP BY caruskas";
    
    $grup = $CI->M_transaksi->get_data_query($query);
    $grup = json_decode($grup);
	// var_dump($grup->data);exit;die;
?>
<div class="header-report">
	<h4 class="text-blue"><?= $company_name; ?></h4>		
	<h3><?= $title; ?></h3>
	<span>Periode : <?= $date1; ?> s/d <?= $date2; ?></span>
</div>
<div class="content-report">
	<table class="table">
		<thead>
			<tr class="bg-dark">
				<th colspan="2" class="left px-1">Keterangan</th>				
				<th class="right px-1" width="20%">Jumlah</th>
			</tr>
		</thead>
		<tbody>
			<?
				$grandtotal = 0;
				foreach ($grup->data as $row_grup) {
					$potong_kalimat = substr($row_grup->caruskas,4);
					echo "<tr>
								<td colspan='3' class='py-1 px-1'><strong> Arus Kas dari $potong_kalimat </strong></td>
						 </tr>";	
					echo "<tr><td colspan='3' class='py-1 px-1 text-blue'>Arus Kas Masuk</td></tr>";
					$query = "SELECT A.cid, A.cnocoa, A.cnama, A.cgd, 
									  (select SUM(ROUND(AA.cddebit,2))
										 FROM ctransaksid AA INNER JOIN ctransaksiu AB ON AA.cdidu=AB.cuid AND AB.cutanggal BETWEEN '".tgl_database($date1)."' AND '".tgl_database($date2)."' 
										WHERE AA.cdnocoa=A.cid) 'saldo' 
								  FROM bcoa A WHERE A.caruskas='".$row_grup->caruskas."' AND A.cgd='D' ORDER BY A.cnocoa ASC";
				    $coa = $CI->M_transaksi->get_data_query($query);
				    $coa = json_decode($coa);		
				    $totalmasuk = 0;
				    foreach ($coa->data as $row) {
				    	// if($row->cgd == 'D'){
						// 	echo "<tr>
						// 				<td colspan='2' class='px-2'>$row->cnama</td>
						// 				<td class='right px-1'>".eFormatNumber(abs($row->saldo),2)."</td>									
						// 		 </tr>";					    
				    	// } else {
							if(abs($row->saldo) == 0 && $tampilNol == 0){
							} else {
								echo "<tr>
											<td colspan='2' class='px-3'>$row->cnocoa &nbsp;&nbsp; $row->cnama</td>
											<td class='right px-1'>".eFormatNumber(abs($row->saldo),2)."</td>									
									 </tr>";					    				    		
							}
				    	// }
						$totalmasuk += $row->saldo;
						$grandtotal += $row->saldo;
					}
					echo "<tr>
											<td colspan='2' class='px-3'><strong>Sub Total</strong></td>
											<td class='right px-1'>".eFormatNumber(abs($totalmasuk),2)."</td>									
							</tr>";	
					echo "<tr><td colspan='3' class='py-1 px-1 text-blue'>Arus Kas Keluar</td></tr>";
					$query = "SELECT A.cid, A.cnocoa, A.cnama, A.cgd, 
									  (select SUM(ROUND(AA.cdkredit,2))	
										 FROM ctransaksid AA INNER JOIN ctransaksiu AB ON AA.cdidu=AB.cuid AND AB.cutanggal BETWEEN '".tgl_database($date1)."' AND '".tgl_database($date2)."' 
										WHERE AA.cdnocoa=A.cid) 'saldo' 
								  FROM bcoa A WHERE A.caruskas='".$row_grup->caruskas."' AND A.cgd='D' ORDER BY A.cnocoa ASC";
				    $coa = $CI->M_transaksi->get_data_query($query);
				    $coa = json_decode($coa);		
				    $totalkeluar = 0;
				    foreach ($coa->data as $row) {
				    	// if($row->cgd == 'D'){
						// 	echo "<tr>
						// 				<td colspan='2' class='px-2'>$row->cnama</td>
						// 				<td class='right px-1'>".eFormatNumber(abs($row->saldo),2)."</td>									
						// 		 </tr>";					    
				    	// } else {
							if(abs($row->saldo) == 0 && $tampilNol == 0){
							} else {
								echo "<tr>
											<td colspan='2' class='px-3'>$row->cnocoa &nbsp;&nbsp; $row->cnama</td>
											<td class='right px-1'>".eFormatNumber(abs($row->saldo),2)."</td>									
									 </tr>";					    				    		
							}
				    	// }
						$totalkeluar += $row->saldo;
						$grandtotal += $row->saldo;
					}
					echo "<tr>
								<td colspan='2' class='px-3'><strong>Sub Total</strong></td>
								<td class='right px-1'>".eFormatNumber(abs($totalkeluar),2)."</td>									
							</tr>";	
					 echo "<tr>
								<td colspan='2' class='px-1 py-2' style=\"border-top:.5px solid black;border-bottom:.5px solid black\"><b>Total Arus Kas dari $potong_kalimat</b></td>
								<td class='right px-1 py-1' style=\"border-top:.5px solid black;border-bottom:.5px solid black\"><b>".eFormatNumber(($totalmasuk-$totalkeluar),2)."</b></td>
							</tr>";	
				}
				echo "
					<tr>
							<td colspan='2' class='px-1 py-2'><b>SALDO AWAL</b></td>
							<td class='right px-1 py-1' style=\"border-top:.5px solid black\"><b>".eFormatNumber((0),2)."</b></td>
					</tr>
					<tr>
							<td colspan='2' class='px-1 py-2'><b>SALDO AKHIR  & SETARA KAS</b></td>
							<td class='right px-1 py-1' style=\"border-top:.5px solid black\"><b>".eFormatNumber((0),2)."</b></td>
					</tr>
					<tr>
							<td colspan='2' class='px-1 py-2'><b>KENAIKAN (+) / PENURUNAN (-) KAS & SETARA KAS</b></td>
							<td class='right px-1 py-1' style=\"border-top:.5px solid black\"><b>".eFormatNumber(($grandtotal*-1),2)."</b></td>
					 </tr>";					    				    						
			?>
		</tbody>
		<tfoot>
		</tfoot>
	</table>
</div>