<?php
	include ('style.php');
	$date1 = $_POST['tgldari'];
	$date2 = $_POST['tglsampai'];	
    if(isset($_POST['item'])){
    	$item = $_POST['item'];
    } else {
    	$item = "";
    }	

    $CI =& get_instance();
    $transcode = element('PJ_Faktur_Penjualan',NID); // Lihat di global_helper
    $transcode = $CI->M_transaksi->prefixtrans($transcode);        
	   $query  = "SELECT
					A.*, 
					B.knama as 'pelanggan'
	                     FROM einvoicepenjualanu A 
	                LEFT JOIN bkontak B ON A.IPUKONTAK=B.kid 
	                LEFT JOIN einvoicepenjualand C ON A.ipuid=C.ipdidipu
	                LEFT JOIN bitem D ON C.ipditem=D.iid  
	                WHERE A.ipusumber = 'IV'  
		                  AND A.iputanggal BETWEEN '".tgl_database($date1)."' 
		                  AND '".tgl_database($date2)."'
						ORDER BY A.IPUNOTRANSAKSI ASC
						";                  
    $datareport = $CI->M_transaksi->get_data_query($query);
    $datareport = json_decode($datareport);

?>
<div class="header-report">
	<h4 class="text-blue"><?= $company_name; ?></h4>		
	<h3><?= $title; ?></h3>
	<span>Periode : <?= $date1; ?> s/d <?= $date2; ?></span>
</div>
<div class="content-report">
	<table class="table">
		<thead>
			<tr class="bg-dark">
				<th class="left px-1" width="15%" style="font-weight: bold">Tanggal</th>
				<th class="left px-1" width="20%" style="font-weight: bold">No Invoice</th>
				<th class="left px-1" width="30%" style="font-weight: bold">Pelanggan</th>
				<th class="right px-1" width="15%" style="font-weight: bold">Nominal</th>						
				<th class="right px-1" width="15%" style="font-weight: bold">Retur</th>						
				<th class="right px-1" width="15%" style="font-weight: bold">Nett</th>						
			</tr>
		</thead>
		<tbody>
			<?
				$subtotal = 0;
				foreach ($datareport->data as $rowdetil) {
					echo "<tr>";
					echo "<td class='left px-1'>".$rowdetil->IPUTANGGAL."</td>";
					echo "<td class='left px-1'>".$rowdetil->IPUNOTRANSAKSI."</td>";
					echo "<td class='left px-1'>".$rowdetil->pelanggan."</td>";
					echo "<td class='right px-1'>".eFormatNumber($rowdetil->total,2)."</td>";
					echo "<td class='right px-1'>".eFormatNumber($rowdetil->total,2)."</td>";
					echo "<td class='right px-1'>".eFormatNumber($rowdetil->total,2)."</td>";
					echo "</tr>";								
					$subtotal += $rowdetil->total;	
				}				
			?>
		</tbody>
		<tfoot>
			<tr>
				<td class="px-1" colspan="3">Jumlah</td>
				<td class="right px-1"><?= eFormatNumber($subtotal,2); ?></td>				
				<td class="right px-1"><?= eFormatNumber($subtotal,2); ?></td>				
				<td class="right px-1"><?= eFormatNumber($subtotal,2); ?></td>				
			</tr>			
		</tfoot>
	</table>
	<div class="clear">&nbsp;</div>	
</div>